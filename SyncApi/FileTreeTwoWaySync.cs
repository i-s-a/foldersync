﻿using System.Collections.Generic;
using ZetaLongPaths;

namespace SyncApi
{
    public class FileTreeTwoWaySync : IFileTreeSync
    {
        private string mRootPathSource;
        private string mRootPathTarget;
        private ISyncManager mSyncManager = null;
        private SyncOptions mSyncOptions = null;

        public FileTreeTwoWaySync(string rootPathSource, string rootPathTarget, ISyncManager syncManager, SyncOptions syncOptions)
        {
            mRootPathSource = rootPathSource;
            mRootPathTarget = rootPathTarget;
            mSyncManager = syncManager;
            mSyncOptions = syncOptions;
        }

        private void CopyFileToTarget(IFolderChildNode fileNode)
        {
            if (mSyncOptions.CopyFileAttributes)
                mSyncManager.CopyFileIncludinghAttributes(mRootPathSource, fileNode.RelativePath, mRootPathTarget, fileNode.RelativePath, true);
            else
                mSyncManager.CopyFile(mRootPathSource, fileNode.RelativePath, mRootPathTarget, fileNode.RelativePath, true);
        }

        private void CopyFileToSource(IFolderChildNode fileNode)
        {
            if (mSyncOptions.CopyFileAttributes)
                mSyncManager.CopyFileIncludinghAttributes(mRootPathTarget, fileNode.RelativePath, mRootPathSource, fileNode.RelativePath, true);
            else
                mSyncManager.CopyFile(mRootPathTarget, fileNode.RelativePath, mRootPathSource, fileNode.RelativePath, true);
        }

        public void Sync(IFolderChildNode fileNode)
        {
            if (fileNode.NodeLocation == Location.UniqueLeft)
            {
                CopyFileToTarget(fileNode);
            }
            else if (fileNode.NodeLocation == Location.UniqueRight)
            {
                CopyFileToSource(fileNode);
            }
            else if (fileNode.NodeLocation == Location.Both)
            {
                string sourcePath = mRootPathSource + fileNode.RelativePath;
                string targetPath = mRootPathTarget + fileNode.RelativePath;

                ZlpFileInfo sourceFileInfo = new ZlpFileInfo(sourcePath);
                ZlpFileInfo targetFileInfo = new ZlpFileInfo(targetPath);

                if (FileHelper.IsSourceFileNewer(sourceFileInfo, targetFileInfo))
                {
                    //Source is newer, so copy it to the target
                    CopyFileToTarget(fileNode);
                }
                //else if (targetFileInfo.LastWriteTime > sourceFileInfo.LastWriteTime)
                else if (FileHelper.IsTargetFileNewer(sourceFileInfo, targetFileInfo))
                {
                    //Target is newer, so copy it to the source
                    CopyFileToSource(fileNode);
                }
                else
                {
                    //Source and target have the same last modified time so do nothing. THINK
                    //WHETHER WE SHOULD STILL COMPARE OTHER META DATA (E.G. FILE SIZE) AND THEN
                    //COPY FROM THE SOURCE TO THE TARGET IF THERE IS A DIFFERENCE!!!! PERHAPS
                    //MAKE THE DECISION CONFIGURABLE (E.G. IF THE LAST MODIFIED TIME ARE THE
                    //SAME AND THE FILE SIZES OR CONTENT ARE DIFFERENT THEN ALWAYS COPY FROM
                    //TARGET TO SOURCE, ETC.)
                }
            }
            else
            {
                throw new SyncException("Unrecognised file location in file two way sync");
            }
        }

        public void Sync(IEnumerable<IFolderChildNode> fileNodes)
        {
            foreach (var fileNode in fileNodes)
            {
                Sync(fileNode);
            }
        }
    }
}
