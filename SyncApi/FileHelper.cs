﻿using System.Linq;
using ZetaLongPaths;
using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;

namespace SyncApi
{
    class FileHelper
    {
        public static bool IsSourceFileNewer(ZlpFileInfo source, ZlpFileInfo target)
        {
            return (source.LastWriteTime > target.LastWriteTime);
        }

        public static bool IsTargetFileNewer(ZlpFileInfo source, ZlpFileInfo target)
        {
            return (target.LastWriteTime > source.LastWriteTime);
        }

        public static bool MetaDataAreNotEqual(ZlpFileInfo source, ZlpFileInfo target)
        {
            return (source.Length != target.Length || source.LastWriteTime != target.LastWriteTime);
        }

        public static bool ContentEqual(string source, string target)
        {
            bool matched = false;
            SafeFileHandle sourceHandle = null;
            SafeFileHandle targetHandle = null;

            try
            {
                sourceHandle = ZlpIOHelper.CreateFileHandle(source, ZetaLongPaths.Native.CreationDisposition.OpenExisting,
                    ZetaLongPaths.Native.FileAccess.GenericRead, ZetaLongPaths.Native.FileShare.Read);

                if (sourceHandle.IsInvalid)
                {
                    Marshal.ThrowExceptionForHR(Marshal.GetHRForLastWin32Error());
                }

                targetHandle = ZlpIOHelper.CreateFileHandle(target, ZetaLongPaths.Native.CreationDisposition.OpenExisting,
                    ZetaLongPaths.Native.FileAccess.GenericRead, ZetaLongPaths.Native.FileShare.Read);

                if (targetHandle.IsInvalid)
                {
                    Marshal.ThrowExceptionForHR(Marshal.GetHRForLastWin32Error());
                }

                //TODO: 1MB here is just hardcoded for now, make this configurable
                const int bufferSize = 1024 * 1024;
                byte[] sourceBuffer = new byte[bufferSize];
                byte[] targetBuffer = new byte[bufferSize];
                int numSourceRead = 0;
                int numTargetRead = 0;
                matched = true;

                do
                {
                    numSourceRead = ZlpIOHelper.ReadFile(sourceHandle, sourceBuffer, 0, bufferSize);
                    numTargetRead = ZlpIOHelper.ReadFile(targetHandle, targetBuffer, 0, bufferSize);
                    if (numSourceRead > 0 && numTargetRead > 0)
                    {
                        if (!sourceBuffer.SequenceEqual(targetBuffer))
                        {
                            matched = false;
                        }
                    }

                } while (matched && numSourceRead > 0 && numTargetRead > 0);

                if (matched)
                {
                    if (numSourceRead != 0 || numTargetRead != 0)
                        matched = false;
                }

            }
            finally
            {
                if (sourceHandle != null && !sourceHandle.IsInvalid)
                {
                    sourceHandle.Close();
                }

                if (targetHandle != null && !targetHandle.IsInvalid)
                {
                    targetHandle.Close();
                }
            }

            return matched;
        }
    }
}
