﻿using System.Collections.Generic;

namespace SyncApi
{
    public interface IFileTreeSync
    {
        void Sync(IFolderChildNode fileNode);
        void Sync(IEnumerable<IFolderChildNode> fileNodes);
    }
}
