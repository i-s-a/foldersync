﻿using System;

namespace SyncApi
{
    public class CompareException : Exception
    {
        public CompareException()
        {
        }

        public CompareException(string message)
            : base(message)
        {
        }

        public CompareException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
