﻿using System;
using System.Collections.Generic;

namespace SyncApi
{
    public class FolderTreeSizeNode : IFolderTreeSizeChildNode, IEquatable<FolderTreeSizeNode>
    {
        public FolderTreeSizeNode()
        {
        }

        public FolderTreeSizeNode Parent { get; set; }

        public string FullPath { get; set; }
        public string Name { get; set; }
        public string RelativePath { get; set; }

        public SortedSet<IFolderTreeSizeChildNode> Children { get; set; }

        public DateTime LastWriteTime { get; set; }

        public long Size { get; set; }

        public bool Equals(FolderTreeSizeNode other)
        {
            if (object.ReferenceEquals(other, null))
                return false;

            if (object.ReferenceEquals(other, this))
                return true;

            return (RelativePath.Equals(other.RelativePath));
        }

        public int CompareTo(IFolderTreeSizeChildNode other)
        {
            //return System.String.Compare(RelativePath, other.RelativePath, System.StringComparison.Ordinal);
            return System.String.Compare(Name, other.Name, System.StringComparison.Ordinal);
        }
    }
}
