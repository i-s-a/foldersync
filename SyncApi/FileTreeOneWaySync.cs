﻿using System.Collections.Generic;
using ZetaLongPaths;

namespace SyncApi
{
    public class FileTreeOneWaySync : IFileTreeSync
    {
        private string mRootPathSource;
        private string mRootPathTarget;
        private SyncDirection mSyncDirection;
        private Location mSourceLocation;
        private Location mTargetLocation;
        private ISyncManager mSyncManager = null;
        private SyncOptions mSyncOptions = null;

        public FileTreeOneWaySync(string rootPathSource, string rootPathTarget, SyncDirection syncDirection, ISyncManager syncManager, SyncOptions syncOptions)
        {
            mRootPathSource = rootPathSource;
            mRootPathTarget = rootPathTarget;
            mSyncDirection = syncDirection;

            if (mSyncDirection == SyncDirection.LeftToRight)        //When syncing from left to right the source is the left side and the target is the right side
            {
                mSourceLocation = Location.UniqueLeft;
                mTargetLocation = Location.UniqueRight;
            }
            else if (mSyncDirection == SyncDirection.RightToLeft)   //When syncing from right to left the source is the right side and the target is the left side
            {
                mSourceLocation = Location.UniqueRight;
                mTargetLocation = Location.UniqueLeft;
            }
            else                                                    //Any other sync direction is unsupported
            {
                throw new SyncException("Sync direction not support with one way sync");
            }

            mSyncManager = syncManager;
            mSyncOptions = syncOptions;
        }

        private void CopyFileToTarget(IFolderChildNode fileNode)
        {
            if (mSyncOptions.CopyFileAttributes)
                mSyncManager.CopyFileIncludinghAttributes(mRootPathSource, fileNode.RelativePath, mRootPathTarget, fileNode.RelativePath, true);
            else
                mSyncManager.CopyFile(mRootPathSource, fileNode.RelativePath, mRootPathTarget, fileNode.RelativePath, true);
        }

        public void Sync(IFolderChildNode fileNode)
        {
            if (fileNode.NodeLocation == mSourceLocation)               //If the file is only in the source then copy the file to the target
            {
                string sourcePath = mRootPathSource + fileNode.RelativePath;

                CopyFileToTarget(fileNode);
            }
            else if (fileNode.NodeLocation == mTargetLocation)          //If the file is only in the target then delete the file from the target
            {
                if (mSyncOptions.PropagateDeletes)
                    mSyncManager.DeleteFile(mRootPathTarget, fileNode.RelativePath);
            }
            else if (fileNode.NodeLocation == Location.Both)            //If the file is in both source and target then copy the file from the source to the target only if the source file is newer, otherwise leave the target file as it is (See comment below though as this might change!!!)
            {
                string sourcePath = mRootPathSource + fileNode.RelativePath;
                string targetPath = mRootPathTarget + fileNode.RelativePath;

                ZlpFileInfo sourceFileInfo = new ZlpFileInfo(sourcePath);
                ZlpFileInfo targetFileInfo = new ZlpFileInfo(targetPath);

                if (mSyncOptions.OnlyCopyFileIfSourceNewer)
                {
                    //Only copy if the source file is newer than the destination file
                    //THINK WHETHER IF THE LAST MODIFIED TIME OF THE SOURCE AND TARGET ARE
                    //EQUAL SHOULD WE COMPARE OTHER META DATA (E.G. FILE SIZE) AND THEN COPY FROM THE
                    //SOURCE TO THE TARGET IF THERE IS A DIFFERENCE!!!!
                    if (FileHelper.IsSourceFileNewer(sourceFileInfo, targetFileInfo))
                    {
                        CopyFileToTarget(fileNode);
                    }
                }
                else if (mSyncOptions.OneWaySyncCompareContent)
                {
                    //Only copy if the content of the source file and target file are not the same
                    if (!FileHelper.ContentEqual(mRootPathSource + fileNode.RelativePath, mRootPathTarget + fileNode.RelativePath))
                    {
                        CopyFileToTarget(fileNode);
                    }
                }
                else
                {
                    //As a quick check use the length and last modified time to determine
                    //if the source file and destination file are different, if they are
                    //then copy the source over the destination. NOTE: We will want to
                    //expand this to be able to support different ways of determining if
                    //the files have changed (e.g. CRC32, SHA1, byte-by-byte comparison, etc.)
                    if (FileHelper.MetaDataAreNotEqual(sourceFileInfo, targetFileInfo))
                    {
                        CopyFileToTarget(fileNode);
                    }
                }
            }
            else
            {
                //Anything else is unsupported
                throw new SyncException("Unrecognised file location in one way sync");
            }
        }

        public void Sync(IEnumerable<IFolderChildNode> fileNodes)
        {
            foreach (var fileNode in fileNodes)
            {
                Sync(fileNode);
            }
        }

    }
}
