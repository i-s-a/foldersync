﻿using System;
using System.Text;
using System.Threading;
using ZetaLongPaths;

namespace SyncApi
{
    public class SyncLogged : ISyncManager
    {
        [Flags]
        private enum LogFlag
        {
            FileCopy = 1 << 0,
            FileDelete = 1 << 1,
            FolderCreate = 1 << 2,
            FolderDelete = 1 << 3,
            FileMoveToTemp = 1 << 4,
            //FolderLastWriteDateTime = 1 << 5,
        }

        private StringBuilder mLog = new StringBuilder();
        private string mLogFilePath = null;
        private string mLogSyncPosFilePath = null;
        private bool mRetainLogFile = false;

        public SyncLogged(SyncOptions syncOptions)
        {
            mLogSyncPosFilePath = syncOptions.LogSyncPosFilenamePrefix;
            mLogFilePath = syncOptions.LogFilenamePrefix;
            mRetainLogFile = syncOptions.RetainLastSuccessfulSync;
        }

        public void CopyFile(string fromRootPath, string from, string toRootPath, string to, bool overwriteIfExists)
        {
            //First, if a file exists in the target, move the target file to the temp folder...
            if (ZlpIOHelper.FileExists(toRootPath + to))
            {
                mLog.Append((byte)LogFlag.FileMoveToTemp);
                mLog.Append("|");
                mLog.Append(toRootPath);
                mLog.Append("|");
                mLog.AppendLine(to);
            }

            //...then copy the file from the source path to the target path
            mLog.Append((byte)LogFlag.FileCopy);
            mLog.Append("|");
            mLog.Append(fromRootPath);
            mLog.Append("|");
            mLog.Append(from);
            mLog.Append("|");
            mLog.Append(toRootPath);
            mLog.Append("|");
            mLog.Append(to);
            mLog.Append("|");
            mLog.AppendLine("False");   //Copy file but don't include attributes
            //mLog.Append("|");

            ////TODO: Check what should happen when rolling back the sync. Will we need to know whether the file was overwritten or not
            ////so that if rolling back we know whether to revert the file or not?
            //if (overwriteIfExists)          //If the file already exists should the file be overwritten
            //    mLog.AppendLine("True");
            //else
            //    mLog.AppendLine("False");
        }

        public void CopyFileIncludinghAttributes(string fromRootPath, string from, string toRootPath, string to, bool overwriteIfExists)
        {
            //First, if a file exists in the target, move the target file to the temp folder...
            if (ZlpIOHelper.FileExists(toRootPath + to))
            {
                mLog.Append((byte)LogFlag.FileMoveToTemp);
                mLog.Append("|");
                mLog.Append(toRootPath);
                mLog.Append("|");
                mLog.AppendLine(to);
            }

            //...then copy the file from the source path to the target path
            mLog.Append((byte)LogFlag.FileCopy);
            mLog.Append("|");
            mLog.Append(fromRootPath);
            mLog.Append("|");
            mLog.Append(from);
            mLog.Append("|");
            mLog.Append(toRootPath);
            mLog.Append("|");
            mLog.Append(to);
            mLog.Append("|");
            mLog.AppendLine("True");   //Copy file including attributes
            //mLog.Append("|");

            ////TODO: Check what should happen when rolling back the sync. Will we need to know whether the file was overwritten or not
            ////so that if rolling back we know whether to revert the file or not?
            //if (overwriteIfExists)          //If the file already exists should the file be overwritten
            //    mLog.AppendLine("True");
            //else
            //    mLog.AppendLine("False");
        }

        public void DeleteFile(string rootPath, string relativePath)
        {
            mLog.Append((byte)LogFlag.FileDelete);
            mLog.Append("|");
            mLog.Append(rootPath);
            mLog.Append("|");
            mLog.AppendLine(relativePath);
        }

        public void CreateFolder(string rootPath, string to, DateTime lastWriteDateTime)
        {
            mLog.Append((byte)LogFlag.FolderCreate);
            mLog.Append("|");
            mLog.Append(rootPath);
            mLog.Append("|");
            mLog.Append(to);
            mLog.Append("|");
            mLog.AppendLine(lastWriteDateTime.ToString());
        }

        public void DeleteFolder(string rootPath, string relativePath)
        {
            mLog.Append((byte)LogFlag.FolderDelete);
            mLog.Append("|");
            mLog.Append(rootPath);
            mLog.Append("|");
            mLog.AppendLine(relativePath);
        }

        public bool IsDirectoryEmpty(string rootPath, string relativePath)
        {
            return ZlpIOHelper.IsDirectoryEmpty(rootPath + relativePath);
        }

        public void Completed(CancellationToken cancellationToken = default)
        {
            ZlpIOHelper.WriteAllText(mLogFilePath, mLog.ToString());

            SyncFromLogFile.SyncLogFile(mLogSyncPosFilePath, mLogFilePath, mRetainLogFile, cancellationToken);
        }
    }
}
