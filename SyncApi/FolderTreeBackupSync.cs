﻿using System.Collections.Generic;
using System.Linq;
using ZetaLongPaths;

namespace SyncApi
{
    public class FolderTreeBackupSync : IFolderTreeSync
    {
        private string mRootPathSource;
        private string mRootPathTarget;
        private FolderTreeNode mRootMergedNode;
        private SyncDirection mSyncDirection;
        private Location mSourceLocation;
        private Location mTargetLocation;
        private IFileTreeSync mFileSync = null;
        private ISyncManager mSyncManager = null;
        private SyncOptions mSyncOptions = null;

        public event CompletedEventHandler SyncCompleted = null;

        public FolderTreeBackupSync(string rootPathSource, string rootPathTarget, FolderTreeNode rootMergedNode, SyncDirection syncDirection, ISyncManager syncManager, SyncOptions syncOptions)
        {
            mRootPathSource = rootPathSource;
            mRootPathTarget = rootPathTarget;
            mRootMergedNode = rootMergedNode;
            mSyncDirection = syncDirection;

            if (mSyncDirection == SyncDirection.LeftToRight)
            {
                mSourceLocation = Location.UniqueLeft;
                mTargetLocation = Location.UniqueRight;
            }
            else if (mSyncDirection == SyncDirection.RightToLeft)
            {
                mSourceLocation = Location.UniqueRight;
                mTargetLocation = Location.UniqueLeft;     
            }
            else
            {
                throw new SyncException("Sync direction not support with mirroring");
            }

            mSyncOptions = syncOptions;

            //mSyncManager = new SyncLogger(@"C:\Users\Ahmed\Documents\Temp\syncpos.bin", @"C:\Users\Ahmed\Documents\Temp\synclog.txt");
            //mSyncManager = new Syncer();
            mSyncManager = syncManager;

            mFileSync = new FileTreeBackupSync(mRootPathSource, mRootPathTarget, syncDirection, mSyncManager);
        }

        public void Sync()
        {
            if (mRootMergedNode.NodeLocation == mSourceLocation)
            {
                AddToTarget(mRootMergedNode);
            }
            else if (mRootMergedNode.NodeLocation == mTargetLocation)
            {
                if (mSyncOptions.PropagateDeletes)
                    DeleteFromTarget(mRootMergedNode);
            }
            else
            {
                Backup(mRootMergedNode);
            }

            mSyncManager.Completed();

            if (SyncCompleted != null)
                SyncCompleted();
        }

        private void Backup(FolderTreeNode parentMergedNode)
        {
            IEnumerable<IFolderChildNode> childFiles = parentMergedNode.Children.Where(x => x is FileTreeNode);
            mFileSync.Sync(childFiles);

            IEnumerable<IFolderChildNode> uniqueFoldersInSource = parentMergedNode.Children.Where(x => x is FolderTreeNode && x.NodeLocation == mSourceLocation);
            foreach (FolderTreeNode folder in uniqueFoldersInSource)
            {
                AddToTarget(folder);
            }

            if (mSyncOptions.PropagateDeletes)
            {
                IEnumerable<IFolderChildNode> uniqueFoldersInTarget = parentMergedNode.Children.Where(x => x is FolderTreeNode && x.NodeLocation == mTargetLocation);
                foreach (FolderTreeNode folder in uniqueFoldersInTarget)
                {
                    DeleteFromTarget(folder);
                }
            }

            IEnumerable<IFolderChildNode> foldersInBoth = parentMergedNode.Children.Where(x => x is FolderTreeNode && x.NodeLocation == Location.Both);
            foreach (FolderTreeNode folder in foldersInBoth)
            {
                Backup(folder);
            }
        }

        private void AddToTarget(FolderTreeNode folderNode)
        {
            mSyncManager.CreateFolder(mRootPathTarget, folderNode.RelativePath, new ZlpDirectoryInfo(mRootPathSource + folderNode.RelativePath).LastWriteTime);

            foreach (FileTreeNode file in folderNode.Children.Where(x => x is FileTreeNode))
            {
                mSyncManager.CopyFile(mRootPathSource, file.RelativePath, mRootPathTarget, file.RelativePath,
                    new ZlpFileInfo(mRootPathSource + file.RelativePath).LastWriteTime, false);
            }

            foreach (FolderTreeNode folder in folderNode.Children.Where(x => x is FolderTreeNode))
            {
                AddToTarget(folder);
            }
        }

        private void DeleteFromTarget(FolderTreeNode folderNode)
        {
            foreach (FolderTreeNode folder in folderNode.Children.Where(x => x is FolderTreeNode))
            {
                DeleteFromTarget(folder);
            }

            foreach (FileTreeNode file in folderNode.Children.Where(x => x is FileTreeNode))
            {
                mSyncManager.DeleteFile(mRootPathTarget, file.RelativePath);
            }

            mSyncManager.DeleteFolder(mRootPathTarget, folderNode.RelativePath);
        }
    }
}
