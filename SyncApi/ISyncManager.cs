﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace SyncApi
{
    public interface ISyncManager
    {
        void CopyFile(string fromRootPath, string from, string toRootPath, string to, bool overwriteIfExists);
        void CopyFileIncludinghAttributes(string fromRootPath, string from, string toRootPath, string to, bool overwriteIfExists);
        void DeleteFile(string rootPath, string relativePath);
        void CreateFolder(string rootPath, string to, DateTime lastWriteDateTime);
        void DeleteFolder(string rootPath, string relativePath);
        bool IsDirectoryEmpty(string rootPath, string relativePath);
        void Completed(CancellationToken cancellationToken);
    }
}
