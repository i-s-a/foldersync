﻿using System.Xml.Serialization;
using System.IO;

namespace SyncApi
{
    [XmlRoot("SyncOptions")]
    sealed public class SyncOptions
    {
        public SyncOptions()
        {
            SyncDirect = false;                     //Default Sync in logged mode
            RetainLastSuccessfulSync = false;       //Don't retain the log file for the last successful sync by default
            CopyFileAttributes = true;              //By default copy file attributes
            PropagateDeletes = false;               //Don't propagate deletes by default
            MoveDeletedFilesToRecycleBin = true;    //Any files deleted should be moved to the recycle bin by default
            ExtensionFilters = "*.*";               //By default sync all files
            OnlyCopyFileIfSourceNewer = false;      //By default always copy from source to destination
        }

        public SyncOptions Clone()
        {
            return new SyncOptions()
            {
                SyncDirect = this.SyncDirect,
                RetainLastSuccessfulSync = this.RetainLastSuccessfulSync,
                CopyFileAttributes = this.CopyFileAttributes,
                PropagateDeletes = this.PropagateDeletes,
                MoveDeletedFilesToRecycleBin = this.MoveDeletedFilesToRecycleBin,
                ExtensionFilters = this.ExtensionFilters,
                OnlyCopyFileIfSourceNewer = this.OnlyCopyFileIfSourceNewer
            };
        }

        //If SyncDirect is false then we assume that syncing should be
        //performed Logged, which will use the TempPath to store the log
        //files and backup files
        //[XmlAttribute]
        public bool SyncDirect
        {
            get;
            set;
        }

        //This is ignored unless the SyncDirect == false, as it requires
        //the sync to be performed with logging. By retaining the last
        //sync this would allow the option to rollback the last sync
        public bool RetainLastSuccessfulSync
        {
            get;
            set;
        }

        public string TempPath
        {
            get;
            set;
        }

        public string LogSyncPosFilenamePrefix
        {
            get;
            set;
        }

        public string LogFilenamePrefix
        {
            get;
            set;
        }

        public bool CopyFileAttributes
        {
            get;
            set;
        }

        //When performing a one-way sync, should files that don't exist in the
        //destination, be deleted from the destination so that it matches the
        //source
        public bool PropagateDeletes
        {
            get;
            set;
        }

        public bool MoveDeletedFilesToRecycleBin
        {
            get;
            set;
        }

        //Semi-colon separated list of extension filters to be applied when
        //syncing, so only those files that match the filters get synced
        public string ExtensionFilters
        {
            get;
            set;
        }

        //File to output logging information as the sync is progressing. Will
        //want to expand on this to allow different levels of output (e.g.
        //verbose, warning and error)
        public string OutputLogFile
        {
            get;
            set;
        }

        //When performing a one-way sync, if this is true, then the source file will
        //only be copied over the destination file if the source is newer, otherwise
        //the destination will be left as it is
        public bool OnlyCopyFileIfSourceNewer
        {
            get;
            set;
        }

        //This only applies to One Way Sync. When comparing the source and target,
        //should the content of the source and target files be compared. If this is
        //true and the content is different then the source file will be copied over
        //the destination file. If this is false then only the file meta data (i.e.
        //last write time and file size) will be compared. This only applies to One
        //Way Sync. In the case of Two Way Sync this is ignored as we always copy
        //the newer file over the older file. NOTE: OnlyCopyFileIfSourceNewer takes
        //precedence over OneWaySyncCompareContent (i.e. this gets ignored if
        //OnlyCopyFileIfSourceNewer is set to true)
        public bool OneWaySyncCompareContent
        {
            get;
            set;
        }

        //public FtpCredentials Ftp
        //{
        //    get;
        //    set;
        //}

        static public void Serialize(string filename, SyncOptions syncOptions)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(SyncOptions));
            TextWriter writer = new StreamWriter(filename);
            serializer.Serialize(writer, syncOptions);
            writer.Close();
        }

        static public SyncOptions Deserialize(string filename)
        {
            SyncOptions syncOptions = null;

            if (File.Exists(filename))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(SyncOptions));
                FileStream stream = new FileStream(filename, FileMode.Open);
                syncOptions = (SyncOptions)serializer.Deserialize(stream);
                stream.Close();
            }

            return syncOptions;
        }
    }
}
