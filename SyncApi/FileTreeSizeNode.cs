﻿using System;

namespace SyncApi
{
    public class FileTreeSizeNode : IFolderTreeSizeChildNode, IEquatable<FileTreeSizeNode>
    {
        public FileTreeSizeNode()
        {
        }

        public FolderTreeSizeNode Parent { get; set; }
        public string FullPath { get; set; }
        public string Name { get; set; }
        public string RelativePath { get; set; }

        public DateTime LastWriteTime { get; set; }

        public long Size { get; set; }

        public bool Equals(FileTreeSizeNode other)
        {
            if (object.ReferenceEquals(other, null))
                return false;

            if (object.ReferenceEquals(other, this))
                return true;

            return (RelativePath.Equals(other.RelativePath));
        }

        public int CompareTo(IFolderTreeSizeChildNode other)
        {
            //return System.String.Compare(RelativePath, other.RelativePath, System.StringComparison.Ordinal);
            return System.String.Compare(Name, other.Name, System.StringComparison.Ordinal);
        }
    }
}
