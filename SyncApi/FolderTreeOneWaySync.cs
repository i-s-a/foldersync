﻿using System.Collections.Generic;
using System.Linq;
using ZetaLongPaths;
using System.Threading;
using System.Threading.Tasks;

namespace SyncApi
{
    public class FolderTreeOneWaySync : IFolderTreeSync
    {
        private string mRootPathSource;
        private string mRootPathTarget;
        private FolderTreeNode mRootMergedNode;
        private SyncDirection mSyncDirection;
        private Location mSourceLocation;
        private Location mTargetLocation;
        private IFileTreeSync mFileSync = null;
        private ISyncManager mSyncManager = null;
        private SyncOptions mSyncOptions = null;

        public event CompletedEventHandler SyncCompleted = null;

        public FolderTreeOneWaySync(string rootPathSource, string rootPathTarget, FolderTreeNode rootMergedNode, SyncDirection syncDirection, ISyncManager syncManager, SyncOptions syncOptions)
        {
            mRootPathSource = rootPathSource;
            mRootPathTarget = rootPathTarget;
            mRootMergedNode = rootMergedNode;
            mSyncDirection = syncDirection;

            if (mSyncDirection == SyncDirection.LeftToRight)        //When syncing from left to right the source is the left side and the target is the right side
            {
                mSourceLocation = Location.UniqueLeft;
                mTargetLocation = Location.UniqueRight;
            }
            else if (mSyncDirection == SyncDirection.RightToLeft)   //When syncing from right to left the source is the right side and the target is the left side
            {
                mSourceLocation = Location.UniqueRight;
                mTargetLocation = Location.UniqueLeft;
            }
            else                                                    //Any other sync direction is unsupported
            {
                throw new SyncException("Sync direction not support with one way sync");
            }

            mSyncOptions = syncOptions;

            mSyncManager = syncManager;

            mFileSync = new FileTreeOneWaySync(mRootPathSource, mRootPathTarget, syncDirection, mSyncManager, mSyncOptions);
        }

        public Task Sync(CancellationToken cancellationToken = default)
        {
            Task task = Task.Run(() =>
            {
                if (mRootMergedNode.NodeLocation == mSourceLocation)        //If the root node is unique to the source, then simply add the whole tree to the target
                {
                    AddToTarget(mRootMergedNode, cancellationToken);
                }
                else if (mRootMergedNode.NodeLocation == mTargetLocation)   //If the root node is unique to the target, then simply delete the whole tree from the target
                {
                    if (mSyncOptions.PropagateDeletes)                      //We only delete if we have been asked to propagate deletes
                        DeleteFromTarget(mRootMergedNode, cancellationToken);
                }
                else                                                        //Otherwise sync the tree starting from the root
                {
                    OneWaySync(mRootMergedNode, cancellationToken);
                }

                mSyncManager.Completed(cancellationToken);

                if (SyncCompleted != null)
                    SyncCompleted();

                return;
            });

            return task;
        }

        private void OneWaySync(FolderTreeNode parentMergedNode, CancellationToken cancellationToken)
        {
            //Sync all the child files of this parent node
            IEnumerable<IFolderChildNode> childFiles = parentMergedNode.Children.Where(x => x is FileTreeNode);
            mFileSync.Sync(childFiles);

            //Add to the target any folders unique to the source. That way any folders that are in the source but not in the target are added to the target
            IEnumerable<IFolderChildNode> uniqueFoldersInSource = parentMergedNode.Children.Where(x => x is FolderTreeNode && x.NodeLocation == mSourceLocation);
            foreach (FolderTreeNode folder in uniqueFoldersInSource)
            {
                AddToTarget(folder, cancellationToken);
            }

            //If we have been asked to propagate deletes, then Delete from the target any folders unique to the target. That way any folders that are in the target but not in the source are removed from the target
            if (mSyncOptions.PropagateDeletes)
            {
                IEnumerable<IFolderChildNode> uniqueFoldersInTarget = parentMergedNode.Children.Where(x => x is FolderTreeNode && x.NodeLocation == mTargetLocation);
                foreach (FolderTreeNode folder in uniqueFoldersInTarget)
                {
                    DeleteFromTarget(folder, cancellationToken);
                }
            }

            //Recursively sync any folders that exist in both the source and the target
            IEnumerable<IFolderChildNode> foldersInBoth = parentMergedNode.Children.Where(x => x is FolderTreeNode && x.NodeLocation == Location.Both);
            foreach (FolderTreeNode folder in foldersInBoth)
            {
                cancellationToken.ThrowIfCancellationRequested();

                OneWaySync(folder, cancellationToken);
            }
        }

        private void AddToTarget(FolderTreeNode folderNode, CancellationToken cancellationToken)
        {
            //Create the folder in the target and set the last modified time to match the folder in the source
            mSyncManager.CreateFolder(mRootPathTarget, folderNode.RelativePath, new ZlpDirectoryInfo(mRootPathSource + folderNode.RelativePath).LastWriteTime);

            //Copy the files from the source to the target setting each last modified time to match the corresponding file in the source
            foreach (FileTreeNode file in folderNode.Children.Where(x => x is FileTreeNode))
            {
                if (mSyncOptions.CopyFileAttributes)
                    mSyncManager.CopyFileIncludinghAttributes(mRootPathSource, file.RelativePath, mRootPathTarget, file.RelativePath, false);
                else
                    mSyncManager.CopyFile(mRootPathSource, file.RelativePath, mRootPathTarget, file.RelativePath, false);
            }

            //Recursively add each sub folder
            foreach (FolderTreeNode folder in folderNode.Children.Where(x => x is FolderTreeNode))
            {
                cancellationToken.ThrowIfCancellationRequested();

                AddToTarget(folder, cancellationToken);
            }
        }

        private void DeleteFromTarget(FolderTreeNode folderNode, CancellationToken cancellationToken)
        {
            //Recursively walk each sub folder so that we go as deep as we can in the folder tree hierarchy before we start deleting the files and folders. This is
            //important to do first as we need to delete the files in a folder first before we can delete the folder itself (Yes the Directory.Delete can be called
            //such that it deletes an entire folder tree but in the future we will want to do extra stuff at each level as we delete which we can't do if we just
            //delete everything e.g. log the name of each file and folder deleted, provide the option to delete to the recycle bin (in which case Directory.Delete
            //will be insufficient), move deleted files and folders to a temporary location before actually deleting them in case something goes wrong and we want to
            //rollback the sync, and anything else I can think of in the future)
            foreach (FolderTreeNode folder in folderNode.Children.Where(x => x is FolderTreeNode))
            {
                cancellationToken.ThrowIfCancellationRequested();

                DeleteFromTarget(folder, cancellationToken);
            }

            //We are as deep as we can go in the folder tree at this level so delete the files in this folder
            foreach (FileTreeNode file in folderNode.Children.Where(x => x is FileTreeNode))
            {
                mSyncManager.DeleteFile(mRootPathTarget, file.RelativePath);
            }

            //Whether the folder is empty will depend on the extension filters that were used
            //when the comparison was made. If no extension filters were used (i.e. *.*) then
            //this folder should now be empty and it can safely be deleted. If extension filters
            //were used (e.g. *.h,*.cpp) then we've only deleted files that match these filters,
            //and there could still be other files that exist that don't match these filters, in
            //which case we don't want to delete the folder. DeleteFolder will only delete the
            //folder if it is empty
            mSyncManager.DeleteFolder(mRootPathTarget, folderNode.RelativePath);
        }
    }
}
