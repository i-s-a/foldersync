﻿using System;
using System.Threading.Tasks;

namespace SyncApi
{
    class Program
    {
        static async Task SyncUsingTree(string source, string target, SyncOptions syncOptions)
        {
            System.Diagnostics.Stopwatch stopWatch = new System.Diagnostics.Stopwatch();

            stopWatch.Start();

            IFolderTreeComparer comparer = new FolderTreeComparer(source, target, null);
            comparer.Compare();

            stopWatch.Stop();
            TimeSpan elapsedTime = stopWatch.Elapsed;
            System.Console.WriteLine("Elapsed seconds: {0}", elapsedTime.TotalSeconds);

            FolderTreeNode rootNode = comparer.Result;

            IFolderTreeSync folderTreeSync = null;
            ISyncManager syncManager = null;

            if (syncOptions.SyncDirect)
                syncManager = new SyncDirect();
            else
                syncManager = new SyncLogged(syncOptions);



            stopWatch.Start();

            //One-way Sync
            folderTreeSync = new FolderTreeOneWaySync(source, target, rootNode, SyncDirection.LeftToRight, syncManager, syncOptions);
            //Two-way Sync
            //folderTreeSync = new FolderTreeTwoWaySync(source, target, rootNode, syncManager, syncOptions);

            await folderTreeSync.Sync(default);
            stopWatch.Stop();
            elapsedTime = stopWatch.Elapsed;
            System.Console.WriteLine("Elapsed seconds: {0}", elapsedTime.TotalSeconds);
        }

        static async Task SyncUsingApi(string source, string target, SyncOptions syncOptions)
        {
            ISyncManager syncManager = null;

            if (syncOptions.SyncDirect)
                syncManager = new SyncDirect();
            else
                syncManager = new SyncLogged(syncOptions);

            SyncApi.SyncType syncType = SyncApi.SyncType.ONEWAY;
            //SyncApi.SyncType syncType = SyncApi.SyncType.TWOWAY;

            System.Diagnostics.Stopwatch stopWatch = new System.Diagnostics.Stopwatch();

            stopWatch.Start();
            SyncApi syncApi = new SyncApi(source, target, syncManager, syncType, syncOptions, null);
            await syncApi.Sync();
            stopWatch.Stop();
            TimeSpan elapsedTime = stopWatch.Elapsed;
            System.Console.WriteLine("Elapsed seconds: {0}", elapsedTime.TotalSeconds);

        }

        static async Task CalculateFolderTreeSize(string rootFoler)
        {
            FolderTreeSize folderTreeSize = new FolderTreeSize(rootFoler);
            var result = await folderTreeSize.Process();
        }

        static async void Main(string[] args)
        {
            string folder1 = @"C:\Folder1";
            string folder2 = @"C:\Folder2";
            SyncOptions syncOptions = new SyncOptions()
            {
                PropagateDeletes = true,
                SyncDirect = false,
                TempPath = System.IO.Path.GetTempPath(),
                LogSyncPosFilenamePrefix = System.IO.Path.GetTempPath() + @"syncpos.txt",
                LogFilenamePrefix = System.IO.Path.GetTempPath() + @"synclog.txt",
                RetainLastSuccessfulSync = false,
                OnlyCopyFileIfSourceNewer = false,
                OneWaySyncCompareContent = false
            };

            await SyncUsingTree(folder1, folder2, syncOptions);
            //await SyncUsingApi(folder1, folder2, syncOptions);
            await CalculateFolderTreeSize(folder1);
        }
    }
}
