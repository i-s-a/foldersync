﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ZetaLongPaths;

namespace SyncApi
{
    public class SyncApi : IDisposable
    {
        //Full path of folder 1 and folder 2. These are the full paths of the folders
        //selected by the user
        private string mFolder1 = null;
        private string mFolder2 = null;

        //Root folder name for folder 1 and folder 2 extracted from the mFolder1 and
        //mFolder2 paths
        private string mRootFolderName1 = null;
        private string mRootFolderName2 = null;

        //Path to the root folder name of folder 1 and folder 2 extracted from the
        //mFolder1 and mFolder2 paths.  mPathToRootX + mRootFolderNameX == mFolderX
        private string mPathToRoot1 = null;
        private string mPathToRoot2 = null;

        private ISyncManager mSyncManager = null;

        System.IO.StreamWriter mOutputLogFile = null;

        public delegate void SyncCompletedCallback();
        private SyncCompletedCallback mSyncCompletedCallback = null;

        //public delegate void SyncErrorCallback(int errorCode, string errorString);
        //private SyncErrorCallback mSyncErrorCallback = null;

        public enum SyncType
        {
            ONEWAY,
            TWOWAY
        };

        private SyncType mSyncType;

        private SyncOptions mSyncOptions;

        private string[] mExtensionFilters = null;

        public SyncApi(string folder1, string folder2, ISyncManager syncManager, SyncType syncType, SyncOptions syncOptions, SyncCompletedCallback syncCompletedCallback)
        {
            //If either folder doesn't exist then can't really do the compare
            if (!ZlpIOHelper.DirectoryExists(folder1))
            {
                throw new SyncException("Folder 1: " + folder1);
            }

            if (!ZlpIOHelper.DirectoryExists(folder2))
            {
                throw new SyncException("Folder 2: " + folder2);
            }

            if (syncOptions == null)
            {
                throw new SyncException("No options specified");
            }

            if (!string.IsNullOrEmpty(syncOptions.OutputLogFile))
            {
                mOutputLogFile = new System.IO.StreamWriter(syncOptions.OutputLogFile);
            }

            mFolder1 = folder1;
            mFolder2 = folder2;

            //Extract from the full path the root folder name and the path to the root folder name
            (mPathToRoot1, mRootFolderName1) = GetPathToRootAndRootFolderName(mFolder1);
            (mPathToRoot2, mRootFolderName2) = GetPathToRootAndRootFolderName(mFolder2);

            mSyncManager = syncManager;
            mSyncType = syncType;
            mSyncOptions = syncOptions;

            //The callback to call when the comparison is completed
            mSyncCompletedCallback = syncCompletedCallback;

            if (string.IsNullOrEmpty(mSyncOptions.ExtensionFilters))
            {
                mSyncOptions.ExtensionFilters = "*.*";
            }

            mExtensionFilters = mSyncOptions.ExtensionFilters.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < mExtensionFilters.Length; ++i)
            {
                mExtensionFilters[i] = mExtensionFilters[i].TrimStart(' ');
                mExtensionFilters[i] = mExtensionFilters[i].TrimEnd(' ');
            }
        }

        private (string pathToRoot, string rootFolderName) GetPathToRootAndRootFolderName(string folder)
        {
            if (folder.EndsWith(@"\"))
                folder = folder.Remove(folder.Length - 1);

            string pathToRoot = "";
            string rootFolderName = folder;

            int index = folder.LastIndexOf('\\');
            if (index != -1)
            {
                pathToRoot = folder.Substring(0, index);
                rootFolderName = folder.Substring(index + 1);
            }

            return (pathToRoot, rootFolderName);
        }

        private string GetName(string folder)
        {
            string name = folder;

            int index = folder.LastIndexOf('\\');
            if (index != -1)
            {
                name = folder.Substring(index + 1);
            }

            return name;
        }

        private string GetRelativePath(string folder, string rootPath)
        {
            if (folder.StartsWith("\\\\?\\"))
                return folder.Substring(rootPath.Length + 4);
            else
                return folder.Substring(rootPath.Length);
        }

        public string BasePath1
        {
            get;
            set;
        }

        public string BasePath2
        {
            get;
            set;
        }

        public Task Sync(CancellationToken cancellationToken = default)
        {
            Task task = Task.Run(() =>
            {
                if (mRootFolderName1 != mRootFolderName2)
                {
                    BasePath1 = mFolder1 + "\\";
                    BasePath2 = mFolder2 + "\\";
                    //If the root folder names are different then we want to use the full path when doing the
                    //comparison. E.g.
                    //Folder 1 - C:\My Folder\Downloads
                    //Folder 2 - C:\My Folder\My Downloads
                    //Then we want to use both full paths as Downloads and My Downloads are different
                    ProcessSubFolders(mFolder1, mFolder2, cancellationToken);
                }
                else
                {
                    BasePath1 = mPathToRoot1 + "\\";
                    BasePath2 = mPathToRoot2 + "\\";
                    //If the root folder names are the same then we want to use the path to the root when doing
                    //the comparison. E.g.
                    //Folder 1 - C:\My Folder\Downloads
                    //Folder 2 - C:\My Folder\Downloads
                    //Then we want to use the path to the root because they both have a root name of Downloads.
                    ProcessSubFolders(mFolder1, mFolder2, cancellationToken);
                }

                mSyncManager.Completed(cancellationToken);

                //We have completed the comparison, now call the callback delegate with the root node of the merged tree
                if (mSyncCompletedCallback != null)
                    mSyncCompletedCallback();


                if (mOutputLogFile != null)
                {
                    mOutputLogFile.Close();
                    mOutputLogFile = null;
                }
            });

            return task;
        }

        private void CopyFileToTarget(string relativePath)
        {
            if (mSyncOptions.CopyFileAttributes)
                mSyncManager.CopyFileIncludinghAttributes(BasePath1, relativePath, BasePath2, relativePath, true);
            else
                mSyncManager.CopyFile(BasePath1, relativePath, BasePath2, relativePath, true);
        }
        
        private void CopyFileToSource(string relativePath)
        {
            if (mSyncOptions.CopyFileAttributes)
                mSyncManager.CopyFileIncludinghAttributes(BasePath2, relativePath, BasePath1, relativePath, true);
            else
                mSyncManager.CopyFile(BasePath2, relativePath, BasePath1, relativePath, true);
        }

        private void DeleteFileFromTarget(string relativePath)
        {
            mSyncManager.DeleteFile(BasePath2, relativePath);
        }

        private void DeleteFileFromSource(string relativePath)
        {
            mSyncManager.DeleteFile(BasePath1, relativePath);
        }

        private void AddFolderToTarget(string relativePath)
        {
            try
            {
                string source = BasePath1 + relativePath;

                mSyncManager.CreateFolder(BasePath2, relativePath, new ZlpDirectoryInfo(source).LastWriteTime);

                ZlpFileInfo[] files = ZlpIOHelper.GetFiles(source, System.IO.SearchOption.TopDirectoryOnly);

                foreach (var file in files)
                {
                    CopyFileToTarget(GetRelativePath(file.FullName, BasePath1));
                }

                ZlpDirectoryInfo[] folders = ZlpIOHelper.GetDirectories(source, System.IO.SearchOption.TopDirectoryOnly);

                foreach (var folder in folders)
                {
                    //Recursively add folders
                    AddFolderToTarget(GetRelativePath(folder.FullName, BasePath1));
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                //We're not allowed access to the file or folder!!
                //Probably want to log this somewhere in a file and also display this somewhere on the UI
                if (mOutputLogFile != null)
                {
                    mOutputLogFile.WriteLine(ex.Message);
                }
            }
        }

        private void AddFolderToSource(string relativePath)
        {
            try
            {
                string target = BasePath2 + relativePath;

                mSyncManager.CreateFolder(BasePath1, relativePath, new ZlpDirectoryInfo(target).LastWriteTime);

                ZlpFileInfo[] files = ZlpIOHelper.GetFiles(target, System.IO.SearchOption.TopDirectoryOnly);
                foreach (var file in files)
                {
                    CopyFileToSource(GetRelativePath(file.FullName, BasePath2));
                }

                ZlpDirectoryInfo[] folders = ZlpIOHelper.GetDirectories(target, System.IO.SearchOption.TopDirectoryOnly);
                foreach (var folder in folders)
                {
                    //Recursively add folders
                    AddFolderToSource(GetRelativePath(folder.FullName, BasePath2));
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                //We're not allowed access to the file or folder!!
                //Probably want to log this somewhere in a file and also display this somewhere on the UI
                if (mOutputLogFile != null)
                {
                    mOutputLogFile.WriteLine(ex.Message);
                }
            }
        }

        private void DeleteFolderFromTarget(string relativePath)
        {
            try
            {
                string target = BasePath2 + relativePath;

                ZlpDirectoryInfo[] folders = ZlpIOHelper.GetDirectories(target, System.IO.SearchOption.TopDirectoryOnly);
                foreach (var folder in folders)
                {
                    //Recursively delete folders
                    DeleteFolderFromTarget(GetRelativePath(folder.FullName, BasePath2));
                }

                ZlpFileInfo[] files = ZlpIOHelper.GetFiles(target, System.IO.SearchOption.TopDirectoryOnly);
                foreach (var file in files)
                {
                    DeleteFileFromTarget(GetRelativePath(file.FullName, BasePath2));
                }

                mSyncManager.DeleteFolder(BasePath2, relativePath);
            }
            catch (UnauthorizedAccessException ex)
            {
                //We're not allowed access to the file or folder!!
                //Probably want to log this somewhere in a file and also display this somewhere on the UI
                if (mOutputLogFile != null)
                {
                    mOutputLogFile.WriteLine(ex.Message);
                }
            }
        }


        private void DeleteFolderFromSource(string relativePath)
        {
            try
            {
                string source = BasePath1 + relativePath;

                ZlpDirectoryInfo[] folders = ZlpIOHelper.GetDirectories(source, System.IO.SearchOption.TopDirectoryOnly);
                foreach (var folder in folders)
                {
                    //Recursively delete folders
                    DeleteFolderFromSource(GetRelativePath(folder.FullName, BasePath1));
                }

                ZlpFileInfo[] files = ZlpIOHelper.GetFiles(source, System.IO.SearchOption.TopDirectoryOnly);
                foreach (var file in files)
                {
                    DeleteFileFromSource(GetRelativePath(file.FullName, BasePath1));
                }

                mSyncManager.DeleteFolder(BasePath1, relativePath);
            }
            catch (UnauthorizedAccessException ex)
            {
                //We're not allowed access to the file or folder!!
                //Probably want to log this somewhere in a file and also display this somewhere on the UI
                if (mOutputLogFile != null)
                {
                    mOutputLogFile.WriteLine(ex.Message);
                }
            }
        }

        private ZlpFileInfo[] GetFiles(string relativePath)
        {
            List<ZlpFileInfo> fileList = new List<ZlpFileInfo>();

            foreach (var extFilter in mExtensionFilters)
            {
                ZlpFileInfo[] files = ZlpIOHelper.GetFiles(relativePath, extFilter, System.IO.SearchOption.TopDirectoryOnly);
                fileList.AddRange(files);
            }

            return fileList.ToArray();
        }

        private ZlpDirectoryInfo[] GetDirectories(string relativePath)
        {
            ZlpDirectoryInfo[] folders = ZlpIOHelper.GetDirectories(relativePath, System.IO.SearchOption.TopDirectoryOnly);
            return folders;
        }

        private void ProcessSubFolders(string relativePath1, string relativePath2, CancellationToken cancellationToken)
        {
            try
            {
                ///////////////////////////////////////////////////////////////////////////////////////////
                //Deal with files
                ZlpFileInfo[] files1 = GetFiles(relativePath1);
                SortedSet<string> filesIn1 = new SortedSet<string>();

                foreach (var file in files1)
                {
                    filesIn1.Add(GetRelativePath(file.FullName, BasePath1));
                }

                ZlpFileInfo[] files2 = GetFiles(relativePath2);
                SortedSet<string> filesIn2 = new SortedSet<string>();

                foreach (var file in files2)
                {
                    filesIn2.Add(GetRelativePath(file.FullName, BasePath2));
                }


                IEnumerable<string> inFiles1Only = filesIn1.Except(filesIn2);
                foreach (var file in inFiles1Only)
                {
                    CopyFileToTarget(file);
                }

                IEnumerable<string> inFiles2Only = filesIn2.Except(filesIn1);
                foreach (var file in inFiles2Only)
                {
                    if (mSyncType == SyncType.ONEWAY)
                    {
                        if (mSyncOptions.PropagateDeletes)
                            DeleteFileFromTarget(file);
                    }
                    else if (mSyncType == SyncType.TWOWAY)
                    {
                        CopyFileToSource(file);
                    }
                    else
                    {
                        throw new Exception("Invalid Sync Type");
                    }
                }

                IEnumerable<string> inBoth = filesIn1.Intersect(filesIn2);
                foreach (var file in inBoth)
                {
                    if (mSyncType == SyncType.ONEWAY)
                    {
                        ZlpFileInfo sourceFileInfo = new ZlpFileInfo(BasePath1 + file);
                        ZlpFileInfo targetFileInfo = new ZlpFileInfo(BasePath2 + file);

                        if (mSyncOptions.OnlyCopyFileIfSourceNewer)
                        {
                            //Only copy if the source file is newer than the destination file
                            if (FileHelper.IsSourceFileNewer(sourceFileInfo, targetFileInfo))
                            {
                                CopyFileToTarget(file);
                            }
                        }
                        else if (mSyncOptions.OneWaySyncCompareContent)
                        {
                            //Only copy if the content of the source file and target file are not the same
                            if (!FileHelper.ContentEqual(BasePath1 + file, BasePath2 + file))
                            {
                                CopyFileToTarget(file);
                            }
                        }
                        else
                        {
                            //As a quick check use the length and last modified time to determine
                            //if the source file and destination file are different, if they are
                            //then copy the source over the destination. NOTE: We will want to
                            //expand this to be able to support different ways of determining if
                            //the files have changed (e.g. CRC32, SHA1, byte-by-byte comparison, etc.)
                            if (FileHelper.MetaDataAreNotEqual(sourceFileInfo, targetFileInfo))
                            {
                                CopyFileToTarget(file);
                            }
                        }
                    }
                    else if (mSyncType == SyncType.TWOWAY)
                    {
                        ZlpFileInfo sourceFileInfo = new ZlpFileInfo(BasePath1 + file);
                        ZlpFileInfo targetFileInfo = new ZlpFileInfo(BasePath2 + file);

                        if (FileHelper.IsSourceFileNewer(sourceFileInfo, targetFileInfo))
                        {
                            CopyFileToTarget(file);
                        }
                        else if (FileHelper.IsTargetFileNewer(sourceFileInfo, targetFileInfo))
                        {
                            CopyFileToSource(file);
                        }
                        else
                        {
                            //They're the same so for now do nothing
                        }
                    }
                    else
                    {
                        throw new SyncException("Invalid Sync Type");
                    }
                }

                cancellationToken.ThrowIfCancellationRequested();

                ///////////////////////////////////////////////////////////////////////////////////////////
                //Deal with folders
                ZlpDirectoryInfo[] folders1 = GetDirectories(relativePath1);
                SortedSet<string> foldersIn1 = new SortedSet<string>();

                foreach (var folder in folders1)
                {
                    foldersIn1.Add(GetRelativePath(folder.FullName, BasePath1));
                }

                ZlpDirectoryInfo[] folders2 = GetDirectories(relativePath2);
                SortedSet<string> foldersIn2 = new SortedSet<string>();

                foreach (var folder in folders2)
                {
                    foldersIn2.Add(GetRelativePath(folder.FullName, BasePath2));
                }


                IEnumerable<string> foldersIn1Only = foldersIn1.Except(foldersIn2);
                foreach (var folder in foldersIn1Only)
                {
                    cancellationToken.ThrowIfCancellationRequested();

                    AddFolderToTarget(folder);
                }

                IEnumerable<string> foldersIn2Only = foldersIn2.Except(foldersIn1);
                foreach (var folder in foldersIn2Only)
                {
                    cancellationToken.ThrowIfCancellationRequested();

                    if (mSyncType == SyncType.ONEWAY)
                    {
                        if (mSyncOptions.PropagateDeletes)
                            DeleteFolderFromTarget(folder);
                    }
                    else if (mSyncType == SyncType.TWOWAY)
                    {
                        AddFolderToSource(folder);
                    }
                    else
                    {
                        throw new Exception("Invalid Sync Type");
                    }
                }

                IEnumerable<string> foldersInBoth = foldersIn1.Intersect(foldersIn2);
                foreach (var folder in foldersInBoth)
                {
                    //For now if a folder is in both then do nothing. Ideally what we
                    //want to do is compare the last write time, last access time, etc.
                    //and make sure they match depending on the sync type

                    //Recursively go through the folders that exist in both source and target
                    ProcessSubFolders(BasePath1 + folder, BasePath2 + folder, cancellationToken);
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                //We're not allowed access to the file or folder!!
                //Probably want to log this somewhere in a file and also display this somewhere on the UI
                if (mOutputLogFile != null)
                {
                    mOutputLogFile.WriteLine(ex.Message);
                }
            }
        }

        public void Dispose()
        {
            if (mOutputLogFile != null)
                mOutputLogFile.Close();
        }
    }
}
