﻿namespace SyncApi
{
    public interface IFolderTreeComparer
    {
        //Compare the folders
        void Compare();

        //Returns the root merged node of the compare
        FolderTreeNode Result
        {
            get;
        }

        string BasePath1
        {
            get;
            set;
        }

        string BasePath2
        {
            get;
            set;
        }

    }
}
