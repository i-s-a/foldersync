﻿using System;

namespace SyncApi
{
    public interface IFolderTreeSizeChildNode : IComparable<IFolderTreeSizeChildNode>
    {
        FolderTreeSizeNode Parent { get; set; }
        string FullPath { get; set; }
        string Name { get; set; }
        string RelativePath { get; set; }
        DateTime LastWriteTime { get; set; }
        long Size { get; set; }
    }
}
