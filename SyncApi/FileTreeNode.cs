﻿using System;
using System.Collections.Generic;

namespace SyncApi
{
    sealed public class FileTreeNode : IFolderChildNode, IEquatable<FileTreeNode>//, IComparable<FileNode>
    {
        public FileTreeNode()
        {
            NodeLocation = Location.Both;
            DiffInfo = new DiffInfo();
        }

        public FolderTreeNode Parent { get; set; }
        public string Name { get; set; }
        public string RelativePath { get; set; }

        public Location NodeLocation { get; set; }

        public DateTime LastWriteTimeLeft { get; set; }
        public DateTime LastWriteTimeRight { get; set; }

        public long FileSizeLeft { get; set; }
        public long FileSizeRight { get; set; }

        public DiffInfo DiffInfo { get; set; }

        public bool Equals(FileTreeNode other)
        {
            if (object.ReferenceEquals(other, null))
                return false;

            if (object.ReferenceEquals(other, this))
                return true;

            return (RelativePath.Equals(other.RelativePath));
        }

        public override int GetHashCode()
        {
            return (RelativePath.GetHashCode());
        }

        public int CompareTo(IFolderChildNode other)
        {
            //return System.String.Compare(RelativePath, other.RelativePath, System.StringComparison.Ordinal);
            return System.String.Compare(Name, other.Name, System.StringComparison.Ordinal);
        }
    }

    public class FileNodePathComparer<T> : IEqualityComparer<FileTreeNode>
    {
        public bool Equals(FileTreeNode lhs, FileTreeNode rhs)
        {
            if (object.ReferenceEquals(lhs, rhs))
                return true;

            return (lhs != null && rhs != null && lhs.RelativePath.Equals(rhs.RelativePath));
        }

        public int GetHashCode(FileTreeNode o)
        {
            return o.RelativePath.GetHashCode();
        }
    }

}
