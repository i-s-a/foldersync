﻿namespace SyncApi
{
    sealed public class DiffInfo
    {
        public DiffInfo()
        {
            LastModifiedTimeDifferent = false;
            FileSizeDifferent = false;
            ContentDifferent = false;
        }

        public bool LastModifiedTimeDifferent { get; set; }

        public bool FileSizeDifferent { get; set; }

        public bool ContentDifferent { get; set; }

        public bool InfoDifferent()
        {
            return (LastModifiedTimeDifferent || FileSizeDifferent || ContentDifferent);
        }

        public bool InfoSame()
        {
            return (!InfoDifferent());
        }
    }
}
