﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ZetaLongPaths;

namespace SyncApi
{
    public class FolderTreeTwoWaySync : IFolderTreeSync
    {
        private string mRootPathSource;
        private string mRootPathTarget;
        private FolderTreeNode mRootMergedNode;
        private IFileTreeSync mFileSync = null;
        private ISyncManager mSyncManager = null;
        private SyncOptions mSyncOptions = null;

        public event CompletedEventHandler SyncCompleted = null;

        public FolderTreeTwoWaySync(string rootPathSource, string rootPathTarget, FolderTreeNode rootMergedNode, ISyncManager syncManager, SyncOptions syncOptions)
        {
            mRootPathSource = rootPathSource;
            mRootPathTarget = rootPathTarget;
            mRootMergedNode = rootMergedNode;
            mSyncOptions = syncOptions;

            mSyncManager = syncManager;

            mFileSync = new FileTreeTwoWaySync(mRootPathSource, mRootPathTarget, mSyncManager, mSyncOptions);
        }

        public Task Sync(CancellationToken cancellationToken = default)
        {
            Task task = Task.Run(() =>
            {
                if (mRootMergedNode.NodeLocation == Location.UniqueLeft)
                {
                    AddToTarget(mRootMergedNode, cancellationToken);
                }
                else if (mRootMergedNode.NodeLocation == Location.UniqueRight)
                {
                    AddToSource(mRootMergedNode, cancellationToken);
                }
                else
                {
                    TwoWaySync(mRootMergedNode, cancellationToken);
                }

                mSyncManager.Completed(cancellationToken);

                if (SyncCompleted != null)
                    SyncCompleted();
            });

            return task;
        }

        private void TwoWaySync(FolderTreeNode parentMergedNode, CancellationToken cancellationToken)
        {
            IEnumerable<IFolderChildNode> childFiles = parentMergedNode.Children.Where(x => x is FileTreeNode);
            mFileSync.Sync(childFiles);

            IEnumerable<IFolderChildNode> uniqueFoldersInSource = parentMergedNode.Children.Where(x => x is FolderTreeNode && x.NodeLocation == Location.UniqueLeft);
            foreach (FolderTreeNode folder in uniqueFoldersInSource)
            {
                AddToTarget(folder, cancellationToken);
            }

            IEnumerable<IFolderChildNode> uniqueFoldersInTarget = parentMergedNode.Children.Where(x => x is FolderTreeNode && x.NodeLocation == Location.UniqueRight);
            foreach (FolderTreeNode folder in uniqueFoldersInTarget)
            {
                AddToSource(folder, cancellationToken);
            }

            IEnumerable<IFolderChildNode> foldersInBoth = parentMergedNode.Children.Where(x => x is FolderTreeNode && x.NodeLocation == Location.Both);
            foreach (FolderTreeNode folder in foldersInBoth)
            {
                TwoWaySync(folder, cancellationToken);
            }
        }

        private void AddToTarget(FolderTreeNode folderNode, CancellationToken cancellationToken)
        {
            mSyncManager.CreateFolder(mRootPathTarget, folderNode.RelativePath, new ZlpDirectoryInfo(mRootPathSource + folderNode.RelativePath).LastWriteTime);

            foreach (FileTreeNode file in folderNode.Children.Where(x => x is FileTreeNode))
            {
                if (mSyncOptions.CopyFileAttributes)
                    mSyncManager.CopyFileIncludinghAttributes(mRootPathSource, file.RelativePath, mRootPathTarget, file.RelativePath, false);
                else
                    mSyncManager.CopyFile(mRootPathSource, file.RelativePath, mRootPathTarget, file.RelativePath, false);
            }

            foreach (FolderTreeNode folder in folderNode.Children.Where(x => x is FolderTreeNode))
            {
                cancellationToken.ThrowIfCancellationRequested();

                AddToTarget(folder, cancellationToken);
            }
        }

        private void AddToSource(FolderTreeNode folderNode, CancellationToken cancellationToken)
        {
            mSyncManager.CreateFolder(mRootPathSource, folderNode.RelativePath, new ZlpDirectoryInfo(mRootPathTarget + folderNode.RelativePath).LastWriteTime);

            foreach (FileTreeNode file in folderNode.Children.Where(x => x is FileTreeNode))
            {
                if (mSyncOptions.CopyFileAttributes)
                    mSyncManager.CopyFileIncludinghAttributes(mRootPathTarget, file.RelativePath, mRootPathSource, file.RelativePath, false);
                else
                    mSyncManager.CopyFile(mRootPathTarget, file.RelativePath, mRootPathSource, file.RelativePath, false);
            }

            foreach (FolderTreeNode folder in folderNode.Children.Where(x => x is FolderTreeNode))
            {
                cancellationToken.ThrowIfCancellationRequested();

                AddToSource(folder, cancellationToken);
            }
        }

    }
}
