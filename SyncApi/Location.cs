﻿namespace SyncApi
{
    public enum Location
    {
        UniqueLeft,             //Only in folder 1
        UniqueRight,            //Only in folder 2
        Both,                   //In both folder 1 and folder 2
    };
}
