﻿using System;
using System.Collections.Generic;
using System.Linq;
using ZetaLongPaths;

namespace SyncApi
{
    //This class doesn't populate the the LastWriteTimeLeft, LastWriteTimeRight, FileSizeLeft,
    //FileSizeRight and InBothButChildrenDiffer members of the FileTreeNode and FolderTreeNode
    //classes. These members are only really useful when wanting to visually display the result
    //of the comparison. NOTE: When syncing the tree, we always determine the last write time
    //and file size at the point of syncing, in case they have changed between performing the
    //comparison and syncing. Therefore we don't need these members when performing a straight
    //forward comparison and sync. If we are displaying the result of the comparison first (e.g.
    //to show a preview of the result) then these members are useful to have populated during
    //the comparison as you can use these members when populating the visual tree without having
    //to determine them again, in which case FolderTreeComparerEx should be used as this does
    //populate these members.
    public class FolderTreeComparer : IFolderTreeComparer
    {
        //Full path of folder 1 and folder 2. These are the full paths of the folders
        //selected by the user
        private string mFolder1 = null;
        private string mFolder2 = null;

        //Root folder name for folder 1 and folder 2 extracted from the mFolder1 and
        //mFolder2 paths
        private string mRootFolderName1 = null;
        private string mRootFolderName2 = null;

        //Path to the root folder name of folder 1 and folder 2 extracted from the
        //mFolder1 and mFolder2 paths.  mPathToRootX + mRootFolderNameX == mFolderX
        private string mPathToRoot1 = null;
        private string mPathToRoot2 = null;

        //For comparing paths stored in the FileNode and FolderNode
        private FolderNodePathComparer<FolderTreeNode> mFolderNodePathComparer = new FolderNodePathComparer<FolderTreeNode>();
        private FileNodePathComparer<FileTreeNode> mFileNodePathComparer = new FileNodePathComparer<FileTreeNode>();

        //The root parent of the merged tree. After the comparison has completed this
        //will be the root node of the resulting tree that merges both trees in to one.
        //Walking the tree from this node will identify each file and folder and specify
        //whether each one is unique to folder 1, unique to folder 2 or present in both
        //folders
        private FolderTreeNode mRootMergedFolderNode = null;

        //Callback to call once the comparison has been completed
        public delegate void CompareCompletedCallback(FolderTreeNode comparedRootNode);
        private CompareCompletedCallback mCompareCompletedCallback = null;

        public FolderTreeComparer(string folder1, string folder2, CompareCompletedCallback compareCompletedCallback)
        {
            //If either folder doesn't exist then can't really do the compare
            if (!ZlpIOHelper.DirectoryExists(folder1))
            {
                throw new CompareException("Invalid Folder 1: " + folder1);
            }

            if (!ZlpIOHelper.DirectoryExists(folder2))
            {
                throw new CompareException("Invalid Folder 2: " + folder2);
            }

            mFolder1 = folder1;
            mFolder2 = folder2;

            //Extract from the full path the root folder name and the path to the root folder name
            GetPathToRootAndRootFolderName(mFolder1, out mPathToRoot1, out mRootFolderName1);
            GetPathToRootAndRootFolderName(mFolder2, out mPathToRoot2, out mRootFolderName2);

            //The callback to call when the comparison is completed
            mCompareCompletedCallback = compareCompletedCallback;
        }

        #region IFolderComparer methods
        public void Compare()
        {
            //Create a root node that will be a common parent for both the left side and the right side
            mRootMergedFolderNode = new FolderTreeNode()
            {
                Name = "..",
                RelativePath = "",
                Parent = null,
                Children = new SortedSet<IFolderChildNode>(),
                NodeLocation = Location.Both
            };

            if (mRootFolderName1 != mRootFolderName2)
            {
                BasePath1 = mFolder1 + "\\";
                BasePath2 = mFolder2 + "\\";
                //If the root folder names are different then we want to use the full path when doing the
                //comparison. E.g.
                //Folder 1 - C:\My Folder\Downloads
                //Folder 2 - C:\My Folder\My Downloads
                //Then we want to use both full paths as Downloads and My Downloads are different
                ProcessSubFolders(mRootMergedFolderNode, mFolder1, mFolder2, mFolder1, mFolder2);
            }
            else
            {
                BasePath1 = mPathToRoot1 + "\\";
                BasePath2 = mPathToRoot2 + "\\";
                //If the root folder names are the same then we want to use the path to the root when doing
                //the comparison. E.g.
                //Folder 1 - C:\My Folder\Downloads
                //Folder 2 - C:\My Folder\Downloads
                //Then we want to use the path to the root because they both have a root name of Downloads.
                ProcessSubFolders(mRootMergedFolderNode, mFolder1, mFolder2, mPathToRoot1, mPathToRoot2);
            }

            //We have completed the comparison, now call the callback delegate with the root node of the merged tree
            if (mCompareCompletedCallback != null)
                mCompareCompletedCallback(mRootMergedFolderNode);
        }

        public FolderTreeNode Result
        {
            get { return mRootMergedFolderNode; }
        }

        public string BasePath1
        {
            get;
            set;
        }

        public string BasePath2
        {
            get;
            set;
        }

        #endregion

        public static bool AreFoldersEqual(string path1, string path2)
        {
            ZlpDirectoryInfo dirInfo1 = new ZlpDirectoryInfo(path1);
            ZlpDirectoryInfo dirInfo2 = new ZlpDirectoryInfo(path2);

            TimeSpan diff = dirInfo1.LastWriteTime - dirInfo2.LastWriteTime;

            return (dirInfo1.Name.Equals(dirInfo2.Name) &&
                    Math.Abs(diff.TotalSeconds) <= 20);
        }

        public static bool AreFilesEqual(string path1, string path2)
        {
            ZlpFileInfo fileInfo1 = new ZlpFileInfo(path1);
            ZlpFileInfo fileInfo2 = new ZlpFileInfo(path2);

            TimeSpan diff = fileInfo1.LastWriteTime - fileInfo2.LastWriteTime;

            return (fileInfo1.Name.Equals(fileInfo2.Name) &&
                    fileInfo1.Length == fileInfo2.Length &&
                    Math.Abs(diff.TotalSeconds) <= 20);
        }

        private void GetPathToRootAndRootFolderName(string folder, out string pathToRoot, out string rootFolderName)
        {
            if (!folder.EndsWith("\\"))
                folder += "\\";

            pathToRoot = "";
            rootFolderName = folder;

            int index = folder.LastIndexOf('\\');
            if (index != -1)
            {
                pathToRoot = folder.Substring(0, index);
                rootFolderName = folder.Substring(index + 1);
            }
        }

        private string GetName(string folder)
        {
            string name = folder;

            int index = folder.LastIndexOf('\\');
            if (index != -1)
            {
                name = folder.Substring(index + 1);
            }

            return name;
        }

        private string GetRelativePath(string folder, string rootPath)
        {
            if (folder.StartsWith("\\\\?\\"))
                return folder.Substring(rootPath.Length + 5);
            else
                return folder.Substring(rootPath.Length + 1);
        }

        private void ProcessSubFolders(FolderTreeNode parentMergedNode, string relativePath1, string relativePath2, string rootPath1, string rootPath2)
        {
            try
            {
                ///////////////////////////////////////////////////////////////////////////////////////////
                //Deal with files
                ZlpFileInfo[] files1 = ZlpIOHelper.GetFiles(relativePath1, System.IO.SearchOption.TopDirectoryOnly);
                SortedSet<string> filesIn1 = new SortedSet<string>();

                foreach (var file in files1)
                {
                    filesIn1.Add(GetRelativePath(file.FullName, rootPath1));
                }

                ZlpFileInfo[] files2 = ZlpIOHelper.GetFiles(relativePath2, System.IO.SearchOption.TopDirectoryOnly);
                SortedSet<string> filesIn2 = new SortedSet<string>();

                foreach (var file in files2)
                {
                    filesIn2.Add(GetRelativePath(file.FullName, rootPath2));
                }

                IEnumerable<string> inFiles1Only = filesIn1.Except(filesIn2);
                foreach (var file in inFiles1Only)
                {
                    FileTreeNode fileNode = new FileTreeNode()
                    {
                        Name = GetName(file),
                        RelativePath = file,
                        Parent = parentMergedNode,
                        NodeLocation = Location.UniqueLeft
                    };

                    parentMergedNode.Children.Add(fileNode);
                }

                IEnumerable<string> inFiles2Only = filesIn2.Except(filesIn1);
                foreach (var file in inFiles2Only)
                {
                    FileTreeNode fileNode = new FileTreeNode()
                    {
                        Name = GetName(file),
                        RelativePath = file,
                        Parent = parentMergedNode,
                        NodeLocation = Location.UniqueRight
                    };

                    parentMergedNode.Children.Add(fileNode);
                }

                IEnumerable<string> inBoth = filesIn1.Intersect(filesIn2);
                foreach (var file in inBoth)
                {
                    FileTreeNode fileNode = new FileTreeNode()
                    {
                        Name = GetName(file),
                        RelativePath = file,
                        Parent = parentMergedNode,
                        NodeLocation = Location.Both
                    };

                    parentMergedNode.Children.Add(fileNode);
                }

                ///////////////////////////////////////////////////////////////////////////////////////////
                //Deal with folders

                ZlpDirectoryInfo[] folders1 = ZlpIOHelper.GetDirectories(relativePath1, System.IO.SearchOption.TopDirectoryOnly);
                SortedSet<string> foldersIn1 = new SortedSet<string>();

                foreach (var folder in folders1)
                {
                    foldersIn1.Add(GetRelativePath(folder.FullName, rootPath1));
                }

                ZlpDirectoryInfo[] folders2 = ZlpIOHelper.GetDirectories(relativePath2, System.IO.SearchOption.TopDirectoryOnly);
                SortedSet<string> foldersIn2 = new SortedSet<string>();
                foreach (var folder in folders2)
                {
                    foldersIn2.Add(GetRelativePath(folder.FullName, rootPath2));
                }

                IEnumerable<string> foldersIn1Only = foldersIn1.Except(foldersIn2);
                foreach (var folder in foldersIn1Only)
                {
                    FolderTreeNode folderNode = new FolderTreeNode()
                    {
                        Name = GetName(folder),
                        RelativePath = folder,
                        NodeLocation = Location.UniqueLeft,
                        Parent = parentMergedNode,
                        Children = new SortedSet<IFolderChildNode>()
                    };

                    parentMergedNode.Children.Add(folderNode);

                    WalkFolderTree(folderNode, rootPath1 + "\\" + folder, rootPath1, Location.UniqueLeft);
                }

                IEnumerable<string> foldersIn2Only = foldersIn2.Except(foldersIn1);
                foreach (var folder in foldersIn2Only)
                {
                    FolderTreeNode folderNode = new FolderTreeNode()
                    {
                        Name = GetName(folder),
                        RelativePath = folder,
                        NodeLocation = Location.UniqueRight,
                        Parent = parentMergedNode,
                        Children = new SortedSet<IFolderChildNode>()
                    };

                    parentMergedNode.Children.Add(folderNode);

                    WalkFolderTree(folderNode, rootPath2 + "\\" + folder, rootPath2, Location.UniqueRight);
                }

                IEnumerable<string> foldersInBoth = foldersIn1.Intersect(foldersIn2);
                foreach (var folder in foldersInBoth)
                {
                    FolderTreeNode folderNode = new FolderTreeNode()
                    {
                        Name = GetName(folder),
                        RelativePath = folder,
                        NodeLocation = Location.Both,
                        Parent = parentMergedNode,
                        Children = new SortedSet<IFolderChildNode>()
                    };

                    parentMergedNode.Children.Add(folderNode);

                    //Process sub folders and files...recursive call!!!!!
                    ProcessSubFolders(folderNode, rootPath1 + "\\" + folder, rootPath2 + "\\" + folder, rootPath1, rootPath2);
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                //We're not allowed access to the file or folder!!
                //Probably want to log this somewhere in a file and also display this somewhere on the UI
                throw new UnauthorizedAccessException("Compare Error", ex);
            }
            catch(Exception ex)
            {
                throw new Exception("Compare Error", ex);
            }
        }

        private void WalkFolderTree(FolderTreeNode parentMergedNode, string path, string pathToRoot, Location location)
        {
            try
            {

                //If there are any files add them to the parent and mark them with the location flag
                ZlpFileInfo[] files = ZlpIOHelper.GetFiles(path, System.IO.SearchOption.TopDirectoryOnly);
                foreach (var file in files)
                {
                    FileTreeNode fileNode = new FileTreeNode()
                    {
                        Name = GetName(file.FullName),
                        RelativePath = GetRelativePath(file.FullName, pathToRoot),
                        NodeLocation = location,
                        Parent = parentMergedNode
                    };

                    parentMergedNode.Children.Add(fileNode);
                }

                //If there are any folders add them to the parent, mark them with the location flag and recursively walk the tree
                ZlpDirectoryInfo[] folders = ZlpIOHelper.GetDirectories(path, System.IO.SearchOption.TopDirectoryOnly);
                foreach (var folder in folders)
                {
                    FolderTreeNode folderNode = new FolderTreeNode()
                    {
                        Name = GetName(folder.FullName),
                        RelativePath = GetRelativePath(folder.FullName, pathToRoot),
                        NodeLocation = location,
                        Parent = parentMergedNode,
                        Children = new SortedSet<IFolderChildNode>(),
                    };

                    parentMergedNode.Children.Add(folderNode);

                    //Recursively walk the tree
                    WalkFolderTree(folderNode, folder.FullName, pathToRoot, location);
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                //We're not allowed access to the file or folder!!
                //Probably want to log this somewhere in a file and also display this somewhere on the UI
                throw new UnauthorizedAccessException("Compare Error", ex);
            }
            catch (Exception ex)
            {
                throw new Exception("Compare Error", ex);
            }
        }
    }
}
