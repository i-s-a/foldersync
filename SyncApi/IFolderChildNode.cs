﻿using System;

namespace SyncApi
{
    public interface IFolderChildNode : IComparable<IFolderChildNode>
    {
        FolderTreeNode Parent { get; set; }
        string Name { get; set; }
        string RelativePath { get; set; }
        Location NodeLocation { get; set; }
        DateTime LastWriteTimeLeft { get; set; }
        DateTime LastWriteTimeRight { get; set; }
    }
}
