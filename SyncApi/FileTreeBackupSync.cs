﻿using System.Collections.Generic;
using ZetaLongPaths;

namespace SyncApi
{
    public class FileTreeBackupSync : IFileTreeSync
    {
        private string mRootPathSource;
        private string mRootPathTarget;
        private SyncDirection mSyncDirection;
        private Location mSourceLocation;
        private Location mTargetLocation;
        private ISyncManager mSyncManager = null;

        public FileTreeBackupSync(string rootPathSource, string rootPathTarget, SyncDirection syncDirection, ISyncManager syncManager)
        {
            mRootPathSource = rootPathSource;
            mRootPathTarget = rootPathTarget;
            mSyncDirection = syncDirection;

            if (mSyncDirection == SyncDirection.LeftToRight)
            {
                mSourceLocation = Location.UniqueLeft;
                mTargetLocation = Location.UniqueRight;
            }
            else
            {
                mSourceLocation = Location.UniqueRight;
                mTargetLocation = Location.UniqueLeft;
            }

            mSyncManager = syncManager;
        }

        public void Sync(IFolderChildNode fileNode)
        {
            if (fileNode.NodeLocation == mSourceLocation)
            {
                string sourcePath = mRootPathSource + fileNode.RelativePath;

                //We want the target file to have the same last modified time as the source
                mSyncManager.CopyFile(mRootPathSource, fileNode.RelativePath, mRootPathTarget, fileNode.RelativePath, new ZlpFileInfo(sourcePath).LastWriteTime, true);
            }
            else if (fileNode.NodeLocation == mTargetLocation)
            {
                mSyncManager.DeleteFile(mRootPathTarget, fileNode.RelativePath);
            }
            else if (fileNode.NodeLocation == Location.Both)
            {
                string sourcePath = mRootPathSource + fileNode.RelativePath;
                string targetPath = mRootPathTarget + fileNode.RelativePath;

                ZlpFileInfo sourceFileInfo = new ZlpFileInfo(sourcePath);
                ZlpFileInfo targetFileInfo = new ZlpFileInfo(targetPath);

                if (sourceFileInfo.Length != targetFileInfo.Length ||
                    sourceFileInfo.LastWriteTime != targetFileInfo.LastWriteTime)
                {
                    //We want the target file to have the same last modified time as the source
                    mSyncManager.CopyFile(mRootPathSource, fileNode.RelativePath, mRootPathTarget, fileNode.RelativePath, sourceFileInfo.LastWriteTime, true);
                }
            }
            else
            {
                throw new SyncException("Unrecognised file location in file backup");
            }
        }

        public void Sync(IEnumerable<IFolderChildNode> fileNodes)
        {
            foreach (var fileNode in fileNodes)
            {
                Sync(fileNode);
            }
        }

    }
}
