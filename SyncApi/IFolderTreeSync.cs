﻿using System.Threading;
using System.Threading.Tasks;

namespace SyncApi
{
    public delegate void CompletedEventHandler();

    public interface IFolderTreeSync
    {
        Task Sync(CancellationToken cancellationToken);

        event CompletedEventHandler SyncCompleted;
    }
}
