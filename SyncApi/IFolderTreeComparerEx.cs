﻿using System.Threading.Tasks;
using System.Threading;

namespace SyncApi
{
    public interface IFolderTreeComparerEx
    {
        //Compare the folders
        Task<FolderTreeNode> Compare(CancellationToken cancellationToken);

        //Return the node found when navigating the result tree using the
        //path
        IFolderChildNode GetNode(string path);

        //Returns the root merged node of the compare
        FolderTreeNode Result
        {
            get;
        }

        string BasePath1
        {
            get;
            set;
        }

        string BasePath2
        {
            get;
            set;
        }

        string CommonRootName
        {
            get;
            set;
        }
    }
}
