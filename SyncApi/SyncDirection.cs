﻿namespace SyncApi
{
    public enum SyncDirection
    {
        LeftToRight,
        RightToLeft,
        Both
    }
}
