﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using ZetaLongPaths;

namespace SyncApi
{
    public class FolderTreeSize
    {
        private string mRootFolder = null;      //Full path of folder (e.g. C:\Users\Admin\Downloads)
        private string mRootFolderName = null;  //Name of the root folder (e.g. Downloads)
        private string mPathToRoot = null;      //Path to get to the root folder (e.g. C:\Users\Admin)

        private FolderTreeSizeNode mRootNode = null;
        private IDictionary<string, IFolderTreeSizeChildNode> mFolderItemsDict = new Dictionary<string, IFolderTreeSizeChildNode>();

        public FolderTreeSize(string rootFolder)
        {
            //If the folder doesn't exist then we can't really do anything
            if (!ZlpIOHelper.DirectoryExists(rootFolder))
            {
                throw new ArgumentException("Invalid Folder: " + rootFolder);
            }

            mRootFolder = rootFolder;
            if (mRootFolder.EndsWith(@"\"))
                mRootFolder = mRootFolder.Remove(mRootFolder.Length - 1);

            (mPathToRoot, mRootFolderName) = GetPathToRootAndRootFolderName(mRootFolder);
        }

        private (string pathToRoot, string rootFolderName) GetPathToRootAndRootFolderName(string folder)
        {
            if (folder.EndsWith(@"\"))
                folder = folder.Remove(folder.Length - 1);

            string pathToRoot = "";
            string rootFolderName = folder;

            int index = folder.LastIndexOf('\\');
            if (index != -1)
            {
                pathToRoot = folder.Substring(0, index);
                rootFolderName = folder.Substring(index + 1);
            }

            return (pathToRoot, rootFolderName);
        }

        private string GetName(string folder)
        {
            string name = folder;

            int index = folder.LastIndexOf('\\');
            if (index != -1)
            {
                name = folder.Substring(index + 1);
            }

            return name;
        }

        private string GetRelativePath(string folder, string rootPath)
        {
            if (folder.StartsWith("\\\\?\\"))
                return folder.Substring(rootPath.Length + 5);
            else
                return folder.Substring(rootPath.Length + 1);
        }

        public Task<FolderTreeSizeNode> Process(CancellationToken cancellationToken = default)
        {
            Task<FolderTreeSizeNode> task = Task.Run(() =>
            {
                var rootFolder = ZlpIOHelper.GetDirectories(mPathToRoot).SingleOrDefault(x => x.FullName == mRootFolder);

                mRootNode = new FolderTreeSizeNode()
                {
                    Parent = null,
                    FullPath = mRootFolder,
                    Name = mRootFolderName,
                    RelativePath = "",
                    //RelativePath = mRootFolderName,
                    Children = new SortedSet<IFolderTreeSizeChildNode>(),
                    LastWriteTime = (rootFolder != null ? rootFolder.LastWriteTime : default(DateTime))
                };

                mFolderItemsDict.Add("", mRootNode);

                ProcessSubFolders(mRootNode, mRootFolder, cancellationToken);

                mRootNode.Size = mRootNode.Children.Sum(x => x.Size);

                return mRootNode;
            });

            return task;
        }

        private void ProcessSubFolders(FolderTreeSizeNode parentNode, string path, CancellationToken cancellationToken)
        {
            try
            {
                //Go through the files first
                ZlpFileInfo[] files = ZlpIOHelper.GetFiles(path);
                foreach (var file in files)
                {
                    FileTreeSizeNode fileNode = new FileTreeSizeNode()
                    {
                        Parent = parentNode,
                        FullPath = file.FullName, // path + @"\" + file.Name,
                        Name = file.Name,
                        RelativePath = GetRelativePath(file.FullName, mRootFolder),
                        LastWriteTime = file.LastWriteTime,
                        Size = file.Length
                    };

                    parentNode.Children.Add(fileNode);

                    mFolderItemsDict.Add(fileNode.RelativePath, fileNode);
                }

                cancellationToken.ThrowIfCancellationRequested();

                //Now go through the folders
                ZlpDirectoryInfo[] directories = ZlpIOHelper.GetDirectories(path);
                foreach (var directory in directories)
                {
                    cancellationToken.ThrowIfCancellationRequested();

                    FolderTreeSizeNode folderNode = new FolderTreeSizeNode()
                    {
                        Parent = parentNode,
                        FullPath = directory.FullName,  //path + @"\" + directory.Name
                        Name = directory.Name,
                        RelativePath = GetRelativePath(directory.FullName, mRootFolder),
                        Children = new SortedSet<IFolderTreeSizeChildNode>(),
                        LastWriteTime = directory.LastWriteTime
                    };

                    parentNode.Children.Add(folderNode);

                    mFolderItemsDict.Add(folderNode.RelativePath, folderNode);

                    ProcessSubFolders(folderNode, directory.FullName, cancellationToken);
                }

                parentNode.Size = parentNode.Children.Sum(x => x.Size);
            }
            catch (UnauthorizedAccessException ex)
            {
                throw new UnauthorizedAccessException("Error determining tree size", ex);
            }
        }

        public string RootFolderName
        {
            get { return mRootFolderName; }
            private set { mRootFolderName = value; }
        }

        public string RootFolder
        {
            get { return mRootFolder; }
            private set { mRootFolder = value; }
        }

        public IFolderTreeSizeChildNode GetNode(string path)
        {
            if (!mFolderItemsDict.ContainsKey(path))
                throw new Exception("Error getting node");

            return mFolderItemsDict[path];
        }
    }
}
