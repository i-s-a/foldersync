﻿using System;
using System.Threading;
using ZetaLongPaths;

namespace SyncApi
{
    public class SyncDirect : ISyncManager
    {
        public void CopyFile(string fromRootPath, string from, string toRootPath, string to, bool overwriteIfExists)
        {
            //Perhaps it would be better to first copy the file to a temporary file and when it is
            //copied, delete the target file, if it exists, and then rename the temporary file to
            //the target filename!!
            ZlpIOHelper.CopyFile(fromRootPath + from, toRootPath + to, overwriteIfExists);
        }

        public void CopyFileIncludinghAttributes(string fromRootPath, string from, string toRootPath, string to, bool overwriteIfExists)
        {
            //Perhaps it would be better to first copy the file to a temporary file and when it is
            //copied, delete the target file, if it exists, and then rename the temporary file to
            //the target filename!!
            ZlpIOHelper.CopyFileExact(fromRootPath + from, toRootPath + to, overwriteIfExists);
        }

        public void DeleteFile(string rootPath, string relativePath)
        {
            ZlpIOHelper.DeleteFile(rootPath + relativePath);
        }

        public void CreateFolder(string rootPath, string to, DateTime lastWriteDateTime)
        {
            ZlpIOHelper.CreateDirectory(rootPath + to);
            ZlpIOHelper.SetFileLastWriteTime(rootPath + to, lastWriteDateTime);
        }

        public void DeleteFolder(string rootPath, string relativePath)
        {
            if (IsDirectoryEmpty(rootPath, relativePath))
                ZlpIOHelper.DeleteDirectory(rootPath + relativePath, false);
        }

        public bool IsDirectoryEmpty(string rootPath, string relativePath)
        {
            return ZlpIOHelper.IsDirectoryEmpty(rootPath + relativePath);
        }

        public void Completed(CancellationToken cancellationToken = default)
        {

        }
    }
}
