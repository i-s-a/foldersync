﻿using System;
using System.Collections.Generic;

namespace SyncApi
{
    sealed public class FolderTreeNode : IFolderChildNode, IEquatable<FolderTreeNode>//, IComparable<FolderNode>
    {
        public FolderTreeNode()
        {
            NodeLocation = Location.Both;
            InBothButChildrenDiffer = false;
        }

        public FolderTreeNode Parent { get; set; }
        public string Name { get; set; }
        public string RelativePath { get; set; }

        public SortedSet<IFolderChildNode> Children { get; set; }

        public Location NodeLocation { get; set; }
        public DateTime LastWriteTimeLeft { get; set; }
        public DateTime LastWriteTimeRight { get; set; }

        ///////////////////////////////////////////////////////
        public bool InBothButChildrenDiffer { get; set; }
        ///////////////////////////////////////////////////////

        public bool Equals(FolderTreeNode other)
        {
            if (object.ReferenceEquals(other, null))
                return false;

            if (object.ReferenceEquals(other, this))
                return true;

            return (RelativePath.Equals(other.RelativePath));
        }

        public override int GetHashCode()
        {
            return (RelativePath.GetHashCode());
        }

        public int CompareTo(IFolderChildNode other)
        {
            //return System.String.Compare(RelativePath, other.RelativePath, System.StringComparison.Ordinal);
            return System.String.Compare(Name, other.Name, System.StringComparison.Ordinal);
        }
    }

    public class FolderNodePathComparer<T> : IEqualityComparer<FolderTreeNode>
    {
        public bool Equals(FolderTreeNode lhs, FolderTreeNode rhs)
        {
            if (object.ReferenceEquals(lhs, rhs))
                return true;

            return (lhs != null && rhs != null && lhs.RelativePath.Equals(rhs.RelativePath));
        }

        public int GetHashCode(FolderTreeNode o)
        {
            return o.RelativePath.GetHashCode();
        }
    }

}
