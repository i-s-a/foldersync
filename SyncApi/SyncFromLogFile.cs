﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using ZetaLongPaths;

namespace SyncApi
{
    public class SyncFromLogFile
    {
        [Flags]
        private enum LogFlag
        {
            FileCopy = 1 << 0,
            FileDelete = 1 << 1,
            FolderCreate = 1 << 2,
            FolderDelete = 1 << 3,
            FileMoveToTemp = 1 << 4,
        }

        private SyncFromLogFile()
        {
        }

        public static bool CheckIfLogExists(string logSyncPosFilePath)
        {
            return ZlpIOHelper.FileExists(logSyncPosFilePath);
        }

        public static void SyncLogFile(string logSyncPosFilePath, string logFilepath, bool retainLogFile, CancellationToken cancellationToken = default)
        {
            if (CheckIfLogExists(logSyncPosFilePath))
                throw new SyncException("Sync still in progress. You need to continue or rollback the current sync first");

            FileStream counterStream = null;

            try
            {
                ZlpFileInfo counterFile = new ZlpFileInfo(logSyncPosFilePath);
                counterStream = counterFile.OpenCreate();

                Sync(0, counterStream, logFilepath, cancellationToken);

                counterStream.Close();

                //If we haven't been asked to retain the log file then delete them
                if (!retainLogFile)
                {
                    ZlpIOHelper.DeleteFile(logSyncPosFilePath);
                    ZlpIOHelper.DeleteFile(logFilepath);  //Comment this line out if we want to keep the log file during testing
                }
            }
            catch (SyncException /*ex*/)
            {
                //Need to output to a log
                throw;
            }
            catch (Exception /*ex*/)    //Expand the Exception catches to be more specific to the type of exceptions that could be thrown
            {
                //Need to output to a log
                throw;
            }
            finally
            {
                if (counterStream != null)
                    counterStream.Close();
            }
        }

        public static Task ContinueSync(string logSyncPosFilePath, string logFilePath, bool retainLogFile, CancellationToken cancellationToken = default)
        {
            Task task = Task.Run(() =>
            {
                if (!CheckIfLogExists(logSyncPosFilePath))
                    throw new SyncException("There is no sync in progress");

                FileStream counterStream = null;
                long nextPos = 0;

                try
                {
                    ZlpFileInfo counterFile = new ZlpFileInfo(logSyncPosFilePath);
                    counterStream = counterFile.OpenWrite();

                    counterStream.Seek(0, SeekOrigin.Begin);
                    long readVal = 0;
                    byte b = (byte)counterStream.ReadByte();
                    readVal |= ((long)b);
                    b = (byte)counterStream.ReadByte();
                    readVal |= ((long)b << 8);
                    b = (byte)counterStream.ReadByte();
                    readVal |= ((long)b << 16);
                    b = (byte)counterStream.ReadByte();
                    readVal |= ((long)b << 24);
                    b = (byte)counterStream.ReadByte();
                    readVal |= ((long)b << 32);
                    b = (byte)counterStream.ReadByte();
                    readVal |= ((long)b << 40);
                    b = (byte)counterStream.ReadByte();
                    readVal |= ((long)b << 48);
                    b = (byte)counterStream.ReadByte();
                    readVal |= ((long)b << 56);

                    nextPos = readVal;
                    
                    Sync(nextPos, counterStream, logFilePath, cancellationToken);

                    counterStream.Close();

                    //If we haven't been asked to retain the log file then delete them
                    if (!retainLogFile)
                    {
                        ZlpIOHelper.DeleteFile(logSyncPosFilePath);
                        ZlpIOHelper.DeleteFile(logFilePath);  //Comment this line out if we want to keep the log file during testing
                    }
                }
                catch (SyncException /*ex*/)
                {
                    //Need to output to a log
                    throw;
                }
                catch (Exception /*ex*/)    //Expand the Exception catches to be more specific to the type of exceptions that could be thrown
                {
                    //Need to output to a log
                    throw;
                }
                finally
                {
                    if (counterStream != null)
                        counterStream.Close();
                }
            });

            return task;
        }

        public static Task RollbackSync(string logSyncPosFilePath, string logFilePath, CancellationToken cancellationToken = default)
        {
            Task task = Task.Run(() =>
            {
                if (!CheckIfLogExists(logSyncPosFilePath))
                    throw new SyncException("There is no sync in progress");

                FileStream counterStream = null;
                long nextPos = 0;

                try
                {
                    ZlpFileInfo counterFile = new ZlpFileInfo(logSyncPosFilePath);
                    counterStream = counterFile.OpenWrite();

                    counterStream.Seek(0, SeekOrigin.Begin);
                    long readVal = 0;
                    byte b = (byte)counterStream.ReadByte();
                    readVal |= ((long)b);
                    b = (byte)counterStream.ReadByte();
                    readVal |= ((long)b << 8);
                    b = (byte)counterStream.ReadByte();
                    readVal |= ((long)b << 16);
                    b = (byte)counterStream.ReadByte();
                    readVal |= ((long)b << 24);
                    b = (byte)counterStream.ReadByte();
                    readVal |= ((long)b << 32);
                    b = (byte)counterStream.ReadByte();
                    readVal |= ((long)b << 40);
                    b = (byte)counterStream.ReadByte();
                    readVal |= ((long)b << 48);
                    b = (byte)counterStream.ReadByte();
                    readVal |= ((long)b << 56);

                    nextPos = readVal;

                    ++nextPos;

                    Rollback(nextPos, counterStream, logFilePath, cancellationToken);

                    counterStream.Close();

                    //Unlike syncing or continuing a sync there is no option to retain
                    //the log file after a rollback
                    ZlpIOHelper.DeleteFile(logSyncPosFilePath);
                    ZlpIOHelper.DeleteFile(logFilePath);  //Comment this line out if we want to keep the log file during testing
                }
                catch (SyncException /*ex*/)
                {
                    //Need to output to a log
                    throw;
                }
                catch (Exception /*ex*/)    //Expand the Exception catches to be more specific to the type of exceptions that could be thrown
                {
                    //Need to output to a log
                    throw;
                }
                finally
                {
                    if (counterStream != null)
                        counterStream.Close();
                }
            });

            return task;
        }

        private static void Sync(long firstPos, FileStream counterStream, string logFilepath, CancellationToken cancellationToken = default)
        {
            string[] logInstructions = ZlpIOHelper.ReadAllLines(logFilepath);

            //We need the temp path as we won't be deleting files and folders straight away. Something could go
            //wrong before completing the sync and so leave things in an inconsistent state. If this occurs
            //we need to allow the caller to choose whether to rollback the sync or continue with the sync. To
            //be able to support rollback we can't actually delete files and folders straight away, we need to be
            //able to restore these deleted files and folders. Instead of deleting the files and folders we will
            //instead move them to a temporary location. If something goes wrong and the caller decides to rollback
            //then we can restore the deleted files and folders from the temporary location. If all the sync
            //instructions are completed successfully then we can simply delete the temporary location of these
            //files and folders. 
            string tmpPath = Path.GetTempPath();

            Queue<string> filesToDeleteOnSuccess = new Queue<string>();

            for (long i = firstPos; i < logInstructions.Length; ++i)
            {
                cancellationToken.ThrowIfCancellationRequested();

                //By writing out the position here it means the value represents the instruction
                //that is currently being processed
                counterStream.Seek(0, SeekOrigin.Begin);
                byte b = (byte)(i & 0xff);
                counterStream.WriteByte(b);
                b = (byte)((i >> 8) & 0xff);
                counterStream.WriteByte(b);
                b = (byte)((i >> 16) & 0xff);
                counterStream.WriteByte(b);
                b = (byte)((i >> 24) & 0xff);
                counterStream.WriteByte(b);
                b = (byte)((i >> 32) & 0xff);
                counterStream.WriteByte(b);
                b = (byte)((i >> 40) & 0xff);
                counterStream.WriteByte(b);
                b = (byte)((i >> 48) & 0xff);
                counterStream.WriteByte(b);
                b = (byte)((i >> 56) & 0xff);
                counterStream.WriteByte(b);

                string[] lineParts = logInstructions[i].Split('|');

                if (lineParts[0] == ((byte)LogFlag.FileMoveToTemp).ToString())
                {
                    string rootPath = lineParts[1];     //Root path to the file that needs moving to the temp folder
                    string relFilePath = lineParts[2];  //Relative path, from the root, to file that needs moving to the temp folder
                    string relPath = "";                //Relative path to the file that needs moving to the temp folder without the filename

                    int index = relFilePath.LastIndexOf("\\");

                    if (index != -1)
                    {
                        relPath = relFilePath.Substring(0, index);

                        if (!ZlpIOHelper.DirectoryExists(tmpPath + relPath))
                        {
                            ZlpIOHelper.CreateDirectory(tmpPath + relPath);
                        }
                    }

                    //throw new Exception("DEBUG");

                    if (ZlpIOHelper.FileExists(rootPath + relFilePath))
                    {
                        if (ZlpIOHelper.FileExists(tmpPath + relFilePath))
                            ZlpIOHelper.DeleteFile(tmpPath + relFilePath);

                        ZlpIOHelper.MoveFile(rootPath + relFilePath, tmpPath + relFilePath);
                        filesToDeleteOnSuccess.Enqueue(tmpPath + relFilePath);
                    }
                    //I guess an 'else' here should be included to check that, as the
                    //file doesn't exist, it should exist in tmpPath + relFilePath and
                    //if not then something has gone wrong with the log file and syncing

                }
                else if (lineParts[0] == ((byte)LogFlag.FileCopy).ToString())
                {
                    //Can't just copy over the file in the destination because we may need to restore
                    //this file if the caller decides to rollback the sync. Therefore if the file already
                    //exists in the destination we need to first move it to the temporary location and
                    //then copy the file from the source to the destination. If a rollback is requested we
                    //can just delete the file that is in the destination and then move the file from the
                    //temporary location back to the destination. If the file didn't exist in the destination
                    //originally then no file will exist in the temporary location and we will just delete
                    //the file in the destination (or would it be better if the log file explicitly indicated
                    //whether the copy involves overwriting a file already in the destination or whether
                    //copying a file that doesn't exist in the destination? e.g. LogFlag.FileCopy and
                    //LogFlag.FileCopyOverwrite. THINK ABOUT IT!!)

                    //lineParts[1] - root source
                    //lineParts[2] - source relative
                    //lineParts[3] - root target
                    //lineParts[4] - target relative
                    //lineParts[5] - should file attributes be copied
                    ////lineParts[6] - if the file already exists should it be overwritten
                    string sourceFile = lineParts[1] + lineParts[2];
                    string targetFile = lineParts[3] + lineParts[4];
                    bool copyFileAttributes = Convert.ToBoolean(lineParts[5]);
                    //bool overwriteIfExists = Convert.ToBoolean(lineParts[6]);

                    ////First copy the file to a temporary file
                    //if (copyFileAttributes)
                    //    ZlpIOHelper.CopyFileExact(sourceFile, targetFile + ".tmp", true);
                    //else
                    //    ZlpIOHelper.CopyFile(sourceFile, targetFile + ".tmp", true);

                    ////If the copy was successful then rename the temporary file to
                    ////the actual target file we would like.
                    //File.Move(targetFile + ".tmp", targetFile);

                    if (copyFileAttributes)
                        ZlpIOHelper.CopyFileExact(sourceFile, targetFile, true);
                    else
                        ZlpIOHelper.CopyFile(sourceFile, targetFile, true);
                }
                else if (lineParts[0] == ((byte)LogFlag.FileDelete).ToString())
                {
                    //Deleting a file is the same as moving a file to the temp folder. Really need to
                    //create a shared method to use with LogFlag.FileMoveToTemp as they are identical
                    //but we need two separate flags so that we can differentiate between moving the
                    //file because of a 'delete' and moving a file because of a 'copy'. We need this
                    //differentiation because if the user chooses to rollback then with a 'delete' we
                    //know the file should no longer exist in the target but with a 'copy' we know a
                    //copy of the file should exist in the target, and we can check these conditions
                    //when rolling back to provide additional consistency and warn the user if the
                    //conditions are not met

                    string rootPath = lineParts[1];     //Root path to the file that needs moving to the temp folder
                    string relFilePath = lineParts[2];  //Relative path, from the root, to file that needs moving to the temp folder
                    string relPath = "";                //Relative path to the file that needs moving to the temp folder without the filename

                    int index = relFilePath.LastIndexOf("\\");

                    if (index != -1)
                    {
                        relPath = relFilePath.Substring(0, index);

                        if (!ZlpIOHelper.DirectoryExists(tmpPath + relPath))
                        {
                            ZlpIOHelper.CreateDirectory(tmpPath + relPath);
                        }
                    }

                    //throw new Exception("DEBUG");

                    if (ZlpIOHelper.FileExists(rootPath + relFilePath))
                    {
                        if (ZlpIOHelper.FileExists(tmpPath + relFilePath))
                            ZlpIOHelper.DeleteFile(tmpPath + relFilePath);

                        ZlpIOHelper.MoveFile(rootPath + relFilePath, tmpPath + relFilePath);
                        filesToDeleteOnSuccess.Enqueue(tmpPath + relFilePath);
                    }
                    //I guess an 'else' here should be included to check that, as the
                    //file doesn't exist, it should exist in tmpPath + relFilePath and
                    //if not then something has gone wrong with the log file and syncing
                }
                else if (lineParts[0] == ((byte)LogFlag.FolderCreate).ToString())
                {
                    string folderToCreate = lineParts[1] + lineParts[2];
                    DateTime lastWriteTime = DateTime.Parse(lineParts[3]);

                    if (!ZlpIOHelper.DirectoryExists(folderToCreate))
                        ZlpIOHelper.CreateDirectory(folderToCreate);

                    //throw new Exception("DEBUG");

                    ZlpIOHelper.SetFileLastWriteTime(folderToCreate, lastWriteTime);
                }
                else if (lineParts[0] == ((byte)LogFlag.FolderDelete).ToString())
                {
                    //Move the folder to a temporary location (actually do we need
                    //to move this, or if we need to restore can we just create the
                    //folder in the correct location again. Any deleted files that
                    //need to be restored to the folder will be moved there from the
                    //temporary location after this folder has been created again, as
                    //long as the instructions are processed in the correct order.
                    //THINK ABOUT IT!! WHAT ABOUT DIRECTORY ATTRIBUTES SUCH AS LASTWRITETIME??
                    string folderToDelete = lineParts[1] + lineParts[2];
                    //if (Directory.Exists(folderToDelete))   //If we try deleting a directory that doesn't exist will it do nothing or throw an exception? (CHECK THIS!!)
                    //    Directory.Delete(folderToDelete);

                    if (ZlpIOHelper.DirectoryExists(folderToDelete) && ZlpIOHelper.IsDirectoryEmpty(folderToDelete))   //If we try deleting a directory that doesn't exist will it do nothing or throw an exception? (CHECK THIS!!)
                        ZlpIOHelper.DeleteDirectory(folderToDelete, false);

                    //throw new Exception("DEBUG");
                }
                else
                {
                    throw new Exception("Invalid instruction");
                }

                //if (i == 151)
                //    throw new Exception("DEBUG");

                //By writing out the position here it means the value represents the last successful
                //instruction completed
                //counterStream.Seek(0, SeekOrigin.Begin);
                //byte b = (byte)(i & 0xff);
                //counterStream.WriteByte(b);
                //b = (byte)((i >> 8) & 0xff);
                //counterStream.WriteByte(b);
                //b = (byte)((i >> 16) & 0xff);
                //counterStream.WriteByte(b);
                //b = (byte)((i >> 24) & 0xff);
                //counterStream.WriteByte(b);
                //b = (byte)((i >> 32) & 0xff);
                //counterStream.WriteByte(b);
                //b = (byte)((i >> 40) & 0xff);
                //counterStream.WriteByte(b);
                //b = (byte)((i >> 48) & 0xff);
                //counterStream.WriteByte(b);
                //b = (byte)((i >> 56) & 0xff);
                //counterStream.WriteByte(b);
            }

            //If we get here then the sync succeeded so clean up the deleted files that were moved to the temporary location
            foreach (var file in filesToDeleteOnSuccess)
            {
                ZlpIOHelper.DeleteFile(file);
            }            
        }

        public static void Rollback(long firstPos, FileStream counterStream, string logFilepath, CancellationToken cancellationToken)
        {
            string[] logInstructions = ZlpIOHelper.ReadAllLines(logFilepath);

            //The temp folder which should contain the files that need to be restored
            string tmpPath = Path.GetTempPath();


            for (long i = firstPos; i >= 0; --i)
            {
                cancellationToken.ThrowIfCancellationRequested();

                //By writing out the position here it means the value represents the instruction
                //that is currently being processed
                counterStream.Seek(0, SeekOrigin.Begin);
                byte b = (byte)(i & 0xff);
                counterStream.WriteByte(b);
                b = (byte)((i >> 8) & 0xff);
                counterStream.WriteByte(b);
                b = (byte)((i >> 16) & 0xff);
                counterStream.WriteByte(b);
                b = (byte)((i >> 24) & 0xff);
                counterStream.WriteByte(b);
                b = (byte)((i >> 32) & 0xff);
                counterStream.WriteByte(b);
                b = (byte)((i >> 40) & 0xff);
                counterStream.WriteByte(b);
                b = (byte)((i >> 48) & 0xff);
                counterStream.WriteByte(b);
                b = (byte)((i >> 56) & 0xff);
                counterStream.WriteByte(b);

                string[] lineParts = logInstructions[i].Split('|');

                if (lineParts[0] == ((byte)LogFlag.FileMoveToTemp).ToString())
                {
                    //We need to move the file back to it's original location

                    string rootPath = lineParts[1];     //Root path of where the file in the temp folder needs to be moved to
                    string relFilePath = lineParts[2];  //Relative path, from the root, of where the file in the temp folder needs to be moved to
                    string relPath = "";                //Relative path of where the file in the temp folder needs to be moved to, without the filename

                    //First check the file is actually in the temp folder because if not then we
                    //can't rollback. Do we want to throw an exception or skip this one and continue
                    //rolling back what we can, and then list to the user the files that couldn't be
                    //rolled back/restored?? Or, perhaps, give the user an option whether to continue
                    //with the rollback or abort??
                    //if (!ZlpIOHelper.FileExists(tmpPath + relFilePath))
                    //    throw new SyncException("Backup file not found - Error rolling back");

                    if (ZlpIOHelper.FileExists(tmpPath + relFilePath))
                    {
                        int index = relFilePath.LastIndexOf("\\");

                        if (index != -1)
                        {
                            relPath = relFilePath.Substring(0, index);

                            if (!ZlpIOHelper.DirectoryExists(rootPath + relPath))
                            {
                                ZlpIOHelper.CreateDirectory(rootPath + relPath);
                            }
                        }

                        if (ZlpIOHelper.FileExists(rootPath + relFilePath))
                            ZlpIOHelper.DeleteFile(rootPath + relFilePath);

                        ZlpIOHelper.MoveFile(tmpPath + relFilePath, rootPath + relFilePath);
                        //filesToDeleteOnSuccess.Enqueue(tmpPath + relFilePath);
                    }
                }
                else if (lineParts[0] == ((byte)LogFlag.FileCopy).ToString())
                {
                    //We simply want to delete the file that is in the target location.
                    //Perhaps it would be better to move it to the recycle bin, that way
                    //there is would still a final opportunity for the user to recover
                    //the file if they need to?? THINK ABOUT IT

                    //lineParts[1] - root source
                    //lineParts[2] - source relative
                    //lineParts[3] - root target
                    //lineParts[4] - target relative
                    //string sourceFile = lineParts[1] + lineParts[2];
                    string targetFile = lineParts[3] + lineParts[4];
                    //DateTime lastWriteTime = DateTime.Parse(lineParts[5]);

                    //See comment above with LogFlag.FileMoveToTemp about whether an excepton
                    //here should be thrown or not
                    //if (!ZlpIOHelper.FileExists(targetFile))
                    //    throw new SyncException("File not found - Error rolling back");

                    if (ZlpIOHelper.FileExists(targetFile))
                        ZlpIOHelper.DeleteFile(targetFile);
                    //ZlpIOHelper.MoveFileToRecycleBin(targetFile);
                }
                else if (lineParts[0] == ((byte)LogFlag.FileDelete).ToString())
                {
                    string rootPath = lineParts[1];     //Root path to the file that needs moving to the temp folder
                    string relFilePath = lineParts[2];  //Relative path, from the root, to file that needs moving to the temp folder
                    string relPath = "";                //Relative path to the file that needs moving to the temp folder without the filename

                    //if (!ZlpIOHelper.FileExists(tmpPath + relFilePath))
                    //    throw new SyncException("Backup file not found - Error rolling back");

                    if (ZlpIOHelper.FileExists(tmpPath + relFilePath))
                    {
                        int index = relFilePath.LastIndexOf("\\");

                        if (index != -1)
                        {
                            relPath = relFilePath.Substring(0, index);

                            if (!ZlpIOHelper.DirectoryExists(rootPath + relPath))
                            {
                                ZlpIOHelper.CreateDirectory(rootPath + relPath);
                            }
                        }

                        if (ZlpIOHelper.FileExists(rootPath + relFilePath))
                            ZlpIOHelper.DeleteFile(rootPath + relFilePath);

                        ZlpIOHelper.MoveFile(tmpPath + relFilePath, rootPath + relFilePath);
                        //filesToDeleteOnSuccess.Enqueue(tmpPath + relFilePath);
                    }
                }
                else if (lineParts[0] == ((byte)LogFlag.FolderCreate).ToString())
                {
                    //We simply want to delete the folder that was created

                    string folderToCreate = lineParts[1] + lineParts[2];
                    //DateTime lastWriteTime = DateTime.Parse(lineParts[3]);

                    //if (!ZlpIOHelper.DirectoryExists(folderToCreate))
                    //    throw new SyncException("Folder not found - Error rolling back");

                    if (ZlpIOHelper.DirectoryExists(folderToCreate))
                    {
                        //Shouldn't need to delete recursively as each folder level should have
                        //it's own instruction, and going backwards through the instructions we
                        //should see the deepest level first and then the next level up, and so
                        //on
                        ZlpIOHelper.DeleteDirectory(folderToCreate, false);
                    }
                }
                else if (lineParts[0] == ((byte)LogFlag.FolderDelete).ToString())
                {
                    //We want to create the folder again in the target location. We don't have the
                    //attributes (e.g. last access time, last write time, etc.) of the folder stored
                    //in the instruction so those attributes will be incorrect at the moment, but
                    //really need to store this information with the instruction so that we can set
                    //them when we create the folder
                    string folderToDelete = lineParts[1] + lineParts[2];

                    //if (ZlpIOHelper.DirectoryExists(folderToDelete))   //If we try deleting a directory that doesn't exist will it do nothing or throw an exception? (CHECK THIS!!)
                    //    throw new SyncException("Folder already exists - Error rolling back");

                    if (!ZlpIOHelper.DirectoryExists(folderToDelete))
                        ZlpIOHelper.CreateDirectory(folderToDelete);
                }
                else
                {
                    throw new SyncException("Invalid instruction");
                }
            }
        }
    }
}
