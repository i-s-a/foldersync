﻿using System;
using System.IO;
using System.Xml.Serialization;
using ZetaLongPaths;

namespace SyncApi
{
    [XmlRoot("CompareOptions")]
    sealed public class CompareOptions
    {
        private const int DEFAULT_TOLERANCE_SECONDS = 20;

        public CompareOptions()
        {
            ToleranceSeconds = DEFAULT_TOLERANCE_SECONDS;   //By default allow 20 seconds tolarence
            CompareLastModifiedTime = true;                 //By default compare last modified time
            CompareFileSize = true;                         //By default compare file sizes
            CompareContent = false;                         //By default don't compare content
            IncludeSubFolders = true;                       //By default compare sub folders recursively
            ExtensionFilters = "*.*";                       //By default compare all files
        }

        public CompareOptions Clone()
        {
            return new CompareOptions()
            {
                ToleranceSeconds = this.ToleranceSeconds,
                CompareLastModifiedTime = this.CompareLastModifiedTime,
                CompareFileSize = this.CompareFileSize,
                CompareContent = this.CompareContent,
                IncludeSubFolders = this.IncludeSubFolders,
                ExtensionFilters = this.ExtensionFilters
            };
        }

        public int ToleranceSeconds
        {
            get;
            set;
        }

        public bool CompareLastModifiedTime
        {
            get;
            set;
        }

        //Only applies to files
        public bool CompareFileSize
        {
            get;
            set;
        }

        //Only applies to files
        public bool CompareContent
        {
            get;
            set;
        }

        //When comparing folders, should the comparison be performed recursively
        //on the sub folders as well
        public bool IncludeSubFolders
        {
            get;
            set;
        }

        //Semi-colon separated list of extension filters to be applied when
        //comparing, so only those files that match the filters get included
        public string ExtensionFilters
        {
            get;
            set;
        }

        public static bool FilesEqual(FileTreeNode fileNode, string leftRootPath,
            string rightRootPath, CompareOptions compareOptions)
        {
            bool lastModifiedTimeEqual = true;
            bool fileSizeEqual = true;
            bool fileContentEqual = true;

            if (compareOptions.CompareLastModifiedTime)
            {
                TimeSpan diff = fileNode.LastWriteTimeLeft - fileNode.LastWriteTimeRight;
                lastModifiedTimeEqual = (Math.Abs(diff.TotalSeconds) <= compareOptions.ToleranceSeconds);

                if (!lastModifiedTimeEqual)
                    fileNode.DiffInfo.LastModifiedTimeDifferent = true;
            }

            if (compareOptions.CompareFileSize)
            {
                fileSizeEqual = (fileNode.FileSizeLeft == fileNode.FileSizeRight);

                if (!fileSizeEqual)
                    fileNode.DiffInfo.FileSizeDifferent = true;
            }

            if (compareOptions.CompareContent)
            {
                fileContentEqual = FileHelper.ContentEqual(leftRootPath + @"\" + fileNode.RelativePath,
                    rightRootPath + @"\" + fileNode.RelativePath);

                if (!fileContentEqual)
                    fileNode.DiffInfo.ContentDifferent = true;
            }

            return (lastModifiedTimeEqual && fileSizeEqual && fileContentEqual);
        }

        public static void Serialize(string filename, CompareOptions compareOptions)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CompareOptions));
            TextWriter writer = new StreamWriter(filename);
            serializer.Serialize(writer, compareOptions);
            writer.Close();
        }

        public static CompareOptions Deserialize(string filename)
        {
            CompareOptions compareOptions = null;

            if (File.Exists(filename))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(CompareOptions));
                FileStream stream = new FileStream(filename, FileMode.Open);
                compareOptions = (CompareOptions)serializer.Deserialize(stream);
                stream.Close();
            }

            return compareOptions;
        }
    }
}
