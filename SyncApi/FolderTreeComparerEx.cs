﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using ZetaLongPaths;

namespace SyncApi
{
    //This class populates the LastWriteTimeLeft, LastWriteTimeRight, FileSizeLeft,
    //FileSizeRight and InBothButChildrenDiffer members of the FileTreeNode and FolderTreeNode
    //classes. These members can be useful when displaying a visual tree of the results (see
    //comment above FolderTreeComparer class). This also populates mFolderItemsDict with the
    //tree nodes so that if the result is displayed visually and the user would like to sync
    //a sub-tree from the hierarchy they can get at the relevant tree node quickly
    public class FolderTreeComparerEx : IFolderTreeComparerEx
    {
        //Full path of folder 1 and folder 2. These are the full paths of the folders
        //selected by the user
        private string mFolder1 = null;
        private string mFolder2 = null;

        //Root folder name for folder 1 and folder 2 extracted from the mFolder1 and
        //mFolder2 paths
        private string mRootFolderName1 = null;
        private string mRootFolderName2 = null;

        //Path to the root folder name of folder 1 and folder 2 extracted from the
        //mFolder1 and mFolder2 paths.  mPathToRootX + mRootFolderNameX == mFolderX
        private string mPathToRoot1 = null;
        private string mPathToRoot2 = null;

        private string mCommonRootName = null;

        //For comparing paths stored in the FileNode and FolderNode
        private FolderNodePathComparer<FolderTreeNode> mFolderNodePathComparer = new FolderNodePathComparer<FolderTreeNode>();
        private FileNodePathComparer<FileTreeNode> mFileNodePathComparer = new FileNodePathComparer<FileTreeNode>();

        //The root parent of the merged tree. After the comparison has completed this
        //will be the root node of the resulting tree that merges both trees in to one.
        //Walking the tree from this node will identify each file and folder and specify
        //whether each one is unique to folder 1, unique to folder 2 or present in both
        //folders
        private FolderTreeNode mRootMergedFolderNode = null;

        private IDictionary<string, IFolderChildNode> mFolderItemsDict = new Dictionary<string, IFolderChildNode>();

        //Callback to call once the comparison has been completed
        public delegate void CompareCompletedCallback(FolderTreeNode comparedRootNode);
        private CompareCompletedCallback mCompareCompletedCallback = null;

        private CompareOptions mCompareOptions = null;

        private string mKeyPrefix = null;

        private string[] mExtensionFilters = null;
        //private readonly object mFileIteratorMutex = new object();

        public FolderTreeComparerEx(string folder1, string folder2, CompareOptions compareOptions, CompareCompletedCallback compareCompletedCallback)
        {
            //If we haven't been provided with a CompareOptions instance we will create
            //a default one to use
            mCompareOptions = compareOptions;

            if (mCompareOptions == null)
                mCompareOptions = new CompareOptions();

            //If either folder doesn't exist then can't really do the compare
            if (!ZlpIOHelper.DirectoryExists(folder1))
            {
                throw new CompareException("Invalid Folder 1: " + folder1);
            }

            if (!ZlpIOHelper.DirectoryExists(folder2))
            {
                throw new CompareException("Invalid Folder 2: " + folder2);
            }

            mFolder1 = folder1;
            if (mFolder1.EndsWith(@"\"))
                mFolder1 = mFolder1.Remove(mFolder1.Length - 1);

            mFolder2 = folder2;
            if (mFolder2.EndsWith(@"\"))
                mFolder2 = mFolder2.Remove(mFolder2.Length - 1);

            //Extract from the full path the root folder name and the path to the root folder name
            (mPathToRoot1, mRootFolderName1) = GetPathToRootAndRootFolderName(mFolder1);
            (mPathToRoot2, mRootFolderName2) = GetPathToRootAndRootFolderName(mFolder2);

            if (mRootFolderName1 == mRootFolderName2)
            {
                mCommonRootName = mRootFolderName1;
                mKeyPrefix = "";
            }
            else
            {
                mCommonRootName = "..";
                mKeyPrefix = mCommonRootName + @"\";
            }

            //The callback to call when the comparison is completed
            mCompareCompletedCallback = compareCompletedCallback;

            if (string.IsNullOrEmpty(mCompareOptions.ExtensionFilters))
            {
                mCompareOptions.ExtensionFilters = "*.*";
            }

            mExtensionFilters = mCompareOptions.ExtensionFilters.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < mExtensionFilters.Length; ++i)
            {
                mExtensionFilters[i] = mExtensionFilters[i].TrimStart(' ');
                mExtensionFilters[i] = mExtensionFilters[i].TrimEnd(' ');
            }
        }

        #region IFolderComparer methods
        public Task<FolderTreeNode> Compare(CancellationToken cancellationToken = default)
        {
            Task<FolderTreeNode> task = Task.Run(() =>
            {
                string rootRelativePath = null;
                if (mCommonRootName == mRootFolderName1)
                    rootRelativePath = mCommonRootName;
                else
                    rootRelativePath = "";

                //Create a root node that will be a common parent for both the left side and the right side
                mRootMergedFolderNode = new FolderTreeNode()
                {
                    Name = mCommonRootName,
                    //RelativePath = "",
                    RelativePath = rootRelativePath,
                    Parent = null,
                    Children = new SortedSet<IFolderChildNode>(),
                    NodeLocation = Location.Both
                };

                //mFolderItemsDict.Add(rootRelativePath, mRootMergedFolderNode);
                mFolderItemsDict.Add(mCommonRootName, mRootMergedFolderNode);

                bool childrenDiffer = false;

                if (mRootFolderName1 != mRootFolderName2)
                {
                    BasePath1 = mFolder1 + @"\";
                    BasePath2 = mFolder2 + @"\";
                    //If the root folder names are different then we want to use the full path when doing the
                    //comparison. E.g.
                    //Folder 1 - C:\My Folder\Downloads
                    //Folder 2 - C:\My Folder\My Downloads
                    //Then we want to use both full paths as Downloads and My Downloads are different
                    childrenDiffer = ProcessSubFolders(mRootMergedFolderNode, mFolder1, mFolder2, mFolder1, mFolder2, cancellationToken);
                }
                else
                {
                    BasePath1 = mPathToRoot1 + @"\";
                    BasePath2 = mPathToRoot2 + @"\";
                    //If the root folder names are the same then we want to use the path to the root when doing
                    //the comparison. E.g.
                    //Folder 1 - C:\My Folder\Downloads
                    //Folder 2 - C:\My Folder\Downloads
                    //Then we want to use the path to the root because they both have a root name of Downloads.
                    childrenDiffer = ProcessSubFolders(mRootMergedFolderNode, mFolder1, mFolder2, mPathToRoot1, mPathToRoot2, cancellationToken);
                }

                ///////////////////////////////////////////////////////
                if (childrenDiffer && mRootMergedFolderNode.NodeLocation == Location.Both)
                    mRootMergedFolderNode.InBothButChildrenDiffer = true;
                ///////////////////////////////////////////////////////

                //We have completed the comparison, now call the callback delegate with the root node of the merged tree
                if (mCompareCompletedCallback != null)
                    mCompareCompletedCallback(mRootMergedFolderNode);

                return mRootMergedFolderNode;
            });

            return task;
        }

        public FolderTreeNode Result
        {
            get { return mRootMergedFolderNode; }
        }

        public string BasePath1
        {
            get;
            set;
        }

        public string BasePath2
        {
            get;
            set;
        }

        public string CommonRootName
        {
            get { return mCommonRootName; }
            set { mCommonRootName = value; }
        }

        public IFolderChildNode GetNode(string path)
        {
            //FolderNode curNode = mRootMergedFolderNode;
            //IFolderChildItem node = null;
            //string[] hierarchy = path.Split(new char[1] { '\\' });
            //for (int i = 0; i < hierarchy.Length; ++i)
            //{
            //    node = curNode.Children.SingleOrDefault(x => x.Name == hierarchy[i]);
            //    if (node == null)
            //        throw new Exception("Error getting node");

            //    if (node is FolderNode)
            //        curNode = node as FolderNode;
            //    else
            //    {
            //        if (i != hierarchy.Length - 1)
            //            throw new Exception("Invalid path");
            //    }
            //}

            //return node;

            //The relative path is used as a key to get easy access to the nodes. This
            //is much quicker than having to walk the hierarchy, like the code above,
            //each time
            if (!mFolderItemsDict.ContainsKey(path))
                throw new Exception("Error getting node");

            return mFolderItemsDict[path];
        }
        #endregion

        private (string pathToRoot, string rootFolderName) GetPathToRootAndRootFolderName(string folder)
        {
            if (folder.EndsWith(@"\"))
                folder = folder.Remove(folder.Length - 1);

            string pathToRoot = "";
            string rootFolderName = folder;

            int index = folder.LastIndexOf('\\');
            if (index != -1)
            {
                pathToRoot = folder.Substring(0, index);
                rootFolderName = folder.Substring(index + 1);
            }

            return (pathToRoot, rootFolderName);
        }

        private string GetName(string folder)
        {
            string name = folder;

            int index = folder.LastIndexOf('\\');
            if (index != -1)
            {
                name = folder.Substring(index + 1);
            }

            return name;
        }

        private string GetRelativePath(string folder, string rootPath)
        {
            if (folder.StartsWith("\\\\?\\"))
                return folder.Substring(rootPath.Length + 5);
            else
                return folder.Substring(rootPath.Length + 1);
        }

        private ZlpFileInfo[] GetFiles(string path)
        {
            //if (mExtensionFilters.Length > 1)
            //{
            //    List<ZlpFileInfo> fileList = new List<ZlpFileInfo>();

            //    Parallel.ForEach(mExtensionFilters, extFilter =>
            //    {
            //        ZlpFileInfo[] files = ZlpIOHelper.GetFiles(path, extFilter, System.IO.SearchOption.TopDirectoryOnly);

            //        lock (mFileIteratorMutex)
            //        {
            //            fileList.AddRange(files);
            //        }
            //    });

            //    return fileList.ToArray();
            //}
            //else
            //{
            //    ZlpFileInfo[] files = ZlpIOHelper.GetFiles(path, mExtensionFilters[0], System.IO.SearchOption.TopDirectoryOnly);
            //    return files;
            //}

            //It seems the overhead of using the parallel foreach above affects performance quite badly compared to just looping
            //round one after the other like below
            List<ZlpFileInfo> fileList = new List<ZlpFileInfo>();

            foreach (var extFilter in mExtensionFilters)
            {
                ZlpFileInfo[] files = ZlpIOHelper.GetFiles(path, extFilter, System.IO.SearchOption.TopDirectoryOnly);
                fileList.AddRange(files);
            }

            return fileList.ToArray();
        }

        private ZlpDirectoryInfo[] GetDirectories(string path)
        {
            ZlpDirectoryInfo[] folders = ZlpIOHelper.GetDirectories(path, System.IO.SearchOption.TopDirectoryOnly);
            return folders;
        }

        private bool ProcessSubFolders(FolderTreeNode parentMergedNode, string relativePath1, string relativePath2, string rootPath1, string rootPath2, CancellationToken cancellationToken)
        {
            try
            {

                ///////////////////////////////////////////////////////////////////////////////////////////
                //Deal with files
                //ZlpFileInfo[] files1 = ZlpIOHelper.GetFiles(relativePath1, mCompareOptions.ExtensionFilters, System.IO.SearchOption.TopDirectoryOnly);
                ZlpFileInfo[] files1 = GetFiles(relativePath1);
                Dictionary<string, ZlpFileInfo> filesIn1 = new Dictionary<string, ZlpFileInfo>();

                foreach (var file in files1)
                {
                    filesIn1.Add(GetRelativePath(file.FullName, rootPath1), file);
                }


                //ZlpFileInfo[] files2 = ZlpIOHelper.GetFiles(relativePath2, mCompareOptions.ExtensionFilters, System.IO.SearchOption.TopDirectoryOnly);
                ZlpFileInfo[] files2 = GetFiles(relativePath2);
                Dictionary<string, ZlpFileInfo> filesIn2 = new Dictionary<string, ZlpFileInfo>();

                foreach (var file in files2)
                {
                    filesIn2.Add(GetRelativePath(file.FullName, rootPath2), file);
                }


                IEnumerable<string> inFiles1Only = filesIn1.Keys.Except(filesIn2.Keys);
                foreach (var file in inFiles1Only)
                {
                    ZlpFileInfo fileInfo = filesIn1[file];

                    FileTreeNode fileNode = new FileTreeNode()
                    {
                        Name = GetName(file),
                        RelativePath = file,
                        Parent = parentMergedNode,
                        NodeLocation = Location.UniqueLeft,
                        LastWriteTimeLeft = fileInfo.LastWriteTime,
                        FileSizeLeft = fileInfo.Length
                    };

                    parentMergedNode.Children.Add(fileNode);

                    //mFolderItemsDict.Add(file, fileNode);
                    mFolderItemsDict.Add(mKeyPrefix + file, fileNode);
                }

                ///////////////////////////////////////////////////////
                if (parentMergedNode.NodeLocation == Location.Both && inFiles1Only.Count() > 0)
                    parentMergedNode.InBothButChildrenDiffer = true;
                ///////////////////////////////////////////////////////

                IEnumerable<string> inFiles2Only = filesIn2.Keys.Except(filesIn1.Keys);
                foreach (var file in inFiles2Only)
                {
                    ZlpFileInfo fileInfo = filesIn2[file];

                    FileTreeNode fileNode = new FileTreeNode()
                    {
                        Name = GetName(file),
                        RelativePath = file,
                        Parent = parentMergedNode,
                        NodeLocation = Location.UniqueRight,
                        LastWriteTimeRight = fileInfo.LastWriteTime,
                        FileSizeRight = fileInfo.Length
                    };

                    parentMergedNode.Children.Add(fileNode);

                    //mFolderItemsDict.Add(file, fileNode);
                    mFolderItemsDict.Add(mKeyPrefix + file, fileNode);
                }

                ///////////////////////////////////////////////////////
                if (parentMergedNode.NodeLocation == Location.Both && inFiles2Only.Count() > 0)
                    parentMergedNode.InBothButChildrenDiffer = true;
                ///////////////////////////////////////////////////////


                IEnumerable<string> inBoth = filesIn1.Keys.Intersect(filesIn2.Keys);
                foreach (var file in inBoth)
                {
                    ZlpFileInfo fileInfoLeft = filesIn1[file];
                    ZlpFileInfo fileInfoRight = filesIn2[file];

                    FileTreeNode fileNode = new FileTreeNode()
                    {
                        Name = GetName(file),
                        RelativePath = file,
                        Parent = parentMergedNode,
                        NodeLocation = Location.Both,
                        LastWriteTimeLeft = fileInfoLeft.LastWriteTime,
                        FileSizeLeft = fileInfoLeft.Length,
                        LastWriteTimeRight = fileInfoRight.LastWriteTime,
                        FileSizeRight = fileInfoRight.Length
                    };

                    ///////////////////////////////////////////////////////
                    if (parentMergedNode.NodeLocation == Location.Both &&
                        !CompareOptions.FilesEqual(fileNode, rootPath1, rootPath2, mCompareOptions))
                    {
                        parentMergedNode.InBothButChildrenDiffer = true;
                    }
                    ///////////////////////////////////////////////////////

                    parentMergedNode.Children.Add(fileNode);

                    //mFolderItemsDict.Add(file, fileNode);
                    mFolderItemsDict.Add(mKeyPrefix + file, fileNode);
                }

                cancellationToken.ThrowIfCancellationRequested();

                ///////////////////////////////////////////////////////////////////////////////////////////
                //Deal with folders

                //ZlpDirectoryInfo[] folders1 = ZlpIOHelper.GetDirectories(relativePath1, System.IO.SearchOption.TopDirectoryOnly);
                ZlpDirectoryInfo[] folders1 = GetDirectories(relativePath1);
                Dictionary<string, ZlpDirectoryInfo> foldersIn1 = new Dictionary<string, ZlpDirectoryInfo>();

                foreach (var folder in folders1)
                {
                    foldersIn1.Add(GetRelativePath(folder.FullName, rootPath1), folder);
                }


                //ZlpDirectoryInfo[] folders2 = ZlpIOHelper.GetDirectories(relativePath2, System.IO.SearchOption.TopDirectoryOnly);
                ZlpDirectoryInfo[] folders2 = GetDirectories(relativePath2);
                Dictionary<string, ZlpDirectoryInfo> foldersIn2 = new Dictionary<string, ZlpDirectoryInfo>();

                foreach (var folder in folders2)
                {
                    foldersIn2.Add(GetRelativePath(folder.FullName, rootPath2), folder);
                }

                IEnumerable<string> foldersIn1Only = foldersIn1.Keys.Except(foldersIn2.Keys);
                foreach (var folder in foldersIn1Only)
                {
                    cancellationToken.ThrowIfCancellationRequested();

                    ZlpDirectoryInfo folderInfo = foldersIn1[folder];

                    FolderTreeNode folderNode = new FolderTreeNode()
                    {
                        Name = GetName(folder),
                        RelativePath = folder,
                        NodeLocation = Location.UniqueLeft,
                        Parent = parentMergedNode,
                        Children = new SortedSet<IFolderChildNode>(),
                        LastWriteTimeLeft = folderInfo.LastWriteTime
                    };

                    parentMergedNode.Children.Add(folderNode);

                    //mFolderItemsDict.Add(folder, folderNode);
                    mFolderItemsDict.Add(mKeyPrefix + folder, folderNode);

                    WalkFolderTree(folderNode, rootPath1 + @"\" + folder, rootPath1, Location.UniqueLeft);
                }

                ///////////////////////////////////////////////////////
                if (parentMergedNode.NodeLocation == Location.Both && foldersIn1Only.Count() > 0)
                    parentMergedNode.InBothButChildrenDiffer = true;
                ///////////////////////////////////////////////////////

                IEnumerable<string> foldersIn2Only = foldersIn2.Keys.Except(foldersIn1.Keys);
                foreach (var folder in foldersIn2Only)
                {
                    cancellationToken.ThrowIfCancellationRequested();

                    ZlpDirectoryInfo folderInfo = foldersIn2[folder];

                    FolderTreeNode folderNode = new FolderTreeNode()
                    {
                        Name = GetName(folder),
                        RelativePath = folder,
                        NodeLocation = Location.UniqueRight,
                        Parent = parentMergedNode,
                        Children = new SortedSet<IFolderChildNode>(),
                        LastWriteTimeRight = folderInfo.LastWriteTime
                    };

                    parentMergedNode.Children.Add(folderNode);

                    //mFolderItemsDict.Add(folder, folderNode);
                    mFolderItemsDict.Add(mKeyPrefix + folder, folderNode);

                    WalkFolderTree(folderNode, rootPath2 + @"\" + folder, rootPath2, Location.UniqueRight);
                }

                ///////////////////////////////////////////////////////
                if (parentMergedNode.NodeLocation == Location.Both && foldersIn2Only.Count() > 0)
                    parentMergedNode.InBothButChildrenDiffer = true;
                ///////////////////////////////////////////////////////

                IEnumerable<string> foldersInBoth = foldersIn1.Keys.Intersect(foldersIn2.Keys);
                foreach (var folder in foldersInBoth)
                {
                    cancellationToken.ThrowIfCancellationRequested();

                    ZlpDirectoryInfo folderInfoLeft = foldersIn1[folder];
                    ZlpDirectoryInfo folderInfoRight = foldersIn2[folder];

                    FolderTreeNode folderNode = new FolderTreeNode()
                    {
                        Name = GetName(folder),
                        RelativePath = folder,
                        NodeLocation = Location.Both,
                        Parent = parentMergedNode,
                        Children = new SortedSet<IFolderChildNode>(),
                        LastWriteTimeLeft = folderInfoLeft.LastWriteTime,
                        LastWriteTimeRight = folderInfoRight.LastWriteTime
                    };

                    parentMergedNode.Children.Add(folderNode);

                    //mFolderItemsDict.Add(folder, folderNode);
                    mFolderItemsDict.Add(mKeyPrefix + folder, folderNode);

                    //Process sub folders and files...recursive call!!
                    bool childrenDiffer = ProcessSubFolders(folderNode, rootPath1 + @"\" + folder, rootPath2 + @"\" + folder, rootPath1, rootPath2, cancellationToken);
                    ///////////////////////////////////////////////////////
                    if (childrenDiffer && parentMergedNode.NodeLocation == Location.Both)
                        parentMergedNode.InBothButChildrenDiffer = true;
                    ///////////////////////////////////////////////////////
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                //We're not allowed access to the file or folder!!
                //Probably want to log this somewhere in a file and also display this somewhere on the UI
                throw new UnauthorizedAccessException("Compare Error", ex);
            }

            return parentMergedNode.InBothButChildrenDiffer;
        }

        private void WalkFolderTree(FolderTreeNode parentMergedNode, string path, string pathToRoot, Location location)
        {
            try
            {

                //If there are any files add them to the parent and mark them with the location flag
                //ZlpFileInfo[] files = ZlpIOHelper.GetFiles(path, mCompareOptions.ExtensionFilters, System.IO.SearchOption.TopDirectoryOnly);
                ZlpFileInfo[] files = GetFiles(path);
                foreach (var file in files)
                {
                    FileTreeNode fileNode = new FileTreeNode()
                    {
                        Name = GetName(file.FullName),
                        RelativePath = GetRelativePath(file.FullName, pathToRoot),
                        NodeLocation = location,
                        Parent = parentMergedNode
                    };

                    if (location == Location.UniqueLeft)
                    {
                        fileNode.LastWriteTimeLeft = file.LastWriteTime;
                        fileNode.FileSizeLeft = file.Length;
                    }
                    else if (location == Location.UniqueRight)
                    {
                        fileNode.LastWriteTimeRight = file.LastWriteTime;
                        fileNode.FileSizeRight = file.Length;
                    }
                    else
                    {
                        throw new Exception("Invalid location while walking the folder tree");
                    }

                    parentMergedNode.Children.Add(fileNode);

                    //mFolderItemsDict.Add(fileNode.RelativePath, fileNode);
                    mFolderItemsDict.Add(mKeyPrefix + fileNode.RelativePath, fileNode);
                }

                //If there are any folders add them to the parent, mark them with the location flag and recursively walk the tree
                //ZlpDirectoryInfo[] folders = ZlpIOHelper.GetDirectories(path, System.IO.SearchOption.TopDirectoryOnly);
                ZlpDirectoryInfo[] folders = GetDirectories(path);
                foreach (var folder in folders)
                {
                    FolderTreeNode folderNode = new FolderTreeNode()
                    {
                        Name = GetName(folder.FullName),
                        RelativePath = GetRelativePath(folder.FullName, pathToRoot),
                        NodeLocation = location,
                        Parent = parentMergedNode,
                        Children = new SortedSet<IFolderChildNode>(),
                    };

                    if (location == Location.UniqueLeft)
                    {
                        folderNode.LastWriteTimeLeft = folder.LastWriteTime;
                    }
                    else if (location == Location.UniqueRight)
                    {
                        folderNode.LastWriteTimeRight = folder.LastWriteTime;
                    }
                    else
                    {
                        throw new Exception("Invalid location while walking the folder tree");
                    }

                    parentMergedNode.Children.Add(folderNode);

                    //mFolderItemsDict.Add(folderNode.RelativePath, folderNode);
                    mFolderItemsDict.Add(mKeyPrefix + folderNode.RelativePath, folderNode);

                    //Recursively walk the tree
                    WalkFolderTree(folderNode, folder.FullName, pathToRoot, location);
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                //We're not allowed access to the file or folder!!
                //Probably want to log this somewhere in a file and also display this somewhere on the UI
                throw new UnauthorizedAccessException("Compare Error", ex);
            }
            catch (Exception ex)
            {
                throw new Exception("Compare Error", ex);
            }
        }

    }
}
