﻿namespace VisualDiff
{
    partial class VisualDiffFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VisualDiffFrm));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.rightSideFilenamePanel = new System.Windows.Forms.Panel();
            this.rightSideBrowseTypeButton = new System.Windows.Forms.Button();
            this.iconImages = new System.Windows.Forms.ImageList(this.components);
            this.rightSideBrowseButton = new System.Windows.Forms.Button();
            this.rightSideFilenameComboBox = new System.Windows.Forms.ComboBox();
            this.leftSideFilenamePanel = new System.Windows.Forms.Panel();
            this.leftSideBrowseTypeButton = new System.Windows.Forms.Button();
            this.leftSideBrowseButton = new System.Windows.Forms.Button();
            this.leftSideFilenameComboBox = new System.Windows.Forms.ComboBox();
            this.comparePanel = new System.Windows.Forms.Panel();
            this.showEqualsCheckbox = new System.Windows.Forms.CheckBox();
            this.showReplacesCheckbox = new System.Windows.Forms.CheckBox();
            this.showDeletesCheckbox = new System.Windows.Forms.CheckBox();
            this.showAddsCheckbox = new System.Windows.Forms.CheckBox();
            this.lastButton = new System.Windows.Forms.Button();
            this.firstButton = new System.Windows.Forms.Button();
            this.prevButton = new System.Windows.Forms.Button();
            this.nextButton = new System.Windows.Forms.Button();
            this.compareButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.wrapTextCheckBox = new System.Windows.Forms.CheckBox();
            this.generateDeltaButton = new System.Windows.Forms.Button();
            this.optionsButton = new System.Windows.Forms.Button();
            this.showCharDiffCheckBox = new System.Windows.Forms.CheckBox();
            this.iconToolTips = new System.Windows.Forms.ToolTip(this.components);
            this.leftBrowseTypeContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.leftArchiveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.leftFTPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rightBrowseTypeContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.rightArchiveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rightFTPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.leftOneDriveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rightOneDriveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanel1.SuspendLayout();
            this.rightSideFilenamePanel.SuspendLayout();
            this.leftSideFilenamePanel.SuspendLayout();
            this.comparePanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.leftBrowseTypeContextMenu.SuspendLayout();
            this.rightBrowseTypeContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.rightSideFilenamePanel, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.leftSideFilenamePanel, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.comparePanel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 66F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 83F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1805, 902);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // rightSideFilenamePanel
            // 
            this.rightSideFilenamePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rightSideFilenamePanel.Controls.Add(this.rightSideBrowseTypeButton);
            this.rightSideFilenamePanel.Controls.Add(this.rightSideBrowseButton);
            this.rightSideFilenamePanel.Controls.Add(this.rightSideFilenameComboBox);
            this.rightSideFilenamePanel.Location = new System.Drawing.Point(906, 70);
            this.rightSideFilenamePanel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rightSideFilenamePanel.Name = "rightSideFilenamePanel";
            this.rightSideFilenamePanel.Size = new System.Drawing.Size(895, 34);
            this.rightSideFilenamePanel.TabIndex = 3;
            // 
            // rightSideBrowseTypeButton
            // 
            this.rightSideBrowseTypeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.rightSideBrowseTypeButton.ImageIndex = 7;
            this.rightSideBrowseTypeButton.ImageList = this.iconImages;
            this.rightSideBrowseTypeButton.Location = new System.Drawing.Point(843, 2);
            this.rightSideBrowseTypeButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rightSideBrowseTypeButton.Name = "rightSideBrowseTypeButton";
            this.rightSideBrowseTypeButton.Size = new System.Drawing.Size(45, 27);
            this.rightSideBrowseTypeButton.TabIndex = 3;
            this.rightSideBrowseTypeButton.UseVisualStyleBackColor = true;
            this.rightSideBrowseTypeButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.rightSideBrowseTypeButton_MouseDown);
            // 
            // iconImages
            // 
            this.iconImages.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("iconImages.ImageStream")));
            this.iconImages.TransparentColor = System.Drawing.Color.Transparent;
            this.iconImages.Images.SetKeyName(0, "Editing-Compare-icon.png");
            this.iconImages.Images.SetKeyName(1, "Arrow-Up-icon.png");
            this.iconImages.Images.SetKeyName(2, "Arrow-Down-icon.png");
            this.iconImages.Images.SetKeyName(3, "Arrow-Up-2-icon.png");
            this.iconImages.Images.SetKeyName(4, "Arrow-Down-2-icon.png");
            this.iconImages.Images.SetKeyName(5, "greekdelta_3.png");
            this.iconImages.Images.SetKeyName(6, "wrench-icon.png");
            this.iconImages.Images.SetKeyName(7, "1473386239_icon-arrow-down-b.ico");
            // 
            // rightSideBrowseButton
            // 
            this.rightSideBrowseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.rightSideBrowseButton.Location = new System.Drawing.Point(795, 2);
            this.rightSideBrowseButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rightSideBrowseButton.Name = "rightSideBrowseButton";
            this.rightSideBrowseButton.Size = new System.Drawing.Size(45, 27);
            this.rightSideBrowseButton.TabIndex = 2;
            this.rightSideBrowseButton.Text = "...";
            this.rightSideBrowseButton.UseVisualStyleBackColor = true;
            this.rightSideBrowseButton.Click += new System.EventHandler(this.rightSideBrowseButton_Click);
            // 
            // rightSideFilenameComboBox
            // 
            this.rightSideFilenameComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rightSideFilenameComboBox.FormattingEnabled = true;
            this.rightSideFilenameComboBox.Location = new System.Drawing.Point(3, 2);
            this.rightSideFilenameComboBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rightSideFilenameComboBox.Name = "rightSideFilenameComboBox";
            this.rightSideFilenameComboBox.Size = new System.Drawing.Size(787, 24);
            this.rightSideFilenameComboBox.TabIndex = 0;
            this.rightSideFilenameComboBox.SelectedIndexChanged += new System.EventHandler(this.rightSideFilenameComboBox_SelectedIndexChanged);
            // 
            // leftSideFilenamePanel
            // 
            this.leftSideFilenamePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.leftSideFilenamePanel.Controls.Add(this.leftSideBrowseTypeButton);
            this.leftSideFilenamePanel.Controls.Add(this.leftSideBrowseButton);
            this.leftSideFilenamePanel.Controls.Add(this.leftSideFilenameComboBox);
            this.leftSideFilenamePanel.Location = new System.Drawing.Point(4, 70);
            this.leftSideFilenamePanel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.leftSideFilenamePanel.Name = "leftSideFilenamePanel";
            this.leftSideFilenamePanel.Size = new System.Drawing.Size(894, 34);
            this.leftSideFilenamePanel.TabIndex = 2;
            // 
            // leftSideBrowseTypeButton
            // 
            this.leftSideBrowseTypeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.leftSideBrowseTypeButton.ImageIndex = 7;
            this.leftSideBrowseTypeButton.ImageList = this.iconImages;
            this.leftSideBrowseTypeButton.Location = new System.Drawing.Point(843, 2);
            this.leftSideBrowseTypeButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.leftSideBrowseTypeButton.Name = "leftSideBrowseTypeButton";
            this.leftSideBrowseTypeButton.Size = new System.Drawing.Size(45, 27);
            this.leftSideBrowseTypeButton.TabIndex = 2;
            this.leftSideBrowseTypeButton.UseVisualStyleBackColor = true;
            this.leftSideBrowseTypeButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.leftSideBrowseTypeButton_MouseDown);
            // 
            // leftSideBrowseButton
            // 
            this.leftSideBrowseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.leftSideBrowseButton.Location = new System.Drawing.Point(795, 2);
            this.leftSideBrowseButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.leftSideBrowseButton.Name = "leftSideBrowseButton";
            this.leftSideBrowseButton.Size = new System.Drawing.Size(45, 27);
            this.leftSideBrowseButton.TabIndex = 1;
            this.leftSideBrowseButton.Text = "...";
            this.leftSideBrowseButton.UseVisualStyleBackColor = true;
            this.leftSideBrowseButton.Click += new System.EventHandler(this.leftSideBrowseButton_Click);
            // 
            // leftSideFilenameComboBox
            // 
            this.leftSideFilenameComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.leftSideFilenameComboBox.FormattingEnabled = true;
            this.leftSideFilenameComboBox.Location = new System.Drawing.Point(3, 2);
            this.leftSideFilenameComboBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.leftSideFilenameComboBox.Name = "leftSideFilenameComboBox";
            this.leftSideFilenameComboBox.Size = new System.Drawing.Size(787, 24);
            this.leftSideFilenameComboBox.TabIndex = 0;
            this.leftSideFilenameComboBox.SelectedIndexChanged += new System.EventHandler(this.leftSideFilenameComboBox_SelectedIndexChanged);
            // 
            // comparePanel
            // 
            this.comparePanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comparePanel.Controls.Add(this.showEqualsCheckbox);
            this.comparePanel.Controls.Add(this.showReplacesCheckbox);
            this.comparePanel.Controls.Add(this.showDeletesCheckbox);
            this.comparePanel.Controls.Add(this.showAddsCheckbox);
            this.comparePanel.Controls.Add(this.lastButton);
            this.comparePanel.Controls.Add(this.firstButton);
            this.comparePanel.Controls.Add(this.prevButton);
            this.comparePanel.Controls.Add(this.nextButton);
            this.comparePanel.Controls.Add(this.compareButton);
            this.comparePanel.Location = new System.Drawing.Point(4, 4);
            this.comparePanel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comparePanel.Name = "comparePanel";
            this.comparePanel.Size = new System.Drawing.Size(894, 57);
            this.comparePanel.TabIndex = 5;
            // 
            // showEqualsCheckbox
            // 
            this.showEqualsCheckbox.AutoSize = true;
            this.showEqualsCheckbox.Location = new System.Drawing.Point(679, 33);
            this.showEqualsCheckbox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.showEqualsCheckbox.Name = "showEqualsCheckbox";
            this.showEqualsCheckbox.Size = new System.Drawing.Size(111, 21);
            this.showEqualsCheckbox.TabIndex = 12;
            this.showEqualsCheckbox.Text = "Show Equals";
            this.showEqualsCheckbox.UseVisualStyleBackColor = true;
            this.showEqualsCheckbox.CheckedChanged += new System.EventHandler(this.showEqualsCheckbox_CheckedChanged);
            // 
            // showReplacesCheckbox
            // 
            this.showReplacesCheckbox.AutoSize = true;
            this.showReplacesCheckbox.Location = new System.Drawing.Point(679, 7);
            this.showReplacesCheckbox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.showReplacesCheckbox.Name = "showReplacesCheckbox";
            this.showReplacesCheckbox.Size = new System.Drawing.Size(127, 21);
            this.showReplacesCheckbox.TabIndex = 11;
            this.showReplacesCheckbox.Text = "Show Replaces";
            this.showReplacesCheckbox.UseVisualStyleBackColor = true;
            this.showReplacesCheckbox.CheckedChanged += new System.EventHandler(this.showReplacesCheckbox_CheckedChanged);
            // 
            // showDeletesCheckbox
            // 
            this.showDeletesCheckbox.AutoSize = true;
            this.showDeletesCheckbox.Location = new System.Drawing.Point(551, 33);
            this.showDeletesCheckbox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.showDeletesCheckbox.Name = "showDeletesCheckbox";
            this.showDeletesCheckbox.Size = new System.Drawing.Size(116, 21);
            this.showDeletesCheckbox.TabIndex = 10;
            this.showDeletesCheckbox.Text = "Show Deletes";
            this.showDeletesCheckbox.UseVisualStyleBackColor = true;
            this.showDeletesCheckbox.CheckedChanged += new System.EventHandler(this.showDeletesCheckbox_CheckedChanged);
            // 
            // showAddsCheckbox
            // 
            this.showAddsCheckbox.AutoSize = true;
            this.showAddsCheckbox.Location = new System.Drawing.Point(551, 7);
            this.showAddsCheckbox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.showAddsCheckbox.Name = "showAddsCheckbox";
            this.showAddsCheckbox.Size = new System.Drawing.Size(100, 21);
            this.showAddsCheckbox.TabIndex = 9;
            this.showAddsCheckbox.Text = "Show Adds";
            this.showAddsCheckbox.UseVisualStyleBackColor = true;
            this.showAddsCheckbox.CheckedChanged += new System.EventHandler(this.showAddsCheckbox_CheckedChanged);
            // 
            // lastButton
            // 
            this.lastButton.ImageIndex = 4;
            this.lastButton.ImageList = this.iconImages;
            this.lastButton.Location = new System.Drawing.Point(432, 7);
            this.lastButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lastButton.Name = "lastButton";
            this.lastButton.Size = new System.Drawing.Size(100, 46);
            this.lastButton.TabIndex = 8;
            this.lastButton.UseVisualStyleBackColor = true;
            this.lastButton.Click += new System.EventHandler(this.lastButton_Click);
            // 
            // firstButton
            // 
            this.firstButton.ImageIndex = 3;
            this.firstButton.ImageList = this.iconImages;
            this.firstButton.Location = new System.Drawing.Point(324, 7);
            this.firstButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.firstButton.Name = "firstButton";
            this.firstButton.Size = new System.Drawing.Size(100, 46);
            this.firstButton.TabIndex = 7;
            this.firstButton.UseVisualStyleBackColor = true;
            this.firstButton.Click += new System.EventHandler(this.firstButton_Click);
            // 
            // prevButton
            // 
            this.prevButton.ImageIndex = 1;
            this.prevButton.ImageList = this.iconImages;
            this.prevButton.Location = new System.Drawing.Point(108, 6);
            this.prevButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.prevButton.Name = "prevButton";
            this.prevButton.Size = new System.Drawing.Size(100, 47);
            this.prevButton.TabIndex = 6;
            this.prevButton.UseVisualStyleBackColor = true;
            this.prevButton.Click += new System.EventHandler(this.prevButton_Click);
            // 
            // nextButton
            // 
            this.nextButton.ImageIndex = 2;
            this.nextButton.ImageList = this.iconImages;
            this.nextButton.Location = new System.Drawing.Point(216, 6);
            this.nextButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(100, 47);
            this.nextButton.TabIndex = 5;
            this.nextButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.nextButton.UseVisualStyleBackColor = true;
            this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // compareButton
            // 
            this.compareButton.ImageIndex = 0;
            this.compareButton.ImageList = this.iconImages;
            this.compareButton.Location = new System.Drawing.Point(0, 7);
            this.compareButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.compareButton.Name = "compareButton";
            this.compareButton.Size = new System.Drawing.Size(100, 46);
            this.compareButton.TabIndex = 4;
            this.compareButton.UseVisualStyleBackColor = true;
            this.compareButton.Click += new System.EventHandler(this.compareButton_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.wrapTextCheckBox);
            this.panel1.Controls.Add(this.generateDeltaButton);
            this.panel1.Controls.Add(this.optionsButton);
            this.panel1.Controls.Add(this.showCharDiffCheckBox);
            this.panel1.Location = new System.Drawing.Point(906, 4);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(895, 57);
            this.panel1.TabIndex = 6;
            // 
            // wrapTextCheckBox
            // 
            this.wrapTextCheckBox.AutoSize = true;
            this.wrapTextCheckBox.Location = new System.Drawing.Point(4, 31);
            this.wrapTextCheckBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.wrapTextCheckBox.Name = "wrapTextCheckBox";
            this.wrapTextCheckBox.Size = new System.Drawing.Size(95, 21);
            this.wrapTextCheckBox.TabIndex = 3;
            this.wrapTextCheckBox.Text = "Wrap Text";
            this.wrapTextCheckBox.UseVisualStyleBackColor = true;
            this.wrapTextCheckBox.CheckedChanged += new System.EventHandler(this.wrapTextCheckBox_CheckedChanged);
            // 
            // generateDeltaButton
            // 
            this.generateDeltaButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.generateDeltaButton.ImageIndex = 5;
            this.generateDeltaButton.ImageList = this.iconImages;
            this.generateDeltaButton.Location = new System.Drawing.Point(676, 6);
            this.generateDeltaButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.generateDeltaButton.Name = "generateDeltaButton";
            this.generateDeltaButton.Size = new System.Drawing.Size(100, 46);
            this.generateDeltaButton.TabIndex = 2;
            this.generateDeltaButton.UseVisualStyleBackColor = true;
            this.generateDeltaButton.Click += new System.EventHandler(this.generateDeltaButton_Click);
            // 
            // optionsButton
            // 
            this.optionsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.optionsButton.ImageIndex = 6;
            this.optionsButton.ImageList = this.iconImages;
            this.optionsButton.Location = new System.Drawing.Point(784, 6);
            this.optionsButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.optionsButton.Name = "optionsButton";
            this.optionsButton.Size = new System.Drawing.Size(100, 46);
            this.optionsButton.TabIndex = 1;
            this.optionsButton.UseVisualStyleBackColor = true;
            this.optionsButton.Click += new System.EventHandler(this.optionsButton_Click);
            // 
            // showCharDiffCheckBox
            // 
            this.showCharDiffCheckBox.AutoSize = true;
            this.showCharDiffCheckBox.Location = new System.Drawing.Point(4, 7);
            this.showCharDiffCheckBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.showCharDiffCheckBox.Name = "showCharDiffCheckBox";
            this.showCharDiffCheckBox.Size = new System.Drawing.Size(174, 21);
            this.showCharDiffCheckBox.TabIndex = 0;
            this.showCharDiffCheckBox.Text = "Show Char Differences";
            this.showCharDiffCheckBox.UseVisualStyleBackColor = true;
            this.showCharDiffCheckBox.CheckedChanged += new System.EventHandler(this.showCharDiffCheckBox_CheckedChanged);
            // 
            // leftBrowseTypeContextMenu
            // 
            this.leftBrowseTypeContextMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.leftBrowseTypeContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.leftArchiveToolStripMenuItem,
            this.leftFTPToolStripMenuItem,
            this.leftOneDriveToolStripMenuItem});
            this.leftBrowseTypeContextMenu.Name = "browseTypeContextMenu";
            this.leftBrowseTypeContextMenu.Size = new System.Drawing.Size(145, 76);
            // 
            // leftArchiveToolStripMenuItem
            // 
            this.leftArchiveToolStripMenuItem.Name = "leftArchiveToolStripMenuItem";
            this.leftArchiveToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.leftArchiveToolStripMenuItem.Text = "Archive";
            this.leftArchiveToolStripMenuItem.Click += new System.EventHandler(this.leftArchiveToolStripMenuItem_Click);
            // 
            // leftFTPToolStripMenuItem
            // 
            this.leftFTPToolStripMenuItem.Name = "leftFTPToolStripMenuItem";
            this.leftFTPToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.leftFTPToolStripMenuItem.Text = "FTP";
            this.leftFTPToolStripMenuItem.Click += new System.EventHandler(this.leftFTPToolStripMenuItem_Click);
            // 
            // rightBrowseTypeContextMenu
            // 
            this.rightBrowseTypeContextMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.rightBrowseTypeContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rightArchiveToolStripMenuItem,
            this.rightFTPToolStripMenuItem,
            this.rightOneDriveToolStripMenuItem});
            this.rightBrowseTypeContextMenu.Name = "rightBrowseTypeContextMenu";
            this.rightBrowseTypeContextMenu.Size = new System.Drawing.Size(145, 76);
            // 
            // rightArchiveToolStripMenuItem
            // 
            this.rightArchiveToolStripMenuItem.Name = "rightArchiveToolStripMenuItem";
            this.rightArchiveToolStripMenuItem.Size = new System.Drawing.Size(144, 24);
            this.rightArchiveToolStripMenuItem.Text = "Archive";
            this.rightArchiveToolStripMenuItem.Click += new System.EventHandler(this.rightArchiveToolStripMenuItem_Click);
            // 
            // rightFTPToolStripMenuItem
            // 
            this.rightFTPToolStripMenuItem.Name = "rightFTPToolStripMenuItem";
            this.rightFTPToolStripMenuItem.Size = new System.Drawing.Size(144, 24);
            this.rightFTPToolStripMenuItem.Text = "FTP";
            this.rightFTPToolStripMenuItem.Click += new System.EventHandler(this.rightFTPToolStripMenuItem_Click);
            // 
            // leftOneDriveToolStripMenuItem
            // 
            this.leftOneDriveToolStripMenuItem.Name = "leftOneDriveToolStripMenuItem";
            this.leftOneDriveToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.leftOneDriveToolStripMenuItem.Text = "One Drive";
            this.leftOneDriveToolStripMenuItem.Click += new System.EventHandler(this.leftOneDriveToolStripMenuItem_Click);
            // 
            // rightOneDriveToolStripMenuItem
            // 
            this.rightOneDriveToolStripMenuItem.Name = "rightOneDriveToolStripMenuItem";
            this.rightOneDriveToolStripMenuItem.Size = new System.Drawing.Size(144, 24);
            this.rightOneDriveToolStripMenuItem.Text = "One Drive";
            this.rightOneDriveToolStripMenuItem.Click += new System.EventHandler(this.rightOneDriveToolStripMenuItem_Click);
            // 
            // VisualDiffFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1805, 902);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "VisualDiffFrm";
            this.Text = "Visual Diff";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VisualDiffFrm_FormClosing);
            this.Load += new System.EventHandler(this.VisualDiffFrm_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.rightSideFilenamePanel.ResumeLayout(false);
            this.leftSideFilenamePanel.ResumeLayout(false);
            this.comparePanel.ResumeLayout(false);
            this.comparePanel.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.leftBrowseTypeContextMenu.ResumeLayout(false);
            this.rightBrowseTypeContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel rightSideFilenamePanel;
        private System.Windows.Forms.Panel leftSideFilenamePanel;
        private System.Windows.Forms.ComboBox leftSideFilenameComboBox;
        private System.Windows.Forms.Button leftSideBrowseButton;
        private System.Windows.Forms.ComboBox rightSideFilenameComboBox;
        private System.Windows.Forms.Button rightSideBrowseButton;
        private System.Windows.Forms.Button compareButton;
        private System.Windows.Forms.Panel comparePanel;
        private System.Windows.Forms.Button prevButton;
        private System.Windows.Forms.Button nextButton;
        private System.Windows.Forms.Button lastButton;
        private System.Windows.Forms.Button firstButton;
        private System.Windows.Forms.CheckBox showDeletesCheckbox;
        private System.Windows.Forms.CheckBox showAddsCheckbox;
        private System.Windows.Forms.CheckBox showEqualsCheckbox;
        private System.Windows.Forms.CheckBox showReplacesCheckbox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox showCharDiffCheckBox;
        private System.Windows.Forms.Button optionsButton;
        private System.Windows.Forms.Button generateDeltaButton;
        private System.Windows.Forms.ImageList iconImages;
        private System.Windows.Forms.ToolTip iconToolTips;
        private System.Windows.Forms.Button leftSideBrowseTypeButton;
        private System.Windows.Forms.Button rightSideBrowseTypeButton;
        private System.Windows.Forms.ContextMenuStrip leftBrowseTypeContextMenu;
        private System.Windows.Forms.ToolStripMenuItem leftArchiveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem leftFTPToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip rightBrowseTypeContextMenu;
        private System.Windows.Forms.ToolStripMenuItem rightArchiveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rightFTPToolStripMenuItem;
        private System.Windows.Forms.CheckBox wrapTextCheckBox;
        private System.Windows.Forms.ToolStripMenuItem leftOneDriveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rightOneDriveToolStripMenuItem;
    }
}

