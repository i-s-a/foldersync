﻿using System;
using System.Runtime.InteropServices;

namespace VisualDiff
{
    class DiffHelper
    {
        public enum Operation
        {
            INVALID,
            EQUALS,
            ADD,
            REMOVE,
            REPLACE,
            REPLACE_LEFT_EMPTY,
            REPLACE_RIGHT_EMPTY
        };

        [StructLayout(LayoutKind.Sequential)]
        public struct FinalDiffLocation
        {
            public FinalDiffLocation(Operation operation, long startLine, long endLine)
            {
                mOperation = operation;
                mStartLine = startLine;
                mEndLine = endLine;
            }

            public Operation mOperation;
            public long mStartLine;
            public long mEndLine;
        };

        [StructLayout(LayoutKind.Sequential)]
        public struct FinalLineOffset
        {
            public FinalLineOffset(Operation operation, long offset, long length)
            {
                mOperation = operation;
                mOffset = offset;
                mLength = length;
            }

            public Operation mOperation;
            public long mOffset;
            public long mLength;
        };
        
        [DllImport(@"..\..\..\..\lib\DiffUtilExDllWrapper.dll", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
        private static extern void CompareText2(string[] source, long sourceSize, string[] target, long targetSize, int diffShowType,
            out IntPtr finalIndividualDiffLocations, out long finalIndividualDiffLocationCount,
            out IntPtr finalDiffLocations, out long finalDiffLocationCount,
            out IntPtr leftText, out IntPtr rightText);

        [DllImport(@"..\..\..\..\lib\DiffUtilExDllWrapper.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern void FreeResourcesText2(IntPtr finalIndividualDiffLocations, long finalIndividualDiffLocationCount,
            IntPtr finalDiffLocations, long finalDiffLocationCount,
            IntPtr leftText, IntPtr rightText);
        
        [DllImport(@"..\..\..\..\lib\DiffUtilExDllWrapper.dll", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
        private static extern void CompareText2W(string[] source, long sourceSize, string[] target, long targetSize, int diffShowType,
            out IntPtr finalIndividualDiffLocations, out long finalIndividualDiffLocationCount,
            out IntPtr finalDiffLocations, out long finalDiffLocationCount,
            out IntPtr leftText, out IntPtr rightText);

        [DllImport(@"..\..\..\..\lib\DiffUtilExDllWrapper.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern void FreeResourcesText2W(IntPtr finalIndividualDiffLocations, long finalIndividualDiffLocationCount,
            IntPtr finalDiffLocations, long finalDiffLocationCount,
            IntPtr leftText, IntPtr rightText);

        [DllImport(@"..\..\..\..\lib\DiffUtilExDllWrapper.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        private static extern bool GenerateDelta(string[] source, long sourceSize, string[] target, long targetSize,
            string deltaFilename, short deltaFilenameLength);

        [DllImport(@"..\..\..\..\lib\DiffUtilExDllWrapper.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        private static extern bool RestoreFromDelta(string deltaFilename, short deltaFilenameLength,
            string[] source, long sourceSize, string targetFilename, short targetFilenameLength);

        [DllImport(@"..\..\..\..\lib\DiffUtilExDllWrapper.dll", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
        private static extern bool GenerateDeltaW(string[] source, long sourceSize, string[] target, long targetSize,
            string deltaFilename, short deltaFilenameLength);

        [DllImport(@"..\..\..\..\lib\DiffUtilExDllWrapper.dll", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
        private static extern bool RestoreFromDeltaW(string deltaFilename, short deltaFilenameLength,
            string[] source, long sourceSize, string targetFilename, short targetFilenameLength);

        [DllImport(@"..\..\..\..\lib\DiffUtilExDllWrapper.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        private static extern void CompareBytes(byte[] source, long sourceSize, byte[] target, long targetSize,
            out IntPtr finalLeftOffsets, out long finalLeftOffsetCount,
            out IntPtr finalRightOffsets, out long finalRightOffsetCount);

        [DllImport(@"..\..\..\..\lib\DiffUtilExDllWrapper.dll", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
        private static extern void CompareBytesW(char[] source, long sourceSize, char[] target, long targetSize,
            out IntPtr finalLeftOffsets, out long finalLeftOffsetCount,
            out IntPtr finalRightOffsets, out long finalRightOffsetCount);

        [DllImport(@"..\..\..\..\lib\DiffUtilExDllWrapper.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern void FreeResourcesBytes(IntPtr finalLeftOffsets, long finalLeftOffsetCount,
            IntPtr finalRightOffsets, long finalRightOffsetCount);


        public static void DoCompareText2(string[] sourceLines, string[] targetLines, int diffShowType,
            out FinalDiffLocation[] finalIndividualDiffLocations, out long finalIndividualDiffLocationCount,
            out FinalDiffLocation[] finalDiffLocations, out long finalDiffLocationCount,
            out string leftText, out string rightText)
        {
            IntPtr finalDiffLocationsPtr = IntPtr.Zero;
            finalDiffLocationCount = 0;

            IntPtr finalIndividualDiffLocationsPtr = IntPtr.Zero;
            finalIndividualDiffLocationCount = 0;

            IntPtr leftTextPtr = IntPtr.Zero;
            IntPtr rightTextPtr = IntPtr.Zero;

            CompareText2(sourceLines, sourceLines.Length, targetLines, targetLines.Length, diffShowType,
                out finalIndividualDiffLocationsPtr, out finalIndividualDiffLocationCount,
                out finalDiffLocationsPtr, out finalDiffLocationCount,
                out leftTextPtr, out rightTextPtr);

            finalIndividualDiffLocations = new FinalDiffLocation[finalIndividualDiffLocationCount];
            for (int i = 0; i < finalIndividualDiffLocationCount; ++i)
            {
                IntPtr tmp = Marshal.ReadIntPtr(finalIndividualDiffLocationsPtr, i * IntPtr.Size);
                finalIndividualDiffLocations[i] = (FinalDiffLocation)Marshal.PtrToStructure(tmp, typeof(FinalDiffLocation));
            }

            finalDiffLocations = new FinalDiffLocation[finalDiffLocationCount];
            for (int i = 0; i < finalDiffLocationCount; ++i)
            {
                IntPtr tmp = Marshal.ReadIntPtr(finalDiffLocationsPtr, i * IntPtr.Size);
                finalDiffLocations[i] = (FinalDiffLocation)Marshal.PtrToStructure(tmp, typeof(FinalDiffLocation));
            }

            leftText = Marshal.PtrToStringAnsi(leftTextPtr);
            rightText = Marshal.PtrToStringAnsi(rightTextPtr);

            FreeResourcesText2(finalIndividualDiffLocationsPtr, finalIndividualDiffLocationCount,
                finalDiffLocationsPtr, finalDiffLocationCount, leftTextPtr, rightTextPtr);
        }

        public static void DoCompareText2W(string[] sourceLines, string[] targetLines, int diffShowType,
            out FinalDiffLocation[] finalIndividualDiffLocations, out long finalIndividualDiffLocationCount,
            out FinalDiffLocation[] finalDiffLocations, out long finalDiffLocationCount,
            out string leftText, out string rightText)
        {
            IntPtr finalDiffLocationsPtr = IntPtr.Zero;
            finalDiffLocationCount = 0;

            IntPtr finalIndividualDiffLocationsPtr = IntPtr.Zero;
            finalIndividualDiffLocationCount = 0;

            IntPtr leftTextPtr = IntPtr.Zero;
            IntPtr rightTextPtr = IntPtr.Zero;

            CompareText2W(sourceLines, sourceLines.Length, targetLines, targetLines.Length, diffShowType,
                out finalIndividualDiffLocationsPtr, out finalIndividualDiffLocationCount,
                out finalDiffLocationsPtr, out finalDiffLocationCount,
                out leftTextPtr, out rightTextPtr);

            finalIndividualDiffLocations = new FinalDiffLocation[finalIndividualDiffLocationCount];
            for (int i = 0; i < finalIndividualDiffLocationCount; ++i)
            {
                IntPtr tmp = Marshal.ReadIntPtr(finalIndividualDiffLocationsPtr, i * IntPtr.Size);
                finalIndividualDiffLocations[i] = (FinalDiffLocation)Marshal.PtrToStructure(tmp, typeof(FinalDiffLocation));
            }

            finalDiffLocations = new FinalDiffLocation[finalDiffLocationCount];
            for (int i = 0; i < finalDiffLocationCount; ++i)
            {
                IntPtr tmp = Marshal.ReadIntPtr(finalDiffLocationsPtr, i * IntPtr.Size);
                finalDiffLocations[i] = (FinalDiffLocation)Marshal.PtrToStructure(tmp, typeof(FinalDiffLocation));
            }

            leftText = Marshal.PtrToStringUni(leftTextPtr);
            rightText = Marshal.PtrToStringUni(rightTextPtr);

            FreeResourcesText2W(finalIndividualDiffLocationsPtr, finalIndividualDiffLocationCount,
                finalDiffLocationsPtr, finalDiffLocationCount, leftTextPtr, rightTextPtr);
        }

        public static void DoCompareBytes(byte[] source, byte[] target,
            out FinalLineOffset[] finalLeftOffsets, out long finalLeftOffsetCount,
            out FinalLineOffset[] finalRightOffsets, out long finalRightOffsetCount)
        {
            IntPtr finalLeftOffsetsPtr = IntPtr.Zero;
            finalLeftOffsetCount = 0;
            IntPtr finalRightOffsetsPtr = IntPtr.Zero;
            finalRightOffsetCount = 0;

            CompareBytes(source, source.Length, target, target.Length,
                out finalLeftOffsetsPtr, out finalLeftOffsetCount,
                out finalRightOffsetsPtr, out finalRightOffsetCount);

            finalLeftOffsets = new FinalLineOffset[finalLeftOffsetCount];
            for (int i = 0; i < finalLeftOffsetCount; ++i)
            {
                IntPtr tmp = Marshal.ReadIntPtr(finalLeftOffsetsPtr, i * IntPtr.Size);
                finalLeftOffsets[i] = (FinalLineOffset)Marshal.PtrToStructure(tmp, typeof(FinalLineOffset));
            }

            finalRightOffsets = new FinalLineOffset[finalRightOffsetCount];
            for (int i = 0; i < finalRightOffsetCount; ++i)
            {
                IntPtr tmp = Marshal.ReadIntPtr(finalRightOffsetsPtr, i * IntPtr.Size);
                finalRightOffsets[i] = (FinalLineOffset)Marshal.PtrToStructure(tmp, typeof(FinalLineOffset));
            }

            FreeResourcesBytes(finalLeftOffsetsPtr, finalLeftOffsetCount, finalRightOffsetsPtr, finalRightOffsetCount);
        }

        public static void DoCompareBytesW(char[] source, char[] target,
            out FinalLineOffset[] finalLeftOffsets, out long finalLeftOffsetCount,
            out FinalLineOffset[] finalRightOffsets, out long finalRightOffsetCount)
        {
            IntPtr finalLeftOffsetsPtr = IntPtr.Zero;
            finalLeftOffsetCount = 0;
            IntPtr finalRightOffsetsPtr = IntPtr.Zero;
            finalRightOffsetCount = 0;

            CompareBytesW(source, source.Length, target, target.Length,
                out finalLeftOffsetsPtr, out finalLeftOffsetCount,
                out finalRightOffsetsPtr, out finalRightOffsetCount);

            finalLeftOffsets = new FinalLineOffset[finalLeftOffsetCount];
            for (int i = 0; i < finalLeftOffsetCount; ++i)
            {
                IntPtr tmp = Marshal.ReadIntPtr(finalLeftOffsetsPtr, i * IntPtr.Size);
                finalLeftOffsets[i] = (FinalLineOffset)Marshal.PtrToStructure(tmp, typeof(FinalLineOffset));
            }

            finalRightOffsets = new FinalLineOffset[finalRightOffsetCount];
            for (int i = 0; i < finalRightOffsetCount; ++i)
            {
                IntPtr tmp = Marshal.ReadIntPtr(finalRightOffsetsPtr, i * IntPtr.Size);
                finalRightOffsets[i] = (FinalLineOffset)Marshal.PtrToStructure(tmp, typeof(FinalLineOffset));
            }

            FreeResourcesBytes(finalLeftOffsetsPtr, finalLeftOffsetCount, finalRightOffsetsPtr, finalRightOffsetCount);
        }

        public static bool CreateDelta(string[] sourceLines, string[] targetLines, string deltaFilename)
        {
            return GenerateDelta(sourceLines, sourceLines.Length, targetLines, targetLines.Length, deltaFilename, (short)deltaFilename.Length);
        }

        public static bool RestoreFile(string deltaFilename, string[] source, string targetFilename)
        {
            return RestoreFromDelta(deltaFilename, (short)deltaFilename.Length, source, source.Length, targetFilename, (short)targetFilename.Length);
        }

        public static bool CreateDeltaW(string[] sourceLines, string[] targetLines, string deltaFilename)
        {
            return GenerateDeltaW(sourceLines, sourceLines.Length, targetLines, targetLines.Length, deltaFilename, (short)deltaFilename.Length);
        }

        public static bool RestoreFileW(string deltaFilename, string[] source, string targetFilename)
        {
            return RestoreFromDeltaW(deltaFilename, (short)deltaFilename.Length, source, source.Length, targetFilename, (short)targetFilename.Length);
        }
    }
}
