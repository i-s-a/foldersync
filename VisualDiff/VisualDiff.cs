﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using ScintillaNET;
using DocumentReader;
using VisualTree;
using System.Threading.Tasks;

namespace VisualDiff
{
    public partial class VisualDiffFrm : Form
    {
        private MyScintilla scintillaLeftSide;
        private MyScintilla scintillaRightSide;
        private MyScintilla scintillaLeftSideReplace;
        private MyScintilla scintillaRightSideReplace;

        [Flags]
        private enum DiffShowType
        {
            EQUALS = 1 << 0,
            ADDS = 1 << 1,
            DELETES = 1 << 2,
            REPLACES = 1 << 3,
            ALL = EQUALS | ADDS | DELETES | REPLACES,
        }

        private DiffShowType mDiffShowType;

        private DiffHelper.FinalDiffLocation[] mFinalDiffLocations = null;
        private long mFinalDiffLocationIndex = -1;
        private long mFinalDiffLocationCount = 0;

        private DiffHelper.FinalDiffLocation[] mFinalIndividualDiffLocations = null;
        private long mFinalIndividualDiffLocationCount = 0;

        private string[] mLeftLines = null;

        private string[] mRightLines = null;

        private DiffHelper.FinalLineOffset[] mLeftCharOffsets = null;
        private long mLeftCharOffsetCount = 0;

        private DiffHelper.FinalLineOffset[] mRightCharOffsets = null;
        private long mRightCharOffsetCount = 0;

        private bool mIsScrolling = false;

        private FileSystemWatcher mLeftFileWatcher = new FileSystemWatcher();
        private FileSystemWatcher mRightFileWatcher = new FileSystemWatcher();

        private static readonly string FILE_PATH = @"..\..\FileHistory.txt";

        private int mMaxFileHistorySize = 10;   //Default to 10

        private IDocumentReaderFactory mDocumentReaderFactory = new DocumentReaderFactory();

        private bool mShowFirstDiffWhenLoaded = false;

        Thread mDiffReplaceThread = null;

        DiffOptions mDiffOptions = null;
        List<string> mFilesToDelete = new List<string>();

        long[] mLeftLineWrapAdjustment = null;
        long[] mRightLineWrapAdjustment = null;

        public VisualDiffFrm()
        {
            Init();
        }

        public VisualDiffFrm(string leftFile, string rightFile)
        {
            Init();

            leftSideFilenameComboBox.Text = leftFile;
            rightSideFilenameComboBox.Text = rightFile;

            mShowFirstDiffWhenLoaded = true;
            Compare();
        }

        private void Init()
        {
            InitializeComponent();
            InitializeScintillaControls();

            showCharDiffCheckBox.Checked = true;
            wrapTextCheckBox.Checked = false;

            WindowState = FormWindowState.Maximized;

            tableLayoutPanel1.CellBorderStyle = TableLayoutPanelCellBorderStyle.InsetDouble;

            scintillaLeftSide.VScroll += new MyScintilla.VScrollEventHandler(ScintillaLeftSide_VScroll);
            scintillaLeftSide.HScroll += new MyScintilla.HScrollEventHandler(ScintillaLeftSide_HScroll);

            scintillaRightSide.VScroll += new MyScintilla.VScrollEventHandler(ScintillaRightSide_VScroll);
            scintillaRightSide.HScroll += new MyScintilla.HScrollEventHandler(ScintillaRightSide_HScroll);

            scintillaLeftSide.Painted += ScintillaLeftSide_Painted;
            scintillaRightSide.Painted += ScintillaRightSide_Painted;

            scintillaLeftSideReplace.VScroll += new MyScintilla.VScrollEventHandler(ScintillaLeftSideReplace_VScroll);
            scintillaLeftSideReplace.HScroll += new MyScintilla.HScrollEventHandler(ScintillaLeftSideReplace_HScroll);

            scintillaRightSideReplace.VScroll += new MyScintilla.VScrollEventHandler(ScintillaRightSideReplace_VScroll);
            scintillaRightSideReplace.HScroll += new MyScintilla.HScrollEventHandler(ScintillaRightSideReplace_HScroll);

            scintillaLeftSide.WrapMode = WrapMode.None;
            scintillaRightSide.WrapMode = WrapMode.None;
            scintillaLeftSideReplace.WrapMode = WrapMode.Word;
            scintillaRightSideReplace.WrapMode = WrapMode.Word;

            firstButton.Enabled = false;
            lastButton.Enabled = false;
            nextButton.Enabled = false;
            prevButton.Enabled = false;

            //mDiffShowType = DiffShowType.ALL;
            showAddsCheckbox.Checked = true;
            showDeletesCheckbox.Checked = true;
            showReplacesCheckbox.Checked = true;
            showEqualsCheckbox.Checked = true;

            mLeftFileWatcher.NotifyFilter = NotifyFilters.LastWrite;
            mLeftFileWatcher.Changed += new FileSystemEventHandler(LeftFileWatcher_Changed);

            mRightFileWatcher.NotifyFilter = NotifyFilters.LastWrite;
            mRightFileWatcher.Changed += new FileSystemEventHandler(RightFileWatcher_Changed);

            if (File.Exists(FILE_PATH))
            {
                string[] fileHistory = File.ReadAllLines(FILE_PATH);

                //If the file history is too large then trim it from the front so that
                //it is no larger than the maximum we want to allow. Removing the extra
                //from the front allow
                int startIndex = 0;
                if (fileHistory.Length > mMaxFileHistorySize)
                {
                    startIndex = fileHistory.Length - mMaxFileHistorySize;
                }

                for (int i = startIndex; i < fileHistory.Length; ++i)
                {
                    leftSideFilenameComboBox.Items.Add(fileHistory[i]);
                    rightSideFilenameComboBox.Items.Add(fileHistory[i]);
                }

            }

            //To capture key presses
            KeyPreview = true;

            iconToolTips.SetToolTip(this.compareButton, "Compare  (Alt+C)");
            iconToolTips.SetToolTip(this.prevButton, "Previous Diff  (Alt+Up)");
            iconToolTips.SetToolTip(this.nextButton, "Next Diff  (Alt+Down)");
            iconToolTips.SetToolTip(this.firstButton, "First Diff  (Alt+PageUp)");
            iconToolTips.SetToolTip(this.lastButton, "Last Diff  (Alt+PageDown)");
            iconToolTips.SetToolTip(this.generateDeltaButton, "Generate Delta File  (Alt+D)");
            iconToolTips.SetToolTip(this.optionsButton, "Set Options  (Alt+O)");
            iconToolTips.SetToolTip(this.wrapTextCheckBox, "Wrap long lines of text");
        }

        private void InitializeScintillaControls()
        {
            this.scintillaLeftSide = new VisualDiff.MyScintilla();
            this.scintillaRightSide = new VisualDiff.MyScintilla();

            this.scintillaLeftSideReplace = new VisualDiff.MyScintilla();
            this.scintillaRightSideReplace = new VisualDiff.MyScintilla();

            // 
            // scintillaLeftSide
            // 
            this.scintillaLeftSide.AddColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(182)))), ((int)(((byte)(193)))));
            this.scintillaLeftSide.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scintillaLeftSide.AnnotationVisible = ScintillaNET.Annotation.Boxed;
            this.scintillaLeftSide.DeleteColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(255)))), ((int)(((byte)(190)))));
            this.scintillaLeftSide.FontName = "Microsoft Sans Serif";
            this.scintillaLeftSide.FontSize = 8;
            this.scintillaLeftSide.Location = new System.Drawing.Point(3, 91);
            this.scintillaLeftSide.Name = "scintillaLeftSide";
            this.scintillaLeftSide.ReplaceColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.scintillaLeftSide.Size = new System.Drawing.Size(674, 533);
            this.scintillaLeftSide.TabIndex = 0;
            // 
            // scintillaRightSide
            // 
            this.scintillaRightSide.AddColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(182)))), ((int)(((byte)(193)))));
            this.scintillaRightSide.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scintillaRightSide.AnnotationVisible = ScintillaNET.Annotation.Boxed;
            this.scintillaRightSide.DeleteColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(255)))), ((int)(((byte)(190)))));
            this.scintillaRightSide.FontName = "Microsoft Sans Serif";
            this.scintillaRightSide.FontSize = 8;
            this.scintillaRightSide.Location = new System.Drawing.Point(683, 91);
            this.scintillaRightSide.Name = "scintillaRightSide";
            this.scintillaRightSide.ReplaceColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.scintillaRightSide.Size = new System.Drawing.Size(675, 533);
            this.scintillaRightSide.TabIndex = 1;


            this.tableLayoutPanel1.Controls.Add(this.scintillaLeftSide, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.scintillaRightSide, 1, 2);


            // 
            // scintillaLeftSideReplace
            // 
            this.scintillaLeftSideReplace.AddColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(182)))), ((int)(((byte)(193)))));
            this.scintillaLeftSideReplace.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scintillaLeftSideReplace.AnnotationVisible = ScintillaNET.Annotation.Boxed;
            this.scintillaLeftSideReplace.DeleteColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(255)))), ((int)(((byte)(190)))));
            this.scintillaLeftSideReplace.FontName = "Microsoft Sans Serif";
            this.scintillaLeftSideReplace.FontSize = 8;
            this.scintillaLeftSideReplace.Location = new System.Drawing.Point(3, 630);
            this.scintillaLeftSideReplace.Name = "scintillaLeftSideReplace";
            this.scintillaLeftSideReplace.ReplaceColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.scintillaLeftSideReplace.Size = new System.Drawing.Size(674, 105);
            this.scintillaLeftSideReplace.TabIndex = 7;
            // 
            // scintillaRightSideReplace
            // 
            this.scintillaRightSideReplace.AddColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(182)))), ((int)(((byte)(193)))));
            this.scintillaRightSideReplace.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scintillaRightSideReplace.AnnotationVisible = ScintillaNET.Annotation.Boxed;
            this.scintillaRightSideReplace.DeleteColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(255)))), ((int)(((byte)(190)))));
            this.scintillaRightSideReplace.FontName = "Microsoft Sans Serif";
            this.scintillaRightSideReplace.FontSize = 8;
            this.scintillaRightSideReplace.Location = new System.Drawing.Point(683, 630);
            this.scintillaRightSideReplace.Name = "scintillaRightSideReplace";
            this.scintillaRightSideReplace.ReplaceColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.scintillaRightSideReplace.Size = new System.Drawing.Size(675, 105);
            this.scintillaRightSideReplace.TabIndex = 8;


            this.tableLayoutPanel1.Controls.Add(this.scintillaLeftSideReplace, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.scintillaRightSideReplace, 1, 3);
        }

        private void ScintillaRightSide_Painted(object sender, EventArgs e)
        {
            if (mRightLineWrapAdjustment == null && mFinalIndividualDiffLocationCount > 0)
            {
                HighlightRightSideDiffs();
                CalculateRightLineWrapAdjustment();
            }
        }

        private void ScintillaLeftSide_Painted(object sender, EventArgs e)
        {
            if (mLeftLineWrapAdjustment == null && mFinalIndividualDiffLocationCount > 0)
            {
                HighlightLeftSideDiffs();
                CalculateLeftLineWrapAdjustment();
            }
        }

        private void HighlightLeftSideDiffs()
        {
            int leftStyleIndex = 0;

            DiffHelper.FinalDiffLocation diffLoc;

            for (long i = 0; i < mFinalIndividualDiffLocationCount; ++i)
            {
                diffLoc = mFinalIndividualDiffLocations[i];

                if (diffLoc.mOperation == DiffHelper.Operation.ADD)
                {
                    leftStyleIndex = MyScintilla.ADD_STYLE_INDEX;
                }
                else if (diffLoc.mOperation == DiffHelper.Operation.REMOVE)
                {
                    leftStyleIndex = MyScintilla.DELETE_STYLE_INDEX;
                }
                else if (diffLoc.mOperation == DiffHelper.Operation.REPLACE)
                {
                    leftStyleIndex = MyScintilla.REPLACE_STYLE_INDEX;
                }
                else if (diffLoc.mOperation == DiffHelper.Operation.REPLACE_LEFT_EMPTY)
                {
                    leftStyleIndex = MyScintilla.DELETE_STYLE_INDEX;
                }
                else if (diffLoc.mOperation == DiffHelper.Operation.REPLACE_RIGHT_EMPTY)
                {
                    leftStyleIndex = MyScintilla.REPLACE_STYLE_INDEX;
                }

                for (long j = diffLoc.mStartLine; j < diffLoc.mEndLine; ++j)
                {
                    scintillaLeftSide.HighlightLine(scintillaLeftSide.Lines[(int)j].Position, scintillaLeftSide.Lines[(int)j].Length, leftStyleIndex);
                }
            }
        }

        private void ClearLeftSideDiffs()
        {
            int leftStyleIndex = MyScintilla.NORMAL_STYLE_INDEX;

            DiffHelper.FinalDiffLocation diffLoc;

            for (long i = 0; i < mFinalIndividualDiffLocationCount; ++i)
            {
                diffLoc = mFinalIndividualDiffLocations[i];

                for (long j = diffLoc.mStartLine; j < diffLoc.mEndLine; ++j)
                {
                    scintillaLeftSide.HighlightLine(scintillaLeftSide.Lines[(int)j].Position, scintillaLeftSide.Lines[(int)j].Length, leftStyleIndex);
                }
            }
        }

        private void CalculateLeftLineWrapAdjustment()
        {
            DiffHelper.FinalDiffLocation diffLoc;

            mLeftLineWrapAdjustment = new long[mFinalDiffLocationCount + 1];

            int leftWrapCount = 0;

            for (int i = 0; i < mFinalDiffLocationCount; ++i)
            {
                if (i > 0)
                    mLeftLineWrapAdjustment[i] = mLeftLineWrapAdjustment[i - 1] + leftWrapCount;

                diffLoc = mFinalDiffLocations[i];

                leftWrapCount = 0;

                for (long j = diffLoc.mStartLine; j < diffLoc.mEndLine; ++j)
                {
                    leftWrapCount += scintillaLeftSide.Lines[(int)j].WrapCount;
                }
            }
        }

        private void HighlightRightSideDiffs()
        {
            int rightStyleIndex = 0;

            DiffHelper.FinalDiffLocation diffLoc;

            for (long i = 0; i < mFinalIndividualDiffLocationCount; ++i)
            {
                diffLoc = mFinalIndividualDiffLocations[i];

                if (diffLoc.mOperation == DiffHelper.Operation.ADD)
                {
                    rightStyleIndex = MyScintilla.ADD_STYLE_INDEX;
                }
                else if (diffLoc.mOperation == DiffHelper.Operation.REMOVE)
                {
                    rightStyleIndex = MyScintilla.DELETE_STYLE_INDEX;
                }
                else if (diffLoc.mOperation == DiffHelper.Operation.REPLACE)
                {
                    rightStyleIndex = MyScintilla.REPLACE_STYLE_INDEX;
                }
                else if (diffLoc.mOperation == DiffHelper.Operation.REPLACE_LEFT_EMPTY)
                {
                    rightStyleIndex = MyScintilla.REPLACE_STYLE_INDEX;
                }
                else if (diffLoc.mOperation == DiffHelper.Operation.REPLACE_RIGHT_EMPTY)
                {
                    rightStyleIndex = MyScintilla.DELETE_STYLE_INDEX;
                }

                for (long j = diffLoc.mStartLine; j < diffLoc.mEndLine; ++j)
                {
                    scintillaRightSide.HighlightLine(scintillaRightSide.Lines[(int)j].Position, scintillaRightSide.Lines[(int)j].Length, rightStyleIndex);
                }
            }
        }

        private void ClearRightSideDiffs()
        {
            int rightStyleIndex = MyScintilla.NORMAL_STYLE_INDEX;

            DiffHelper.FinalDiffLocation diffLoc;

            for (long i = 0; i < mFinalIndividualDiffLocationCount; ++i)
            {
                diffLoc = mFinalIndividualDiffLocations[i];

                for (long j = diffLoc.mStartLine; j < diffLoc.mEndLine; ++j)
                {
                    scintillaRightSide.HighlightLine(scintillaRightSide.Lines[(int)j].Position, scintillaRightSide.Lines[(int)j].Length, rightStyleIndex);
                }
            }
        }

        private void CalculateRightLineWrapAdjustment()
        {
            DiffHelper.FinalDiffLocation diffLoc;

            mRightLineWrapAdjustment = new long[mFinalDiffLocationCount + 1];

            int rightWrapCount = 0;

            for (int i = 0; i < mFinalDiffLocationCount; ++i)
            {
                if (i > 0)
                    mRightLineWrapAdjustment[i] = mRightLineWrapAdjustment[i - 1] + rightWrapCount;

                diffLoc = mFinalDiffLocations[i];

                rightWrapCount = 0;

                for (long j = diffLoc.mStartLine; j < diffLoc.mEndLine; ++j)
                {
                    rightWrapCount += scintillaRightSide.Lines[(int)j].WrapCount;
                }
            }
        }

        private void Reset()
        {
            mFinalDiffLocations = null;
            mFinalDiffLocationIndex = -1;
            mFinalDiffLocationCount = 0;
            mFinalIndividualDiffLocations = null;
            mFinalIndividualDiffLocationCount = 0;
            mLeftLineWrapAdjustment = null;
            mRightLineWrapAdjustment = null;

            firstButton.Enabled = false;
            lastButton.Enabled = false;
            nextButton.Enabled = false;
            prevButton.Enabled = false;
        }

        void ScintillaLeftSide_VScroll(int lineNumber)
        {
            if (!mIsScrolling)
            {
                mIsScrolling = true;

                long adjusted = 0;

                if (mLeftLineWrapAdjustment != null && mRightLineWrapAdjustment != null && mFinalDiffLocationIndex >= 0)
                {
                    long leftWrapCount = mLeftLineWrapAdjustment[mFinalDiffLocationIndex];
                    long rightWrapCount = mRightLineWrapAdjustment[mFinalDiffLocationIndex];

                    adjusted = rightWrapCount - leftWrapCount;
                }

                scintillaRightSide.ScrollToLine(lineNumber + adjusted);
            }

            mIsScrolling = false;
        }

        void ScintillaLeftSide_HScroll(int pos)
        {
            if (!mIsScrolling)
            {
                mIsScrolling = true;
                scintillaRightSide.ScrollToHPos(pos);
            }

            mIsScrolling = false;
        }

        void ScintillaRightSide_VScroll(int lineNumber)
        {
            if (!mIsScrolling)
            {
                mIsScrolling = true;

                long adjusted = 0;

                if (mLeftLineWrapAdjustment != null && mRightLineWrapAdjustment != null && mFinalDiffLocationIndex >= 0)
                {
                    long leftWrapCount = mLeftLineWrapAdjustment[mFinalDiffLocationIndex];
                    long rightWrapCount = mRightLineWrapAdjustment[mFinalDiffLocationIndex];

                    adjusted = rightWrapCount - leftWrapCount;
                }

                scintillaLeftSide.ScrollToLine(lineNumber + adjusted);
            }

            mIsScrolling = false;
        }

        void ScintillaRightSide_HScroll(int pos)
        {
            if (!mIsScrolling)
            {
                mIsScrolling = true;
                scintillaLeftSide.ScrollToHPos(pos);
            }

            mIsScrolling = false;
        }

        void ScintillaLeftSideReplace_VScroll(int lineNumber)
        {
            if (!mIsScrolling)
            {
                mIsScrolling = true;
                scintillaRightSideReplace.ScrollToLine(lineNumber);
            }

            mIsScrolling = false;
        }

        void ScintillaLeftSideReplace_HScroll(int pos)
        {
            if (!mIsScrolling)
            {
                mIsScrolling = true;
                scintillaRightSideReplace.ScrollToHPos(pos);
            }

            mIsScrolling = false;
        }

        void ScintillaRightSideReplace_VScroll(int lineNumber)
        {
            if (!mIsScrolling)
            {
                mIsScrolling = true;
                scintillaLeftSideReplace.ScrollToLine(lineNumber);
            }

            mIsScrolling = false;
        }

        void ScintillaRightSideReplace_HScroll(int pos)
        {
            if (!mIsScrolling)
            {
                mIsScrolling = true;
                scintillaLeftSideReplace.ScrollToHPos(pos);
            }

            mIsScrolling = false;
        }

        private void LeftSideLoadText(string filename)
        {
            ReadFileContent(filename, ref mLeftLines);
        }

        private void RightSideLoadText(string filename)
        {
            ReadFileContent(filename, ref mRightLines);
        }

        private void ReadFileContent(string filename, ref string[] lines)
        {
            IDocumentReader reader = mDocumentReaderFactory.CreateDocumentReader(filename);
            if (reader != null)
            {
                lines = reader.ReadLines();
            }
        }

        private void leftSideBrowseButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDlg = new OpenFileDialog();
            fileDlg.Title = "File 1";
            fileDlg.RestoreDirectory = true;
            if (fileDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //leftSideFilenameComboBox.Text = fileDlg.FileName;
                leftSideFilenameComboBox.Text = SelectFile(fileDlg.FileName, "Left");

                if (!leftSideFilenameComboBox.Items.Contains(fileDlg.FileName))
                {
                    if (leftSideFilenameComboBox.Items.Count == mMaxFileHistorySize)
                        leftSideFilenameComboBox.Items.RemoveAt(0);

                    leftSideFilenameComboBox.Items.Add(fileDlg.FileName);
                }

                if (!rightSideFilenameComboBox.Items.Contains(fileDlg.FileName))
                {
                    if (rightSideFilenameComboBox.Items.Count == mMaxFileHistorySize)
                        rightSideFilenameComboBox.Items.RemoveAt(0);

                    rightSideFilenameComboBox.Items.Add(fileDlg.FileName);
                }
            }
        }

        void LeftFileWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            DialogResult result = MessageBox.Show(this, "Left file changed. Would you like to re-compare", "Alert", MessageBoxButtons.YesNo);
            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                Compare();
            }
        }

        private void rightSideBrowseButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDlg = new OpenFileDialog();
            fileDlg.Title = "File 2";
            fileDlg.RestoreDirectory = true;
            if (fileDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //rightSideFilenameComboBox.Text = fileDlg.FileName;
                rightSideFilenameComboBox.Text = SelectFile(fileDlg.FileName, "Right");

                if (!rightSideFilenameComboBox.Items.Contains(fileDlg.FileName))
                {
                    if (rightSideFilenameComboBox.Items.Count == mMaxFileHistorySize)
                        rightSideFilenameComboBox.Items.RemoveAt(0);

                    rightSideFilenameComboBox.Items.Add(fileDlg.FileName);
                }

                if (!leftSideFilenameComboBox.Items.Contains(fileDlg.FileName))
                {
                    if (leftSideFilenameComboBox.Items.Count == mMaxFileHistorySize)
                        leftSideFilenameComboBox.Items.RemoveAt(0);

                    leftSideFilenameComboBox.Items.Add(fileDlg.FileName);
                }
            }
        }

        void RightFileWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            DialogResult result = MessageBox.Show(this, "Right file changed. Would you like to re-compare", "Alert", MessageBoxButtons.YesNo);
            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                Compare();
            }
        }

        private string SelectFile(string path, string location)
        {
            ITreeItemNodeReader reader = null;

            if (path.EndsWith(".zip"))
            {
                reader = new ZipArchiveReader(path);
            }

            string localFilename = null;
            if (reader != null)
            {
                TreeViewerDlg treeViewer = new TreeViewerDlg(reader, path);

                DialogResult dlgRes = treeViewer.ShowDialog(this);

                if (dlgRes == System.Windows.Forms.DialogResult.OK)
                {
                    string localPath = Path.GetTempPath() + location + @"\";
                    if (!Directory.Exists(localPath))
                    {
                        Directory.CreateDirectory(localPath);
                    }

                    localFilename = reader.RetrieveFile(treeViewer.SelectedFile, localPath);
                    mFilesToDelete.Add(localFilename);
                }
            }
            else
            {
                localFilename = path;
            }

            return localFilename;
        }

        private void HighlightLeftBlock(int startOffset, int length, int styleIndex)
        {
            scintillaLeftSide.HighlightLine(startOffset, length, styleIndex);
        }

        private void HighlightRightBlock(int startOffset, int length, int styleIndex)
        {
            scintillaRightSide.HighlightLine(startOffset, length, styleIndex);
        }

        private void ScrollToLeftLine(long lineNumber)
        {
            //Update this so that instead of retrieving this every time we can just retrieve it once
            //when the form is first loaded and then any time the window is resized
            long linesVisible = scintillaLeftSide.LinesOnScreen;
            scintillaLeftSide.ScrollToLine(lineNumber - (linesVisible / 2));
        }

        private void ScrollToRightLine(int lineNumber)
        {
            //Update this so that instead of retrieving this every time we can just retrieve it once
            //when the form is first loaded and then any time the window is resized
            int linesVisible = scintillaRightSide.LinesOnScreen;
            scintillaRightSide.ScrollToLine(lineNumber - (linesVisible / 2));
        }

        private void compareButton_Click(object sender, EventArgs e)
        {
            Compare();
        }

        private void Compare()
        {
            if (!File.Exists(leftSideFilenameComboBox.Text))
            {
                MessageBox.Show("Left filename is invalid", "Invalid File");
                return;
            }

            if (!File.Exists(rightSideFilenameComboBox.Text))
            {
                MessageBox.Show("Right filename is invalid", "Invalid File");
                return;
            }

            string leftFile = leftSideFilenameComboBox.Text;
            string rightFile = rightSideFilenameComboBox.Text;

            if (!CanDiffFiles(leftFile, rightFile))
            {
                MessageBox.Show("Can only show differences if both files are text, .doc, .docx or .pdf files", "Invalid file");
                mShowFirstDiffWhenLoaded = false;
                return;
            }

            mLeftFileWatcher.EnableRaisingEvents = false;
            mRightFileWatcher.EnableRaisingEvents = false;

            LeftSideLoadText(leftFile);

            //Check first whether the right file is a delta file, and if it is
            //then restore the file from the delta before loading the right side
            if (rightFile.EndsWith(".vddelta"))
            {
                //rightFile = RestoreFromDeltaFile(rightFile, mLeftLines);
                rightFile = RestoreFromDeltaFile(rightFile, mLeftLines);
                if (rightFile == "")
                    return;

                rightSideFilenameComboBox.Text = rightFile;
            }

            RightSideLoadText(rightFile);

            int index = leftFile.LastIndexOf('\\');
            mLeftFileWatcher.Path = leftFile.Substring(0, index);
            mLeftFileWatcher.Filter = leftFile.Substring(index + 1);

            index = rightFile.LastIndexOf('\\');
            mRightFileWatcher.Path = rightFile.Substring(0, index);
            mRightFileWatcher.Filter = rightFile.Substring(index + 1);

            DisplayCompareResult(mLeftLines, mRightLines);

            //I suppose the file could change while the DisplayCompareResult is processing,
            //in which case maybe we should start watching earlier!!! Think about it!!!
            mLeftFileWatcher.EnableRaisingEvents = true;
            mRightFileWatcher.EnableRaisingEvents = true;
        }

        private void DoCompareText(string[] sourceLines, string[] targetLines, out string leftText, out string rightText)
        {
            DiffHelper.DoCompareText2W(sourceLines, targetLines, (int)mDiffShowType,
                            out mFinalIndividualDiffLocations, out mFinalIndividualDiffLocationCount,
                            out mFinalDiffLocations, out mFinalDiffLocationCount, out leftText, out rightText);
        }

        //private async void DisplayCompareResult(string[] sourceLines, string[] targetLines)
        private void DisplayCompareResult(string[] sourceLines, string[] targetLines)
        {
            Reset();

            scintillaLeftSide.ClearAll();
            scintillaRightSide.ClearAll();

            string leftText = null;
            string rightText = null;

            //compareButton.Enabled = false;

            //await Task.Run(() => DoCompareText(sourceLines, targetLines, out leftText, out rightText));

            //compareButton.Enabled = true;
            DoCompareText(sourceLines, targetLines, out leftText, out rightText);

            //scintillaLeftSide.WrapMode = WrapMode.Word;
            //scintillaRightSide.WrapMode = WrapMode.Word;

            scintillaLeftSide.AppendText(leftText);
            scintillaRightSide.AppendText(rightText);

            HighlightLeftSideDiffs();
            HighlightRightSideDiffs();

            if (wrapTextCheckBox.Checked)
            {
                CalculateLeftLineWrapAdjustment();
                CalculateRightLineWrapAdjustment();
            }

            scintillaLeftSide.Margins[4].Width = 24;
            scintillaLeftSide.Margins[4].Type = MarginType.Text;
            //scintillaLeftSide.Margins[4].Sensitive = true;  //Don't need receive mouse click notifications from the margin
            //scintillaLeftSide.Margins[4].Type = MarginType.Symbol;
            scintillaLeftSide.Margins[4].Mask = Marker.MaskAll;
            //scintillaLeftSide.Margins[4].Cursor = MarginCursor.Arrow;

            scintillaRightSide.Margins[4].Width = 24;
            scintillaRightSide.Margins[4].Type = MarginType.Text;
            //scintillaRightSide.Margins[4].Sensitive = true;  //Don't need receive mouse click notifications from the margin
            //scintillaRightSide.Margins[4].Type = MarginType.Symbol;
            scintillaRightSide.Margins[4].Mask = Marker.MaskAll;
            //scintillaRightSide.Margins[4].Cursor = MarginCursor.Arrow;

            if (mFinalDiffLocationCount > 0)
            {
                firstButton.Enabled = true;
                lastButton.Enabled = true;
                nextButton.Enabled = true;
            }

            //We no longer need the original left and right lines. If we need access to the individual
            //lines then we can either access the Lines from the left and right scintilla controls or
            //split the leftText and rightText here before they go out of scope. I think we can do
            //everything we need with the scintilla Lines
            mLeftLines = null;
            mRightLines = null;
        }

        private void nextButton_Click(object sender, EventArgs e)
        {
            GoToNextDiff();
        }

        private void prevButton_Click(object sender, EventArgs e)
        {
            GoToPrevDiff();
        }

        private void firstButton_Click(object sender, EventArgs e)
        {
            GoToFirstDiff();
        }

        private void lastButton_Click(object sender, EventArgs e)
        {
            GoToLastDiff();
        }

        private void GoToFirstDiff()
        {
            ClearReplaceText();

            if (mFinalDiffLocationIndex != -1)
                ClearMargin(mFinalDiffLocationIndex);

            mFinalDiffLocationIndex = 0;

            if (mFinalDiffLocations[mFinalDiffLocationIndex].mOperation == DiffHelper.Operation.REPLACE)
            {
                DiffReplaceText(mFinalDiffLocationIndex);
            }

            //ScrollToLeftLine(mFinalDiffLocations[mFinalDiffLocationIndex].mStartLine);
            ScrollToLeftLine(scintillaLeftSide.Lines[(int)mFinalDiffLocations[mFinalDiffLocationIndex].mStartLine].DisplayIndex);

            SetMargin(mFinalDiffLocationIndex);

            firstButton.Enabled = false;
            lastButton.Enabled = true;
            prevButton.Enabled = false;

            if (mFinalDiffLocationCount > 1)
                nextButton.Enabled = true;
            else
                nextButton.Enabled = false;
        }

        private void GoToLastDiff()
        {
            ClearReplaceText();

            if (mFinalDiffLocationIndex != -1)
                ClearMargin(mFinalDiffLocationIndex);

            mFinalDiffLocationIndex = mFinalDiffLocationCount - 1;

            if (mFinalDiffLocations[mFinalDiffLocationIndex].mOperation == DiffHelper.Operation.REPLACE)
            {
                DiffReplaceText(mFinalDiffLocationIndex);
            }

            //ScrollToLeftLine(mFinalDiffLocations[mFinalDiffLocationIndex].mStartLine);
            ScrollToLeftLine(scintillaLeftSide.Lines[(int)mFinalDiffLocations[mFinalDiffLocationIndex].mStartLine].DisplayIndex);

            SetMargin(mFinalDiffLocationIndex);

            firstButton.Enabled = true;
            lastButton.Enabled = false;

            if (mFinalDiffLocationCount > 1)
                prevButton.Enabled = true;
            else
                prevButton.Enabled = false;

            nextButton.Enabled = false;
        }

        private void GoToNextDiff()
        {
            ClearReplaceText();

            if (mFinalDiffLocations[mFinalDiffLocationIndex + 1].mOperation == DiffHelper.Operation.REPLACE)
            {
                DiffReplaceText(mFinalDiffLocationIndex + 1);
            }

            //We only need to scroll on one side and that will automatically scroll the other.
            //Randomly choosing the left
            //ScrollToLeftLine(mFinalDiffLocations[++mFinalDiffLocationIndex].mStartLine);
            ScrollToLeftLine(scintillaLeftSide.Lines[(int)mFinalDiffLocations[++mFinalDiffLocationIndex].mStartLine].DisplayIndex);

            if (mFinalDiffLocationIndex == mFinalDiffLocationCount - 1)
            {
                lastButton.Enabled = false;
                nextButton.Enabled = false;
            }

            if (mFinalDiffLocationIndex > 0)
            {
                firstButton.Enabled = true;
                prevButton.Enabled = true;

                ClearMargin(mFinalDiffLocationIndex - 1);
            }

            SetMargin(mFinalDiffLocationIndex);
        }

        private void GoToPrevDiff()
        {
            ClearReplaceText();

            if (mFinalDiffLocations[mFinalDiffLocationIndex - 1].mOperation == DiffHelper.Operation.REPLACE)
            {
                DiffReplaceText(mFinalDiffLocationIndex - 1);
            }

            //We only need to scroll on one side and that will automatically scroll the other.
            //Randomly choosing the left
            //ScrollToLeftLine(mFinalDiffLocations[--mFinalDiffLocationIndex].mStartLine);
            ScrollToLeftLine(scintillaLeftSide.Lines[(int)mFinalDiffLocations[--mFinalDiffLocationIndex].mStartLine].DisplayIndex);

            if (mFinalDiffLocationIndex == 0)
            {
                firstButton.Enabled = false;
                prevButton.Enabled = false;
            }

            if (mFinalDiffLocationIndex < mFinalDiffLocationCount - 1)
            {
                lastButton.Enabled = true;
                nextButton.Enabled = true;

                ClearMargin(mFinalDiffLocationIndex + 1);
            }

            SetMargin(mFinalDiffLocationIndex);
        }

        private void SetMargin(long index)
        {
            for (long i = mFinalDiffLocations[index].mStartLine; i < mFinalDiffLocations[index].mEndLine; ++i)
            {
                if (mFinalDiffLocations[index].mOperation == DiffHelper.Operation.ADD)
                {
                    scintillaLeftSide.Markers[4].SetForeColor(Color.Black);
                    scintillaLeftSide.Markers[4].SetBackColor(scintillaLeftSide.AddColor);
                    scintillaLeftSide.Markers[4].Symbol = MarkerSymbol.Minus;
                    scintillaLeftSide.Lines[(int)i].MarginStyle = MyScintilla.ADD_STYLE_INDEX;

                    scintillaRightSide.Markers[4].SetForeColor(Color.Black);
                    scintillaRightSide.Markers[4].SetBackColor(scintillaRightSide.AddColor);
                    scintillaRightSide.Markers[4].Symbol = MarkerSymbol.Plus;
                    scintillaRightSide.Lines[(int)i].MarginStyle = MyScintilla.ADD_STYLE_INDEX;
                }
                else if (mFinalDiffLocations[index].mOperation == DiffHelper.Operation.REMOVE)
                {
                    scintillaLeftSide.Markers[4].SetForeColor(Color.Black);
                    scintillaLeftSide.Markers[4].SetBackColor(scintillaLeftSide.DeleteColor);
                    scintillaLeftSide.Markers[4].Symbol = MarkerSymbol.Plus;
                    scintillaLeftSide.Lines[(int)i].MarginStyle = MyScintilla.DELETE_STYLE_INDEX;

                    scintillaRightSide.Markers[4].SetForeColor(Color.Black);
                    scintillaRightSide.Markers[4].SetBackColor(scintillaRightSide.DeleteColor);
                    scintillaRightSide.Markers[4].Symbol = MarkerSymbol.Minus;
                    scintillaRightSide.Lines[(int)i].MarginStyle = MyScintilla.DELETE_STYLE_INDEX;
                }
                else if (mFinalDiffLocations[index].mOperation == DiffHelper.Operation.REPLACE)
                {
                    scintillaLeftSide.Markers[4].SetForeColor(Color.Black);
                    scintillaLeftSide.Markers[4].SetBackColor(scintillaLeftSide.ReplaceColor);
                    scintillaLeftSide.Markers[4].Symbol = MarkerSymbol.Arrows;
                    scintillaLeftSide.Lines[(int)i].MarginStyle = MyScintilla.REPLACE_STYLE_INDEX;

                    scintillaRightSide.Markers[4].SetForeColor(Color.Black);
                    scintillaRightSide.Markers[4].SetBackColor(scintillaRightSide.ReplaceColor);
                    scintillaRightSide.Markers[4].Symbol = MarkerSymbol.Arrows;
                    scintillaRightSide.Lines[(int)i].MarginStyle = MyScintilla.REPLACE_STYLE_INDEX;
                }
                //else
                //    ;

                scintillaLeftSide.Lines[(int)i].MarkerAdd(4);
                scintillaRightSide.Lines[(int)i].MarkerAdd(4);
            }
        }

        private void ClearMargin(long index)
        {
            for (long i = mFinalDiffLocations[index].mStartLine; i < mFinalDiffLocations[index].mEndLine; ++i)
            {
                //scintillaLeftSide.Lines[i].MarginStyle = MyScintilla.NORMAL_STYLE_INDEX;
                //scintillaRightSide.Lines[i].MarginStyle = MyScintilla.NORMAL_STYLE_INDEX;

                scintillaLeftSide.Lines[(int)i].MarginText = "";
                scintillaRightSide.Lines[(int)i].MarginText = "";

                scintillaLeftSide.Lines[(int)i].MarkerDelete(4);
                scintillaRightSide.Lines[(int)i].MarkerDelete(4);
            }
        }

        private void showAddsCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if (showAddsCheckbox.Checked)
            {
                mDiffShowType |= DiffShowType.ADDS;
            }
            else
            {
                mDiffShowType &= (~DiffShowType.ADDS);
            }
        }

        private void showDeletesCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if (showDeletesCheckbox.Checked)
            {
                mDiffShowType |= DiffShowType.DELETES;
            }
            else
            {
                mDiffShowType &= (~DiffShowType.DELETES);
            }
        }

        private void showReplacesCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if (showReplacesCheckbox.Checked)
            {
                mDiffShowType |= DiffShowType.REPLACES;
            }
            else
            {
                mDiffShowType &= (~DiffShowType.REPLACES);
            }
        }

        private void showEqualsCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if (showEqualsCheckbox.Checked)
            {
                mDiffShowType |= DiffShowType.EQUALS;
            }
            else
            {
                mDiffShowType &= (~DiffShowType.EQUALS);
            }
        }

        private void generateDeltaButton_Click(object sender, EventArgs e)
        {
            string[] leftLines = GetLinesFromScintillaControl(scintillaLeftSide);
            string[] rightLines = GetLinesFromScintillaControl(scintillaRightSide);

            GenerateDeltaFile(leftLines, rightLines);
        }

        private string[] GetLinesFromScintillaControl(MyScintilla myScintilla)
        {
            string[] lines = new string[myScintilla.Lines.Count];
            for (int i = 0; i < myScintilla.Lines.Count; ++i)
            {
                lines[i] = myScintilla.Lines[i].Text;
            }

            return lines;
        }

        private void GenerateDeltaFile(string[] sourceLines, string[] targetLines)
        {
            if ((sourceLines == null || targetLines == null) ||
                (sourceLines.Length == 0 && targetLines.Length == 0))
            {
                MessageBox.Show(this, "Left and Right files needs to be loaded to generate a delta", "Invalid data");
            }
            else
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Visual Diff Delta|*.vddelta";
                saveFileDialog.Title = "Save Delta File";
                saveFileDialog.ShowDialog(this);
                if (saveFileDialog.FileName != "")
                {
                    bool res = DiffHelper.CreateDeltaW(sourceLines, targetLines, saveFileDialog.FileName);
                    if (res)
                    {
                        MessageBox.Show("Delta file generated successfully");
                    }
                    else
                    {
                        MessageBox.Show(this, "Failed to create the delta file");
                    }
                }
            }
        }

        private string RestoreFromDeltaFile(string deltaFile, string[] sourceLines)
        {
            string savedFile = "";
            if (!File.Exists(deltaFile))
            {
                MessageBox.Show("Delta file does not exist", "Delta File Error");
            }
            else if (sourceLines == null)
            {
                MessageBox.Show("Left file needs to be loaded to restore from the delta file", "Invalid Left File");
            }
            else
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Title = "Save Restored File";
                saveFileDialog.ShowDialog(this);
                if (saveFileDialog.FileName != "")
                {
                    savedFile = saveFileDialog.FileName;
                    bool res = DiffHelper.RestoreFileW(deltaFile, sourceLines, savedFile);
                    if (res)
                    {
                        MessageBox.Show("File restored from delta file successfully");
                    }
                    else
                    {
                        MessageBox.Show("Failed to restore file from delta file");
                    }
                }
            }

            return savedFile;
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Alt | Keys.Down))
            {
                if (mFinalDiffLocationIndex < mFinalDiffLocationCount - 1)
                    GoToNextDiff();
            }
            else if (e.KeyData == (Keys.Alt | Keys.Up))
            {
                if (mFinalDiffLocationIndex > 0)
                    GoToPrevDiff();
            }
            else if (e.KeyData == (Keys.Alt | Keys.PageDown))
            {
                if (mFinalDiffLocationIndex < mFinalDiffLocationCount - 1)
                    GoToLastDiff();
            }
            else if (e.KeyData == (Keys.Alt | Keys.PageUp))
            {
                if (mFinalDiffLocationIndex > 0)
                    GoToFirstDiff();
            }
            else if (e.KeyData == (Keys.Alt | Keys.C))
            {
                Compare();
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
            else if (e.KeyData == (Keys.Alt | Keys.D))
            {
                GenerateDeltaFile(mLeftLines, mRightLines);
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
            else if (e.KeyData == (Keys.Alt | Keys.O))
            {
                HandleOptionsDialog();
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
            else
            {
                ;
            }

            base.OnKeyDown(e);
        }

        private void VisualDiffFrm_FormClosing(object sender, FormClosingEventArgs e)
        {
            mLeftFileWatcher.EnableRaisingEvents = false;
            mRightFileWatcher.EnableRaisingEvents = false;

            mLeftFileWatcher.Dispose();
            mRightFileWatcher.Dispose();

            mLeftFileWatcher = null;
            mRightFileWatcher = null;

            //For now we assume that both the left and right combo boxes will have the same list
            using (StreamWriter fileStream = new StreamWriter(FILE_PATH))
            {
                for (int i = 0; i < leftSideFilenameComboBox.Items.Count; ++i)
                {
                    fileStream.WriteLine(leftSideFilenameComboBox.Items[i].ToString());
                }
            }

            ClearAll();
            DeleteFiles();
            SaveDiffOptions();
        }

        private void DeleteFiles()
        {
            foreach (var filename in mFilesToDelete)
            {
                File.Delete(filename);
            }
        }

        private void optionsButton_Click(object sender, EventArgs e)
        {
            HandleOptionsDialog();
        }

        private void HandleOptionsDialog()
        {
            LoadDiffOptions();
            OptionsDlg optionsDlg = new OptionsDlg(mDiffOptions, "FontsAndColor");

            DialogResult res = optionsDlg.ShowDialog(this);
            if (res == System.Windows.Forms.DialogResult.OK)
            {
                mDiffOptions = optionsDlg.Options;

                scintillaLeftSide.AddColor = mDiffOptions.AddColor;
                scintillaLeftSide.DeleteColor = mDiffOptions.DeleteColor;
                scintillaLeftSide.ReplaceColor = mDiffOptions.ReplaceColor;

                scintillaRightSide.AddColor = mDiffOptions.AddColor;
                scintillaRightSide.DeleteColor = mDiffOptions.DeleteColor;
                scintillaRightSide.ReplaceColor = mDiffOptions.ReplaceColor;

                scintillaLeftSideReplace.AddColor = mDiffOptions.AddColor;
                scintillaLeftSideReplace.DeleteColor = mDiffOptions.DeleteColor;
                scintillaLeftSideReplace.ReplaceColor = mDiffOptions.ReplaceColor;

                scintillaRightSideReplace.AddColor = mDiffOptions.AddColor;
                scintillaRightSideReplace.DeleteColor = mDiffOptions.DeleteColor;
                scintillaRightSideReplace.ReplaceColor = mDiffOptions.ReplaceColor;

                if (mDiffOptions.FontName != scintillaLeftSide.FontName ||
                    mDiffOptions.FontSize != scintillaLeftSide.FontSize)
                {
                    scintillaLeftSide.UpdateStyleFont(mDiffOptions.FontName, mDiffOptions.FontSize);
                    scintillaLeftSide.StyleAllText(MyScintilla.NORMAL_STYLE_INDEX);

                    scintillaRightSide.UpdateStyleFont(mDiffOptions.FontName, mDiffOptions.FontSize);
                    scintillaRightSide.StyleAllText(MyScintilla.NORMAL_STYLE_INDEX);

                    scintillaLeftSideReplace.UpdateStyleFont(mDiffOptions.FontName, mDiffOptions.FontSize);
                    scintillaLeftSideReplace.StyleAllText(MyScintilla.NORMAL_STYLE_INDEX);
                    scintillaRightSideReplace.UpdateStyleFont(mDiffOptions.FontName, mDiffOptions.FontSize);
                    scintillaRightSideReplace.StyleAllText(MyScintilla.NORMAL_STYLE_INDEX);
                }

                //For now do the highlighting again in full. Ideally we only we want to do this
                //if one of the colours has changed and then only highlight the diffs of the changed
                //colour
                //HighlightDiffs();
                HighlightLeftSideDiffs();
                HighlightRightSideDiffs();
                HighlightCharDiffs();
            }
        }
        private void leftSideFilenameComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!File.Exists(leftSideFilenameComboBox.Text))
            {
                DialogResult res = MessageBox.Show(this, "Delete from recent file list?", "File does not exist", MessageBoxButtons.YesNo);
                if (res == DialogResult.Yes)
                {
                    rightSideFilenameComboBox.Items.Remove(leftSideFilenameComboBox.Text);
                    leftSideFilenameComboBox.Items.Remove(leftSideFilenameComboBox.Text);
                    leftSideFilenameComboBox.Text = "";
                }
            }
        }

        private void rightSideFilenameComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!File.Exists(rightSideFilenameComboBox.Text))
            {
                DialogResult res = MessageBox.Show(this, "Delete from recent file list?", "File does not exist", MessageBoxButtons.YesNo);
                if (res == DialogResult.Yes)
                {
                    leftSideFilenameComboBox.Items.Remove(rightSideFilenameComboBox.Text);
                    rightSideFilenameComboBox.Items.Remove(rightSideFilenameComboBox.Text);
                    rightSideFilenameComboBox.Text = "";
                }
            }
        }

        private void ClearReplaceText()
        {
            if (mDiffReplaceThread != null && mDiffReplaceThread.IsAlive)
            {
                //mDiffReplaceThread.Abort();
                mDiffReplaceThread = null;
            }

            scintillaLeftSideReplace.ClearAll();
            scintillaRightSideReplace.ClearAll();
            //mCharDiffLocations = null;
            //mCharDiffLength = 0;
            mLeftCharOffsetCount = 0;
            mLeftCharOffsets = null;
            mRightCharOffsetCount = 0;
            mRightCharOffsets = null;
        }

        private void ClearAll()
        {
            ClearReplaceText();

            mLeftLines = null;
            mRightLines = null;
            mFinalDiffLocations = null;
            mFinalIndividualDiffLocations = null;
            //mLeftLineOffsets = null;
            //mRightLineOffsets = null;
            scintillaLeftSide.ClearAll();
            scintillaLeftSide = null;
            scintillaRightSide.ClearAll();
            scintillaRightSide = null;
        }

        //private async void DiffReplaceText(long diffLocationIndex)
        private void DiffReplaceText(long diffLocationIndex)
        {
            long startLineNumber = mFinalDiffLocations[diffLocationIndex].mStartLine;
            long endLineNumber = mFinalDiffLocations[diffLocationIndex].mEndLine;

            if (showCharDiffCheckBox.Checked)
            {
                scintillaLeftSideReplace.UpdateText("Loading...Please wait");
                scintillaRightSideReplace.UpdateText("Loading...Please wait");

                StringBuilder leftReplace = new StringBuilder((int)endLineNumber - (int)startLineNumber);
                StringBuilder rightReplace = new StringBuilder((int)endLineNumber - (int)startLineNumber);

                for (long i = startLineNumber; i < endLineNumber; ++i)
                {
                    leftReplace.Append(scintillaLeftSide.Lines[(int)i].Text);
                    rightReplace.Append(scintillaRightSide.Lines[(int)i].Text);
                }

                mDiffReplaceThread = new Thread(() =>
                {
                    char[] leftChars = leftReplace.ToString().ToCharArray();
                    char[] rightChars = rightReplace.ToString().ToCharArray();
                    CompareReplaceBlock(leftChars, rightChars);
                    this.BeginInvoke(new Action(() => this.UpdateReplaceText(leftChars, rightChars)));
                });

                mDiffReplaceThread.Start();
            }
        }

        private void CompareReplaceBlock(char[] leftChars, char[] rightChars)
        {
            DiffHelper.DoCompareBytesW(leftChars, rightChars, out mLeftCharOffsets, out mLeftCharOffsetCount,
                out mRightCharOffsets, out mRightCharOffsetCount);
        }

        private void UpdateReplaceText(char[] leftChars, char[] rightChars)
        {
            scintillaLeftSideReplace.UpdateText(leftChars);
            scintillaRightSideReplace.UpdateText(rightChars);
            HighlightCharDiffs();
        }

        private void HighlightCharDiffs()
        {
            int styleIndex = 0;

            for (long i = 0; i < mLeftCharOffsetCount; ++i)
            {
                /*if (mLeftCharOffsets[i].mOperation == DiffHelper.Operation.ADD)
                    styleIndex = MyScintilla.ADD_STYLE_INDEX;
                else*/
                if (mLeftCharOffsets[i].mOperation == DiffHelper.Operation.REMOVE)
                    styleIndex = MyScintilla.DELETE_STYLE_INDEX;
                else if (mLeftCharOffsets[i].mOperation == DiffHelper.Operation.REPLACE)
                    styleIndex = MyScintilla.REPLACE_STYLE_INDEX;
                else if (mLeftCharOffsets[i].mOperation == DiffHelper.Operation.EQUALS)
                    styleIndex = MyScintilla.NORMAL_STYLE_INDEX;

                scintillaLeftSideReplace.StyleTextRange(mLeftCharOffsets[i].mOffset, mLeftCharOffsets[i].mLength, styleIndex);
            }

            for (long i = 0; i < mRightCharOffsetCount; ++i)
            {
                if (mRightCharOffsets[i].mOperation == DiffHelper.Operation.ADD)
                    styleIndex = MyScintilla.ADD_STYLE_INDEX;
                /*else if (mRightCharOffsets[i].mOperation == DiffHelper.Operation.REMOVE)
                    styleIndex = MyScintilla.DELETE_STYLE_INDEX;*/
                else if (mRightCharOffsets[i].mOperation == DiffHelper.Operation.REPLACE)
                    styleIndex = MyScintilla.REPLACE_STYLE_INDEX;
                else if (mRightCharOffsets[i].mOperation == DiffHelper.Operation.EQUALS)
                    styleIndex = MyScintilla.NORMAL_STYLE_INDEX;

                scintillaRightSideReplace.StyleTextRange(mRightCharOffsets[i].mOffset, mRightCharOffsets[i].mLength, styleIndex);
            }
        }

        private void showCharDiffCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (showCharDiffCheckBox.Checked)
            {
                if (mFinalDiffLocationIndex != -1 && mFinalDiffLocations[mFinalDiffLocationIndex].mOperation == DiffHelper.Operation.REPLACE)
                {
                    DiffReplaceText(mFinalDiffLocationIndex);
                }

                tableLayoutPanel1.RowStyles[3].SizeType = SizeType.Percent;
                tableLayoutPanel1.RowStyles[3].Height = 17;
            }
            else
            {
                ClearReplaceText();

                tableLayoutPanel1.RowStyles[3].SizeType = SizeType.Percent;
                tableLayoutPanel1.RowStyles[3].Height = 0;
            }
        }

        private void VisualDiffFrm_Load(object sender, EventArgs e)
        {
            if (mShowFirstDiffWhenLoaded)
                GoToFirstDiff();
        }

        private static bool CanDiffFiles(string leftFile, string rightFile)
        {
            bool canDiffLeftFile = false;
            bool canDiffRightFile = false;

            if (IsAcceptableFileFormat(leftFile))
                canDiffLeftFile = true;

            if (IsAcceptableFileFormat(rightFile))
                canDiffRightFile = true;

            //At this point if we can't diff the left or right file, then check if the one we can't
            //diff is a binary file, and if either one is a binary file then we can't diff
            if (!canDiffLeftFile)
                canDiffLeftFile = !IsBinaryFile(leftFile);

            if (!canDiffRightFile)
                canDiffRightFile = !IsBinaryFile(rightFile);

            return (canDiffLeftFile && canDiffRightFile);
        }

        private static bool IsAcceptableFileFormat(string file)
        {
            if (file.EndsWith(".docx") || 
                file.EndsWith(".xlsx") ||
                file.EndsWith(".pptx") ||
                file.EndsWith(".doc") ||
                file.EndsWith(".xps") ||
                file.EndsWith(".pdf"))
                return true;

            return false;
        }

        private static bool IsBinaryFile(string path)
        {
            bool isBinary = false;
            FileInfo fileInfo = new FileInfo(path);

            using (System.IO.FileStream fileStream = fileInfo.OpenRead())
            {
                //If we find a control character within the first 512 bytes then
                //we assume it is a binary file, otherwise we assume it is
                //text...should do for now
                byte[] buf = new byte[512];
                int bytesRead = fileStream.Read(buf, 0, 512);
                for (int i = 0; i < bytesRead && isBinary == false; ++i)
                {
                    if ((buf[i] >= 0 && buf[i] <= 7) || (buf[i] >= 14 && buf[i] <= 26))
                        isBinary = true;
                }
            }

            return isBinary;
        }

        private void leftSideBrowseTypeButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                int x = scintillaLeftSide.Left + leftSideBrowseTypeButton.Left;
                int y = leftSideFilenamePanel.Top + leftSideBrowseTypeButton.Bottom;
                leftBrowseTypeContextMenu.Show(this, new Point(x, y));
            }
        }

        private void rightSideBrowseTypeButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                int x = scintillaRightSide.Left + rightSideBrowseTypeButton.Left;
                int y = rightSideFilenamePanel.Top + rightSideBrowseTypeButton.Bottom;
                rightBrowseTypeContextMenu.Show(this, new Point(x, y));
            }
        }

        private void leftArchiveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string localFilename = LoadFileFromArchive("File 1", "Left");
            if (localFilename != null)
            {
                leftSideFilenameComboBox.Text = localFilename;
            }
        }

        private async void leftFTPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string localFilename = await LoadFileFromFtp("Left");
            if (localFilename != null)
            {
                leftSideFilenameComboBox.Text = localFilename;
            }
        }

        private async void leftOneDriveToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            string localFilename = await LoadFileFromOneDrive("Left");
            if (localFilename != null)
            {
                leftSideFilenameComboBox.Text = localFilename;
            }
        }

        private void rightArchiveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string localFilename = LoadFileFromArchive("File 2", "Right");
            if (localFilename != null)
            {
                rightSideFilenameComboBox.Text = localFilename;
            }
        }

        private async void rightFTPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string localFilename = await LoadFileFromFtp("Right");
            if (localFilename != null)
            {
                rightSideFilenameComboBox.Text = localFilename;
            }
        }

        private async void rightOneDriveToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            string localFilename = await LoadFileFromOneDrive("Right");
            if (localFilename != null)
            {
                rightSideFilenameComboBox.Text = localFilename;
            }
        }

        private string LoadFileFromArchive(string title, string location)
        {
            string localFilename = null;
            OpenFileDialog fileDlg = new OpenFileDialog();
            fileDlg.Title = title;
            fileDlg.RestoreDirectory = true;
            if (fileDlg.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
            {
                ITreeItemNodeReader reader = new ZipArchiveReader(fileDlg.FileName);
                TreeViewerDlg treeViewer = new TreeViewerDlg(reader, "");
                DialogResult res = treeViewer.ShowDialog(this);
                if (res == System.Windows.Forms.DialogResult.OK)
                {
                    string localPath = Path.GetTempPath() + location + @"\";
                    if (!Directory.Exists(localPath))
                    {
                        Directory.CreateDirectory(localPath);
                    }

                    localFilename = reader.RetrieveFile(treeViewer.SelectedFile, localPath);
                    mFilesToDelete.Add(localFilename);
                }
            }

            return localFilename;
        }

        private async Task<string> LoadFileFromFtp(string location)
        {
            string localFilename = null;

            try
            {
                LoadDiffOptions();

                FtpDlg ftpDlg = new FtpDlg(mDiffOptions);

                if (ftpDlg.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
                {
                    mDiffOptions = ftpDlg.Options;

                    ITreeItemNodeReader reader = new FtpReader(mDiffOptions.Ftp.Hostname, mDiffOptions.Ftp.Username, mDiffOptions.Ftp.Password);
                    await reader.ConnectAsync();
                    TreeViewerDlg treeViewer = new TreeViewerDlg(reader, "");
                    DialogResult res = treeViewer.ShowDialog(this);
                    if (res == System.Windows.Forms.DialogResult.OK)
                    {
                        string localPath = Path.GetTempPath() + location + @"\";
                        if (!Directory.Exists(localPath))
                        {
                            Directory.CreateDirectory(localPath);
                        }

                        localFilename = await reader.RetrieveFileAsync(treeViewer.SelectedFile, localPath);
                        mFilesToDelete.Add(localFilename);
                    }
                }
            }
            catch (Exception ex)
            {
                localFilename = null;
                MessageBox.Show(this, ex.Message, "FTP Error");
            }

            return localFilename;
        }

        private async Task<string> LoadFileFromOneDrive(string location)
        {
            string localFilename = null;

            try
            {
                ITreeItemNodeReader reader = new OneDriveReader();
                TreeViewerDlg treeViewer = new TreeViewerDlg(reader, "");
                DialogResult res = treeViewer.ShowDialog(this);
                if (res == System.Windows.Forms.DialogResult.OK)
                {
                    string localPath = Path.GetTempPath() + location + @"\";
                    if (!Directory.Exists(localPath))
                    {
                        Directory.CreateDirectory(localPath);
                    }

                    localFilename = await reader.RetrieveFileAsync(treeViewer.SelectedFile, localPath);
                    mFilesToDelete.Add(localFilename);
                }
            }
            catch (Exception ex)
            {
                localFilename = null;
                MessageBox.Show(this, ex.Message, "One Drive Error");
            }

            return localFilename;
        }

        private void LoadDiffOptions()
        {
            if (mDiffOptions == null)
            {
                mDiffOptions = DiffOptions.Deserialize("DiffOptions.xml");
                if (mDiffOptions == null)
                {
                    mDiffOptions = new DiffOptions();
                    mDiffOptions.AddColor = scintillaLeftSide.AddColor;
                    mDiffOptions.DeleteColor = scintillaLeftSide.DeleteColor;
                    mDiffOptions.ReplaceColor = scintillaLeftSide.ReplaceColor;
                    mDiffOptions.FontName = scintillaLeftSide.FontName;
                    mDiffOptions.FontSize = scintillaLeftSide.FontSize;
                    mDiffOptions.Ftp = new FtpCredentials();
                }
            }
        }

        private void SaveDiffOptions()
        {
            if (mDiffOptions != null)
            {
                DiffOptions.Serialize("DiffOptions.xml", mDiffOptions);
            }
        }

        private void wrapTextCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (wrapTextCheckBox.Checked && (scintillaLeftSide.WrapMode != WrapMode.Word || scintillaRightSide.WrapMode != WrapMode.Word))
            {
                scintillaLeftSide.WrapMode = WrapMode.Word;
                scintillaRightSide.WrapMode = WrapMode.Word;
                //Technically we shouldn't need to clear the highlights and then highlight
                //the diffs again. We should only really need to recalculate the wrap count,
                //but the paint method doesn't currently separate out the highlighting from
                //the wrap count calculation and so therefore for now we will clear the diffs
                //and the paint method will highlight them again
                ClearLeftSideDiffs();
                ClearRightSideDiffs();
                mLeftLineWrapAdjustment = null;
                mRightLineWrapAdjustment = null;
                scintillaLeftSide.Update();
                scintillaRightSide.Update();
            }
            else if (!wrapTextCheckBox.Checked && (scintillaLeftSide.WrapMode != WrapMode.None || scintillaRightSide.WrapMode != WrapMode.None))
            {
                scintillaLeftSide.WrapMode = WrapMode.None;
                scintillaRightSide.WrapMode = WrapMode.None;
                //Technically we shouldn't need to clear the highlights and then highlight
                //the diffs again. We should only really need to recalculate the wrap count,
                //but the paint method doesn't currently separate out the highlighting from
                //the wrap count calculation and so therefore for now we will clear the diffs
                //and the paint method will highlight them again
                ClearLeftSideDiffs();
                ClearRightSideDiffs();
                mLeftLineWrapAdjustment = null;
                mRightLineWrapAdjustment = null;
                scintillaLeftSide.Update();
                scintillaRightSide.Update();
            }
        }
    }
}
