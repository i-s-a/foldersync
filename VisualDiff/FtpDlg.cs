﻿using System;
using System.Windows.Forms;

namespace VisualDiff
{
    public partial class FtpDlg : Form
    {
        public DiffOptions Options
        {
            get;
            private set;
        }

        public FtpDlg(DiffOptions diffOptions)
        {
            InitializeComponent();

            Options = diffOptions;

            FormBorderStyle = FormBorderStyle.FixedDialog;
            MinimizeBox = false;
            MaximizeBox = false;

            ftpHostnameTextBox.Text = Options.Ftp.Hostname;
            ftpUsernameTextBox.Text = Options.Ftp.Username;
            ftpPortTextBox.Text = Options.Ftp.Port.ToString();

        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            Options.Ftp.Hostname = ftpHostnameTextBox.Text;
            Options.Ftp.Username = ftpUsernameTextBox.Text;
            Options.Ftp.Password = ftpPasswordTextBox.Text;
            Options.Ftp.Port = Convert.ToInt32(ftpPortTextBox.Text);

            DialogResult = DialogResult.OK;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
