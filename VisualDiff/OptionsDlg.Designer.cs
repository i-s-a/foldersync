﻿namespace VisualDiff
{
    partial class OptionsDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cancelButton = new System.Windows.Forms.Button();
            this.applyButton = new System.Windows.Forms.Button();
            this.resetButton = new System.Windows.Forms.Button();
            this.optionsTabControl = new System.Windows.Forms.TabControl();
            this.colorsAndFontsTab = new System.Windows.Forms.TabPage();
            this.changeFontGroupBox = new System.Windows.Forms.GroupBox();
            this.fontSizeComboBox = new System.Windows.Forms.ComboBox();
            this.fontSampleTextLabel = new System.Windows.Forms.Label();
            this.fontListComboBox = new System.Windows.Forms.ComboBox();
            this.changeColorsGroupBox = new System.Windows.Forms.GroupBox();
            this.addColorButton = new System.Windows.Forms.Button();
            this.replaceColorButton = new System.Windows.Forms.Button();
            this.deleteColorButton = new System.Windows.Forms.Button();
            this.optionsTabControl.SuspendLayout();
            this.colorsAndFontsTab.SuspendLayout();
            this.changeFontGroupBox.SuspendLayout();
            this.changeColorsGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(396, 196);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 0;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // applyButton
            // 
            this.applyButton.Location = new System.Drawing.Point(315, 196);
            this.applyButton.Name = "applyButton";
            this.applyButton.Size = new System.Drawing.Size(75, 23);
            this.applyButton.TabIndex = 1;
            this.applyButton.Text = "Apply";
            this.applyButton.UseVisualStyleBackColor = true;
            this.applyButton.Click += new System.EventHandler(this.applyButton_Click);
            // 
            // resetButton
            // 
            this.resetButton.Location = new System.Drawing.Point(12, 196);
            this.resetButton.Name = "resetButton";
            this.resetButton.Size = new System.Drawing.Size(75, 23);
            this.resetButton.TabIndex = 2;
            this.resetButton.Text = "Reset";
            this.resetButton.UseVisualStyleBackColor = true;
            this.resetButton.Click += new System.EventHandler(this.resetButton_Click);
            // 
            // optionsTabControl
            // 
            this.optionsTabControl.Controls.Add(this.colorsAndFontsTab);
            this.optionsTabControl.Location = new System.Drawing.Point(12, 12);
            this.optionsTabControl.Name = "optionsTabControl";
            this.optionsTabControl.SelectedIndex = 0;
            this.optionsTabControl.Size = new System.Drawing.Size(459, 178);
            this.optionsTabControl.TabIndex = 3;
            // 
            // colorsAndFontsTab
            // 
            this.colorsAndFontsTab.BackColor = System.Drawing.Color.Gainsboro;
            this.colorsAndFontsTab.Controls.Add(this.changeFontGroupBox);
            this.colorsAndFontsTab.Controls.Add(this.changeColorsGroupBox);
            this.colorsAndFontsTab.Location = new System.Drawing.Point(4, 22);
            this.colorsAndFontsTab.Name = "colorsAndFontsTab";
            this.colorsAndFontsTab.Padding = new System.Windows.Forms.Padding(3);
            this.colorsAndFontsTab.Size = new System.Drawing.Size(451, 152);
            this.colorsAndFontsTab.TabIndex = 0;
            this.colorsAndFontsTab.Text = "Colors & Fonts";
            // 
            // changeFontGroupBox
            // 
            this.changeFontGroupBox.Controls.Add(this.fontSizeComboBox);
            this.changeFontGroupBox.Controls.Add(this.fontSampleTextLabel);
            this.changeFontGroupBox.Controls.Add(this.fontListComboBox);
            this.changeFontGroupBox.Location = new System.Drawing.Point(187, 21);
            this.changeFontGroupBox.Name = "changeFontGroupBox";
            this.changeFontGroupBox.Size = new System.Drawing.Size(171, 115);
            this.changeFontGroupBox.TabIndex = 4;
            this.changeFontGroupBox.TabStop = false;
            this.changeFontGroupBox.Text = "Change Font";
            // 
            // fontSizeComboBox
            // 
            this.fontSizeComboBox.FormattingEnabled = true;
            this.fontSizeComboBox.Location = new System.Drawing.Point(9, 52);
            this.fontSizeComboBox.Name = "fontSizeComboBox";
            this.fontSizeComboBox.Size = new System.Drawing.Size(61, 21);
            this.fontSizeComboBox.TabIndex = 2;
            this.fontSizeComboBox.SelectedIndexChanged += new System.EventHandler(this.fontSizeComboBox_SelectedIndexChanged);
            // 
            // fontSampleTextLabel
            // 
            this.fontSampleTextLabel.Location = new System.Drawing.Point(6, 83);
            this.fontSampleTextLabel.Name = "fontSampleTextLabel";
            this.fontSampleTextLabel.Size = new System.Drawing.Size(159, 23);
            this.fontSampleTextLabel.TabIndex = 1;
            this.fontSampleTextLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fontListComboBox
            // 
            this.fontListComboBox.FormattingEnabled = true;
            this.fontListComboBox.Location = new System.Drawing.Point(6, 25);
            this.fontListComboBox.Name = "fontListComboBox";
            this.fontListComboBox.Size = new System.Drawing.Size(159, 21);
            this.fontListComboBox.TabIndex = 0;
            this.fontListComboBox.SelectedIndexChanged += new System.EventHandler(this.fontListComboBox_SelectedIndexChanged);
            // 
            // changeColorsGroupBox
            // 
            this.changeColorsGroupBox.Controls.Add(this.addColorButton);
            this.changeColorsGroupBox.Controls.Add(this.replaceColorButton);
            this.changeColorsGroupBox.Controls.Add(this.deleteColorButton);
            this.changeColorsGroupBox.Location = new System.Drawing.Point(20, 21);
            this.changeColorsGroupBox.Name = "changeColorsGroupBox";
            this.changeColorsGroupBox.Size = new System.Drawing.Size(146, 115);
            this.changeColorsGroupBox.TabIndex = 3;
            this.changeColorsGroupBox.TabStop = false;
            this.changeColorsGroupBox.Text = "Change Colors";
            // 
            // addColorButton
            // 
            this.addColorButton.Location = new System.Drawing.Point(6, 25);
            this.addColorButton.Name = "addColorButton";
            this.addColorButton.Size = new System.Drawing.Size(131, 23);
            this.addColorButton.TabIndex = 0;
            this.addColorButton.Text = "Change Add Color";
            this.addColorButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.addColorButton.UseVisualStyleBackColor = true;
            this.addColorButton.Click += new System.EventHandler(this.addColorButton_Click);
            // 
            // replaceColorButton
            // 
            this.replaceColorButton.Location = new System.Drawing.Point(6, 83);
            this.replaceColorButton.Name = "replaceColorButton";
            this.replaceColorButton.Size = new System.Drawing.Size(131, 23);
            this.replaceColorButton.TabIndex = 2;
            this.replaceColorButton.Text = "Change Replace Color";
            this.replaceColorButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.replaceColorButton.UseVisualStyleBackColor = true;
            this.replaceColorButton.Click += new System.EventHandler(this.replaceColorButton_Click);
            // 
            // deleteColorButton
            // 
            this.deleteColorButton.Location = new System.Drawing.Point(6, 54);
            this.deleteColorButton.Name = "deleteColorButton";
            this.deleteColorButton.Size = new System.Drawing.Size(131, 23);
            this.deleteColorButton.TabIndex = 1;
            this.deleteColorButton.Text = "Change Delete Color";
            this.deleteColorButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.deleteColorButton.UseVisualStyleBackColor = true;
            this.deleteColorButton.Click += new System.EventHandler(this.deleteColorButton_Click);
            // 
            // OptionsDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(480, 227);
            this.Controls.Add(this.optionsTabControl);
            this.Controls.Add(this.resetButton);
            this.Controls.Add(this.applyButton);
            this.Controls.Add(this.cancelButton);
            this.Name = "OptionsDlg";
            this.Text = "Options";
            this.optionsTabControl.ResumeLayout(false);
            this.colorsAndFontsTab.ResumeLayout(false);
            this.changeFontGroupBox.ResumeLayout(false);
            this.changeColorsGroupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button applyButton;
        private System.Windows.Forms.Button resetButton;
        private System.Windows.Forms.TabControl optionsTabControl;
        private System.Windows.Forms.TabPage colorsAndFontsTab;
        private System.Windows.Forms.Button replaceColorButton;
        private System.Windows.Forms.Button deleteColorButton;
        private System.Windows.Forms.Button addColorButton;
        private System.Windows.Forms.GroupBox changeColorsGroupBox;
        private System.Windows.Forms.GroupBox changeFontGroupBox;
        private System.Windows.Forms.ComboBox fontListComboBox;
        private System.Windows.Forms.Label fontSampleTextLabel;
        private System.Windows.Forms.ComboBox fontSizeComboBox;
    }
}