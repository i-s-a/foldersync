﻿using System;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Drawing;
using ScintillaNET;

namespace VisualDiff
{
    class MyScintilla : ScintillaNET.Scintilla
    {
        private const int WM_HSCROLL = 276; // Horizontal scroll
        private const int WM_VSCROLL = 277; // Vertical scroll
        private const int SB_HORZ = 0;  //Horizontal scroll bar
        private const int SB_VERT = 1;  //Vertical scroll bar
        private const int SB_LINEUP = 0; // Scrolls one line up
        private const int SB_LINELEFT = 0;// Scrolls one cell left
        private const int SB_LINEDOWN = 1; // Scrolls one line down
        private const int SB_LINERIGHT = 1;// Scrolls one cell right
        private const int SB_PAGEUP = 2; // Scrolls one page up
        private const int SB_PAGELEFT = 2;// Scrolls one page left
        private const int SB_PAGEDOWN = 3; // Scrolls one page down
        private const int SB_PAGERIGHT = 3; // Scrolls one page right
        private const int SB_LEFT = 6; // Scrolls to the left
        private const int SB_RIGHT = 7; // Scrolls to the right
        private const int SB_TOP = 6; // Scrolls to the upper left
        private const int SB_BOTTOM = 7; // Scrolls to the upper right
        private const int SB_ENDSCROLL = 8; // Ends scroll

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        //private static extern int SendMessage(IntPtr hWnd, int wMsg, UIntPtr wParam, IntPtr lParam);
        private static extern int SendMessage(IntPtr hWnd, int wMsg, int wParam, int lParam);

        [DllImport("user32.dll")]
        static extern int SetScrollPos(IntPtr hWnd, int nBar, int nPos, bool bRedraw);

        [DllImport("user32.dll")]
        public static extern int GetScrollPos(IntPtr hwnd, int nBar);

        [DllImport("User32.dll", EntryPoint = "PostMessageA")]
        static extern bool PostMessage(IntPtr hWnd, uint msg, UIntPtr wParam, IntPtr lParam);

        //Indexes 0-7 are reserved for use by Scintilla
        public const int ADD_STYLE_INDEX = 8;
        public const int DELETE_STYLE_INDEX = 9;
        public const int REPLACE_STYLE_INDEX = 10;
        public const int NORMAL_STYLE_INDEX = 11;
        public const int CHAR_DIFF_STYLE_INDEX = 12;

        //Delegate and event for when vertical scrolling takes place
        public delegate void VScrollEventHandler(int lineNumber);
        public event VScrollEventHandler VScroll;

        //Delegate and event for when vertical scrolling takes place
        public delegate void HScrollEventHandler(int pos);
        public event HScrollEventHandler HScroll;

        //The default colours to use when highlighting ADDs, DELETEs and REPLACEs. Will
        //probably want to provide the caller with API calls to change these as required
        private readonly Color ADD_COLOUR = Color.FromArgb(255, 182, 193);
        private readonly Color DELETE_COLOUR = Color.FromArgb(190, 255, 190);
        private readonly Color REPLACE_COLOUR = Color.FromArgb(0, 255, 255);
        private readonly Color NORMAL_COLOUR = Color.FromArgb(255, 255, 255);

        private readonly Color CHAR_DIFF_COLOR = Color.FromArgb(192, 192, 192);

        //private readonly string FONT_NAME = "Arial";
        //private readonly int FONT_SIZE = 8;

        private Color mAddColor;
        private Color mDeleteColor;
        private Color mReplaceColor;
        private Color mNormalColor;
        private Color mCharDiffColor;

        private string mFontName;
        private int mFontSize;

        public MyScintilla() : base()
        {
            mAddColor = ADD_COLOUR;
            mDeleteColor = DELETE_COLOUR;
            mReplaceColor = REPLACE_COLOUR;
            mNormalColor = NORMAL_COLOUR;
            mCharDiffColor = CHAR_DIFF_COLOR;

            mFontName = DefaultFont.FontFamily.Name;
            mFontSize = (int)DefaultFont.Size;

            Styles[ADD_STYLE_INDEX].BackColor = mAddColor;
            Styles[ADD_STYLE_INDEX].ForeColor = Color.Black;
            Styles[ADD_STYLE_INDEX].FillLine = true;
            Styles[ADD_STYLE_INDEX].Font = mFontName;
            Styles[ADD_STYLE_INDEX].Size = mFontSize;

            Styles[DELETE_STYLE_INDEX].BackColor = mDeleteColor;
            Styles[DELETE_STYLE_INDEX].ForeColor = Color.Black;
            Styles[DELETE_STYLE_INDEX].FillLine = true;
            Styles[DELETE_STYLE_INDEX].Font = mFontName;
            Styles[DELETE_STYLE_INDEX].Size = mFontSize;

            Styles[REPLACE_STYLE_INDEX].BackColor = mReplaceColor;
            Styles[REPLACE_STYLE_INDEX].ForeColor = Color.Black;
            Styles[REPLACE_STYLE_INDEX].FillLine = true;
            Styles[REPLACE_STYLE_INDEX].Font = mFontName;
            Styles[REPLACE_STYLE_INDEX].Size = mFontSize;
            
            Styles[NORMAL_STYLE_INDEX].BackColor = mNormalColor;
            Styles[NORMAL_STYLE_INDEX].ForeColor = Color.Black;
            Styles[NORMAL_STYLE_INDEX].FillLine = false;   //Don't need to fill the line for normal styling as the background is already white anyway
            Styles[NORMAL_STYLE_INDEX].Font = mFontName;
            Styles[NORMAL_STYLE_INDEX].Size = mFontSize;

            Styles[CHAR_DIFF_STYLE_INDEX].BackColor = mCharDiffColor;
            Styles[CHAR_DIFF_STYLE_INDEX].ForeColor = Color.Black;
            Styles[CHAR_DIFF_STYLE_INDEX].FillLine = false;   //Don't need to fill the line for normal styling as the background is already white anyway
            Styles[CHAR_DIFF_STYLE_INDEX].Font = mFontName;
            Styles[CHAR_DIFF_STYLE_INDEX].Size = mFontSize;

            AnnotationVisible = Annotation.Boxed;
        }

        public Color AddColor
        {
            get { return mAddColor; }
            set
            {
                mAddColor = value;
                Styles[ADD_STYLE_INDEX].BackColor = mAddColor;
            }
        }

        public Color DeleteColor
        {
            get { return mDeleteColor; }
            set 
            {
                mDeleteColor = value;
                Styles[DELETE_STYLE_INDEX].BackColor = mDeleteColor;
            }
        }

        public Color ReplaceColor
        {
            get { return mReplaceColor; }
            set
            {
                mReplaceColor = value;
                Styles[REPLACE_STYLE_INDEX].BackColor = mReplaceColor;
            }
        }

        public string FontName
        {
            get { return mFontName; }
            set { mFontName = value; }
        }

        public int FontSize
        {
            get { return mFontSize; }
            set { mFontSize = value; }
        }

        public void ScrollToLine(long lineNumber)
        {
            //Scroll to a specific line
            SetScrollPos(Handle, SB_VERT, (int)lineNumber, true);
            //SendMessage(Handle, WM_VSCROLL, (UIntPtr)(4 + 0x10000 * lineNumber), IntPtr.Zero);
            SendMessage(Handle, WM_VSCROLL, 4 + 0x10000 * (int)lineNumber, 0);
        }

        public void ScrollToVPos(int pos)
        {
            //Scroll to a specific position (scrolling to 'pos' will be the same as scrolling to a line)
            SetScrollPos(Handle, SB_VERT, pos, true);
            SendMessage(Handle, WM_VSCROLL, 4 + 0x10000 * pos, 0);
        }

        public void ScrollToHPos(int pos)
        {
            SetScrollPos(Handle, SB_HORZ, pos, true);
            SendMessage(Handle, WM_HSCROLL, 4 + 0x10000 * pos, 0);
        }

        public int GetScrollLineNumber()
        {
            return GetScrollPos(Handle, SB_VERT);
        }

        public int GetVScrollPos()
        {
            return GetScrollPos(Handle, SB_VERT);
        }

        public int GetHScrollPos()
        {
            return GetScrollPos(Handle, SB_HORZ);
        }

        public void HighlightLine(long startLineOffset, long lineLength, int styleIndex)
        {
            StartStyling((int)startLineOffset);
            SetStyling((int)lineLength, styleIndex);
        }

        public void AddAnnotation(string text, int lineNumber, int styleIndex)
        {
            Lines[lineNumber].AnnotationStyle = styleIndex;
            Lines[lineNumber].AnnotationText = text;
        }

        public void UpdateStyleFont(string fontName, int fontSize)
        {
            Styles[ADD_STYLE_INDEX].Font = fontName;
            Styles[ADD_STYLE_INDEX].Size = fontSize;

            Styles[DELETE_STYLE_INDEX].Font = fontName;
            Styles[DELETE_STYLE_INDEX].Size = fontSize;

            Styles[REPLACE_STYLE_INDEX].Font = fontName;
            Styles[REPLACE_STYLE_INDEX].Size = fontSize;

            Styles[NORMAL_STYLE_INDEX].Font = fontName;
            Styles[NORMAL_STYLE_INDEX].Size = fontSize;

            Styles[CHAR_DIFF_STYLE_INDEX].Font = fontName;
            Styles[CHAR_DIFF_STYLE_INDEX].Size = fontSize;

            FontName = fontName;
            FontSize = fontSize;
        }

        public void StyleAllText(int style)
        {
            StartStyling(0);
            SetStyling(TextLength, NORMAL_STYLE_INDEX);
        }

        public void StyleTextRange(long startPosition, long length, int style)
        {
            StartStyling((int)startPosition);
            SetStyling((int)length, style);
        }

        public void UpdateText(char[] chars)
        {
            ClearAll();

            StringBuilder content = new StringBuilder(chars.Length);

            content.Append(chars);

            AppendText(content.ToString());
        }

        public void UpdateText(string content)
        {
            ClearAll();

            AppendText(content);
        }

        public void UpdateText(string[] lines)
        {
            ClearAll();

            StringBuilder content = new StringBuilder();
            foreach (string line in lines)
            {
                content.AppendLine(line);
            }

            AppendText(content.ToString());
        }

        private void VScrollChanged(int lineNumber)
        {
            if (VScroll != null)
            {
                VScroll(lineNumber);
            }
        }

        private void HScrollChanged(int pos)
        {
            if (HScroll != null)
            {
                HScroll(pos);
            }
        }

        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);

            if (m.Msg == WM_VSCROLL)
            {
                VScrollChanged(GetScrollLineNumber());
            }
            else if (m.Msg == WM_HSCROLL)
            {
                HScrollChanged(GetHScrollPos());
            }
        }
    }
}
