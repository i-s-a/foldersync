﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace VisualDiff3
{
    class FileHistoryManager
    {
        private static readonly string FILE_PATH = @"..\..\FileHistory.txt";

        private string[] mFilenames = null;

        public FileHistoryManager()
        {
        }

        public List<string> LoadFilenames()
        {
            if (File.Exists(FILE_PATH))
            {
                mFilenames = File.ReadAllLines(FILE_PATH);
            }

            if (mFilenames != null)
                return mFilenames.ToList();
            else
                return new List<string>();
        }

        public void SaveFilenames(List<string> files)
        {
            if (files.Count > 0)
            {
                IList<string> deltaList = files;
                if (mFilenames != null)
                    deltaList = files.Except(mFilenames).ToList();

                if (File.Exists(FILE_PATH))
                {
                    File.AppendAllLines(FILE_PATH, deltaList);
                }
                else
                {
                    File.WriteAllLines(FILE_PATH, deltaList);
                }
            }
        }
    }
}
