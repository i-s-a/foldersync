﻿using System.Xml.Serialization;
using System.IO;
using System.Drawing;
using System.ComponentModel;

namespace VisualDiff
{
    public class FtpCredentials
    {
        public string Hostname
        {
            get;
            set;
        }

        public string Username
        {
            get;
            set;
        }

        [XmlIgnore]
        public string Password
        {
            get;
            set;
        }

        public int Port
        {
            get;
            set;
        }
    };

    [XmlRoot("DiffOptions")]
    public class DiffOptions
    {
        public DiffOptions()
        {
            IgnoreCase = false;
        }

        public bool IgnoreCase
        {
            get;
            set;
        }

        [XmlIgnore]
        public Color AddColor
        {
            get;
            set;
        }

        [XmlElement("AddColor")]
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        public int AddColorAsArgb
        {
            get { return AddColor.ToArgb(); }
            set { AddColor = Color.FromArgb(value); }
        }

        [XmlIgnore]
        public Color DeleteColor
        {
            get;
            set;
        }

        [XmlElement("DeleteColor")]
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        public int DeleteColorAsArgb
        {
            get { return DeleteColor.ToArgb(); }
            set { DeleteColor = Color.FromArgb(value); }
        }

        [XmlIgnore]
        public Color ReplaceColor
        {
            get;
            set;
        }

        [XmlElement("ReplaceColor")]
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        public int ReplaceColorAsArgb
        {
            get { return ReplaceColor.ToArgb(); }
            set { ReplaceColor = Color.FromArgb(value); }
        }

        public string FontName
        {
            get;
            set;
        }

        public int FontSize
        {
            get;
            set;
        }

        public FtpCredentials Ftp
        {
            get;
            set;
        }

        static public void Serialize(string filename, DiffOptions diffOptions)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(DiffOptions));
            TextWriter writer = new StreamWriter(filename);
            serializer.Serialize(writer, diffOptions);
            writer.Close();
        }

        static public DiffOptions Deserialize(string filename)
        {
            DiffOptions diffOptions = null;

            if (File.Exists(filename))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(DiffOptions));
                FileStream stream = new FileStream(filename, FileMode.Open);
                diffOptions = (DiffOptions)serializer.Deserialize(stream);
                stream.Close();
            }

            return diffOptions;
        }
    }
}
