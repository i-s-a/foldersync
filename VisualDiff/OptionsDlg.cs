﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace VisualDiff
{
    public partial class OptionsDlg : Form
    {
        public DiffOptions Options
        {
            get;
            private set;
        }

        public OptionsDlg(DiffOptions diffOptions, string whichTab)
        {
            InitializeComponent();

            Options = diffOptions;

            colorsAndFontsTab.BackColor = this.BackColor;

            FormBorderStyle = FormBorderStyle.FixedDialog;
            MinimizeBox = false;
            MaximizeBox = false;

            addColorButton.BackColor = Options.AddColor;
            deleteColorButton.BackColor = Options.DeleteColor;
            replaceColorButton.BackColor = Options.ReplaceColor;

            foreach (FontFamily fontFamily in FontFamily.Families)
            {
                fontListComboBox.Items.Add(fontFamily.Name);
            }

            fontListComboBox.SelectedItem = Options.FontName;

            for (int i = 8; i < 15; ++i)
            {
                fontSizeComboBox.Items.Add(i);
            }

            fontSizeComboBox.SelectedItem = Options.FontSize;

            if (whichTab == "FontsAndColor")
            {
                optionsTabControl.SelectTab(0);
            }
            else
            {
                throw new Exception("Invalid Tab");
            }
        }

        private void applyButton_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void addColorButton_Click(object sender, EventArgs e)
        {
            ColorDialog dlg = new ColorDialog();
            dlg.FullOpen = true;
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Options.AddColor = dlg.Color;
                addColorButton.BackColor = Options.AddColor;
            }
        }

        private void deleteColorButton_Click(object sender, EventArgs e)
        {
            ColorDialog dlg = new ColorDialog();
            dlg.FullOpen = true;
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Options.DeleteColor = dlg.Color;
                deleteColorButton.BackColor = Options.DeleteColor;
            }
        }

        private void replaceColorButton_Click(object sender, EventArgs e)
        {
            ColorDialog dlg = new ColorDialog();
            dlg.FullOpen = true;
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Options.ReplaceColor = dlg.Color;
                replaceColorButton.BackColor = Options.ReplaceColor;
            }
        }

        private void fontListComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (fontListComboBox.SelectedItem != null)
            {
                Options.FontName = (string)fontListComboBox.SelectedItem;
            }

            if (fontSizeComboBox.SelectedItem != null)
            {
                Options.FontSize = (int)fontSizeComboBox.SelectedItem;
            }

            fontSampleTextLabel.Font = new System.Drawing.Font(Options.FontName, Options.FontSize);
            fontSampleTextLabel.Text = "Font applied";
        }

        private void fontSizeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (fontListComboBox.SelectedItem != null)
            {
                Options.FontName = (string)fontListComboBox.SelectedItem;
            }

            if (fontSizeComboBox.SelectedItem != null)
            {
                Options.FontSize = (int)fontSizeComboBox.SelectedItem;
            }

            fontSampleTextLabel.Font = new System.Drawing.Font(Options.FontName, Options.FontSize);
            fontSampleTextLabel.Text = "Font applied";
        }

        private void resetButton_Click(object sender, EventArgs e)
        {
            Options.AddColor = Color.FromArgb(255, 182, 193);
            Options.DeleteColor = Color.FromArgb(190, 255, 190);
            Options.ReplaceColor = Color.FromArgb(0, 255, 255);

            addColorButton.BackColor = Options.AddColor;
            deleteColorButton.BackColor = Options.DeleteColor;
            replaceColorButton.BackColor = Options.ReplaceColor;

            //Note: Setting the SelectedItem will trigger the event for the
            //SelectedIndexChanged on the combo boxes which will set the Options
            //for the font size and font name, so we shouldn't set them here
            //directly like we do for the colors above
            fontListComboBox.SelectedItem = DefaultFont.FontFamily.Name;
            fontSizeComboBox.SelectedItem = (int)DefaultFont.Size;

            fontSampleTextLabel.Font = new System.Drawing.Font(Options.FontName, Options.FontSize);
            fontSampleTextLabel.Text = "Font applied";
        }

    }
}
