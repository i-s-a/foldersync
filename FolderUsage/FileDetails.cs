﻿namespace FolderUsage
{
    class FileDetails
    {
        public string Name { get; set; }
        public string Path { get; set; }
    }
}
