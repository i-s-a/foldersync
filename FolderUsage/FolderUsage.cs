﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Collections.Generic;
using SyncApi;

namespace FolderUsage
{
    public partial class FolderUsageFrm : Form
    {
        private MyTreeView folderTreeView = null;
        private MyListView filesListView = null;
        private MyListView topFilesListView = null;
        private MyListView fileTypeListView = null;

        private TreeNode mRootTreeNode = null;

        private FolderTreeSize mFolderTreeSize = null;
        private FolderTreeSizeNode mSelectedFolder = null;

        private const int FILE_ICON_IMAGE_INDEX = 0;
        private const int FOLDER_ICON_IMAGE_INDEX = 1;
        private const int FORWARD_ICON_IMAGE_INDEX = 2;
        private const int BACK_ICON_IMAGE_INDEX = 3;
        private const int START_ICON_IMAGE_INDEX = 4;
        private const int CANCEL_ICON_IMAGE_INDEX = 5;

        private static Font FONT = new Font("Arial", 9);

        // The column we are currently using for sorting.
        private ColumnHeader SortingColumn = null;

        private List<string> mFoldersVisited = new List<string>();
        private int mCurrentFolderIndex = 0;
        private bool mCurrentlyNavigating = false;
        private ListViewItem mSelectedListViewItem = null;

        private CancellationTokenSource mCancellationTokenSource = null;

        //TODO: Make this configurable so that the user can decide how many to display
        private int MaxNumberOfLargestFilesToDisplay = 15;

        public FolderUsageFrm()
        {
            InitializeComponent();

            InitializeViews();

            InitializeContextMenus();
        }

        private void InitializeViews()
        {
            this.folderTreeView = new MyTreeView();
            this.filesListView = new MyListView();

            this.folderTreeView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            //this.folderTreeView.ContextMenuStrip = this.contextMenuLeftSideTree;
            this.folderTreeView.Location = new System.Drawing.Point(3, 3);
            this.folderTreeView.Name = "treeView";
            this.folderTreeView.Size = new System.Drawing.Size(150, 331);
            this.folderTreeView.TabIndex = 4;
            this.folderTreeView.ImageList = Icons;
            this.folderTreeView.Font = FONT;
            //this.folderTreeView.AfterCollapse += new System.Windows.Forms.TreeViewEventHandler(this.leftTreeView_AfterCollapse);
            //this.folderTreeView.AfterExpand += new System.Windows.Forms.TreeViewEventHandler(this.leftTreeView_AfterExpand);
            this.folderTreeView.BeforeSelect += FolderTreeView_BeforeSelect;
            this.folderTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.folderTreeView_AfterSelect);

            this.filesListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.filesListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.filenameHeader,
            this.fileSizeHeader,
            this.lastModifiedHeader,
            this.filePercentageHeader});
            //this.filesListView.ContextMenuStrip = this.contextMenuLeftSideList;
            this.filesListView.FullRowSelect = true;
            this.filesListView.Location = new System.Drawing.Point(159, 3);
            this.filesListView.Name = "listView";
            this.filesListView.Size = new System.Drawing.Size(285, 331);
            this.filesListView.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.filesListView.TabIndex = 5;
            this.filesListView.UseCompatibleStateImageBehavior = false;
            this.filesListView.SmallImageList = Icons;
            this.filesListView.View = System.Windows.Forms.View.Details;
            //this.filesListView.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.leftListView_ItemSelectionChanged);
            this.filesListView.MouseDoubleClick += FilesListView_MouseDoubleClick;
            this.filesListView.MouseClick += FilesListView_MouseClick;
            this.filesListView.ColumnClick += FilesListView_ColumnClick;
            this.filesListView.ContextMenuStrip = this.contextMenuStrip;

            this.tableLayoutPanel1.Controls.Add(this.folderTreeView, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.filesListView, 1, 1);

            ////////////////////////////////////////////////////

            this.topFilesListView = new MyListView();
            this.fileTypeListView = new MyListView();

            this.topFilesListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.topFilesListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.topFilenameHeader,
            this.topFileSizeHeader,
            this.topFilePathHeader});
            //this.filesListView.ContextMenuStrip = this.contextMenuLeftSideList;
            this.topFilesListView.FullRowSelect = true;
            this.topFilesListView.Location = new System.Drawing.Point(159, 3);
            this.topFilesListView.Name = "topFileLstView";
            this.topFilesListView.Size = new System.Drawing.Size(285, 331);
            this.topFilesListView.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.topFilesListView.TabIndex = 6;
            this.topFilesListView.UseCompatibleStateImageBehavior = false;
            this.topFilesListView.SmallImageList = Icons;
            this.topFilesListView.View = System.Windows.Forms.View.Details;
            //this.topFilesListView.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.leftListView_ItemSelectionChanged);
            this.topFilesListView.MouseDoubleClick += TopFilesListView_MouseDoubleClick;
            this.topFilesListView.ColumnClick += TopFilesListView_ColumnClick;


            this.fileTypeListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fileTypeListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.fileTypeHeader,
            this.fileTypeSizeHeader,
            this.fileTypeCountHeader});
            //this.fileTypeListView.ContextMenuStrip = this.contextMenuLeftSideList;
            this.fileTypeListView.FullRowSelect = true;
            this.fileTypeListView.Location = new System.Drawing.Point(159, 3);
            this.fileTypeListView.Name = "fileTypeListView";
            this.fileTypeListView.Size = new System.Drawing.Size(285, 331);
            this.fileTypeListView.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.fileTypeListView.TabIndex = 7;
            this.fileTypeListView.UseCompatibleStateImageBehavior = false;
            this.fileTypeListView.SmallImageList = Icons;
            this.fileTypeListView.View = System.Windows.Forms.View.Details;
            //this.fileTypeListView.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.leftListView_ItemSelectionChanged);
            //this.fileTypeListView.MouseDoubleClick += FilesListView_MouseDoubleClick;
            this.fileTypeListView.ColumnClick += FileTypeListView_ColumnClick;

            this.tableLayoutPanel2.Controls.Add(this.topFilesListView, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.fileTypeListView, 0, 1);

            ////////////////////////////////////////////////////
            
            forwardButton.Enabled = false;
            backButton.Enabled = false;
        }

        private void InitializeContextMenus()
        {
            this.contextMenuStrip.Items[0].Click += FolderUsageFrm_ClickGenerateColumnChart;
            this.contextMenuStrip.Items[1].Click += FolderUsageFrm_ClickGeneratePieChart;
        }

        private void FolderUsageFrm_ClickGenerateColumnChart(object sender, EventArgs e)
        {
            if (mSelectedFolder != null)
            {
                string folderName = mSelectedListViewItem.SubItems["Filename"].Text;
                var child = FindFolderTreeSizeChildNode(mSelectedFolder, folderName);

                if (child != null)
                {
                    var folder = child as FolderTreeSizeNode;
                    if (folder != null)
                    {
                        var names = folder.Children.Select(x => x.Name).ToArray();
                        var sizes = folder.Children.Select(x => x.Size).ToArray();

                        ChartFrm chartFrm = new ChartFrm(SeriesChartType.Column, names, sizes);
                        chartFrm.Show();
                    }
                }
            }
        }

        private void FolderUsageFrm_ClickGeneratePieChart(object sender, EventArgs e)
        {
            if (mSelectedFolder != null)
            {
                string folderName = mSelectedListViewItem.SubItems["Filename"].Text;
                var child = FindFolderTreeSizeChildNode(mSelectedFolder, folderName);

                if (child != null)
                {
                    var folder = child as FolderTreeSizeNode;
                    if (folder != null)
                    {
                        var names = folder.Children.Select(x => x.Name).ToArray();
                        var sizes = folder.Children.Select(x => x.Size).ToArray();

                        ChartFrm chartFrm = new ChartFrm(SeriesChartType.Pie, names, sizes);
                        chartFrm.Show();
                    }
                }
            }
        }

        private void FilesListView_MouseClick(object sender, MouseEventArgs e)
        {
            if (mSelectedListViewItem != null)
            {
                mSelectedListViewItem.ForeColor = Color.Black;
                mSelectedListViewItem.BackColor = Color.White;
            }

            ListViewHitTestInfo info = filesListView.HitTest(e.X, e.Y);
            ListViewItem item = info.Item;

            if (item != null)
            {
                mSelectedListViewItem = item;
                string name = mSelectedListViewItem.SubItems["Filename"].Text;
                var child = FindFolderTreeSizeChildNode(mSelectedFolder, name);
                if (child != null)
                {
                    if (child is FileTreeSizeNode)
                    {
                        DisableMenuItems();
                    }
                    else if (child is FolderTreeSizeNode)
                    {
                        EnableMenuItems();
                    }
                    else
                    {
                        throw new Exception("Invalid child node");
                    }
                }
            }
        }

        private void TopFilesListView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ListViewHitTestInfo info = topFilesListView.HitTest(e.X, e.Y);
            ListViewItem item = info.Item;

            if (item != null)
            {
                string relativePath = item.SubItems["Path"].Text;
                int index = relativePath.LastIndexOf('\\');
                if (index == -1)
                {
                    string filename = relativePath;
                    string path = "";
                    SelectTreeNode(path);
                }
                else
                {
                    string filename = relativePath.Substring(index + 1);
                    string path = relativePath.Substring(0, index);
                    SelectTreeNode(path);

                    foreach (ListViewItem listViewItem in filesListView.Items)
                    {
                        if (listViewItem.Text == filename)
                        {
                            listViewItem.Selected = true;
                            listViewItem.ForeColor = Color.White;
                            listViewItem.BackColor = SystemColors.Highlight;
                            mSelectedListViewItem = listViewItem;
                            break;
                        }
                    }
                }
            }
        }

        private void FilesListView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            SortListView(filesListView, e);
        }

        private void TopFilesListView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            SortListView(topFilesListView, e);
        }

        private void FileTypeListView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            SortListView(fileTypeListView, e);
        }

        private void SortListView(MyListView myListView, ColumnClickEventArgs e)
        {
            // Get the new sorting column.
            ColumnHeader new_sorting_column = myListView.Columns[e.Column];

            // Figure out the new sorting order.
            SortOrder sort_order;
            if (SortingColumn == null)
            {
                // New column. Sort ascending.
                sort_order = SortOrder.Ascending;
            }
            else
            {
                // See if this is the same column.
                if (new_sorting_column == SortingColumn)
                {
                    // Same column. Switch the sort order.
                    if (SortingColumn.Text.StartsWith("> "))
                    {
                        sort_order = SortOrder.Descending;
                    }
                    else
                    {
                        sort_order = SortOrder.Ascending;
                    }
                }
                else
                {
                    // New column. Sort ascending.
                    sort_order = SortOrder.Ascending;
                }

                // Remove the old sort indicator.
                SortingColumn.Text = SortingColumn.Text.Substring(2);
            }

            // Display the new sort order.
            SortingColumn = new_sorting_column;
            if (sort_order == SortOrder.Ascending)
            {
                SortingColumn.Text = "> " + SortingColumn.Text;
            }
            else
            {
                SortingColumn.Text = "< " + SortingColumn.Text;
            }

            // Create a comparer.
            myListView.ListViewItemSorter = new ListViewComparer(e.Column, sort_order);

            // Sort.
            myListView.Sort();
        }

        private void FilesListView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ListViewHitTestInfo info = filesListView.HitTest(e.X, e.Y);
            ListViewItem item = info.Item;

            if (item != null)
            {
                var selectedNode = mSelectedFolder.Children.Single(x => x.Name == item.Text);
                var selectedFolderNode = selectedNode as FolderTreeSizeNode;
                if (selectedFolderNode != null)
                {
                    folderTextBox.Text = mFolderTreeSize.RootFolder + @"\" + selectedFolderNode.RelativePath;

                    SelectTreeNode(selectedFolderNode.RelativePath);
                }
            }
        }

        private void FolderTreeView_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            if (folderTreeView.SelectedNode != null)
            {
                folderTreeView.SelectedNode.BackColor = Color.White;
                folderTreeView.SelectedNode.ForeColor = Color.Black;
            }
        }

        private void folderTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            folderTreeView.SelectedNode.BackColor = SystemColors.Highlight;
            folderTreeView.SelectedNode.ForeColor = Color.White;

            StringBuilder path = new StringBuilder();
            string[] parts = e.Node.FullPath.Split('\\');

            for (int i = 1; i < parts.Length; ++i)
            {
                int index = parts[i].LastIndexOf("  ");
                if (index != -1)
                {
                    path.Append(parts[i].Substring(0, index));
                }

                if (i != parts.Length - 1)
                    path.Append("\\");
            }

            var selectedFolderNode = mFolderTreeSize.GetNode(path.ToString());
            var folderNode = selectedFolderNode as FolderTreeSizeNode;
            if (folderNode != null)
            {
                PopulateListView(folderNode);
                mSelectedFolder = folderNode;
                folderTextBox.Text = mFolderTreeSize.RootFolder + @"\" + mSelectedFolder.RelativePath;

                if (!mCurrentlyNavigating)
                {
                    if (mFoldersVisited.Count > 0)
                        mFoldersVisited.RemoveRange(mCurrentFolderIndex + 1, (mFoldersVisited.Count - 1) - mCurrentFolderIndex);

                    mFoldersVisited.Add(mSelectedFolder.RelativePath);
                    mCurrentFolderIndex = mFoldersVisited.Count - 1;
                }

                SetForwardAndBackButtons();

                //folderTreeView.SelectedNode.BackColor = SystemColors.Highlight;
                //folderTreeView.SelectedNode.ForeColor = Color.White;
            }
        }

        private async void runButton_Click(object sender, EventArgs e)
        {
            if (mCancellationTokenSource == null)
            {
                if (folderTextBox.Text.Length == 0)
                    MessageBox.Show("Need to select a folder");
                else
                    await CalculateFolderTreeSize(folderTextBox.Text);
            }
            else
            {
                mCancellationTokenSource.Cancel();
            }
        }

        private async Task CalculateFolderTreeSize(string rootFolder)
        {
            try
            {
                TurnOnProgressBar();
                runButton.ImageIndex = CANCEL_ICON_IMAGE_INDEX;

                mCurrentFolderIndex = 0;
                mFoldersVisited.Clear();

                folderTreeView.Nodes.Clear();
                filesListView.Items.Clear();
                topFilesListView.Items.Clear();
                fileTypeListView.Items.Clear();

                mCancellationTokenSource = new CancellationTokenSource();

                FolderTreeSizeNode rootTreeSizeNode = null;

                mFolderTreeSize = new FolderTreeSize(rootFolder);
                rootTreeSizeNode = await mFolderTreeSize.Process(mCancellationTokenSource.Token);

                mRootTreeNode = CreateTreeNode(mFolderTreeSize.RootFolderName, mFolderTreeSize.RootFolderName, FOLDER_ICON_IMAGE_INDEX);
                folderTreeView.Nodes.Add(mRootTreeNode);

                PopulateTreeView(mRootTreeNode, rootTreeSizeNode);

                folderTreeView.SelectedNode = mRootTreeNode;

                PopulateFileStatsListViews(rootTreeSizeNode, MaxNumberOfLargestFilesToDisplay);

                TurnOffProgressBar();
                runButton.ImageIndex = START_ICON_IMAGE_INDEX;

                SetForwardAndBackButtons();
            }
            catch(OperationCanceledException ex)
            {
                TurnOffProgressBar();
                runButton.ImageIndex = START_ICON_IMAGE_INDEX;
                MessageBox.Show(ex.Message, "Folder usage cancelled");
            }
            finally
            {
                ResetCancellationTokenSource();
            }
        }

        private void PopulateTreeView(TreeNode parentNode, FolderTreeSizeNode parentFolderNode)
        {
            var folders = parentFolderNode
                .Children
                .Where(x => x is FolderTreeSizeNode);

            long totalFolderTreeSize = parentFolderNode.Children.Sum(x => x.Size);

            foreach (var folder in folders)
            {
                double percentage = 0;

                if (totalFolderTreeSize != 0)
                    percentage = ((double)folder.Size / (double)totalFolderTreeSize);// * 100;

                string percent = percentage.ToString("0.000%");

                TreeNode treeNode = CreateTreeNode(folder.Name, $"{folder.Name}  ({percent})", FOLDER_ICON_IMAGE_INDEX);
                parentNode.Nodes.Add(treeNode);
                PopulateTreeView(treeNode, folder as FolderTreeSizeNode);
            }
        }

        private void PopulateListView(FolderTreeSizeNode folderTreeSizeNode)
        {
            filesListView.Items.Clear();

            long totalFolderTreeSize = folderTreeSizeNode.Children.Sum(x => x.Size);

            var files = folderTreeSizeNode
                .Children
                .Where(x => x is FileTreeSizeNode)
                .OrderBy(y => y.RelativePath);

            var folders = folderTreeSizeNode
                .Children
                .Where(x => x is FolderTreeSizeNode)
                .OrderBy(y => y.RelativePath);

            foreach (var file in files)
            {
                double percentage = 0;

                if (totalFolderTreeSize != 0)
                    percentage = ((double)file.Size / (double)totalFolderTreeSize) * 100;

                string percent = percentage.ToString("0.000");

                ListViewItem listViewItem = CreateListViewItem(
                    file.Name, file.Size.ToString(), file.LastWriteTime.ToString(), percent, FILE_ICON_IMAGE_INDEX);

                filesListView.Items.Add(listViewItem);
            }

            foreach (var folder in folders)
            {
                double percentage = 0;

                if (totalFolderTreeSize != 0)
                    percentage = ((double)folder.Size / (double)totalFolderTreeSize) * 100;

                string percent = percentage.ToString("0.000");

                ListViewItem listViewItem = CreateListViewItem(
                    folder.Name, folder.Size.ToString(), folder.LastWriteTime.ToString(), percent, FOLDER_ICON_IMAGE_INDEX);

                filesListView.Items.Add(listViewItem);
            }
        }

        private void PopulateFileStatsListViews(FolderTreeSizeNode parentFolderNode, int maxNumberOfFiles)
        {
            FileStats fileStats = new FileStats();

            RetrieveFileStats(parentFolderNode, fileStats, maxNumberOfFiles);

            topFilesListView.Items.Clear();
            fileTypeListView.Items.Clear();

            foreach(var topFile in fileStats.TopFiles)
            {
                foreach(var item in topFile.Value)
                {
                    var listViewItem = CreateTopFileListViewItem(item.Name, topFile.Key.ToString(), item.Path, FILE_ICON_IMAGE_INDEX);
                    topFilesListView.Items.Add(listViewItem);
                }
            }

            foreach(var fileType in fileStats.FileTypes)
            {
                var listViewItem = CreateFileTypeListViewItem(fileType.Key, fileType.Value.Size.ToString(), fileType.Value.Count.ToString(), -1);
                fileTypeListView.Items.Add(listViewItem);
            }

            fileStats = null;
        }


        private void RetrieveFileStats(FolderTreeSizeNode parentFolderNode, FileStats fileStats, int maxNumberOfFiles)
        {
            var childFiles = parentFolderNode.Children.Where(x => x is FileTreeSizeNode);

            foreach (var file in childFiles)
            {
                if (fileStats.TopFiles.Count < maxNumberOfFiles)
                {
                    AddToTopFileList(fileStats.TopFiles, file as FileTreeSizeNode);
                }
                else
                {
                    if (file.Size > fileStats.TopFiles.ElementAt(0).Key)
                    {
                        fileStats.TopFiles.RemoveAt(0);
                        AddToTopFileList(fileStats.TopFiles, file as FileTreeSizeNode);
                    }
                }

                string fileExt = GetFileExtension(file.Name);
                if (fileStats.FileTypes.ContainsKey(fileExt))
                {
                    ++fileStats.FileTypes[fileExt].Count;
                    fileStats.FileTypes[fileExt].Size += file.Size;
                }
                else
                {
                    fileStats.FileTypes.Add(fileExt, new FileTypeDetails()
                    {
                        Count = 1,
                        Size = file.Size
                    });
                }
            }

            var childFolders = parentFolderNode
                .Children
                .Where(x => x is FolderTreeSizeNode);

            foreach (var folder in childFolders)
            {
                RetrieveFileStats(folder as FolderTreeSizeNode, fileStats, maxNumberOfFiles);
            }
        }

        private void AddToTopFileList(SortedList<long, List<FileDetails>> topFiles, FileTreeSizeNode fileTreeSizeNode)
        {
            int index = topFiles.IndexOfKey(fileTreeSizeNode.Size);
            if (index == -1)
            {
                var list = new List<FileDetails>();
                list.Add(new FileDetails()
                {
                    Name = fileTreeSizeNode.Name,
                    Path = fileTreeSizeNode.RelativePath
                });

                topFiles.Add(fileTreeSizeNode.Size, list);
            }
            else
            {
                topFiles.ElementAt(index).Value.Add(new FileDetails()
                {
                    Name = fileTreeSizeNode.Name,
                    Path = fileTreeSizeNode.RelativePath
                });
            }
        }

        private string GetFileExtension(string filename)
        {
            int index = filename.LastIndexOf('.');
            if (index == -1)
                return "[[empty]]";

            return filename.Substring(index + 1);
        }

        static private TreeNode CreateTreeNode(string name, string text, int imageIndex)
        {
            TreeNode treeNode = new TreeNode(name);
            treeNode.Name = name;
            treeNode.Text = text;
            treeNode.NodeFont = FONT;
            treeNode.ForeColor = Color.Black;
            treeNode.ImageIndex = imageIndex;
            treeNode.SelectedImageIndex = imageIndex;
            return treeNode;
        }

        static private ListViewItem CreateListViewItem(string name, string size, string lastModified, string percentage, int imageIndex)
        {
            ListViewItem listViewItem = null;

            ListViewItem.ListViewSubItem[] listViewSubItems = new ListViewItem.ListViewSubItem[4];

            listViewSubItems[0] = new ListViewItem.ListViewSubItem();
            listViewSubItems[0].Name = "Filename";
            listViewSubItems[0].Text = name;
            listViewSubItems[0].ForeColor = Color.Black;
            listViewSubItems[0].BackColor = Color.White;
            listViewSubItems[0].Font = FONT;

            listViewSubItems[1] = new ListViewItem.ListViewSubItem();
            listViewSubItems[1].Name = "Size";
            listViewSubItems[1].Text = size;
            listViewSubItems[1].ForeColor = Color.Black;
            listViewSubItems[1].BackColor = Color.White;
            listViewSubItems[1].Font = FONT;

            listViewSubItems[2] = new ListViewItem.ListViewSubItem();
            listViewSubItems[2].Name = "LastModified";
            listViewSubItems[2].Text = lastModified;
            listViewSubItems[2].ForeColor = Color.Black;
            listViewSubItems[2].BackColor = Color.White;
            listViewSubItems[2].Font = FONT;

            listViewSubItems[3] = new ListViewItem.ListViewSubItem();
            listViewSubItems[3].Name = "Percentage";
            listViewSubItems[3].Text = percentage;
            listViewSubItems[3].ForeColor = Color.Black;
            listViewSubItems[3].BackColor = Color.White;
            listViewSubItems[3].Font = FONT;

            listViewItem = new ListViewItem(listViewSubItems, imageIndex);

            //listViewItem = new ListViewItem(new string[] { name, size, lastModified, percentage }, imageIndex, Color.Black, Color.White, FONT);

            return listViewItem;
        }

        static private ListViewItem CreateTopFileListViewItem(string name, string size, string path, int imageIndex)
        {
            ListViewItem listViewItem = null;

            ListViewItem.ListViewSubItem[] listViewSubItems = new ListViewItem.ListViewSubItem[3];

            listViewSubItems[0] = new ListViewItem.ListViewSubItem();
            listViewSubItems[0].Name = "Filename";
            listViewSubItems[0].Text = name;
            listViewSubItems[0].ForeColor = Color.Black;
            listViewSubItems[0].BackColor = Color.White;
            listViewSubItems[0].Font = FONT;

            listViewSubItems[1] = new ListViewItem.ListViewSubItem();
            listViewSubItems[1].Name = "Size";
            listViewSubItems[1].Text = size;
            listViewSubItems[1].ForeColor = Color.Black;
            listViewSubItems[1].BackColor = Color.White;
            listViewSubItems[1].Font = FONT;

            listViewSubItems[2] = new ListViewItem.ListViewSubItem();
            listViewSubItems[2].Name = "Path";
            listViewSubItems[2].Text = path;
            listViewSubItems[2].ForeColor = Color.Black;
            listViewSubItems[2].BackColor = Color.White;
            listViewSubItems[2].Font = FONT;

            listViewItem = new ListViewItem(listViewSubItems, imageIndex);

            //listViewItem = new ListViewItem(new string[] { name, size, path}, imageIndex, Color.Black, Color.White, FONT);

            return listViewItem;
        }

        static private ListViewItem CreateFileTypeListViewItem(string fileType, string size, string count, int imageIndex)
        {
            ListViewItem listViewItem = null;

            listViewItem = new ListViewItem(new string[] { fileType, size, count }, imageIndex, Color.Black, Color.White, FONT);

            return listViewItem;
        }

        private void TurnOnProgressBar()
        {
            DisableControls();

            FolderAnalyserProgressBar.Visible = true;
            FolderAnalyserProgressBar.Style = ProgressBarStyle.Marquee;
            FolderAnalyserProgressBar.MarqueeAnimationSpeed = 3;
        }

        private void TurnOffProgressBar()
        {
            FolderAnalyserProgressBar.Visible = false;

            EnableControls();
        }

        private void EnableControls()
        {
            folderTextBox.Enabled = true;
            browseFolderButton.Enabled = true;
            forwardButton.Enabled = true;
            backButton.Enabled = true;
            //runButton.Enabled = true;
        }

        private void DisableControls()
        {
            folderTextBox.Enabled = false;
            browseFolderButton.Enabled = false;
            forwardButton.Enabled = false;
            backButton.Enabled = false;
            //runButton.Enabled = false;
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            mCurrentlyNavigating = true;
            --mCurrentFolderIndex;
            SetForwardAndBackButtons();
            SelectTreeNode(mFoldersVisited[mCurrentFolderIndex]);
            mCurrentlyNavigating = false;
        }

        private void forwardButton_Click(object sender, EventArgs e)
        {
            mCurrentlyNavigating = true;
            ++mCurrentFolderIndex;
            SetForwardAndBackButtons();
            SelectTreeNode(mFoldersVisited[mCurrentFolderIndex]);
            mCurrentlyNavigating = false;
        }

        private void SetForwardAndBackButtons()
        {
            SetForwardButton();
            SetBackButton();
        }

        private void SetForwardButton()
        {
            if (mCurrentFolderIndex == mFoldersVisited.Count - 1)
                forwardButton.Enabled = false;
            else
                forwardButton.Enabled = true;
        }

        private void SetBackButton()
        {
            if (mCurrentFolderIndex == 0)
                backButton.Enabled = false;
            else
                backButton.Enabled = true;
        }

        private TreeNode FindTreeNode(TreeNode rootNode, string path)
        {
            if (path.Length == 0)
                return mRootTreeNode;

            TreeNode parent = rootNode;
            TreeNode child = null;
            string[] hierarchy = path.Split('\\');

            for (int i = 0; i < hierarchy.Length; ++i)
            {
                //We expect to find the node, if not then the two trees have gone out
                //of sync somehow. Single will throw an exception if the item is not
                //found (or there is more than one item but that should never happen
                //either)
                child = parent.Nodes.Cast<TreeNode>().Single(x => x.Name == hierarchy[i]);

                parent = child;
            }

            return parent;
        }

        private void SelectTreeNode(string path)
        {
            TreeNode treeNode = FindTreeNode(mRootTreeNode, path);
            folderTreeView.SelectedNode = treeNode;
        }

        private IFolderTreeSizeChildNode FindFolderTreeSizeChildNode(FolderTreeSizeNode parent, string childName)
        {
            return parent.Children.SingleOrDefault(x => x.Name == childName);
        }

        private void EnableMenuItems()
        {
            contextMenuStrip.Items[0].Enabled = true;
            contextMenuStrip.Items[1].Enabled = true;
        }

        private void DisableMenuItems()
        {
            contextMenuStrip.Items[0].Enabled = false;
            contextMenuStrip.Items[1].Enabled = false;
        }

        private void ResetCancellationTokenSource()
        {
            mCancellationTokenSource.Dispose();
            mCancellationTokenSource = null;
        }

        private void browseFolderButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog browserDialog = new FolderBrowserDialog();

            browserDialog.ShowNewFolderButton = false;
            browserDialog.RootFolder = System.Environment.SpecialFolder.MyComputer;
            DialogResult dr = browserDialog.ShowDialog();

            if (dr == DialogResult.OK)
            {
                folderTextBox.Text = browserDialog.SelectedPath;
            }
        }
    }

}
