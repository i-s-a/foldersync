﻿using System.Collections.Generic;

namespace FolderUsage
{
    class FileStats
    {
        public FileStats()
        {
            TopFiles = new SortedList<long, List<FileDetails>>();
            FileTypes = new Dictionary<string, FileTypeDetails>();
        }
        public SortedList<long, List<FileDetails>> TopFiles { get; set; }
        public Dictionary<string, FileTypeDetails> FileTypes { get; set; }
    }
}
