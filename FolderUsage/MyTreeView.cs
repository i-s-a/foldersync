﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace FolderUsage
{
    class MyTreeView : TreeView
    {
        private const int WM_HSCROLL = 276; // Horizontal scroll
        private const int WM_VSCROLL = 277; // Vertical scroll
        private const int SB_HORZ = 0;  //Horizontal scroll bar
        private const int SB_VERT = 1;  //Vertical scroll bar
        private const int SB_LINEUP = 0; // Scrolls one line up
        private const int SB_LINELEFT = 0;// Scrolls one cell left
        private const int SB_LINEDOWN = 1; // Scrolls one line down
        private const int SB_LINERIGHT = 1;// Scrolls one cell right
        private const int SB_PAGEUP = 2; // Scrolls one page up
        private const int SB_PAGELEFT = 2;// Scrolls one page left
        private const int SB_PAGEDOWN = 3; // Scrolls one page down
        private const int SB_PAGERIGHT = 3; // Scrolls one page right
        private const int SB_LEFT = 6; // Scrolls to the left
        private const int SB_RIGHT = 7; // Scrolls to the right
        private const int SB_TOP = 6; // Scrolls to the upper left
        private const int SB_BOTTOM = 7; // Scrolls to the upper right
        private const int SB_ENDSCROLL = 8; // Ends scroll

        private const int TVM_SETEXTENDEDSTYLE = 0x1100 + 44;
        private const int TVM_GETEXTENDEDSTYLE = 0x1100 + 45;
        private const int TVS_EX_DOUBLEBUFFER = 0x0004;

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        //private static extern int SendMessage(IntPtr hWnd, int wMsg, UIntPtr wParam, IntPtr lParam);
        private static extern int SendMessage(IntPtr hWnd, int wMsg, int wParam, int lParam);

        [DllImport("user32.dll")]
        static extern int SetScrollPos(IntPtr hWnd, int nBar, int nPos, bool bRedraw);

        [DllImport("user32.dll")]
        public static extern int GetScrollPos(IntPtr hwnd, int nBar);

        [DllImport("User32.dll", EntryPoint = "PostMessageA")]
        static extern bool PostMessage(IntPtr hWnd, uint msg, UIntPtr wParam, IntPtr lParam);

        //Delegate and event for when vertical scrolling takes place
        public delegate void VScrollEventHandler(int pos);
        public event VScrollEventHandler VScroll;

        //Delegate and event for when vertical scrolling takes place
        public delegate void HScrollEventHandler(int pos);
        public event HScrollEventHandler HScroll;

        public MyTreeView() : base()
        {

        }

        public void ScrollToVPos(int pos)
        {
            //Scroll to a specific position (scrolling to 'pos' will be the same as scrolling to a line)
            SetScrollPos(Handle, SB_VERT, pos, true);
            SendMessage(Handle, WM_VSCROLL, 4 + 0x10000 * pos, 0);
        }

        public void ScrollToHPos(int pos)
        {
            SetScrollPos(Handle, SB_HORZ, pos, true);
            SendMessage(Handle, WM_HSCROLL, 4 + 0x10000 * pos, 0);
        }

        public int GetVScrollPos()
        {
            return GetScrollPos(Handle, SB_VERT);
        }

        public int GetHScrollPos()
        {
            return GetScrollPos(Handle, SB_HORZ);
        }

        private void VScrollChanged(int pos)
        {
            if (VScroll != null)
            {
                VScroll(pos);
            }
        }

        private void HScrollChanged(int pos)
        {
            if (HScroll != null)
            {
                HScroll(pos);
            }
        }

        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);

            if (m.Msg == WM_VSCROLL)
            {
                VScrollChanged(GetVScrollPos());
            }
            else if (m.Msg == WM_HSCROLL)
            {
                HScrollChanged(GetHScrollPos());
            }
        }

        //To support double-buffering and avoid the tree view flickering
        //See here: https://stackoverflow.com/questions/10362988/treeview-flickering
        protected override void OnHandleCreated(EventArgs e)
        {
            SendMessage(this.Handle, TVM_SETEXTENDEDSTYLE, TVS_EX_DOUBLEBUFFER, TVS_EX_DOUBLEBUFFER);
            base.OnHandleCreated(e);
        }
    }
}
