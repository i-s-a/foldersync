﻿namespace FolderUsage
{
    class FileTypeDetails
    {
        public long Count { get; set; } = default;
        public long Size { get; set; } = default;

    }
}
