﻿using System;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace FolderUsage
{
    public partial class ChartFrm : Form
    {
        private string[] mNames = null;
        private long[] mData = null;
        private SeriesChartType mSeriesChartType = default;

        public ChartFrm()
        {
            InitializeComponent();
        }

        public ChartFrm(SeriesChartType seriesChartType, string[]names, long []data)
        {
            if (names.Length != data.Length)
                throw new ArgumentException("Names and data must be the same length");

            InitializeComponent();

            mNames = names;
            mData = data;
            mSeriesChartType = seriesChartType;
        }

        //https://stackoverflow.com/questions/34104484/how-to-build-a-pie-chart-based-on-int-values
        //https://www.aspsnippets.com/Articles/Create-Bar-Chart-in-Windows-Forms-Application-using-C-and-VBNet.aspx
        //https://www.c-sharpcorner.com/UploadFile/1e050f/chart-control-in-windows-form-application/
        private void ChartFrm_Load(object sender, EventArgs e)
        {
            if (mSeriesChartType == SeriesChartType.Column)
            {
                DisplayColumnChart();
            }
            else if (mSeriesChartType == SeriesChartType.Pie)
            {
                DisplayPieChart();
            }
            else
            {
                throw new ArgumentException("Chart type not supported");
            }
        }

        private void DisplayColumnChart()
        {
            chart1.Series[0].ChartType = SeriesChartType.Column;
            chart1.Series[0].LegendText = "Folder";
            chart1.Series[0].IsValueShownAsLabel = true;
            chart1.Series[0].Points.DataBindXY(mNames, mData);
            chart1.ChartAreas[0].Area3DStyle.Enable3D = true;
        }

        private void DisplayPieChart()
        {
            chart1.Series[0].ChartType = SeriesChartType.Pie;
            chart1.Series[0].Points.DataBindXY(mNames, mData);
            chart1.Legends[0].Enabled = true;
            chart1.ChartAreas[0].Area3DStyle.Enable3D = true;
        }
    }
}
