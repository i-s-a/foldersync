This is mainly an experimental project to learn and practice C#. It has its roots in an older project I wrote back around 2007/2008 in C++ and MFC, which I use for syncing important folders to my external hard drive and USB stick. This is a complete rewrite in C# and WinForms. This is still a work-in-progress as I'm still doing a lot of testing on it and there are some features that are available in the settings dialog that have not yet been implemented.

This project is made up of several sub-projects

* Folder Sync - This is the main application and it allows a user to compare 2 folder structures, displaying the differences between them, and allowing the user to synchronize the folders

* Visual Diff - This allows a user to compare 2 files, displaying the differences between them. The project can be run as an application on its own, but is also used by the Folder Sync project to allow the user to compare 2 files when looking at the differences between folder structures. When used on its own the files to be compared can be selected from any drive that can be mounted on your local hardrive, from a zip archive, Microsoft OneDrive or FTP Server (proxy settings are not yet supported). Note that the Microsoft OneDrive and FTP Server support are the most recent additions and are still being tested. When comparing files, obviously you can compare any file type that is formatted as text, but it can also extract text and compare the text from .doc, .docx, .pptx, .xlsx, .xps and .pdf files.

* Folder Usage - This allows a user to view details about a folder. Details displayed include the size of each file and sub-folder recursively, the percentage of space each file and sub-folder is using, the top 10 largest files and how much space files with particular extensions are using up.

* DocumentReader - This a component that can be used to extract text from from files formatted as text, .doc, .docx, .pptx, .xlsx, .xps and .pdf files. This is currently used by the Visual Diff tool to extract text from the supported formats when comparing 2 files.

* Visual Tree - This is a visual component that can display the folder structure of an FTP Server, Microsoft OneDrive account or an zip archive, and it allows the user to select a file from the folder structure to download. This allows a unified view of a folder structure independent of the target and can easily be extended to support other targets in the future. This is currently used by the Visual Diff tool when a user wants to select and compare a file from FTP Server, Microsoft OneDrive or an zip archive. Note that to be able to access Microsoft OneDrive you need a hotmail account. You will need to log on to Microsoft's Azure Portal using your account to create a new registration for the application granting the application permission to access the OneDrive account. This will create a Client ID which you can use when accessing OneDrive. The Client ID must be assigned to the _clientId in the AuthenticationHelper class (Updating this to use a config file is on my TODO list). You can log on to Azure and remove the permissions at anytime if you no longer want the application to access your account. The following link describes the process

https://www.emoreau.com/Entries/Articles/2018/03/Using-OneDrive-from-your-Net-Windows-Forms-application.aspx

* SyncApi - This is the main API that can be used to compare 2 folder structures; sync 2 folder structures; sync a selected sub-tree of 2 folder structures; return statistics about a folder structure. This is used by the main Folder Sync application and the Folder Usage application. The API supports both one-way syncing and two-syncing. Options can be provided to the API describing how to compare folder structures (e.g. comparing any combination of last modified time, file sizes, file content or only comparing files with specific extensions, etc.). Syncing can be performed in one of two ways: Either direct syncing. or logged syncing. Logged syncing will keep track of the changes and if anything goes wrong during the syncing process (e.g. power failure) then the next time you run the application you can choose to continue the sync or rollback. Direct syncing doesn't do any logging and therefore if something goes wrong then it will be left in whatever state it was in at the point of the failure. Direct syncing is quicker than logged syncing but it doesn't offer you the option of continuing or rolling back the sync if somthing goes wrong.


Some screenshots of the projects above can be found in the Screenshots folder. The UI for each project is very simple. It is not as full-featured as commercial products that are available as it only really includes various features and settings that I found personally useful for my own use.

Dependencies

* ScinitllaNET - This is the control that displays the text in Visual Diff
* OpenXML - This is used in the DocumentReader to extract text from a docx, xlsx and pptx
* PDFSharp - This is used in the DocumentReader to extract text from a pdf
* SharpZipLib - This is used in the VisualTree to access files in a ZIP archive
* FluentFTP - This is used in the VisualTree to access an FTP server
* Microsoft Identity and Graph - This is used in the VisualTree to access Microsoft OneDrive
* ZetaLongPaths - This is used generally to support path names longer than 255 characters
* DiffUtilEx - This is my implementation in C++ of the diff algorithm described in http://www.xmailserver.org/diff2.pdf.  This is used to identify the diffs between 2 files that are then displayed in Visual Diff. This is the original implementation in C++ for the original project which I haven't yet ported to C# and so for now it is accessed from the C# project as a DLL using P/Invoke. Porting this to C# is on my TODO list, though as a low priority

