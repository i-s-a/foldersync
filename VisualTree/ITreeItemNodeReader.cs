﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DocumentReader;

namespace VisualTree
{
    public interface ITreeItemNodeReader : IDisposable
    {
        void Connect();
        Task ConnectAsync();
        void Disconnect();
        Task DisconnectAsync();
        TreeItemNode ReadTree();
        Task<TreeItemNode> ReadTreeAsync();
        List<TreeItemNode> ReadList(string path);
        Task<List<TreeItemNode>> ReadListAsync(string path);
        string RetrieveFile(string path, string localFilepath);
        Task<string> RetrieveFileAsync(string path, string localFilepath);
    }
}
