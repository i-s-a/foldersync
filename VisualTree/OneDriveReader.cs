﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Graph;
using System.IO;
using DocumentReader;

namespace VisualTree
{
    public class OneDriveReader : ITreeItemNodeReader
    {
        public OneDriveReader()
        {
        }

        public void Connect()
        {
            var task = AuthenticationHelper.GetAuthenticatedClient();
            task.Wait();
            _graphClient = task.Result;
        }

        public async Task ConnectAsync()
        {
            //TODO
            //Change AuthenticationHelper to accept the _clientId. Also
            //we are currently hardcoding the tenancy as "common", perhaps
            //pass this in as a parameter as well
            _graphClient = await AuthenticationHelper.GetAuthenticatedClient();
        }

        public List<TreeItemNode> ReadList(string path)
        {
            throw new NotImplementedException();
        }

        public async Task<List<TreeItemNode>> ReadListAsync(string path)
        {
            if (_graphClient == null)
            {
                await ConnectAsync();
            }

            DriveItem folder = null;

            if (string.IsNullOrEmpty(path))
            {
                folder = await _graphClient.Drive.Root.Request().Expand("thumbnails,children($expand=thumbnails)").GetAsync();
                return ReadListFromDriveItem(folder, path);
            }
            else
            {
                string id = null;
                _driveItemIdCache.TryGetValue(path, out id);
                if (id != default)
                {
                    string expandString = "thumbnails,children($expand=thumbnails)";

                    folder = await _graphClient.Drive.Items[id].Request().Expand(expandString).GetAsync();

                    if (folder != null)
                    {
                        if (folder.Folder != null && folder.Children != null && folder.Children.CurrentPage != null)
                        {
                            return ReadListFromDriveItem(folder, path);
                        }
                    }
                }
            }

            return null;
        }

        private List<TreeItemNode> ReadListFromDriveItem(DriveItem folder, string path)
        {
            string curPath = path;

            List<TreeItemNode> treeItemNodeList = new List<TreeItemNode>();
            foreach (var child in folder.Children.CurrentPage)
            {
                TreeItemNode childNode = null;

                if (child.Folder != null)
                {
                    childNode = new TreeItemNode()
                    {
                        FullPath = curPath + "/" + child.Name,
                        Name = child.Name,
                        IsFolder = true,
                        Parent = null,
                        Size = child.Size.Value,
                        LastModified = child.LastModifiedDateTime.Value.DateTime,
                        Children = new HashSet<TreeItemNode>()
                    };

                    if (!_driveItemIdCache.ContainsKey(childNode.FullPath))
                        _driveItemIdCache.Add(childNode.FullPath, child.Id);

                }
                else if (child.File != null)
                {
                    childNode = new TreeItemNode()
                    {
                        FullPath = curPath + "/" + child.Name,
                        Name = child.Name,
                        IsFolder = false,
                        Parent = null,
                        Size = child.Size.Value,
                        LastModified = child.LastModifiedDateTime.Value.DateTime,
                        Children = null
                    };

                    if (!_driveItemIdCache.ContainsKey(childNode.FullPath))
                        _driveItemIdCache.Add(childNode.FullPath, child.Id);
                }

                treeItemNodeList.Add(childNode);
            }

            return treeItemNodeList;
        }

        public TreeItemNode ReadTree()
        {
            throw new NotImplementedException();
        }

        public async Task<TreeItemNode> ReadTreeAsync()
        {
            TreeItemNode rootTreeNode = null;

            if (_graphClient == null)
            {
                await ConnectAsync();
            }

            rootTreeNode = new TreeItemNode()
            {
                FullPath = "../",
                Name = "..",
                IsFolder = true,
                Parent = null,
                Children = new HashSet<TreeItemNode>()
            };

            string curPath = rootTreeNode.FullPath;

            DriveItem folder = await _graphClient.Drive.Root.Request().Expand("thumbnails,children($expand=thumbnails)").GetAsync();
            if (folder != null)
            {
                if (folder.Folder != null && folder.Children != null && folder.Children.CurrentPage != null)
                {
                    //_driveItemCache.Add(rootTreeNode.FullPath, folder.Children.CurrentPage[0].las);

                    foreach (var child in folder.Children.CurrentPage)
                    {
                        TreeItemNode childNode = null;

                        if (child.Folder != null)
                        {
                            childNode = new TreeItemNode()
                            {
                                FullPath = curPath + child.Name + "/",
                                Name = child.Name,
                                IsFolder = true,
                                Parent = rootTreeNode,
                                Size = child.Size.Value,
                                LastModified = child.LastModifiedDateTime.Value.DateTime,
                                Children = new HashSet<TreeItemNode>()
                            };

                            rootTreeNode.Children.Add(childNode);

                            _driveItemIdCache.Add(childNode.FullPath, child.Id);

                            await ReadTreeAsync(childNode, child.Id);
                        }
                        else if (child.File != null)
                        {
                            childNode = new TreeItemNode()
                            {
                                FullPath = curPath + child.Name,
                                Name = child.Name,
                                IsFolder = false,
                                Parent = rootTreeNode,
                                Size = child.Size.Value,
                                LastModified = child.LastModifiedDateTime.Value.DateTime,
                                Children = null
                            };

                            rootTreeNode.Children.Add(childNode);

                            _driveItemIdCache.Add(childNode.FullPath, child.Id);
                        }
                    }
                }
            }

            return rootTreeNode;
        }

        public string RetrieveFile(string path, string localFilepath)
        {
            throw new NotImplementedException();
        }

        public async Task<string> RetrieveFileAsync(string path, string localFilepath)
        {
            string id = null;
            _driveItemIdCache.TryGetValue(path, out id);
            if (id == default)
            {
                throw new Exception("Invalid path requested");
            }

            string filename = path;
            int index = path.LastIndexOf('/');
            if (index != -1)
            {
                filename = path.Substring(index + 1);
            }

            string downloadedFilePath = localFilepath + filename;

            using (var stream = await _graphClient.Drive.Items[id].Content.Request().GetAsync())
            {
                using (var outputFileStream = new FileStream(downloadedFilePath, FileMode.Create))
                {
                    await stream.CopyToAsync(outputFileStream);
                }
            }

            return downloadedFilePath;
        }

        private async Task ReadTreeAsync(TreeItemNode parentNode, string id, bool recursive = true)
        {
            string expandString = "thumbnails,children($expand=thumbnails)";
            
            var folder = await _graphClient.Drive.Items[id].Request().Expand(expandString).GetAsync();
            if (folder != null)
            {
                if (folder.Folder != null && folder.Children != null && folder.Children.CurrentPage != null)
                {
                    string curPath = parentNode.FullPath;

                    foreach (var child in folder.Children.CurrentPage)
                    {
                        TreeItemNode childNode = null;

                        if (child.Folder != null)
                        {
                            childNode = new TreeItemNode()
                            {
                                FullPath = curPath + child.Name + "/",
                                Name = child.Name,
                                IsFolder = true,
                                Parent = parentNode,
                                Size = child.Size.Value,
                                LastModified = child.LastModifiedDateTime.Value.DateTime,
                                Children = new HashSet<TreeItemNode>()
                            };

                            parentNode.Children.Add(childNode);

                            _driveItemIdCache.Add(childNode.FullPath, child.Id);

                            await ReadTreeAsync(childNode, child.Id);

                        }
                        else if (child.File != null)
                        {
                            childNode = new TreeItemNode()
                            {
                                FullPath = curPath + child.Name,
                                Name = child.Name,
                                IsFolder = false,
                                Parent = parentNode,
                                Size = child.Size.Value,
                                LastModified = child.LastModifiedDateTime.Value.DateTime,
                                Children = null
                            };

                            parentNode.Children.Add(childNode);

                            _driveItemIdCache.Add(childNode.FullPath, child.Id);
                        }
                    }
                }
            }
        }

        public void Disconnect()
        {
            AuthenticationHelper.SignOut().Wait();
        }

        public async Task DisconnectAsync()
        {
            await AuthenticationHelper.SignOut();
        }

        public void Dispose()
        {
            Disconnect();
        }

        private GraphServiceClient _graphClient = null;
        private Dictionary<string, string> _driveItemIdCache = new Dictionary<string, string>();
    }
}
