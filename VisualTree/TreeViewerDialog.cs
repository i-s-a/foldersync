﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DocumentReader;

namespace VisualTree
{
    public partial class TreeViewerDlg : Form
    {
        private ITreeItemNodeReader mReader = null;
        private string mRootName = null;
        List<TreeItemNode> mCurrentTreeNodeList = null;
        HashSet<string> mLoadedTreeNodeCache = new HashSet<string>();

        private const int ICON_FILE_INDEX = 0;
        private const int ICON_FOLDER_INDEX = 1;

        public TreeViewerDlg(ITreeItemNodeReader reader, string rootPath)
        {
            InitializeComponent();

            FormBorderStyle = FormBorderStyle.FixedDialog;

            Initialize(reader, rootPath);
        }

        private void Initialize(ITreeItemNodeReader reader, string rootPath)
        {
            mRootName = rootPath;
            int index = mRootName.LastIndexOf('\\');
            if (index != -1)
            {
                mRootName = mRootName.Substring(index + 1);
            }

            mReader = reader;
        }

        public string SelectedPath
        {
            get;
            set;
        }

        public string SelectedFile
        {
            get
            {
                string filePath = null;
                if (FilenameListView.SelectedItems.Count > 0)
                {
                    filePath = SelectedPath + "/" + FilenameListView.SelectedItems[0].Text;
                }

                return filePath;
            }
        }

        private void TreeViewerDlg_Load(object sender, EventArgs e)
        {
            folderTreeView.Nodes.Add(mRootName, mRootName, ICON_FOLDER_INDEX);
            folderTreeView.SelectedImageIndex = ICON_FOLDER_INDEX;
            mCurrentTreeNodeList = new List<TreeItemNode>();
            LoadTreeNodes(folderTreeView.Nodes[0], mCurrentTreeNodeList);
        }

        private void LoadTree(TreeItemNode rootNode)
        {
            foreach (var node in rootNode.Children.OrderBy(x => !x.IsFolder))
            {
                TreeNode treeNode = folderTreeView.Nodes.Add(node.Name, node.Name, ICON_FOLDER_INDEX);

                if (node.IsFolder)
                    LoadTreeNode(treeNode, node);
            }
        }

        private void LoadTreeNode(TreeNode parentTreeNode, TreeItemNode parentTreeItemNode)
        {
            foreach (var node in parentTreeItemNode.Children.OrderBy(x => !x.IsFolder))
            {
                TreeNode treeNode = parentTreeNode.Nodes.Add(node.Name, node.Name, ICON_FOLDER_INDEX);
                if (node.IsFolder)
                    LoadTreeNode(treeNode, node);
            }
        }

        private void LoadTreeNodes(TreeNode parentTreeNode, List<TreeItemNode> treeItemNodeList)
        {
            foreach (var node in treeItemNodeList)
            {
                if (node.IsFolder)
                    parentTreeNode.Nodes.Add(node.Name, node.Name, ICON_FOLDER_INDEX);
            }
        }

        private void LoadTreeNodesRoot(List<TreeItemNode> treeItemNodeList)
        {
            foreach (var node in treeItemNodeList)
            {
                folderTreeView.Nodes.Add(node.Name, node.Name, ICON_FOLDER_INDEX);
            }
        }

        private void LoadFilenames(List<TreeItemNode> treeItemNodeList)
        {
            foreach (var item in treeItemNodeList)
            {
                if (!item.IsFolder)
                {
                    FilenameListView.Items.Add(new ListViewItem(new string[] { item.Name, item.Size.ToString(), item.LastModified.ToString() }, ICON_FILE_INDEX));
                }
            }
        }

        private async void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            TurnOnProgressBar();

            try
            {
                FilenameListView.Items.Clear();

                SelectedPath = e.Node.FullPath;
                mCurrentTreeNodeList = await mReader.ReadListAsync(SelectedPath);
                if (mCurrentTreeNodeList != null)
                {
                    if (mCurrentTreeNodeList.Count == 1)
                    {
                        if (mCurrentTreeNodeList[0].IsFolder && mCurrentTreeNodeList[0].Children.Count > 0)
                        {
                            if (!mLoadedTreeNodeCache.Contains(SelectedPath))
                            {
                                LoadTreeNodes(e.Node, mCurrentTreeNodeList);
                                mLoadedTreeNodeCache.Add(SelectedPath);
                            }

                            e.Node.Expand();
                            folderTreeView.SelectedImageIndex = ICON_FOLDER_INDEX;
                        }
                    }
                    else
                    {
                        if (!mLoadedTreeNodeCache.Contains(SelectedPath))
                        {
                            LoadTreeNodes(e.Node, mCurrentTreeNodeList);
                            mLoadedTreeNodeCache.Add(SelectedPath);
                        }

                        e.Node.Expand();
                        folderTreeView.SelectedImageIndex = ICON_FOLDER_INDEX;
                    }

                    LoadFilenames(mCurrentTreeNodeList);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
                DialogResult = System.Windows.Forms.DialogResult.Abort;
                Close();
            }
            finally
            {
                TurnOffProgressBar();
            }
        }

        private void TurnOnProgressBar()
        {
            progressBar.Visible = true;
            progressBar.Style = ProgressBarStyle.Marquee;
            progressBar.MarqueeAnimationSpeed = 3;
        }

        private void TurnOffProgressBar()
        {
            progressBar.Visible = false;
        }

        //private void ExpandNode(string fullPath)
        //{
        //    TreeNodeCollection nodes = folderTreeView.Nodes;
        //    TreeNode[] childNodes = null;

        //    string[] hierarchy = fullPath.Split('\\');
        //    foreach (var part in hierarchy)
        //    {
        //        childNodes = nodes.Find(part, false);
        //        if (childNodes.Length == 1)
        //        {
        //            childNodes[0].Expand();
        //            nodes = childNodes[0].Nodes;
        //        }
        //        else
        //        {
        //            MessageBox.Show("Path error");
        //        }
        //    }

        //    if (childNodes != null && childNodes.Length == 1)
        //    {
        //        folderTreeView.SelectedNode = childNodes[0];
        //    }
        //}

        private void selectButton_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }

        private void TreeViewerDlg_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void FilenameListView_DoubleClick(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }
    }
}
