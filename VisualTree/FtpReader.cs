﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FluentFTP;
using DocumentReader;

namespace VisualTree
{
    public class FtpReader : ITreeItemNodeReader
    {
        private string mHostname;
        private string mUsername;
        private string mPassword;
        private FtpClient mFtpClient = null;
        private string mFtpPath = null;

        public FtpReader(string hostname, string username, string password, string ftpPath = "")
        {
            mHostname = hostname;
            mUsername = username;
            mPassword = password;
            mFtpPath = ftpPath;

            mFtpClient = new FtpClient();
            mFtpClient.Host = mHostname;
            mFtpClient.Credentials = new System.Net.NetworkCredential(mUsername, mPassword);
        }

        public void Connect()
        {
            mFtpClient.Connect();
        }

        public async Task ConnectAsync()
        {
            await mFtpClient.ConnectAsync();
        }

        public TreeItemNode ReadTree()
        {
            TreeItemNode rootTreeNode = null;

            if (!mFtpClient.IsConnected)
            {
                mFtpClient.Connect();
            }

            rootTreeNode = new TreeItemNode()
            {
                FullPath = "..",
                Name = "..",
                IsFolder = true,
                Parent = null,
                Children = new HashSet<TreeItemNode>()
            };

            ReadTree(rootTreeNode, mFtpPath);

            return rootTreeNode;
        }

        public async Task<TreeItemNode> ReadTreeAsync()
        {
            TreeItemNode rootTreeNode = null;

            if (!mFtpClient.IsConnected)
            {
                await ConnectAsync();
            }

            rootTreeNode = new TreeItemNode()
            {
                FullPath = "..",
                Name = "..",
                IsFolder = true,
                Parent = null,
                Children = new HashSet<TreeItemNode>()
            };

            await ReadTreeAsync(rootTreeNode, mFtpPath);

            return rootTreeNode;
        }

        public List<TreeItemNode> ReadList(string path)
        {
            List<TreeItemNode> treeItemNodeList = new List<TreeItemNode>();

            if (!mFtpClient.IsConnected)
            {
                Connect();
            }

            path = path.Replace('\\', '/');

            string name = path;

            int index = path.LastIndexOf('/');

            if (index != -1)
                name = path.Substring(index + 1);

            //path = mFtpPath + path;

            string curPath = path;

            FtpListItem[] files = mFtpClient.GetListing(path, FtpListOption.AllFiles);

            foreach (var file in files)
            {
                TreeItemNode childNode = null;
                if (file.Type == FtpFileSystemObjectType.Directory)
                {
                    childNode = new TreeItemNode()
                    {
                        FullPath = file.FullName, //curPath + file.Name + "/",
                        Name = file.Name,
                        IsFolder = true,
                        Parent = null,
                        Size = file.Size,
                        LastModified = file.Modified,
                        Children = new HashSet<TreeItemNode>()
                    };

                    treeItemNodeList.Add(childNode);
                }
                else if (file.Type == FtpFileSystemObjectType.File)
                {
                    childNode = new TreeItemNode()
                    {
                        FullPath = file.FullName, //curPath + file.Name,
                        Name = file.Name,
                        IsFolder = false,
                        Parent = null,
                        Size = file.Size,
                        LastModified = file.Modified,
                        Children = null
                    };

                    treeItemNodeList.Add(childNode);
                }
                //else
                //    ;
            }

            return treeItemNodeList;
        }

        public async Task<List<TreeItemNode>> ReadListAsync(string path)
        {
            List<TreeItemNode> treeItemNodeList = new List<TreeItemNode>();

            if (!mFtpClient.IsConnected)
            {
                await ConnectAsync();
            }

            path = path.Replace('\\', '/');

            string name = path;

            int index = path.LastIndexOf('/');

            if (index != -1)
                name = path.Substring(index + 1);

            //path = mFtpPath + path;
            
            string curPath = path;

            FtpListItem[] files = await mFtpClient.GetListingAsync(path, FtpListOption.AllFiles);

            foreach (var file in files)
            {
                TreeItemNode childNode = null;
                if (file.Type == FtpFileSystemObjectType.Directory)
                {
                    childNode = new TreeItemNode()
                    {
                        FullPath = file.FullName, //curPath + file.Name + "/",
                        Name = file.Name,
                        IsFolder = true,
                        Parent = null,
                        Size = file.Size,
                        LastModified = file.Modified,
                        Children = new HashSet<TreeItemNode>()
                    };

                    treeItemNodeList.Add(childNode);
                }
                else if (file.Type == FtpFileSystemObjectType.File)
                {
                    childNode = new TreeItemNode()
                    {
                        FullPath = file.FullName, //curPath + file.Name,
                        Name = file.Name,
                        IsFolder = false,
                        Parent = null,
                        Size = file.Size,
                        LastModified = file.Modified,
                        Children = null
                    };

                    treeItemNodeList.Add(childNode);
                }
                //else
                //    ;
            }

            return treeItemNodeList;
        }

        private void ReadTree(TreeItemNode parentNode, string path, bool recursive = true)
        {
            FtpListItem[] files = mFtpClient.GetListing(path, FtpListOption.AllFiles);

            string curPath = path;

            foreach (var file in files)
            {
                TreeItemNode childNode = null;
                if (file.Type == FtpFileSystemObjectType.Directory)
                {
                    childNode = new TreeItemNode()
                    {
                        FullPath = curPath + file.Name + "/",
                        Name = file.Name,
                        IsFolder = true,
                        Parent = parentNode,
                        Size = file.Size,
                        LastModified = file.Modified,
                        Children = new HashSet<TreeItemNode>()
                    };

                    parentNode.Children.Add(childNode);

                    if (recursive)
                        ReadTree(childNode, curPath + file.Name + "/");
                }
                else if (file.Type == FtpFileSystemObjectType.File)
                {
                    childNode = new TreeItemNode()
                    {
                        FullPath = curPath + file.Name,
                        Name = file.Name,
                        IsFolder = false,
                        Parent = parentNode,
                        Size = file.Size,
                        LastModified = file.Modified,
                        Children = null
                    };

                    parentNode.Children.Add(childNode);
                }
                //else
                //    ;

            }
        }

        private async Task ReadTreeAsync(TreeItemNode parentNode, string path, bool recursive = true)
        {
            FtpListItem[] files = await mFtpClient.GetListingAsync(path, FtpListOption.AllFiles);

            string curPath = path;

            foreach (var file in files)
            {
                TreeItemNode childNode = null;
                if (file.Type == FtpFileSystemObjectType.Directory)
                {
                    childNode = new TreeItemNode()
                    {
                        FullPath = curPath + file.Name + "/",
                        Name = file.Name,
                        IsFolder = true,
                        Parent = parentNode,
                        Size = file.Size,
                        LastModified = file.Modified,
                        Children = new HashSet<TreeItemNode>()
                    };

                    parentNode.Children.Add(childNode);

                    if (recursive)
                        await ReadTreeAsync(childNode, curPath + file.Name + "/");
                }
                else if (file.Type == FtpFileSystemObjectType.File)
                {
                    childNode = new TreeItemNode()
                    {
                        FullPath = curPath + file.Name,
                        Name = file.Name,
                        IsFolder = false,
                        Parent = parentNode,
                        Size = file.Size,
                        LastModified = file.Modified,
                        Children = null
                    };

                    parentNode.Children.Add(childNode);
                }
                //else
                //    ;

            }
        }

        public string RetrieveFile(string path, string localFilepath)
        {
            if (!mFtpClient.IsConnected)
            {
                Connect();
            }

            System.IO.Stream reader = null;
            System.IO.Stream writer = null;
            string downloadedFilePath = null;

            try
            {
                string filename = path;
                int index = path.LastIndexOf('/');
                if (index != -1)
                {
                    filename = path.Substring(index + 1);
                }

                reader = mFtpClient.OpenRead(path);
                writer = new System.IO.FileStream(localFilepath + filename, System.IO.FileMode.Create);

                //TODO
                //Hardcoded 64K for now make this configurable
                const int bufferSize = 1024 * 64;
                byte[] buffer = new byte[bufferSize];
                int bytesRead = reader.Read(buffer, 0, bufferSize);
                while (bytesRead > 0)
                {
                    writer.Write(buffer, 0, bytesRead);
                    bytesRead = reader.Read(buffer, 0, bufferSize);
                }

                downloadedFilePath = localFilepath + filename;
            }
            finally
            {
                if (reader != null)
                    reader.Close();

                if (writer != null)
                    writer.Close();
            }

            return downloadedFilePath;
        }

        public async Task<string> RetrieveFileAsync(string path, string localFilepath)
        {
            if (!mFtpClient.IsConnected)
            {
                await ConnectAsync();
            }

            System.IO.Stream reader = null;
            System.IO.Stream writer = null;
            string downloadedFilePath = null;

            try
            {
                string filename = path;
                int index = path.LastIndexOf('/');
                if (index != -1)
                {
                    filename = path.Substring(index + 1);
                }

                reader = await mFtpClient.OpenReadAsync(path);
                writer = new System.IO.FileStream(localFilepath + filename, System.IO.FileMode.Create);

                //TODO
                //Hardcoded 64K for now make this configurable
                const int bufferSize = 1024 * 64;
                byte[] buffer = new byte[bufferSize];
                int bytesRead = await reader.ReadAsync(buffer, 0, bufferSize);
                while (bytesRead > 0)
                {
                    await writer.WriteAsync(buffer, 0, bytesRead);
                    bytesRead = await reader.ReadAsync(buffer, 0, bufferSize);
                }

                downloadedFilePath = localFilepath + filename;
            }
            finally
            {
                if (reader != null)
                    reader.Close();

                if (writer != null)
                    writer.Close();
            }

            return downloadedFilePath;
        }

        public void Disconnect()
        {
            if (mFtpClient.IsConnected)
            {
                mFtpClient.Disconnect();
            }
        }

        public async Task DisconnectAsync()
        {
            if (mFtpClient.IsConnected)
            {
                await mFtpClient.DisconnectAsync();
            }
        }

        public void Dispose()
        {
            Disconnect();
        }
    }
}
