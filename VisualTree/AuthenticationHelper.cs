﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Net.Http.Headers;
using Microsoft.Graph;
using Microsoft.Identity.Client;

namespace VisualTree
{
    public class AuthenticationHelper
    {
        // The Client ID is used by the application to uniquely identify itself to the v2.0 authentication endpoint.
        private static string _clientId = "YOU_NEED_TO_PUT_YOUR_OWN_KEY_HERE";
        private static GraphServiceClient _graphClient;

        public static string[] Scopes = { "Files.ReadWrite.All" };
        public static IPublicClientApplication IdentityClientApp = 
            PublicClientApplicationBuilder.Create(_clientId)
            .WithAuthority(AzureCloudInstance.AzurePublic, "common")
            .Build();

        public static string TokenForUser;
        public static DateTimeOffset Expiration;

        // Get an access token for the given context and resourceId. An attempt is first made to 
        // acquire the token silently. If that fails, then we try to acquire the token by prompting the user.
        public static Task<GraphServiceClient> GetAuthenticatedClient()
        {
            if (_graphClient == null)
            {
                // Create Microsoft Graph client.
                try
                {
                    _graphClient = new GraphServiceClient(
                        "https://graph.microsoft.com/v1.0",
                        new DelegateAuthenticationProvider(
                            async (requestMessage) =>
                            {
                                var token = await GetTokenForUserAsync();
                                requestMessage.Headers.Authorization = new AuthenticationHeaderValue("bearer", token);
                            }));
                    return Task.FromResult(_graphClient);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Could not create a graph client: " + ex.Message);
                    throw;
                }
            }

            return Task.FromResult(_graphClient);
        }

        public static async Task<string> GetTokenForUserAsync()
        {
            AuthenticationResult authResult;
            IEnumerable<IAccount> accounts = null;
            IAccount firstAccount = null;

            try
            {
                accounts = await IdentityClientApp.GetAccountsAsync();
                firstAccount = accounts.FirstOrDefault();

                //First try to acquire the token silently
                authResult = await IdentityClientApp.AcquireTokenSilent(Scopes, firstAccount).ExecuteAsync();
                TokenForUser = authResult.AccessToken;
                Expiration = authResult.ExpiresOn;
            }
            catch(MsalUiRequiredException /*ex*/)
            {
                //Now try to acquire the token interactively
                try
                {
                    authResult = await IdentityClientApp.AcquireTokenInteractive(Scopes)
                        .WithAccount(firstAccount)
                        .WithPrompt(Prompt.SelectAccount)
                        .ExecuteAsync();

                    TokenForUser = authResult.AccessToken;
                    Expiration = authResult.ExpiresOn;
                }
                catch(MsalException /*ex2*/)
                {
                    throw;
                }
                catch(Exception /*ex2*/)
                {
                    throw;
                }
            }
            catch(Exception /*ex*/)
            {
                throw;
            }

            return TokenForUser;
        }

        public static async Task SignOut()
        {
            try
            {
                var accounts = await IdentityClientApp.GetAccountsAsync();
                if (accounts.Any())
                {
                    await IdentityClientApp.RemoveAsync(accounts.FirstOrDefault());
                    _graphClient = null;
                    TokenForUser = null;
                }
            }
            catch (MsalException /*ex*/)
            {
                throw;
            }
        }

    }
}
