﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DocumentReader;

namespace VisualTree
{
    public class ZipArchiveReader : ITreeItemNodeReader
    {
        private TreeItemNode mRootTreeNode = null;
        private string mZipFilename;

        public ZipArchiveReader(string zipFilename)
        {
            mZipFilename = zipFilename;
        }

        public TreeItemNode ReadTree()
        {
            if (mRootTreeNode == null)
                mRootTreeNode = ArchiveManager.BuildTreeFromZipArchive(mZipFilename);

            return mRootTreeNode;
        }

        public List<TreeItemNode> ReadList(string path)
        {
            List<TreeItemNode> treeItemNodeList = new List<TreeItemNode>();

            if (mRootTreeNode == null)
                mRootTreeNode = ArchiveManager.BuildTreeFromZipArchive(mZipFilename);

            string[] parts = path.Split('/');

            TreeItemNode curNode = mRootTreeNode;
            for (int i = 1; i < parts.Length; ++i)
            {
                string part = parts[i];
                curNode = curNode.Children.SingleOrDefault(x => x.Name == part);
                if (curNode == null)
                    throw new Exception("Invalid path");
            }

            if (curNode.Children != null)
                treeItemNodeList = curNode.Children.ToList();

            return treeItemNodeList;
        }

        public string RetrieveFile(string path, string localFilePath)
        {
            return ArchiveManager.ExtractFile(mZipFilename, path, localFilePath);
        }

        public void Connect()
        {
        }

        public Task ConnectAsync()
        {
            return Task.Delay(0);
        }

        public Task<TreeItemNode> ReadTreeAsync()
        {
            return Task.FromResult<TreeItemNode>(ReadTree());
        }

        public Task<List<TreeItemNode>> ReadListAsync(string path)
        {
            return Task.FromResult<List<TreeItemNode>>(ReadList(path));
        }

        public Task<string> RetrieveFileAsync(string path, string localFilepath)
        {
            return Task.FromResult<string>(RetrieveFile(path, localFilepath));
        }

        public void Disconnect()
        {
            throw new NotImplementedException();
        }

        public Task DisconnectAsync()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
        }

    }
}
