﻿namespace DocumentReader
{
    public interface IDocumentReader
    {
        string ReadContent();
        string[] ReadLines();
    }
}
