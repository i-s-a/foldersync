﻿using System;
using System.Text;
using System.Runtime.InteropServices.ComTypes;

//Prior to Word 2007, Word used a structured storage format to store documents in binary format.
//This code is adapted from the description of extracting text from a .doc file found in
//http://b2xtranslator.sourceforge.net/howtos/How_to_retrieve_text_from_a_binary_doc_file.pdf

namespace DocumentReader
{
    internal class StgTextExtractor
    {
        //When we read the piece descriptor this mask is used to determine
        //whether the text in the piece descriptor is unicode or not. NOTE
        //that each piece descriptors within the document can be either
        //unicode or ASCII, they don't all have to be the same within the
        //same document
        private static readonly uint UnicodeMask = 0x40000000;

        private string mFilename;

        public StgTextExtractor(string filename)
        {
            mFilename = filename;
        }

        public string ExtractText()
        {
            IStorage storage = OpenStorage(mFilename);
            IStream wordStream = OpenStream(storage, "WordDocument");
            UInt32 pieceTablePos = 0;
            UInt32 pieceTableLength = 0;
            
            //Read the table stream name (i.e. 0Table or 1Table) and the peice table position and length
            string tableName = GetTableStreamName(wordStream, out pieceTablePos, out pieceTableLength);
            
            //Open the table stream and use the piece table position and length to read the piece table
            IStream tableStream = OpenStream(storage, tableName);
            byte[] pieceTable = ReadPieceTable(tableStream, pieceTablePos, pieceTableLength);
            
            //Extract the text from the piece table
            return ReadText(wordStream, pieceTable);
        }

        private IStorage OpenStorage(string path)
        {
            IStorage storage = null;
            Guid IID_IStorage = new Guid("0000000b-0000-0000-C000-000000000046"); //The IStorage IID
            int hr = NativeMethods.StgOpenStorageEx(path, (int)STGMFlags.STGM_READ | (int)STGMFlags.STGM_SHARE_DENY_WRITE, (int)STGFMTFlags.STGFMT_STORAGE, 0, IntPtr.Zero, IntPtr.Zero, ref IID_IStorage, out storage);
            if (hr != 0 || storage == null)
            {
                Exception ex = NativeMethods.GetExceptionForHr(hr);
                throw ex;
            }

            return storage;
        }

        private IStream OpenStream(IStorage storage, string name)
        {
            IStream stream = null;
            int hr = storage.OpenStream(name, IntPtr.Zero, (int)STGMFlags.STGM_READ | (int)STGMFlags.STGM_SHARE_EXCLUSIVE, 0, out stream);
            if (hr != 0 || stream == null)
            {
                Exception ex = NativeMethods.GetExceptionForHr(hr);
                throw ex;
            }

            return stream;
        }

        private string GetTableStreamName(IStream wordStream, out UInt32 pieceTablePos, out UInt32 pieceTableLength)
        {
            //The fib is a maximum size of 1472 (some older versions of Word it could be smaller).
            //The flag needed to determine whether we need the 0Table stream or the 1Table stream
            //is located at position 0xA (10). The offset and length we need from the fib are located
            //at 0x01A2 and 0x01A6 (418 and 422 respecively). The fib should be at the beginning of
            //the word stream so we can just seek to the correct position from  the start of the stream
            string tableName = "0Table";

            wordStream.Seek(10, (int)STGM_STREAM_SEEK.STREAM_SEEK_SET, IntPtr.Zero);
            byte[] flags = new byte[2];
            wordStream.Read(flags, 2, IntPtr.Zero);
            UInt16 flag = BitConverter.ToUInt16(flags, 0);
            if ((flag & 0x200) == 0x200)
            {
                tableName = "1Table";
            }

            wordStream.Seek(0x01A2, (int)STGM_STREAM_SEEK.STREAM_SEEK_SET, IntPtr.Zero);
            byte[] buf = new byte[4];
            wordStream.Read(buf, 4, IntPtr.Zero);
            pieceTablePos = BitConverter.ToUInt32(buf, 0);
            wordStream.Read(buf, 4, IntPtr.Zero);
            pieceTableLength = BitConverter.ToUInt32(buf, 0);

            return tableName;
        }

        private byte[] ReadPieceTable(IStream tableStream, UInt32 pieceTablePos, UInt32 pieceTableLength)
        {
            byte[] pieceTable = null;
            byte[] clx = new byte[pieceTableLength];

            tableStream.Seek(pieceTablePos, (int)STGM_STREAM_SEEK.STREAM_SEEK_SET, IntPtr.Zero);
            tableStream.Read(clx, (int)pieceTableLength, IntPtr.Zero);

            int pos = 0;
            bool goOn = true;
            while (goOn)
            {
                byte type = clx[pos];
                if (type == 2)
                {
                    goOn = false;
                    int sizeOfPieceTable = BitConverter.ToInt32(clx, pos + 1);
                    pieceTable = new byte[sizeOfPieceTable];
                    Array.Copy(clx, pos + 5, pieceTable, 0, sizeOfPieceTable);
                }
                else if (type == 1)
                {
                    pos = pos + 1 + 1 + clx[pos + 1];
                }
                else
                {
                    goOn = false;
                }
            }

            if (pieceTable == null)
            {
                throw new Exception("Failed to find piece table");
            }

            return pieceTable;
        }

        private string ReadText(IStream wordDocStream, byte[] pieceTable)
        {
            StringBuilder text = new StringBuilder();

            //uint UnicodeMask = (uint)0x40000000;
            int pieceCount = (pieceTable.Length - 4) / 12;
            for (int i = 0; i < pieceCount; ++i)
            {
                int start = BitConverter.ToInt32(pieceTable, i * 4);
                int end = BitConverter.ToInt32(pieceTable, (i + 1) * 4);
                //The offset for the piece descriptor is at
                //  ((pieceCount + 1) * 4) + (i * 8))
                //The fcValue we need starts at the 3rd byte in the piece
                //descriptor, hence we then need to + 2 to the offset
                int offset = (((pieceCount + 1) * 4) + (i * 8)) + 2;
                uint fcValue = BitConverter.ToUInt32(pieceTable, offset);
                bool isUnicode = ((fcValue & UnicodeMask) == 0);
                uint fc = isUnicode ? fcValue : (fcValue & ~UnicodeMask) / 2;
                uint startPos = fc;
                int cb = end - start;

                //Encoding encoding = Encoding.GetEncoding(1252);
                if (isUnicode)
                {
                    cb *= 2;
                    //encoding = Encoding.Unicode;
                }

                uint endPos = startPos + (uint)cb;

                //Console.WriteLine("{0} {1} {2} {3}", fc, startPos, endPos, endPos - startPos);

                int length = cb;
                byte[] buf = new byte[length];

                wordDocStream.Seek(startPos, (int)STGM_STREAM_SEEK.STREAM_SEEK_SET, IntPtr.Zero);
                wordDocStream.Read(buf, length, IntPtr.Zero);

                //text.Append(encoding.GetString(buf));

                if (isUnicode)
                {
                    char ch;
                    for (int j = 0; j < length - 1; j += 2)
                    {
                        ch = BitConverter.ToChar(buf, j);
                        text.Append(ch);
                    }
                }
                else
                {
                    char ch;
                    for (int j = 0; j < length; ++j)
                    {
                        ch = (char)buf[j];
                        text.Append(ch);
                    }
                }
            }

            return text.ToString();
        }
    }
}
