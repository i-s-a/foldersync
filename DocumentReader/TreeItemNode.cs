﻿using System;
using System.Collections.Generic;

namespace DocumentReader
{
    public class TreeItemNode
    {
        public TreeItemNode Parent { get; set; }
        public string FullPath { get; set; }
        public string Name { get; set; }
        public DateTime LastModified { get; set; }
        public long Size { get; set; }
        public bool IsFolder { get; set; }
        public HashSet<TreeItemNode> Children { get; set; }
    }
}
