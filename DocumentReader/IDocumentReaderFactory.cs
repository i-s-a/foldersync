﻿namespace DocumentReader
{
    public interface IDocumentReaderFactory
    {
        IDocumentReader CreateDocumentReader(string filename);
    }
}
