﻿using System;
using System.Collections.Generic;
using System.Text;
using PdfSharp.Pdf;
using PdfSharp.Pdf.Content;
using PdfSharp.Pdf.Content.Objects;
using PdfSharp.Pdf.IO;

namespace DocumentReader
{
    public class PdfSharpPdfReader : IDocumentReader
    {
        private string mFilename;

        public PdfSharpPdfReader(string filename)
        {
            mFilename = filename;
        }

        public string ReadContent()
        {
            return ExtractText(mFilename);
        }

        public string[] ReadLines()
        {
            return ExtractText(mFilename).Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
        }

        public string ExtractText(string filename)
        {
            using (var _document = PdfSharp.Pdf.IO.PdfReader.Open(filename, PdfDocumentOpenMode.ReadOnly))
            {
                var content = new StringBuilder();
                //foreach (var page in _document.Pages.OfType<PdfPage>())
                foreach (PdfPage page in _document.Pages)
                {
                    var pageObject = ContentReader.ReadContent(page);
                    var pageLineList = ExtractTextFromCObject(pageObject);
                    foreach (var line in pageLineList)
                    {
                        content.Append(line);
                    }

                    content.Append(Environment.NewLine);
                }

                return content.ToString();
            }
        }

        public IEnumerable<string> ExtractTextFromCObject(CObject cObject)
        {
            List<string> lineList = new List<string>();

            if (cObject is COperator)
            {
                var cOperator = cObject as COperator;
                if (cOperator.OpCode.Name == OpCodeName.Tj.ToString() ||
                    cOperator.OpCode.Name == OpCodeName.TJ.ToString())
                {
                    foreach (var cOperand in cOperator.Operands)
                    {
                        lineList.AddRange(ExtractTextFromCObject(cOperand));
                    }
                }
            }
            else if (cObject is CSequence)
            {
                var cSequence = cObject as CSequence;
                foreach (var element in cSequence)
                {
                    lineList.AddRange(ExtractTextFromCObject(element));
                }
            }
            else if (cObject is CString)
            {
                var cString = cObject as CString;

                //Really don't want to do this 'if' check because it means we won't support
                //unicode characters but leave it like this for now so that we don't get
                //strange characters in the output text. Need to figure out how to properly
                //filter the strange characters so that we only get valid text!!
                if (((cString.Value[0] >= ' ') && (cString.Value[0] <= '~')) ||
                    ((cString.Value[0] >= 128) && (cString.Value[0] < 255)))
                {
                    lineList.Add(cString.Value);
                }
            }

            return lineList;
        }
    }
}
