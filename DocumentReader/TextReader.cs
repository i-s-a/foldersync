﻿using System.IO;

namespace DocumentReader
{
    public class TextReader : IDocumentReader
    {
        private string mFilename;

        public TextReader(string filename)
        {
            mFilename = filename;
        }

        public string ReadContent()
        {
            return File.ReadAllText(mFilename);
        }

        public string[] ReadLines()
        {
            return File.ReadAllLines(mFilename);
        }
    }
}
