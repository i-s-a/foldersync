﻿using System;
using System.Linq;
using System.Text;
using System.IO;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace DocumentReader
{
    public class OpenXmlExcelReader : IDocumentReader
    {
        private string mFilename;

        public OpenXmlExcelReader(string filename)
        {
            mFilename = filename;
        }

        public string ReadContent()
        {
            return ExtractText(mFilename);
        }

        public string[] ReadLines()
        {
            return ExtractText(mFilename).Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
        }

        private string ExtractText(string filename)
        {
            //return ExtractTextDom(filename);
            return ExtractTextSax(filename);
        }

        private string ExtractTextDom(string filename)
        {
            StringBuilder text = new StringBuilder();

            using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(fs, false))
                {
                    WorkbookPart workbookPart = spreadsheetDocument.WorkbookPart;
                    SharedStringTablePart sstpart = workbookPart.GetPartsOfType<SharedStringTablePart>().First();
                    SharedStringTable sst = sstpart.SharedStringTable;

                    //Loop over each worksheet part
                    foreach (WorksheetPart worksheetPart in workbookPart.WorksheetParts)
                    {
                        SheetData sheetData = worksheetPart.Worksheet.Elements<SheetData>().First();

                        string cellText;
                        foreach (Row r in sheetData.Elements<Row>())
                        {
                            foreach (Cell c in r.Elements<Cell>())
                            {
                                if ((c.DataType != null) && (c.DataType == CellValues.SharedString))
                                {
                                    int ssid = int.Parse(c.CellValue.Text);
                                    cellText = sst.ChildElements[ssid].InnerText;
                                    text.Append(cellText);
                                    text.Append(" | ");
                                }
                                else if (c.CellValue != null)
                                {
                                    cellText = c.CellValue.Text;
                                    text.Append(cellText);
                                    text.Append(" | ");
                                }
                            }

                            text.Append(Environment.NewLine);
                        }
                    }
                }
            }

            return text.ToString();
        }

        private string ExtractTextSax(string filename)
        {
            StringBuilder text = new StringBuilder();

            using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(fs, false))
                {
                    WorkbookPart workbookPart = spreadsheetDocument.WorkbookPart;

                    foreach (WorksheetPart worksheetPart in workbookPart.WorksheetParts)
                    {
                        OpenXmlReader reader = OpenXmlReader.Create(worksheetPart);

                        while (reader.Read())
                        {
                            if (reader.ElementType == typeof(Row))
                            {
                                reader.ReadFirstChild();

                                do
                                {
                                    if (reader.ElementType == typeof(Cell))
                                    {
                                        Cell c = (Cell)reader.LoadCurrentElement();

                                        string cellText;

                                        if (c.DataType != null && c.DataType == CellValues.SharedString)
                                        {
                                            int ssid = int.Parse(c.CellValue.InnerText);
                                            SharedStringItem ssi = workbookPart.SharedStringTablePart.SharedStringTable.Elements<SharedStringItem>().ElementAt(ssid);

                                            cellText = ssi.Text.Text;
                                            text.Append(cellText);
                                            text.Append(" | ");
                                        }
                                        else if (c.CellValue != null)
                                        {
                                            cellText = c.CellValue.InnerText;
                                            text.Append(cellText);
                                            text.Append(" | ");
                                        }
                                    }
                                } while (reader.ReadNextSibling());

                                text.Append(Environment.NewLine);
                            }
                        }
                    }
                }
            }

            return text.ToString();
        }
    }
}
