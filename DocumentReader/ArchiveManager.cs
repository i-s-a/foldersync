﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ICSharpCode.SharpZipLib.Zip;

namespace DocumentReader
{
    public class ZipException : Exception
    {
        public ZipException()
        {
        }

        public ZipException(string message)
            : base(message)
        {
        }

        public ZipException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }

    public class ArchiveManager
    {
        private const int BUFFER_SIZE = 1024 * 64;

        public static void ExtractZipFileContent(string zipFilename, string extractRootPath, bool overwriteFileIfExists = true)
        {
            ZipFile zipFile = new ZipFile(zipFilename);

            foreach (ZipEntry entry in zipFile)
            {
                if (entry.IsDirectory)
                {
                    CreateFolderHierarchy(extractRootPath, entry.Name);
                }
                else if (entry.IsFile)
                {

                    //Sometimes a file with a folder hierarchy (e.g. folder1/folder2/test.txt)
                    //comes through as a file entry and we don't get a separate entry for the
                    //folders (i.e. we don't hit the "if (entry.IsDirectory above). Therefore
                    //we need to ensure that the folder hierarchy for a file exists before we
                    //extract the file
                    string folder = Path.GetDirectoryName(entry.Name);
                    if (!string.IsNullOrEmpty(folder))
                    {
                        CreateFolderHierarchy(extractRootPath, folder);
                    }

                    //If the file doesn't exist, or we've been asked to overwrite the file if it does exist
                    //then decompress the zip entry to a file on the file system
                    if (!File.Exists(extractRootPath + entry.Name) || overwriteFileIfExists)
                    {
                        using (Stream reader = zipFile.GetInputStream(entry))
                        {
                            using (Stream writer = new FileStream(extractRootPath + entry.Name, FileMode.Create))
                            {
                                byte[] buffer = new byte[BUFFER_SIZE];
                                int bytesRead = reader.Read(buffer, 0, BUFFER_SIZE);
                                while (bytesRead > 0)
                                {
                                    writer.Write(buffer, 0, bytesRead);
                                    bytesRead = reader.Read(buffer, 0, BUFFER_SIZE);
                                }
                            }
                        }
                    }
                }
            }

            zipFile.Close();
        }

        public static TreeItemNode BuildTreeFromZipArchive(string zipFilename)
        {
            string rootPath = zipFilename + '/';

            int rootIndex = zipFilename.LastIndexOf('\\');
            if (rootIndex != -1)
            {
                rootPath = zipFilename.Substring(rootIndex + 1);
            }

            TreeItemNode rootTreeNode = new TreeItemNode()
            {
                FullPath = rootPath + '/',
                Name = rootPath,
                Size = 0,
                Children = new HashSet<TreeItemNode>()
            };

            TreeItemNode curNode = null;

            ZipFile zipFile = new ZipFile(zipFilename);

            foreach (ZipEntry entry in zipFile)
            {
                curNode = rootTreeNode;

                if (entry.IsDirectory)
                {
                    string folder = entry.Name;
                    int index = folder.LastIndexOf('/');
                    if (index != -1)
                    {
                        folder = folder.Substring(0, index);
                    }

                    string curPath = "";
                    if (folder.Length > 0)
                    {
                        string[] parts = folder.Split('/');

                        foreach (var part in parts)
                        {
                            curPath += part + "/";

                            TreeItemNode node = curNode.Children.FirstOrDefault(x => x.Name == part && x.IsFolder == true);
                            if (node == null)
                            {
                                node = new TreeItemNode()
                                {
                                    Parent = curNode,
                                    Name = part,
                                    FullPath = curPath,
                                    IsFolder = true,
                                    LastModified = entry.DateTime,
                                    Size = entry.Size,
                                    Children = new HashSet<TreeItemNode>()
                                };
                                curNode.Children.Add(node);
                                curNode = node;
                            }
                            else
                            {
                                curNode = node;
                            }
                        }
                    }
                }
                else if (entry.IsFile)
                {
                    string filename = entry.Name;
                    string folder = "";
                    int index = filename.LastIndexOf('/');
                    if (index != -1)
                    {
                        folder = filename.Substring(0, index);
                        filename = filename.Substring(index + 1);
                    }

                    string curPath = "";

                    if (folder.Length > 0)
                    {
                        string[] parts = folder.Split('/');

                        foreach (var part in parts)
                        {
                            curPath += part + "/";

                            TreeItemNode node = curNode.Children.FirstOrDefault(x => x.Name == part && x.IsFolder == true);
                            if (node == null)
                            {
                                node = new TreeItemNode()
                                {
                                    Parent = curNode,
                                    Name = part,
                                    FullPath = curPath,
                                    IsFolder = true,
                                    LastModified = entry.DateTime,
                                    Size = entry.Size,
                                    Children = new HashSet<TreeItemNode>()
                                };
                                curNode.Children.Add(node);
                                curNode = node;
                            }
                            else
                            {
                                curNode = node;
                            }
                        }
                    }

                    curPath += filename;

                    TreeItemNode fileNode = curNode.Children.FirstOrDefault(x => x.Name == filename && x.IsFolder == false);
                    if (fileNode == null)
                    {
                        fileNode = new TreeItemNode()
                        {
                            Parent = curNode,
                            Name = filename,
                            FullPath = curPath,
                            IsFolder = false,
                            LastModified = entry.DateTime,
                            Size = entry.Size,
                            Children = null
                        };

                        curNode.Children.Add(fileNode);
                    }

                }
            }

            zipFile.Close();

            return rootTreeNode;
        }

        private static void CreateFolderHierarchy(string rootPath, string hierarchy)
        {
            if (!Directory.Exists(rootPath + hierarchy))
                Directory.CreateDirectory(rootPath + hierarchy);
        }

        public static string ExtractFile(string zipFilename, string pathInArchive, string extractRootPath)
        {
            if (!Directory.Exists(extractRootPath))
                throw new ZipException("Root path does not exist");

            string extractedFilePath = null;

            ZipFile zipFile = new ZipFile(zipFilename);

            pathInArchive = pathInArchive.Replace('\\', '/');

            int archiveIndex = pathInArchive.IndexOf('/');
            if (archiveIndex != -1)
            {
                pathInArchive = pathInArchive.Substring(archiveIndex + 1);
            }

            ZipEntry zipEntry = zipFile.GetEntry(pathInArchive);

            if (zipEntry != null)
            {
                if (zipEntry.IsFile)
                {
                    //////////////////////////
                    //Extract the full path directly under the root path
                    //string extractPathToCreate = pathInArchive;

                    //int index = pathInArchive.LastIndexOf('/');
                    //if (index != -1)
                    //{
                    //    extractPathToCreate = pathInArchive.Substring(0, index);
                    //}

                    //if (extractPathToCreate.Length > 0)
                    //{
                    //    CreateFolderHierarchy(extractRootPath, extractPathToCreate);
                    //}

                    //////////////////////////
                    //Extract the file directly under the root path
                    string extractFilename = pathInArchive;

                    int index = pathInArchive.LastIndexOf('/');
                    if (index != -1)
                    {
                        extractFilename = pathInArchive.Substring(index + 1);
                    }


                    using (Stream reader = zipFile.GetInputStream(zipEntry))
                    {
                        //Extract the file directly under the root path
                        using (Stream writer = new FileStream(extractRootPath + extractFilename, FileMode.Create))
                        //Extract the full path directly under the root path
                        //using (Stream writer = new FileStream(extractRootPath + pathInArchive, FileMode.Create))
                        {
                            byte[] buffer = new byte[BUFFER_SIZE];
                            int bytesRead = reader.Read(buffer, 0, BUFFER_SIZE);
                            while (bytesRead > 0)
                            {
                                writer.Write(buffer, 0, bytesRead);
                                bytesRead = reader.Read(buffer, 0, BUFFER_SIZE);
                            }
                        }
                    }

                    extractedFilePath = extractRootPath + extractFilename;
                }
            }

            return extractedFilePath;
        }

        public static string ExtractFilePath(string zipFilename, string pathInArchive, string extractRootPath)
        {
            if (!Directory.Exists(extractRootPath))
                throw new ZipException("Root path does not exist");

            string extractedFilePath = null;

            ZipFile zipFile = new ZipFile(zipFilename);

            pathInArchive = pathInArchive.Replace('\\', '/');
            //string[] parts = pathInArchive.Split('\\');
            //ZipEntry zipEntry = zipFile.GetEntry(parts[0]);

            ZipEntry zipEntry = zipFile.GetEntry(pathInArchive);

            if (zipEntry != null)
            {
                if (zipEntry.IsFile)
                {
                    //extract the full path directly under the root path
                    string extractPathToCreate = pathInArchive;

                    int index = pathInArchive.LastIndexOf('/');
                    if (index != -1)
                    {
                        extractPathToCreate = pathInArchive.Substring(0, index);
                    }

                    if (extractPathToCreate.Length > 0)
                    {
                        CreateFolderHierarchy(extractRootPath, extractPathToCreate);
                    }

                    using (Stream reader = zipFile.GetInputStream(zipEntry))
                    {
                        //Extract the full path directly under the root path
                        using (Stream writer = new FileStream(extractRootPath + pathInArchive, FileMode.Create))
                        {
                            byte[] buffer = new byte[BUFFER_SIZE];
                            int bytesRead = reader.Read(buffer, 0, BUFFER_SIZE);
                            while (bytesRead > 0)
                            {
                                writer.Write(buffer, 0, bytesRead);
                                bytesRead = reader.Read(buffer, 0, BUFFER_SIZE);
                            }
                        }
                    }

                    extractedFilePath = extractRootPath + pathInArchive;
                }
            }

            return extractedFilePath;
        }
    }
}
