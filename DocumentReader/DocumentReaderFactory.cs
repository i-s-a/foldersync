﻿namespace DocumentReader
{
    public class DocumentReaderFactory : IDocumentReaderFactory
    {
        public IDocumentReader CreateDocumentReader(string filename)
        {
            IDocumentReader reader = null;

            //For now just use the extension of the filename to determine which
            //reader to return.
            if (filename.EndsWith(".docx"))
            {
                reader = new OpenXmlWordReader(filename);
            }
            else if (filename.EndsWith(".doc"))
            {
                reader = new DocReader(filename);
            }
            else if (filename.EndsWith(".xlsx"))
            {
                reader = new OpenXmlExcelReader(filename);
            }
            else if (filename.EndsWith(".pptx"))
            {
                reader = new OpenXmlPowerPointReader(filename);
            }
            else if (filename.EndsWith(".pdf"))
            {
                //reader = new PdfReader(filename);
                reader = new PdfSharpPdfReader(filename);
            }
            else if (filename.EndsWith(".xps"))
            {
                reader = new XpsReader(filename);
            }
            else
            {
                reader = new TextReader(filename);  //Anything else assume it is text
            }

            return reader;
        }
    }
}
