﻿using System;
using System.Linq;
using System.Text;
using System.Xml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Drawing;
using System.IO;

namespace DocumentReader
{
    public class OpenXmlWordReader : IDocumentReader
    {
        private string mFilename;

        public OpenXmlWordReader(string filename)
        {
            mFilename = filename;
        }

        public string ReadContent()
        {
            return ExtractText(mFilename);
        }

        public string[] ReadLines()
        {
            return ExtractText(mFilename).Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
        }

        private string ExtractText(string filename)
        {
            StringBuilder text = new StringBuilder();

            using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                using (WordprocessingDocument wDoc = WordprocessingDocument.Open(fs, false))
                {
                    var parts = wDoc.MainDocumentPart.Document.Descendants().FirstOrDefault();
                    if (parts != null)
                    {
                        foreach (var node in parts.ChildElements)
                        {
                            if (node is DocumentFormat.OpenXml.Wordprocessing.Paragraph)
                            {
                                ProcessParagraph((DocumentFormat.OpenXml.Wordprocessing.Paragraph)node, text);
                                text.AppendLine("");
                            }

                            if (node is DocumentFormat.OpenXml.Wordprocessing.Table)
                            {
                                ProcessTable((DocumentFormat.OpenXml.Wordprocessing.Table)node, text);
                            }
                        }
                    }
                }
            }

            return text.ToString();
        }

        private static void ProcessTable(DocumentFormat.OpenXml.Wordprocessing.Table node, StringBuilder textBuilder)
        {
            foreach (var row in node.Descendants<DocumentFormat.OpenXml.Wordprocessing.TableRow>())
            {
                textBuilder.Append("| ");
                foreach (var cell in row.Descendants<DocumentFormat.OpenXml.Wordprocessing.TableCell>())
                {
                    foreach (var para in cell.Descendants<DocumentFormat.OpenXml.Wordprocessing.Paragraph>())
                    {
                        ProcessParagraph(para, textBuilder);
                    }
                    textBuilder.Append(" | ");
                }
                textBuilder.AppendLine("");
            }
        }

        private static void ProcessParagraph(DocumentFormat.OpenXml.Wordprocessing.Paragraph node, StringBuilder textBuilder)
        {
            foreach (var text in node.Descendants<DocumentFormat.OpenXml.Wordprocessing.Text>())
            {
                textBuilder.Append(text.InnerText);
            }
        }
    }
}
