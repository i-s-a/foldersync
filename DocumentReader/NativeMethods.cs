﻿using System;
using System.Runtime.InteropServices;

namespace DocumentReader
{
    [Flags]
    internal enum STGMFlags : int
    {
        STGM_DIRECT = 0,
        STGM_FAILIFTHERE = 0,
        STGM_READ = 0,
        STGM_WRITE = 1,
        STGM_READWRITE = 2,
        STGM_SHARE_EXCLUSIVE = 0x10,
        STGM_SHARE_DENY_WRITE = 0x20,
        STGM_SHARE_DENY_READ = 0x30,
        STGM_SHARE_DENY_NONE = 0x40,
        STGM_CREATE = 0x1000,
        STGM_TRANSACTED = 0x10000,
        STGM_CONVERT = 0x20000,
        STGM_PRIORITY = 0x40000,
        STGM_NOSCRATCH = 0x100000,
        STGM_NOSNAPSHOT = 0x200000,
        STGM_DIRECT_SWMR = 0x400000,
        STGM_DELETEONRELEASE = 0x4000000,
        STGM_SIMPLE = 0x8000000,
    }

    [Flags]
    internal enum STGFMTFlags : int
    {
        STGFMT_STORAGE = 0,
        STGFMT_NATIVE = 1,
        STGFMT_FILE = 3,    //Intentionally skipping 2 as that is how it is on Win32
        STGFMT_ANY = 4,
        STGFMT_DOCFILE = 5
    }

    [Flags]
    internal enum STGM_STREAM_SEEK : int
    {
        STREAM_SEEK_SET = 0,
        STREAM_SEEK_CUR = 1,
        STREAM_SEEK_END = 2
    }

    [ComImport]
    [Guid("0000000d-0000-0000-c000-000000000046")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    internal interface IEnumSTATSTG
    {
        int Next(uint celt, out System.Runtime.InteropServices.ComTypes.STATSTG rgelt, out uint pceltFetched);
        int RemoteNext(uint celt, out System.Runtime.InteropServices.ComTypes.STATSTG rgelt, out uint pceltFetched);
        int Skip(uint celt);
        int Reset();
        int Clone(out IEnumSTATSTG ppenum);
    }

    internal class tagRemSNB
    {
        public uint ulCntStr;
        public uint ulCntChar;
        public IntPtr rgString;

        public tagRemSNB(IntPtr rgString, uint ulCntStr, uint ulCntChar)
        {
            this.rgString = rgString;
            this.ulCntStr = ulCntStr;
            this.ulCntChar = ulCntChar;
        }
    }

    //[ComImport, ComConversionLoss, InterfaceType(ComInterfaceType.InterfaceIsIUnknown), Guid(Guids.IStorage)]
    [ComImport]
    [Guid("0000000b-0000-0000-c000-000000000046")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    internal interface IStorage
    {
        int CreateStream(string pwcsName, int grfMode, int reserved1, int reserved2, out System.Runtime.InteropServices.ComTypes.IStream ppstm);
        int OpenStream(string pwcsName, IntPtr reserved1, int grfMode, int reserved2, out System.Runtime.InteropServices.ComTypes.IStream ppstm);
        int CreateStorage(string pwcsName, int grfMode, int reserved1, int reserved2, out IStorage ppstg);
        int OpenStorage(string pwcsName, IStorage pstgPriority, int grfMode, tagRemSNB snbExclude, int reserved, out IStorage ppstg);
        int CopyTo(int ciidExclude, ref Guid rgiidExclude, ref tagRemSNB snbExclude, IStorage pstgDest);
        int MoveElementTo(string pwcsName, IStorage pstgDest, string pwcsNewName, int grfFlags);
        int Commit(int grfCommitFlags);
        int Revert();
        int EnumElements(int reserved1, IntPtr reserved2, int reserved3, out IEnumSTATSTG ppenum);
        int DestroyElement(string pwcsName);
        int RenameElement(string pwcsOldName, string pwcsNewName);
        int SetElementTimes(string pwcsName, ref System.Runtime.InteropServices.ComTypes.FILETIME pctime, ref System.Runtime.InteropServices.ComTypes.FILETIME patime, ref System.Runtime.InteropServices.ComTypes.FILETIME pmtime);
        int SetClass(ref Guid clsid);
        int SetStateBits(int grfStateBits, int grfMask);
        int Stat(out System.Runtime.InteropServices.ComTypes.STATSTG pstatstg, int grfStatFlag);
    }

    internal class NativeMethods
    {
        [StructLayout(LayoutKind.Explicit)]
        public struct STGOPTIONS
        {
            [FieldOffset(0)]
            ushort usVersion;
            [FieldOffset(2)]
            ushort reserved;
            [FieldOffset(4)]
            uint uiSectorSize;
            [FieldOffset(8), MarshalAs(UnmanagedType.LPWStr)]
            string pwcsname;
        }

        [DllImport("ole32.dll", SetLastError = true)]
        public static extern int StgOpenStorageEx([MarshalAs(UnmanagedType.LPWStr)] string pwcsname, int grfMode, int stgfmt,
            int grfAttrs, IntPtr pStgOptions, IntPtr reserved2, [In] ref Guid riid,
            //int grfAttrs, ref STGOPTIONS pStgOptions, IntPtr reserved2, [In] ref Guid riid,
            //[MarshalAs(UnmanagedType.IUnknown)] out IStorage ppobjectOpen);
            [MarshalAs(UnmanagedType.Interface)] out IStorage ppobjectOpen);

        public static int GetLastWin32Error()
        {
            return Marshal.GetLastWin32Error();
        }

        public static Exception GetExceptionForHr(int hr)
        {
            return Marshal.GetExceptionForHR(hr);
        }

        public static string GetMessageForHr(int hr)
        {
            return Marshal.GetExceptionForHR(hr).Message;
        }
    }
}