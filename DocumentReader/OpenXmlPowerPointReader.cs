﻿using System;
using System.Linq;
using System.Text;
using DocumentFormat.OpenXml.Presentation;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml;
using System.IO;

namespace DocumentReader
{
    public class OpenXmlPowerPointReader : IDocumentReader
    {
        private string mFilename;

        public OpenXmlPowerPointReader(string filename)
        {
            mFilename = filename;
        }

        public string ReadContent()
        {
            return ExtractText(mFilename);
        }

        public string[] ReadLines()
        {
            return ExtractText(mFilename).Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
        }

        private string ExtractText(string filename)
        {
            StringBuilder text = new StringBuilder();

            using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                using (PresentationDocument presentationDocument = PresentationDocument.Open(fs, false))
                {
                    int slideCount = CountSlides(presentationDocument);
                    for (int i = 0; i < slideCount; ++i)
                    {
                        text.Append(GetSlideText(presentationDocument, i));
                    }
                }
            }

            return text.ToString();
        }

        //Count the slides in the presentation.
        private int CountSlides(PresentationDocument presentationDocument)
        {
            //Check for a null document object.
            if (presentationDocument == null)
            {
                throw new ArgumentNullException("presentationDocument");
            }

            int slidesCount = 0;

            //Get the presentation part of document.
            PresentationPart presentationPart = presentationDocument.PresentationPart;
            //Get the slide count from the SlideParts.
            if (presentationPart != null)
            {
                slidesCount = presentationPart.SlideParts.Count();
            }

            //Return the slide count.
            return slidesCount;
        }

        private string GetSlideText(PresentationDocument presentationDocument, int index)
        {
            StringBuilder slideText = new StringBuilder();

            //Get the relationship ID of the first slide.
            PresentationPart part = presentationDocument.PresentationPart;
            OpenXmlElementList slideIds = part.Presentation.SlideIdList.ChildElements;

            string relId = (slideIds[index] as SlideId).RelationshipId;

            //Get the slide part from the relationship ID.
            SlidePart slidePart = (SlidePart)part.GetPartById(relId);

            //If the slide exists...
            if (slidePart.Slide != null)
            {
                //Iterate through all the paragraphs in the slide.
                foreach (DocumentFormat.OpenXml.Drawing.Paragraph paragraph in
                    slidePart.Slide.Descendants<DocumentFormat.OpenXml.Drawing.Paragraph>())
                {
                    //Create a new string builder.                    
                    StringBuilder paragraphText = new StringBuilder();

                    //Iterate through the lines of the paragraph.
                    foreach (DocumentFormat.OpenXml.Drawing.Text text in
                        paragraph.Descendants<DocumentFormat.OpenXml.Drawing.Text>())
                    {
                        // Append each line to the previous lines.
                        paragraphText.Append(text.Text);
                    }

                    paragraphText.Append(Environment.NewLine);
                    slideText.Append(paragraphText);
                    //if (paragraphText.Length > 0)
                    //{
                    //    // Add each paragraph to the linked list.
                    //    texts.AddLast(paragraphText.ToString());
                    //}
                }
            }

            return slideText.ToString();
        }
    }
}
