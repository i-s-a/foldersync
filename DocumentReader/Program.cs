﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocumentReader
{
    class Program
    {
        static void Main(string[] args)
        {
            var filename = @"C:\Downloads\file1.docx";
            IDocumentReaderFactory documentReaderFactory = new DocumentReaderFactory();
            IDocumentReader documentReader = documentReaderFactory.CreateDocumentReader(filename);
            var content = documentReader.ReadContent();
            var lines = documentReader.ReadLines();
        }
    }
}
