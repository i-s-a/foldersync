﻿using System;

namespace DocumentReader
{
    public class DocReader : IDocumentReader
    {
        private string mFilename;

        public DocReader(string filename)
        {
            mFilename = filename;
        }

        public string ReadContent()
        {
            return ExtractText(mFilename);
        }

        public string[] ReadLines()
        {
            return ExtractText(mFilename).Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
        }

        private string ExtractText(string filename)
        {
            StgTextExtractor stgTextExtractor = new StgTextExtractor(filename);
            return stgTextExtractor.ExtractText();
        }
    }
}
