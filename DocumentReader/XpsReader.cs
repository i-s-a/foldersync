﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Xps.Packaging;

namespace DocumentReader
{
    public class XpsReader : IDocumentReader
    {
        private string mFilename;

        public XpsReader(string filename)
        {
            mFilename = filename;
        }

        public string ReadContent()
        {
            return ExtractText(mFilename);
        }

        public string[] ReadLines()
        {
            return ExtractText(mFilename).Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
        }

        //Need reference to ReachFramework and WindowsBase, and the using statement - using System.Windows.Xps.Packaging;
        public string ExtractText(string xpsFilePath)
        {
            StringBuilder text = new StringBuilder();

            using (var xpsDocument = new XpsDocument(xpsFilePath, System.IO.FileAccess.Read))
            {
                var fixedDocSeqReader = xpsDocument.FixedDocumentSequenceReader;
                if (fixedDocSeqReader == null)
                    return null;

                const string UnicodeString = "UnicodeString";
                const string GlyphsString = "Glyphs";

                var textLists = new List<List<string>>();
                foreach (IXpsFixedDocumentReader fixedDocumentReader in fixedDocSeqReader.FixedDocuments)
                {
                    foreach (IXpsFixedPageReader pageReader in fixedDocumentReader.FixedPages)
                    {
                        var pageContentReader = pageReader.XmlReader;
                        if (pageContentReader == null)
                            continue;

                        while (pageContentReader.Read())
                        {
                            if (pageContentReader.Name == GlyphsString && pageContentReader.HasAttributes)
                            {
                                string str = pageContentReader.GetAttribute(UnicodeString);
                                if (str != null)
                                {
                                    text.Append(str);
                                    //text.Append(Environment.NewLine);
                                }
                            }
                        }

                        text.Append(Environment.NewLine);
                    }
                }
            }

            return text.ToString();
        }
    }
}
