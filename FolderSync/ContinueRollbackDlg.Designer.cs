﻿namespace FolderSync
{
    partial class ContinueRollbackDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ContinueRollbackLabel = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ContinueButton = new System.Windows.Forms.Button();
            this.RollbackButton = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ContinueRollbackLabel
            // 
            this.ContinueRollbackLabel.Location = new System.Drawing.Point(3, 8);
            this.ContinueRollbackLabel.Name = "ContinueRollbackLabel";
            this.ContinueRollbackLabel.Size = new System.Drawing.Size(251, 37);
            this.ContinueRollbackLabel.TabIndex = 0;
            this.ContinueRollbackLabel.Text = "A previous sync was started but not completed. Would you like to Continue or Roll" +
    "back the sync";
            this.ContinueRollbackLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.RollbackButton);
            this.panel1.Controls.Add(this.ContinueButton);
            this.panel1.Controls.Add(this.ContinueRollbackLabel);
            this.panel1.Location = new System.Drawing.Point(15, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(257, 97);
            this.panel1.TabIndex = 1;
            // 
            // ContinueButton
            // 
            this.ContinueButton.Location = new System.Drawing.Point(4, 59);
            this.ContinueButton.Name = "ContinueButton";
            this.ContinueButton.Size = new System.Drawing.Size(118, 23);
            this.ContinueButton.TabIndex = 1;
            this.ContinueButton.Text = "Continue";
            this.ContinueButton.UseVisualStyleBackColor = true;
            this.ContinueButton.Click += new System.EventHandler(this.ContinueButton_Click);
            // 
            // RollbackButton
            // 
            this.RollbackButton.Location = new System.Drawing.Point(136, 59);
            this.RollbackButton.Name = "RollbackButton";
            this.RollbackButton.Size = new System.Drawing.Size(118, 23);
            this.RollbackButton.TabIndex = 2;
            this.RollbackButton.Text = "Rollback";
            this.RollbackButton.UseVisualStyleBackColor = true;
            this.RollbackButton.Click += new System.EventHandler(this.RollbackButton_Click);
            // 
            // ContinueRollbackDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 124);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "ContinueRollbackDlg";
            this.Text = "Continue/Rollback";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ContinueRollbackDlg_FormClosing);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label ContinueRollbackLabel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button RollbackButton;
        private System.Windows.Forms.Button ContinueButton;
    }
}