﻿using System;
using System.Drawing;
using System.Xml.Serialization;
using System.IO;
using System.ComponentModel;

namespace FolderSync
{
    [XmlRoot("VisualOptions")]
    public class VisualOptions
    {
        public VisualOptions()
        {
            AddColor = Color.Violet;
            DeleteColor = Color.LightGray;
            ChangeColor = Color.Red;
            ChildrenDifferColor = Color.Green;
            FontName = "Arial";
            FontSize = 9;
        }

        [XmlIgnore]
        public Color AddColor { get; set; }

        [XmlElement("AddColor")]
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        public int AddColorAsArgb
        {
            get { return AddColor.ToArgb(); }
            set { AddColor = Color.FromArgb(value); }
        }

        [XmlIgnore]
        public Color DeleteColor { get; set; }

        [XmlElement("DeleteColor")]
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        public int DeleteColorAsArgb
        {
            get { return DeleteColor.ToArgb(); }
            set { DeleteColor = Color.FromArgb(value); }
        }

        [XmlIgnore]
        public Color ChangeColor { get; set; }

        [XmlElement("ChangeColor")]
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        public int ChangeColorAsArgb
        {
            get { return ChangeColor.ToArgb(); }
            set { ChangeColor = Color.FromArgb(value); }
        }

        [XmlIgnore]
        public Color ChildrenDifferColor { get; set; }

        [XmlElement("ChildrenDifferColor")]
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        public int ChildrenDifferColorAsArgb
        {
            get { return ChildrenDifferColor.ToArgb(); }
            set { ChildrenDifferColor = Color.FromArgb(value); }
        }

        public string FontName { get; set; }
        public float FontSize { get; set; }

        static public void Serialize(string filename, VisualOptions visualOptions)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(VisualOptions));
            TextWriter writer = new StreamWriter(filename);
            serializer.Serialize(writer, visualOptions);
            writer.Close();
        }

        static public VisualOptions Deserialize(string filename)
        {
            VisualOptions visualOptions = null;

            if (File.Exists(filename))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(VisualOptions));
                FileStream stream = new FileStream(filename, FileMode.Open);
                visualOptions = (VisualOptions)serializer.Deserialize(stream);
                stream.Close();
            }

            return visualOptions;
        }
    }
}
