﻿using System;
using System.Windows.Forms;

namespace FolderSync
{
    public partial class ContinueRollbackDlg : Form
    {
        public enum SyncDecision
        {
            UNDEFINED,
            CONTINUE,
            ROLLBACK
        }

        public ContinueRollbackDlg()
        {
            InitializeComponent();
            UserSyncDecision = SyncDecision.UNDEFINED;
        }

        private void ContinueButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            UserSyncDecision = SyncDecision.CONTINUE;
        }

        private void RollbackButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            UserSyncDecision = SyncDecision.ROLLBACK;
        }

        public SyncDecision UserSyncDecision
        {
            get; set;
        }

        private void ContinueRollbackDlg_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (UserSyncDecision == SyncDecision.UNDEFINED)
            {
                MessageBox.Show("Cannot close the form. You must select Continue or Rollback", "Error");
                e.Cancel = true;
            }
        }
    }
}
