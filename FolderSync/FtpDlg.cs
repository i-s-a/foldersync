﻿using System;
using System.Windows.Forms;

namespace FolderSync
{
    public partial class FtpDlg : Form
    {
        public FtpCredentials Credentials
        {
            get;
            private set;
        }

        public FtpDlg(FtpCredentials ftpCredentials)
        {
            InitializeComponent();

            Credentials = ftpCredentials;

            FormBorderStyle = FormBorderStyle.FixedDialog;
            MinimizeBox = false;
            MaximizeBox = false;

            ftpHostnameTextBox.Text = Credentials.Hostname;
            ftpUsernameTextBox.Text = Credentials.Username;
            ftpPortTextBox.Text = Credentials.Port.ToString();

        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            Credentials.Hostname = ftpHostnameTextBox.Text;
            Credentials.Username = ftpUsernameTextBox.Text;
            Credentials.Password = ftpPasswordTextBox.Text;
            Credentials.Port = Convert.ToInt32(ftpPortTextBox.Text);

            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}
