﻿namespace FolderSync
{
    partial class OptionsDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OptionsPanel = new System.Windows.Forms.Panel();
            this.OptionsTable = new System.Windows.Forms.TableLayoutPanel();
            this.SyncTypeGroupBox = new System.Windows.Forms.GroupBox();
            this.OnlyCopyIfSourceNewerCheckBox = new System.Windows.Forms.CheckBox();
            this.SyncDirectRadioButton = new System.Windows.Forms.RadioButton();
            this.SyncLoggedRadioButton = new System.Windows.Forms.RadioButton();
            this.MoveToRecycleBinCheckBox = new System.Windows.Forms.CheckBox();
            this.TempPathBrowseButton = new System.Windows.Forms.Button();
            this.OutputLogFileBrowseButton = new System.Windows.Forms.Button();
            this.TempPathTextBox = new System.Windows.Forms.TextBox();
            this.OutputLogFileTextBox = new System.Windows.Forms.TextBox();
            this.TempPathLabel = new System.Windows.Forms.Label();
            this.OutputLogFileLabel = new System.Windows.Forms.Label();
            this.RetainLogCheckBox = new System.Windows.Forms.CheckBox();
            this.CopyFileAttributesCheckBox = new System.Windows.Forms.CheckBox();
            this.PropagateDeletesCheckBox = new System.Windows.Forms.CheckBox();
            this.OkCancelPanel = new System.Windows.Forms.Panel();
            this.OkButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.CompareGroupBox = new System.Windows.Forms.GroupBox();
            this.TolarenceTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ExtensionFiltersTextBox = new System.Windows.Forms.TextBox();
            this.FileContentCheckBox = new System.Windows.Forms.CheckBox();
            this.IncludeSubFoldersCheckBox = new System.Windows.Forms.CheckBox();
            this.FileSizeCheckBox = new System.Windows.Forms.CheckBox();
            this.ExtensionFiltersLabel = new System.Windows.Forms.Label();
            this.LastModifiedTimeCheckBox = new System.Windows.Forms.CheckBox();
            this.FontsAndColorGroupBox = new System.Windows.Forms.GroupBox();
            this.changeColorsGroupBox = new System.Windows.Forms.GroupBox();
            this.childrenDifferColorButton = new System.Windows.Forms.Button();
            this.addColorButton = new System.Windows.Forms.Button();
            this.changedColorButton = new System.Windows.Forms.Button();
            this.deleteColorButton = new System.Windows.Forms.Button();
            this.changeFontGroupBox = new System.Windows.Forms.GroupBox();
            this.fontSizeComboBox = new System.Windows.Forms.ComboBox();
            this.fontSampleTextLabel = new System.Windows.Forms.Label();
            this.fontListComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.OptionsPanel.SuspendLayout();
            this.OptionsTable.SuspendLayout();
            this.SyncTypeGroupBox.SuspendLayout();
            this.OkCancelPanel.SuspendLayout();
            this.CompareGroupBox.SuspendLayout();
            this.FontsAndColorGroupBox.SuspendLayout();
            this.changeColorsGroupBox.SuspendLayout();
            this.changeFontGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // OptionsPanel
            // 
            this.OptionsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.OptionsPanel.Controls.Add(this.OptionsTable);
            this.OptionsPanel.Location = new System.Drawing.Point(-1, -1);
            this.OptionsPanel.Name = "OptionsPanel";
            this.OptionsPanel.Size = new System.Drawing.Size(463, 611);
            this.OptionsPanel.TabIndex = 0;
            // 
            // OptionsTable
            // 
            this.OptionsTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.OptionsTable.ColumnCount = 1;
            this.OptionsTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.OptionsTable.Controls.Add(this.SyncTypeGroupBox, 0, 0);
            this.OptionsTable.Controls.Add(this.OkCancelPanel, 0, 3);
            this.OptionsTable.Controls.Add(this.CompareGroupBox, 0, 1);
            this.OptionsTable.Controls.Add(this.FontsAndColorGroupBox, 0, 2);
            this.OptionsTable.Location = new System.Drawing.Point(3, 3);
            this.OptionsTable.Name = "OptionsTable";
            this.OptionsTable.RowCount = 4;
            this.OptionsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.OptionsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.OptionsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.OptionsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.OptionsTable.Size = new System.Drawing.Size(457, 605);
            this.OptionsTable.TabIndex = 0;
            // 
            // SyncTypeGroupBox
            // 
            this.SyncTypeGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SyncTypeGroupBox.Controls.Add(this.OnlyCopyIfSourceNewerCheckBox);
            this.SyncTypeGroupBox.Controls.Add(this.SyncDirectRadioButton);
            this.SyncTypeGroupBox.Controls.Add(this.SyncLoggedRadioButton);
            this.SyncTypeGroupBox.Controls.Add(this.MoveToRecycleBinCheckBox);
            this.SyncTypeGroupBox.Controls.Add(this.TempPathBrowseButton);
            this.SyncTypeGroupBox.Controls.Add(this.OutputLogFileBrowseButton);
            this.SyncTypeGroupBox.Controls.Add(this.TempPathTextBox);
            this.SyncTypeGroupBox.Controls.Add(this.OutputLogFileTextBox);
            this.SyncTypeGroupBox.Controls.Add(this.TempPathLabel);
            this.SyncTypeGroupBox.Controls.Add(this.OutputLogFileLabel);
            this.SyncTypeGroupBox.Controls.Add(this.RetainLogCheckBox);
            this.SyncTypeGroupBox.Controls.Add(this.CopyFileAttributesCheckBox);
            this.SyncTypeGroupBox.Controls.Add(this.PropagateDeletesCheckBox);
            this.SyncTypeGroupBox.Location = new System.Drawing.Point(3, 3);
            this.SyncTypeGroupBox.Name = "SyncTypeGroupBox";
            this.SyncTypeGroupBox.Size = new System.Drawing.Size(451, 195);
            this.SyncTypeGroupBox.TabIndex = 0;
            this.SyncTypeGroupBox.TabStop = false;
            this.SyncTypeGroupBox.Text = "Sync Option";
            // 
            // OnlyCopyIfSourceNewerCheckBox
            // 
            this.OnlyCopyIfSourceNewerCheckBox.AutoSize = true;
            this.OnlyCopyIfSourceNewerCheckBox.Location = new System.Drawing.Point(248, 120);
            this.OnlyCopyIfSourceNewerCheckBox.Name = "OnlyCopyIfSourceNewerCheckBox";
            this.OnlyCopyIfSourceNewerCheckBox.Size = new System.Drawing.Size(163, 17);
            this.OnlyCopyIfSourceNewerCheckBox.TabIndex = 8;
            this.OnlyCopyIfSourceNewerCheckBox.Text = "Copy file only if source newer";
            this.OnlyCopyIfSourceNewerCheckBox.UseVisualStyleBackColor = true;
            // 
            // SyncDirectRadioButton
            // 
            this.SyncDirectRadioButton.AutoSize = true;
            this.SyncDirectRadioButton.Location = new System.Drawing.Point(94, 21);
            this.SyncDirectRadioButton.Name = "SyncDirectRadioButton";
            this.SyncDirectRadioButton.Size = new System.Drawing.Size(53, 17);
            this.SyncDirectRadioButton.TabIndex = 8;
            this.SyncDirectRadioButton.TabStop = true;
            this.SyncDirectRadioButton.Text = "Direct";
            this.SyncDirectRadioButton.UseVisualStyleBackColor = true;
            // 
            // SyncLoggedRadioButton
            // 
            this.SyncLoggedRadioButton.AutoSize = true;
            this.SyncLoggedRadioButton.Location = new System.Drawing.Point(8, 21);
            this.SyncLoggedRadioButton.Name = "SyncLoggedRadioButton";
            this.SyncLoggedRadioButton.Size = new System.Drawing.Size(61, 17);
            this.SyncLoggedRadioButton.TabIndex = 7;
            this.SyncLoggedRadioButton.TabStop = true;
            this.SyncLoggedRadioButton.Text = "Logged";
            this.SyncLoggedRadioButton.UseVisualStyleBackColor = true;
            // 
            // MoveToRecycleBinCheckBox
            // 
            this.MoveToRecycleBinCheckBox.AutoSize = true;
            this.MoveToRecycleBinCheckBox.Location = new System.Drawing.Point(94, 50);
            this.MoveToRecycleBinCheckBox.Name = "MoveToRecycleBinCheckBox";
            this.MoveToRecycleBinCheckBox.Size = new System.Drawing.Size(129, 17);
            this.MoveToRecycleBinCheckBox.TabIndex = 6;
            this.MoveToRecycleBinCheckBox.Text = "Move To Recycle Bin";
            this.MoveToRecycleBinCheckBox.UseVisualStyleBackColor = true;
            // 
            // TempPathBrowseButton
            // 
            this.TempPathBrowseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TempPathBrowseButton.Location = new System.Drawing.Point(415, 77);
            this.TempPathBrowseButton.Name = "TempPathBrowseButton";
            this.TempPathBrowseButton.Size = new System.Drawing.Size(30, 23);
            this.TempPathBrowseButton.TabIndex = 5;
            this.TempPathBrowseButton.Text = "...";
            this.TempPathBrowseButton.UseVisualStyleBackColor = true;
            // 
            // OutputLogFileBrowseButton
            // 
            this.OutputLogFileBrowseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OutputLogFileBrowseButton.Location = new System.Drawing.Point(414, 154);
            this.OutputLogFileBrowseButton.Name = "OutputLogFileBrowseButton";
            this.OutputLogFileBrowseButton.Size = new System.Drawing.Size(29, 23);
            this.OutputLogFileBrowseButton.TabIndex = 4;
            this.OutputLogFileBrowseButton.Text = "...";
            this.OutputLogFileBrowseButton.UseVisualStyleBackColor = true;
            // 
            // TempPathTextBox
            // 
            this.TempPathTextBox.Location = new System.Drawing.Point(66, 79);
            this.TempPathTextBox.Name = "TempPathTextBox";
            this.TempPathTextBox.Size = new System.Drawing.Size(349, 20);
            this.TempPathTextBox.TabIndex = 4;
            // 
            // OutputLogFileTextBox
            // 
            this.OutputLogFileTextBox.Location = new System.Drawing.Point(88, 155);
            this.OutputLogFileTextBox.Name = "OutputLogFileTextBox";
            this.OutputLogFileTextBox.Size = new System.Drawing.Size(326, 20);
            this.OutputLogFileTextBox.TabIndex = 3;
            // 
            // TempPathLabel
            // 
            this.TempPathLabel.AutoSize = true;
            this.TempPathLabel.Location = new System.Drawing.Point(4, 83);
            this.TempPathLabel.Name = "TempPathLabel";
            this.TempPathLabel.Size = new System.Drawing.Size(62, 13);
            this.TempPathLabel.TabIndex = 3;
            this.TempPathLabel.Text = "Temp Path:";
            // 
            // OutputLogFileLabel
            // 
            this.OutputLogFileLabel.AutoSize = true;
            this.OutputLogFileLabel.Location = new System.Drawing.Point(3, 157);
            this.OutputLogFileLabel.Name = "OutputLogFileLabel";
            this.OutputLogFileLabel.Size = new System.Drawing.Size(82, 13);
            this.OutputLogFileLabel.TabIndex = 2;
            this.OutputLogFileLabel.Text = "Output Log File:";
            // 
            // RetainLogCheckBox
            // 
            this.RetainLogCheckBox.AutoSize = true;
            this.RetainLogCheckBox.Location = new System.Drawing.Point(7, 50);
            this.RetainLogCheckBox.Name = "RetainLogCheckBox";
            this.RetainLogCheckBox.Size = new System.Drawing.Size(78, 17);
            this.RetainLogCheckBox.TabIndex = 2;
            this.RetainLogCheckBox.Text = "Retain Log";
            this.RetainLogCheckBox.UseVisualStyleBackColor = true;
            // 
            // CopyFileAttributesCheckBox
            // 
            this.CopyFileAttributesCheckBox.AutoSize = true;
            this.CopyFileAttributesCheckBox.Location = new System.Drawing.Point(126, 120);
            this.CopyFileAttributesCheckBox.Name = "CopyFileAttributesCheckBox";
            this.CopyFileAttributesCheckBox.Size = new System.Drawing.Size(116, 17);
            this.CopyFileAttributesCheckBox.TabIndex = 1;
            this.CopyFileAttributesCheckBox.Text = "Copy File Attributes";
            this.CopyFileAttributesCheckBox.UseVisualStyleBackColor = true;
            // 
            // PropagateDeletesCheckBox
            // 
            this.PropagateDeletesCheckBox.AutoSize = true;
            this.PropagateDeletesCheckBox.Location = new System.Drawing.Point(6, 120);
            this.PropagateDeletesCheckBox.Name = "PropagateDeletesCheckBox";
            this.PropagateDeletesCheckBox.Size = new System.Drawing.Size(114, 17);
            this.PropagateDeletesCheckBox.TabIndex = 0;
            this.PropagateDeletesCheckBox.Text = "Propagate Deletes";
            this.PropagateDeletesCheckBox.UseVisualStyleBackColor = true;
            // 
            // OkCancelPanel
            // 
            this.OkCancelPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.OkCancelPanel.Controls.Add(this.OkButton);
            this.OkCancelPanel.Controls.Add(this.cancelButton);
            this.OkCancelPanel.Location = new System.Drawing.Point(3, 577);
            this.OkCancelPanel.Name = "OkCancelPanel";
            this.OkCancelPanel.Size = new System.Drawing.Size(451, 25);
            this.OkCancelPanel.TabIndex = 3;
            // 
            // OkButton
            // 
            this.OkButton.Location = new System.Drawing.Point(288, 0);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(75, 23);
            this.OkButton.TabIndex = 1;
            this.OkButton.Text = "OK";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(369, 0);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 0;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // CompareGroupBox
            // 
            this.CompareGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CompareGroupBox.Controls.Add(this.label3);
            this.CompareGroupBox.Controls.Add(this.label2);
            this.CompareGroupBox.Controls.Add(this.TolarenceTextBox);
            this.CompareGroupBox.Controls.Add(this.label1);
            this.CompareGroupBox.Controls.Add(this.ExtensionFiltersTextBox);
            this.CompareGroupBox.Controls.Add(this.FileContentCheckBox);
            this.CompareGroupBox.Controls.Add(this.IncludeSubFoldersCheckBox);
            this.CompareGroupBox.Controls.Add(this.FileSizeCheckBox);
            this.CompareGroupBox.Controls.Add(this.ExtensionFiltersLabel);
            this.CompareGroupBox.Controls.Add(this.LastModifiedTimeCheckBox);
            this.CompareGroupBox.Location = new System.Drawing.Point(3, 204);
            this.CompareGroupBox.Name = "CompareGroupBox";
            this.CompareGroupBox.Size = new System.Drawing.Size(451, 195);
            this.CompareGroupBox.TabIndex = 1;
            this.CompareGroupBox.TabStop = false;
            this.CompareGroupBox.Text = "Compare Options";
            // 
            // TolarenceTextBox
            // 
            this.TolarenceTextBox.Location = new System.Drawing.Point(242, 28);
            this.TolarenceTextBox.Name = "TolarenceTextBox";
            this.TolarenceTextBox.Size = new System.Drawing.Size(101, 20);
            this.TolarenceTextBox.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(138, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Tolarence Seconds";
            // 
            // ExtensionFiltersTextBox
            // 
            this.ExtensionFiltersTextBox.Location = new System.Drawing.Point(91, 158);
            this.ExtensionFiltersTextBox.Name = "ExtensionFiltersTextBox";
            this.ExtensionFiltersTextBox.Size = new System.Drawing.Size(126, 20);
            this.ExtensionFiltersTextBox.TabIndex = 7;
            // 
            // FileContentCheckBox
            // 
            this.FileContentCheckBox.AutoSize = true;
            this.FileContentCheckBox.Location = new System.Drawing.Point(8, 76);
            this.FileContentCheckBox.Name = "FileContentCheckBox";
            this.FileContentCheckBox.Size = new System.Drawing.Size(82, 17);
            this.FileContentCheckBox.TabIndex = 2;
            this.FileContentCheckBox.Text = "File Content";
            this.FileContentCheckBox.UseVisualStyleBackColor = true;
            // 
            // IncludeSubFoldersCheckBox
            // 
            this.IncludeSubFoldersCheckBox.AutoSize = true;
            this.IncludeSubFoldersCheckBox.Location = new System.Drawing.Point(141, 76);
            this.IncludeSubFoldersCheckBox.Name = "IncludeSubFoldersCheckBox";
            this.IncludeSubFoldersCheckBox.Size = new System.Drawing.Size(120, 17);
            this.IncludeSubFoldersCheckBox.TabIndex = 5;
            this.IncludeSubFoldersCheckBox.Text = "Include Sub Folders";
            this.IncludeSubFoldersCheckBox.UseVisualStyleBackColor = true;
            // 
            // FileSizeCheckBox
            // 
            this.FileSizeCheckBox.AutoSize = true;
            this.FileSizeCheckBox.Location = new System.Drawing.Point(8, 53);
            this.FileSizeCheckBox.Name = "FileSizeCheckBox";
            this.FileSizeCheckBox.Size = new System.Drawing.Size(65, 17);
            this.FileSizeCheckBox.TabIndex = 1;
            this.FileSizeCheckBox.Text = "File Size";
            this.FileSizeCheckBox.UseVisualStyleBackColor = true;
            // 
            // ExtensionFiltersLabel
            // 
            this.ExtensionFiltersLabel.AutoSize = true;
            this.ExtensionFiltersLabel.Location = new System.Drawing.Point(7, 161);
            this.ExtensionFiltersLabel.Name = "ExtensionFiltersLabel";
            this.ExtensionFiltersLabel.Size = new System.Drawing.Size(83, 13);
            this.ExtensionFiltersLabel.TabIndex = 6;
            this.ExtensionFiltersLabel.Text = "Extension Filters";
            // 
            // LastModifiedTimeCheckBox
            // 
            this.LastModifiedTimeCheckBox.AutoSize = true;
            this.LastModifiedTimeCheckBox.Location = new System.Drawing.Point(8, 30);
            this.LastModifiedTimeCheckBox.Name = "LastModifiedTimeCheckBox";
            this.LastModifiedTimeCheckBox.Size = new System.Drawing.Size(115, 17);
            this.LastModifiedTimeCheckBox.TabIndex = 0;
            this.LastModifiedTimeCheckBox.Text = "Last Modified Time";
            this.LastModifiedTimeCheckBox.UseVisualStyleBackColor = true;
            // 
            // FontsAndColorGroupBox
            // 
            this.FontsAndColorGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FontsAndColorGroupBox.Controls.Add(this.changeColorsGroupBox);
            this.FontsAndColorGroupBox.Controls.Add(this.changeFontGroupBox);
            this.FontsAndColorGroupBox.Location = new System.Drawing.Point(3, 405);
            this.FontsAndColorGroupBox.Name = "FontsAndColorGroupBox";
            this.FontsAndColorGroupBox.Size = new System.Drawing.Size(451, 166);
            this.FontsAndColorGroupBox.TabIndex = 4;
            this.FontsAndColorGroupBox.TabStop = false;
            this.FontsAndColorGroupBox.Text = "Font and Color";
            // 
            // changeColorsGroupBox
            // 
            this.changeColorsGroupBox.Controls.Add(this.childrenDifferColorButton);
            this.changeColorsGroupBox.Controls.Add(this.addColorButton);
            this.changeColorsGroupBox.Controls.Add(this.changedColorButton);
            this.changeColorsGroupBox.Controls.Add(this.deleteColorButton);
            this.changeColorsGroupBox.Location = new System.Drawing.Point(10, 19);
            this.changeColorsGroupBox.Name = "changeColorsGroupBox";
            this.changeColorsGroupBox.Size = new System.Drawing.Size(192, 141);
            this.changeColorsGroupBox.TabIndex = 5;
            this.changeColorsGroupBox.TabStop = false;
            this.changeColorsGroupBox.Text = "Change Colors";
            // 
            // childrenDifferColorButton
            // 
            this.childrenDifferColorButton.Location = new System.Drawing.Point(6, 112);
            this.childrenDifferColorButton.Name = "childrenDifferColorButton";
            this.childrenDifferColorButton.Size = new System.Drawing.Size(180, 23);
            this.childrenDifferColorButton.TabIndex = 3;
            this.childrenDifferColorButton.Text = "Update Children Differ Color";
            this.childrenDifferColorButton.UseVisualStyleBackColor = true;
            this.childrenDifferColorButton.Click += new System.EventHandler(this.childrenDifferColor_Click);
            // 
            // addColorButton
            // 
            this.addColorButton.Location = new System.Drawing.Point(6, 25);
            this.addColorButton.Name = "addColorButton";
            this.addColorButton.Size = new System.Drawing.Size(180, 23);
            this.addColorButton.TabIndex = 0;
            this.addColorButton.Text = "Update Add Color";
            this.addColorButton.UseVisualStyleBackColor = true;
            this.addColorButton.Click += new System.EventHandler(this.addColorButton_Click);
            // 
            // changedColorButton
            // 
            this.changedColorButton.Location = new System.Drawing.Point(6, 83);
            this.changedColorButton.Name = "changedColorButton";
            this.changedColorButton.Size = new System.Drawing.Size(180, 23);
            this.changedColorButton.TabIndex = 2;
            this.changedColorButton.Text = "Update Change Color";
            this.changedColorButton.UseVisualStyleBackColor = true;
            this.changedColorButton.Click += new System.EventHandler(this.changedColorButton_Click);
            // 
            // deleteColorButton
            // 
            this.deleteColorButton.Location = new System.Drawing.Point(6, 54);
            this.deleteColorButton.Name = "deleteColorButton";
            this.deleteColorButton.Size = new System.Drawing.Size(180, 23);
            this.deleteColorButton.TabIndex = 1;
            this.deleteColorButton.Text = "Update Delete Color";
            this.deleteColorButton.UseVisualStyleBackColor = true;
            this.deleteColorButton.Click += new System.EventHandler(this.deleteColorButton_Click);
            // 
            // changeFontGroupBox
            // 
            this.changeFontGroupBox.Controls.Add(this.fontSizeComboBox);
            this.changeFontGroupBox.Controls.Add(this.fontSampleTextLabel);
            this.changeFontGroupBox.Controls.Add(this.fontListComboBox);
            this.changeFontGroupBox.Location = new System.Drawing.Point(208, 19);
            this.changeFontGroupBox.Name = "changeFontGroupBox";
            this.changeFontGroupBox.Size = new System.Drawing.Size(171, 141);
            this.changeFontGroupBox.TabIndex = 6;
            this.changeFontGroupBox.TabStop = false;
            this.changeFontGroupBox.Text = "Change Font";
            // 
            // fontSizeComboBox
            // 
            this.fontSizeComboBox.FormattingEnabled = true;
            this.fontSizeComboBox.Location = new System.Drawing.Point(9, 52);
            this.fontSizeComboBox.Name = "fontSizeComboBox";
            this.fontSizeComboBox.Size = new System.Drawing.Size(61, 21);
            this.fontSizeComboBox.TabIndex = 2;
            this.fontSizeComboBox.SelectedIndexChanged += new System.EventHandler(this.fontSizeComboBox_SelectedIndexChanged);
            // 
            // fontSampleTextLabel
            // 
            this.fontSampleTextLabel.Location = new System.Drawing.Point(6, 83);
            this.fontSampleTextLabel.Name = "fontSampleTextLabel";
            this.fontSampleTextLabel.Size = new System.Drawing.Size(159, 23);
            this.fontSampleTextLabel.TabIndex = 1;
            this.fontSampleTextLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fontListComboBox
            // 
            this.fontListComboBox.FormattingEnabled = true;
            this.fontListComboBox.Location = new System.Drawing.Point(6, 25);
            this.fontListComboBox.Name = "fontListComboBox";
            this.fontListComboBox.Size = new System.Drawing.Size(159, 21);
            this.fontListComboBox.TabIndex = 0;
            this.fontListComboBox.SelectedIndexChanged += new System.EventHandler(this.fontListComboBox_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 118);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(330, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Only files matching these extensions will be compared and displayed.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 136);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(274, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Multiple extensions should be separated by a semi-colon.";
            // 
            // OptionsDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(456, 610);
            this.Controls.Add(this.OptionsPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "OptionsDlg";
            this.Text = "Options Dialog";
            this.Load += new System.EventHandler(this.OptionsDlg_Load);
            this.OptionsPanel.ResumeLayout(false);
            this.OptionsTable.ResumeLayout(false);
            this.SyncTypeGroupBox.ResumeLayout(false);
            this.SyncTypeGroupBox.PerformLayout();
            this.OkCancelPanel.ResumeLayout(false);
            this.CompareGroupBox.ResumeLayout(false);
            this.CompareGroupBox.PerformLayout();
            this.FontsAndColorGroupBox.ResumeLayout(false);
            this.changeColorsGroupBox.ResumeLayout(false);
            this.changeFontGroupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel OptionsPanel;
        private System.Windows.Forms.TableLayoutPanel OptionsTable;
        private System.Windows.Forms.GroupBox CompareGroupBox;
        private System.Windows.Forms.TextBox OutputLogFileTextBox;
        private System.Windows.Forms.Label OutputLogFileLabel;
        private System.Windows.Forms.CheckBox CopyFileAttributesCheckBox;
        private System.Windows.Forms.CheckBox PropagateDeletesCheckBox;
        private System.Windows.Forms.Button OutputLogFileBrowseButton;
        private System.Windows.Forms.CheckBox IncludeSubFoldersCheckBox;
        private System.Windows.Forms.TextBox ExtensionFiltersTextBox;
        private System.Windows.Forms.Label ExtensionFiltersLabel;
        private System.Windows.Forms.Panel OkCancelPanel;
        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.CheckBox OnlyCopyIfSourceNewerCheckBox;
        private System.Windows.Forms.GroupBox SyncTypeGroupBox;
        private System.Windows.Forms.RadioButton SyncDirectRadioButton;
        private System.Windows.Forms.RadioButton SyncLoggedRadioButton;
        private System.Windows.Forms.CheckBox MoveToRecycleBinCheckBox;
        private System.Windows.Forms.Button TempPathBrowseButton;
        private System.Windows.Forms.TextBox TempPathTextBox;
        private System.Windows.Forms.Label TempPathLabel;
        private System.Windows.Forms.CheckBox RetainLogCheckBox;
        private System.Windows.Forms.CheckBox FileContentCheckBox;
        private System.Windows.Forms.CheckBox FileSizeCheckBox;
        private System.Windows.Forms.CheckBox LastModifiedTimeCheckBox;
        private System.Windows.Forms.TextBox TolarenceTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox FontsAndColorGroupBox;
        private System.Windows.Forms.GroupBox changeColorsGroupBox;
        private System.Windows.Forms.Button addColorButton;
        private System.Windows.Forms.Button changedColorButton;
        private System.Windows.Forms.Button deleteColorButton;
        private System.Windows.Forms.GroupBox changeFontGroupBox;
        private System.Windows.Forms.ComboBox fontSizeComboBox;
        private System.Windows.Forms.Label fontSampleTextLabel;
        private System.Windows.Forms.ComboBox fontListComboBox;
        private System.Windows.Forms.Button childrenDifferColorButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}