﻿using System.Xml.Serialization;
using System.IO;

namespace FolderSync
{
    [XmlRoot("FtpCredentials")]
    public class FtpCredentials
    {
        public string Hostname
        {
            get;
            set;
        }

        public string Username
        {
            get;
            set;
        }

        [XmlIgnore]
        public string Password
        {
            get;
            set;
        }

        public int Port
        {
            get;
            set;
        }

        static public void Serialize(string filename, FtpCredentials ftpCredentials)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(FtpCredentials));
            TextWriter writer = new StreamWriter(filename);
            serializer.Serialize(writer, ftpCredentials);
            writer.Close();
        }

        static public FtpCredentials Deserialize(string filename)
        {
            FtpCredentials ftpCredentials = null;

            if (File.Exists(filename))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(FtpCredentials));
                FileStream stream = new FileStream(filename, FileMode.Open);
                ftpCredentials = (FtpCredentials)serializer.Deserialize(stream);
                stream.Close();
            }

            return ftpCredentials;
        }
    };
}
