﻿namespace FolderSync
{
    partial class frmVisualDiffSync
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVisualDiffSync));
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanelSync = new System.Windows.Forms.TableLayoutPanel();
            this.leftSideFolderPanel = new System.Windows.Forms.Panel();
            this.leftSideBrowseTypeButton = new System.Windows.Forms.Button();
            this.leftSideFolderComboBox = new System.Windows.Forms.ComboBox();
            this.leftSideBrowseButton = new System.Windows.Forms.Button();
            this.rightSideFolderPanel = new System.Windows.Forms.Panel();
            this.rightSideBrowseTypeButton = new System.Windows.Forms.Button();
            this.rightSideBrowseButton = new System.Windows.Forms.Button();
            this.rightSideFolderComboBox = new System.Windows.Forms.ComboBox();
            this.comparePanel = new System.Windows.Forms.Panel();
            this.SyncBothWaysRightButton = new System.Windows.Forms.Button();
            this.syncIconImages = new System.Windows.Forms.ImageList(this.components);
            this.SyncBothWaysLeftButton = new System.Windows.Forms.Button();
            this.SyncRightLeftButton = new System.Windows.Forms.Button();
            this.SyncLeftRightButton = new System.Windows.Forms.Button();
            this.compareButton = new System.Windows.Forms.Button();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.SyncOptionsPanel = new System.Windows.Forms.Panel();
            this.SyncOptionsButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.contextMenuLeftSideTree = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.syncLeftToRightToolStripTreeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.syncBothWaysToolStripLeftTreeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuLeftSideList = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.syncLeftToRightToolStripListMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.syncBothWaysToolStripLeftListMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemDiffLeftToRight = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuRightSideTree = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.syncRightToLeftToolStripTreeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.syncBothWaysToolStripRightTreeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuRightSideList = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.syncRightToLeftToolStripListMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.syncBothWaysToolStripRightListMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemDiffRightToLeft = new System.Windows.Forms.ToolStripMenuItem();
            this.syncToolTips = new System.Windows.Forms.ToolTip(this.components);
            this.leftFilenameHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.leftFileSizeHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.leftLastModifiedHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.rightFilenameHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.rightSizeHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.rightLastModifiedHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.leftBrowseTypeContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.leftArchiveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.leftFTPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rightBrowseTypeContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.rightArchiveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rightFTPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.IconImages = new System.Windows.Forms.ImageList(this.components);
            this.tabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanelSync.SuspendLayout();
            this.leftSideFolderPanel.SuspendLayout();
            this.rightSideFolderPanel.SuspendLayout();
            this.comparePanel.SuspendLayout();
            this.SyncOptionsPanel.SuspendLayout();
            this.contextMenuLeftSideTree.SuspendLayout();
            this.contextMenuLeftSideList.SuspendLayout();
            this.contextMenuRightSideTree.SuspendLayout();
            this.contextMenuRightSideList.SuspendLayout();
            this.leftBrowseTypeContextMenu.SuspendLayout();
            this.rightBrowseTypeContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Location = new System.Drawing.Point(1, 1);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(924, 523);
            this.tabControl.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tableLayoutPanelSync);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(916, 497);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Sync";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanelSync
            // 
            this.tableLayoutPanelSync.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelSync.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tableLayoutPanelSync.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.InsetDouble;
            this.tableLayoutPanelSync.ColumnCount = 2;
            this.tableLayoutPanelSync.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelSync.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelSync.Controls.Add(this.leftSideFolderPanel, 0, 2);
            this.tableLayoutPanelSync.Controls.Add(this.rightSideFolderPanel, 1, 2);
            this.tableLayoutPanelSync.Controls.Add(this.comparePanel, 0, 1);
            this.tableLayoutPanelSync.Controls.Add(this.progressBar, 0, 4);
            this.tableLayoutPanelSync.Controls.Add(this.SyncOptionsPanel, 0, 0);
            this.tableLayoutPanelSync.Controls.Add(this.tableLayoutPanel1, 0, 3);
            this.tableLayoutPanelSync.Controls.Add(this.tableLayoutPanel2, 1, 3);
            this.tableLayoutPanelSync.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelSync.Name = "tableLayoutPanelSync";
            this.tableLayoutPanelSync.RowCount = 5;
            this.tableLayoutPanelSync.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanelSync.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54F));
            this.tableLayoutPanelSync.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanelSync.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 94F));
            this.tableLayoutPanelSync.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6F));
            this.tableLayoutPanelSync.Size = new System.Drawing.Size(916, 501);
            this.tableLayoutPanelSync.TabIndex = 0;
            // 
            // leftSideFolderPanel
            // 
            this.leftSideFolderPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.leftSideFolderPanel.Controls.Add(this.leftSideBrowseTypeButton);
            this.leftSideFolderPanel.Controls.Add(this.leftSideFolderComboBox);
            this.leftSideFolderPanel.Controls.Add(this.leftSideBrowseButton);
            this.leftSideFolderPanel.Location = new System.Drawing.Point(6, 96);
            this.leftSideFolderPanel.Name = "leftSideFolderPanel";
            this.leftSideFolderPanel.Size = new System.Drawing.Size(447, 28);
            this.leftSideFolderPanel.TabIndex = 0;
            // 
            // leftSideBrowseTypeButton
            // 
            this.leftSideBrowseTypeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.leftSideBrowseTypeButton.Image = ((System.Drawing.Image)(resources.GetObject("leftSideBrowseTypeButton.Image")));
            this.leftSideBrowseTypeButton.Location = new System.Drawing.Point(407, 4);
            this.leftSideBrowseTypeButton.Name = "leftSideBrowseTypeButton";
            this.leftSideBrowseTypeButton.Size = new System.Drawing.Size(36, 22);
            this.leftSideBrowseTypeButton.TabIndex = 1;
            this.leftSideBrowseTypeButton.UseVisualStyleBackColor = true;
            this.leftSideBrowseTypeButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.leftSideBrowseTypeButton_MouseDown);
            // 
            // leftSideFolderComboBox
            // 
            this.leftSideFolderComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.leftSideFolderComboBox.FormattingEnabled = true;
            this.leftSideFolderComboBox.Location = new System.Drawing.Point(4, 4);
            this.leftSideFolderComboBox.Name = "leftSideFolderComboBox";
            this.leftSideFolderComboBox.Size = new System.Drawing.Size(363, 21);
            this.leftSideFolderComboBox.TabIndex = 1;
            // 
            // leftSideBrowseButton
            // 
            this.leftSideBrowseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.leftSideBrowseButton.Location = new System.Drawing.Point(369, 4);
            this.leftSideBrowseButton.Name = "leftSideBrowseButton";
            this.leftSideBrowseButton.Size = new System.Drawing.Size(36, 22);
            this.leftSideBrowseButton.TabIndex = 0;
            this.leftSideBrowseButton.Text = "...";
            this.leftSideBrowseButton.UseVisualStyleBackColor = true;
            this.leftSideBrowseButton.Click += new System.EventHandler(this.leftSideBrowseButton_Click);
            // 
            // rightSideFolderPanel
            // 
            this.rightSideFolderPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rightSideFolderPanel.Controls.Add(this.rightSideBrowseTypeButton);
            this.rightSideFolderPanel.Controls.Add(this.rightSideBrowseButton);
            this.rightSideFolderPanel.Controls.Add(this.rightSideFolderComboBox);
            this.rightSideFolderPanel.Location = new System.Drawing.Point(462, 96);
            this.rightSideFolderPanel.Name = "rightSideFolderPanel";
            this.rightSideFolderPanel.Size = new System.Drawing.Size(448, 28);
            this.rightSideFolderPanel.TabIndex = 1;
            // 
            // rightSideBrowseTypeButton
            // 
            this.rightSideBrowseTypeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.rightSideBrowseTypeButton.Image = ((System.Drawing.Image)(resources.GetObject("rightSideBrowseTypeButton.Image")));
            this.rightSideBrowseTypeButton.Location = new System.Drawing.Point(407, 4);
            this.rightSideBrowseTypeButton.Name = "rightSideBrowseTypeButton";
            this.rightSideBrowseTypeButton.Size = new System.Drawing.Size(36, 22);
            this.rightSideBrowseTypeButton.TabIndex = 2;
            this.rightSideBrowseTypeButton.UseVisualStyleBackColor = true;
            this.rightSideBrowseTypeButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.rightSideBrowseTypeButton_MouseDown);
            // 
            // rightSideBrowseButton
            // 
            this.rightSideBrowseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.rightSideBrowseButton.Location = new System.Drawing.Point(370, 4);
            this.rightSideBrowseButton.Name = "rightSideBrowseButton";
            this.rightSideBrowseButton.Size = new System.Drawing.Size(36, 22);
            this.rightSideBrowseButton.TabIndex = 1;
            this.rightSideBrowseButton.Text = "...";
            this.rightSideBrowseButton.UseVisualStyleBackColor = true;
            this.rightSideBrowseButton.Click += new System.EventHandler(this.rightSideBrowseButton_Click);
            // 
            // rightSideFolderComboBox
            // 
            this.rightSideFolderComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rightSideFolderComboBox.FormattingEnabled = true;
            this.rightSideFolderComboBox.Location = new System.Drawing.Point(3, 5);
            this.rightSideFolderComboBox.Name = "rightSideFolderComboBox";
            this.rightSideFolderComboBox.Size = new System.Drawing.Size(364, 21);
            this.rightSideFolderComboBox.TabIndex = 0;
            // 
            // comparePanel
            // 
            this.comparePanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelSync.SetColumnSpan(this.comparePanel, 2);
            this.comparePanel.Controls.Add(this.SyncBothWaysRightButton);
            this.comparePanel.Controls.Add(this.SyncBothWaysLeftButton);
            this.comparePanel.Controls.Add(this.SyncRightLeftButton);
            this.comparePanel.Controls.Add(this.SyncLeftRightButton);
            this.comparePanel.Controls.Add(this.compareButton);
            this.comparePanel.Location = new System.Drawing.Point(6, 39);
            this.comparePanel.Name = "comparePanel";
            this.comparePanel.Size = new System.Drawing.Size(904, 47);
            this.comparePanel.TabIndex = 2;
            // 
            // SyncBothWaysRightButton
            // 
            this.SyncBothWaysRightButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SyncBothWaysRightButton.ImageIndex = 4;
            this.SyncBothWaysRightButton.ImageList = this.syncIconImages;
            this.SyncBothWaysRightButton.Location = new System.Drawing.Point(735, 3);
            this.SyncBothWaysRightButton.Name = "SyncBothWaysRightButton";
            this.SyncBothWaysRightButton.Size = new System.Drawing.Size(80, 42);
            this.SyncBothWaysRightButton.TabIndex = 6;
            this.SyncBothWaysRightButton.UseVisualStyleBackColor = true;
            this.SyncBothWaysRightButton.Click += new System.EventHandler(this.SyncBothWaysRightButton_Click);
            // 
            // syncIconImages
            // 
            this.syncIconImages.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("syncIconImages.ImageStream")));
            this.syncIconImages.TransparentColor = System.Drawing.Color.Transparent;
            this.syncIconImages.Images.SetKeyName(0, "Actions-go-next-view-icon.png");
            this.syncIconImages.Images.SetKeyName(1, "Actions-go-next-icon.png");
            this.syncIconImages.Images.SetKeyName(2, "Actions-go-previous-icon.png");
            this.syncIconImages.Images.SetKeyName(3, "Actions-go-previous-view-icon.png");
            this.syncIconImages.Images.SetKeyName(4, "sign-sync-icon.png");
            this.syncIconImages.Images.SetKeyName(5, "Editing-Compare-icon.png");
            this.syncIconImages.Images.SetKeyName(6, "Cancel.png");
            // 
            // SyncBothWaysLeftButton
            // 
            this.SyncBothWaysLeftButton.ImageIndex = 4;
            this.SyncBothWaysLeftButton.ImageList = this.syncIconImages;
            this.SyncBothWaysLeftButton.Location = new System.Drawing.Point(90, 3);
            this.SyncBothWaysLeftButton.Name = "SyncBothWaysLeftButton";
            this.SyncBothWaysLeftButton.Size = new System.Drawing.Size(80, 42);
            this.SyncBothWaysLeftButton.TabIndex = 5;
            this.SyncBothWaysLeftButton.UseVisualStyleBackColor = true;
            this.SyncBothWaysLeftButton.Click += new System.EventHandler(this.SyncBothWaysLeftButton_Click);
            // 
            // SyncRightLeftButton
            // 
            this.SyncRightLeftButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SyncRightLeftButton.ImageIndex = 2;
            this.SyncRightLeftButton.ImageList = this.syncIconImages;
            this.SyncRightLeftButton.Location = new System.Drawing.Point(820, 3);
            this.SyncRightLeftButton.Name = "SyncRightLeftButton";
            this.SyncRightLeftButton.Size = new System.Drawing.Size(80, 42);
            this.SyncRightLeftButton.TabIndex = 4;
            this.SyncRightLeftButton.UseVisualStyleBackColor = true;
            this.SyncRightLeftButton.Click += new System.EventHandler(this.SyncRightLeftButton_Click);
            // 
            // SyncLeftRightButton
            // 
            this.SyncLeftRightButton.ImageIndex = 1;
            this.SyncLeftRightButton.ImageList = this.syncIconImages;
            this.SyncLeftRightButton.Location = new System.Drawing.Point(4, 3);
            this.SyncLeftRightButton.Name = "SyncLeftRightButton";
            this.SyncLeftRightButton.Size = new System.Drawing.Size(80, 42);
            this.SyncLeftRightButton.TabIndex = 2;
            this.SyncLeftRightButton.UseVisualStyleBackColor = true;
            this.SyncLeftRightButton.Click += new System.EventHandler(this.SyncLeftRightButton_Click);
            // 
            // compareButton
            // 
            this.compareButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.compareButton.ImageIndex = 5;
            this.compareButton.ImageList = this.syncIconImages;
            this.compareButton.Location = new System.Drawing.Point(409, 3);
            this.compareButton.Name = "compareButton";
            this.compareButton.Size = new System.Drawing.Size(80, 42);
            this.compareButton.TabIndex = 0;
            this.compareButton.UseVisualStyleBackColor = true;
            this.compareButton.Click += new System.EventHandler(this.compareButton_Click);
            // 
            // progressBar
            // 
            this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelSync.SetColumnSpan(this.progressBar, 2);
            this.progressBar.Location = new System.Drawing.Point(6, 479);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(904, 16);
            this.progressBar.TabIndex = 5;
            // 
            // SyncOptionsPanel
            // 
            this.SyncOptionsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelSync.SetColumnSpan(this.SyncOptionsPanel, 2);
            this.SyncOptionsPanel.Controls.Add(this.SyncOptionsButton);
            this.SyncOptionsPanel.Location = new System.Drawing.Point(6, 6);
            this.SyncOptionsPanel.Name = "SyncOptionsPanel";
            this.SyncOptionsPanel.Size = new System.Drawing.Size(904, 24);
            this.SyncOptionsPanel.TabIndex = 6;
            // 
            // SyncOptionsButton
            // 
            this.SyncOptionsButton.Location = new System.Drawing.Point(4, 0);
            this.SyncOptionsButton.Name = "SyncOptionsButton";
            this.SyncOptionsButton.Size = new System.Drawing.Size(75, 21);
            this.SyncOptionsButton.TabIndex = 0;
            this.SyncOptionsButton.Text = "Options";
            this.SyncOptionsButton.UseVisualStyleBackColor = true;
            this.SyncOptionsButton.Click += new System.EventHandler(this.SyncOptionsButton_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 133);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(447, 337);
            this.tableLayoutPanel1.TabIndex = 7;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.tableLayoutPanel2.Location = new System.Drawing.Point(462, 133);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(448, 337);
            this.tableLayoutPanel2.TabIndex = 8;
            // 
            // contextMenuLeftSideTree
            // 
            this.contextMenuLeftSideTree.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuLeftSideTree.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.syncLeftToRightToolStripTreeMenuItem,
            this.syncBothWaysToolStripLeftTreeMenuItem});
            this.contextMenuLeftSideTree.Name = "contextMenuLeftSide";
            this.contextMenuLeftSideTree.Size = new System.Drawing.Size(172, 56);
            // 
            // syncLeftToRightToolStripTreeMenuItem
            // 
            this.syncLeftToRightToolStripTreeMenuItem.Image = global::FolderSync.Properties.Resources.Actions_go_next_icon;
            this.syncLeftToRightToolStripTreeMenuItem.Name = "syncLeftToRightToolStripTreeMenuItem";
            this.syncLeftToRightToolStripTreeMenuItem.Size = new System.Drawing.Size(171, 26);
            this.syncLeftToRightToolStripTreeMenuItem.Text = "Sync Left to Right";
            // 
            // syncBothWaysToolStripLeftTreeMenuItem
            // 
            this.syncBothWaysToolStripLeftTreeMenuItem.Image = global::FolderSync.Properties.Resources.sign_sync_icon;
            this.syncBothWaysToolStripLeftTreeMenuItem.Name = "syncBothWaysToolStripLeftTreeMenuItem";
            this.syncBothWaysToolStripLeftTreeMenuItem.Size = new System.Drawing.Size(171, 26);
            this.syncBothWaysToolStripLeftTreeMenuItem.Text = "Sync Both Ways";
            // 
            // contextMenuLeftSideList
            // 
            this.contextMenuLeftSideList.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuLeftSideList.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.syncLeftToRightToolStripListMenuItem,
            this.syncBothWaysToolStripLeftListMenuItem,
            this.toolStripSeparator1,
            this.toolStripMenuItemDiffLeftToRight});
            this.contextMenuLeftSideList.Name = "contextMenuLeftSideList";
            this.contextMenuLeftSideList.Size = new System.Drawing.Size(172, 88);
            // 
            // syncLeftToRightToolStripListMenuItem
            // 
            this.syncLeftToRightToolStripListMenuItem.Image = global::FolderSync.Properties.Resources.Actions_go_next_icon;
            this.syncLeftToRightToolStripListMenuItem.Name = "syncLeftToRightToolStripListMenuItem";
            this.syncLeftToRightToolStripListMenuItem.Size = new System.Drawing.Size(171, 26);
            this.syncLeftToRightToolStripListMenuItem.Text = "Sync Left to Right";
            // 
            // syncBothWaysToolStripLeftListMenuItem
            // 
            this.syncBothWaysToolStripLeftListMenuItem.Image = global::FolderSync.Properties.Resources.sign_sync_icon;
            this.syncBothWaysToolStripLeftListMenuItem.Name = "syncBothWaysToolStripLeftListMenuItem";
            this.syncBothWaysToolStripLeftListMenuItem.Size = new System.Drawing.Size(171, 26);
            this.syncBothWaysToolStripLeftListMenuItem.Text = "Sync Both Ways";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(168, 6);
            // 
            // toolStripMenuItemDiffLeftToRight
            // 
            this.toolStripMenuItemDiffLeftToRight.Image = global::FolderSync.Properties.Resources.Editing_Compare_icon;
            this.toolStripMenuItemDiffLeftToRight.Name = "toolStripMenuItemDiffLeftToRight";
            this.toolStripMenuItemDiffLeftToRight.Size = new System.Drawing.Size(171, 26);
            this.toolStripMenuItemDiffLeftToRight.Text = "Diff Left to Right";
            // 
            // contextMenuRightSideTree
            // 
            this.contextMenuRightSideTree.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuRightSideTree.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.syncRightToLeftToolStripTreeMenuItem,
            this.syncBothWaysToolStripRightTreeMenuItem});
            this.contextMenuRightSideTree.Name = "contextMenuRightSide";
            this.contextMenuRightSideTree.Size = new System.Drawing.Size(172, 56);
            // 
            // syncRightToLeftToolStripTreeMenuItem
            // 
            this.syncRightToLeftToolStripTreeMenuItem.Image = global::FolderSync.Properties.Resources.Actions_go_previous_icon;
            this.syncRightToLeftToolStripTreeMenuItem.Name = "syncRightToLeftToolStripTreeMenuItem";
            this.syncRightToLeftToolStripTreeMenuItem.Size = new System.Drawing.Size(171, 26);
            this.syncRightToLeftToolStripTreeMenuItem.Text = "Sync Right to Left";
            // 
            // syncBothWaysToolStripRightTreeMenuItem
            // 
            this.syncBothWaysToolStripRightTreeMenuItem.Image = global::FolderSync.Properties.Resources.sign_sync_icon;
            this.syncBothWaysToolStripRightTreeMenuItem.Name = "syncBothWaysToolStripRightTreeMenuItem";
            this.syncBothWaysToolStripRightTreeMenuItem.Size = new System.Drawing.Size(171, 26);
            this.syncBothWaysToolStripRightTreeMenuItem.Text = "Sync Both Ways";
            // 
            // contextMenuRightSideList
            // 
            this.contextMenuRightSideList.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuRightSideList.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.syncRightToLeftToolStripListMenuItem,
            this.syncBothWaysToolStripRightListMenuItem,
            this.toolStripSeparator2,
            this.toolStripMenuItemDiffRightToLeft});
            this.contextMenuRightSideList.Name = "contextMenuRightSideList";
            this.contextMenuRightSideList.Size = new System.Drawing.Size(172, 88);
            // 
            // syncRightToLeftToolStripListMenuItem
            // 
            this.syncRightToLeftToolStripListMenuItem.Image = global::FolderSync.Properties.Resources.Actions_go_previous_icon;
            this.syncRightToLeftToolStripListMenuItem.Name = "syncRightToLeftToolStripListMenuItem";
            this.syncRightToLeftToolStripListMenuItem.Size = new System.Drawing.Size(171, 26);
            this.syncRightToLeftToolStripListMenuItem.Text = "Sync Right to Left";
            // 
            // syncBothWaysToolStripRightListMenuItem
            // 
            this.syncBothWaysToolStripRightListMenuItem.Image = global::FolderSync.Properties.Resources.sign_sync_icon;
            this.syncBothWaysToolStripRightListMenuItem.Name = "syncBothWaysToolStripRightListMenuItem";
            this.syncBothWaysToolStripRightListMenuItem.Size = new System.Drawing.Size(171, 26);
            this.syncBothWaysToolStripRightListMenuItem.Text = "Sync Both Ways";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(168, 6);
            // 
            // toolStripMenuItemDiffRightToLeft
            // 
            this.toolStripMenuItemDiffRightToLeft.Image = global::FolderSync.Properties.Resources.Editing_Compare_icon;
            this.toolStripMenuItemDiffRightToLeft.Name = "toolStripMenuItemDiffRightToLeft";
            this.toolStripMenuItemDiffRightToLeft.Size = new System.Drawing.Size(171, 26);
            this.toolStripMenuItemDiffRightToLeft.Text = "Diff Right to Left";
            // 
            // leftFilenameHeader
            // 
            this.leftFilenameHeader.Text = "Filename";
            this.leftFilenameHeader.Width = 180;
            // 
            // leftFileSizeHeader
            // 
            this.leftFileSizeHeader.Text = "Size";
            this.leftFileSizeHeader.Width = 100;
            // 
            // leftLastModifiedHeader
            // 
            this.leftLastModifiedHeader.Text = "Last Modified";
            this.leftLastModifiedHeader.Width = 120;
            // 
            // rightFilenameHeader
            // 
            this.rightFilenameHeader.Text = "Filename";
            this.rightFilenameHeader.Width = 180;
            // 
            // rightSizeHeader
            // 
            this.rightSizeHeader.Text = "Size";
            this.rightSizeHeader.Width = 100;
            // 
            // rightLastModifiedHeader
            // 
            this.rightLastModifiedHeader.Text = "Last Modified";
            this.rightLastModifiedHeader.Width = 120;
            // 
            // leftBrowseTypeContextMenu
            // 
            this.leftBrowseTypeContextMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.leftBrowseTypeContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.leftArchiveToolStripMenuItem,
            this.leftFTPToolStripMenuItem});
            this.leftBrowseTypeContextMenu.Name = "leftBrowseTypeContextMenu";
            this.leftBrowseTypeContextMenu.Size = new System.Drawing.Size(115, 48);
            // 
            // leftArchiveToolStripMenuItem
            // 
            this.leftArchiveToolStripMenuItem.Name = "leftArchiveToolStripMenuItem";
            this.leftArchiveToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.leftArchiveToolStripMenuItem.Text = "Archive";
            this.leftArchiveToolStripMenuItem.Click += new System.EventHandler(this.leftArchiveToolStripMenuItem_Click);
            // 
            // leftFTPToolStripMenuItem
            // 
            this.leftFTPToolStripMenuItem.Name = "leftFTPToolStripMenuItem";
            this.leftFTPToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.leftFTPToolStripMenuItem.Text = "FTP";
            this.leftFTPToolStripMenuItem.Click += new System.EventHandler(this.leftFTPToolStripMenuItem_Click);
            // 
            // rightBrowseTypeContextMenu
            // 
            this.rightBrowseTypeContextMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.rightBrowseTypeContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rightArchiveToolStripMenuItem,
            this.rightFTPToolStripMenuItem});
            this.rightBrowseTypeContextMenu.Name = "rightBrowseTypeContextMenu";
            this.rightBrowseTypeContextMenu.Size = new System.Drawing.Size(115, 48);
            // 
            // rightArchiveToolStripMenuItem
            // 
            this.rightArchiveToolStripMenuItem.Name = "rightArchiveToolStripMenuItem";
            this.rightArchiveToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.rightArchiveToolStripMenuItem.Text = "Archive";
            this.rightArchiveToolStripMenuItem.Click += new System.EventHandler(this.rightArchiveToolStripMenuItem_Click);
            // 
            // rightFTPToolStripMenuItem
            // 
            this.rightFTPToolStripMenuItem.Name = "rightFTPToolStripMenuItem";
            this.rightFTPToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.rightFTPToolStripMenuItem.Text = "FTP";
            this.rightFTPToolStripMenuItem.Click += new System.EventHandler(this.rightFTPToolStripMenuItem_Click);
            // 
            // IconImages
            // 
            this.IconImages.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("IconImages.ImageStream")));
            this.IconImages.TransparentColor = System.Drawing.Color.Transparent;
            this.IconImages.Images.SetKeyName(0, "icons8-file-26.png");
            this.IconImages.Images.SetKeyName(1, "icons8-color-48.png");
            // 
            // frmVisualDiffSync
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(919, 519);
            this.Controls.Add(this.tabControl);
            this.Name = "frmVisualDiffSync";
            this.Text = "Visual Diff/Sync";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmVisualDiffSync_Load);
            this.tabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tableLayoutPanelSync.ResumeLayout(false);
            this.leftSideFolderPanel.ResumeLayout(false);
            this.rightSideFolderPanel.ResumeLayout(false);
            this.comparePanel.ResumeLayout(false);
            this.SyncOptionsPanel.ResumeLayout(false);
            this.contextMenuLeftSideTree.ResumeLayout(false);
            this.contextMenuLeftSideList.ResumeLayout(false);
            this.contextMenuRightSideTree.ResumeLayout(false);
            this.contextMenuRightSideList.ResumeLayout(false);
            this.leftBrowseTypeContextMenu.ResumeLayout(false);
            this.rightBrowseTypeContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelSync;
        private System.Windows.Forms.Panel leftSideFolderPanel;
        private System.Windows.Forms.Panel rightSideFolderPanel;
        private System.Windows.Forms.Button leftSideBrowseButton;
        private System.Windows.Forms.ComboBox leftSideFolderComboBox;
        private System.Windows.Forms.Button rightSideBrowseButton;
        private System.Windows.Forms.ComboBox rightSideFolderComboBox;
        private System.Windows.Forms.Panel comparePanel;
        private System.Windows.Forms.Button compareButton;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.ContextMenuStrip contextMenuLeftSideTree;
        private System.Windows.Forms.ToolStripMenuItem syncLeftToRightToolStripTreeMenuItem;
        private System.Windows.Forms.ToolStripMenuItem syncBothWaysToolStripLeftTreeMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuRightSideTree;
        private System.Windows.Forms.ToolStripMenuItem syncRightToLeftToolStripTreeMenuItem;
        private System.Windows.Forms.ToolStripMenuItem syncBothWaysToolStripRightTreeMenuItem;
        private System.Windows.Forms.Button SyncRightLeftButton;
        private System.Windows.Forms.Button SyncLeftRightButton;
        private System.Windows.Forms.Button SyncBothWaysRightButton;
        private System.Windows.Forms.Button SyncBothWaysLeftButton;
        private System.Windows.Forms.ImageList syncIconImages;
        private System.Windows.Forms.ToolTip syncToolTips;
        private System.Windows.Forms.Panel SyncOptionsPanel;
        private System.Windows.Forms.Button SyncOptionsButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.ColumnHeader leftFilenameHeader;
        private System.Windows.Forms.ColumnHeader leftFileSizeHeader;
        private System.Windows.Forms.ColumnHeader leftLastModifiedHeader;
        private System.Windows.Forms.ColumnHeader rightFilenameHeader;
        private System.Windows.Forms.ColumnHeader rightSizeHeader;
        private System.Windows.Forms.ColumnHeader rightLastModifiedHeader;
        private System.Windows.Forms.ContextMenuStrip contextMenuLeftSideList;
        private System.Windows.Forms.ContextMenuStrip contextMenuRightSideList;
        private System.Windows.Forms.ToolStripMenuItem syncLeftToRightToolStripListMenuItem;
        private System.Windows.Forms.ToolStripMenuItem syncBothWaysToolStripLeftListMenuItem;
        private System.Windows.Forms.ToolStripMenuItem syncRightToLeftToolStripListMenuItem;
        private System.Windows.Forms.ToolStripMenuItem syncBothWaysToolStripRightListMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemDiffLeftToRight;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemDiffRightToLeft;
        private System.Windows.Forms.Button leftSideBrowseTypeButton;
        private System.Windows.Forms.ContextMenuStrip leftBrowseTypeContextMenu;
        private System.Windows.Forms.ContextMenuStrip rightBrowseTypeContextMenu;
        private System.Windows.Forms.Button rightSideBrowseTypeButton;
        private System.Windows.Forms.ToolStripMenuItem leftArchiveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem leftFTPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rightArchiveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rightFTPToolStripMenuItem;
        private System.Windows.Forms.ImageList IconImages;
    }
}

