﻿namespace FolderSync
{
    partial class FtpDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ftpHostnameTextBox = new System.Windows.Forms.TextBox();
            this.ftpHostnameLabel = new System.Windows.Forms.Label();
            this.ftpPanel = new System.Windows.Forms.Panel();
            this.ftpPortTextBox = new System.Windows.Forms.TextBox();
            this.ftpPortLabel = new System.Windows.Forms.Label();
            this.ftpPasswordTextBox = new System.Windows.Forms.TextBox();
            this.ftpPasswordLabel = new System.Windows.Forms.Label();
            this.ftpUsernameTextBox = new System.Windows.Forms.TextBox();
            this.ftpUsernameLabel = new System.Windows.Forms.Label();
            this.OkButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.ftpPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ftpHostnameTextBox
            // 
            this.ftpHostnameTextBox.Location = new System.Drawing.Point(64, 19);
            this.ftpHostnameTextBox.Name = "ftpHostnameTextBox";
            this.ftpHostnameTextBox.Size = new System.Drawing.Size(185, 20);
            this.ftpHostnameTextBox.TabIndex = 2;
            // 
            // ftpHostnameLabel
            // 
            this.ftpHostnameLabel.AutoSize = true;
            this.ftpHostnameLabel.Location = new System.Drawing.Point(3, 22);
            this.ftpHostnameLabel.Name = "ftpHostnameLabel";
            this.ftpHostnameLabel.Size = new System.Drawing.Size(55, 13);
            this.ftpHostnameLabel.TabIndex = 0;
            this.ftpHostnameLabel.Text = "Hostname";
            // 
            // ftpPanel
            // 
            this.ftpPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ftpPanel.Controls.Add(this.ftpPortTextBox);
            this.ftpPanel.Controls.Add(this.ftpPortLabel);
            this.ftpPanel.Controls.Add(this.ftpPasswordTextBox);
            this.ftpPanel.Controls.Add(this.ftpPasswordLabel);
            this.ftpPanel.Controls.Add(this.ftpUsernameTextBox);
            this.ftpPanel.Controls.Add(this.ftpHostnameTextBox);
            this.ftpPanel.Controls.Add(this.ftpUsernameLabel);
            this.ftpPanel.Controls.Add(this.ftpHostnameLabel);
            this.ftpPanel.Location = new System.Drawing.Point(1, 2);
            this.ftpPanel.Name = "ftpPanel";
            this.ftpPanel.Size = new System.Drawing.Size(411, 121);
            this.ftpPanel.TabIndex = 1;
            // 
            // ftpPortTextBox
            // 
            this.ftpPortTextBox.Location = new System.Drawing.Point(312, 19);
            this.ftpPortTextBox.Name = "ftpPortTextBox";
            this.ftpPortTextBox.Size = new System.Drawing.Size(78, 20);
            this.ftpPortTextBox.TabIndex = 7;
            // 
            // ftpPortLabel
            // 
            this.ftpPortLabel.AutoSize = true;
            this.ftpPortLabel.Location = new System.Drawing.Point(280, 22);
            this.ftpPortLabel.Name = "ftpPortLabel";
            this.ftpPortLabel.Size = new System.Drawing.Size(26, 13);
            this.ftpPortLabel.TabIndex = 6;
            this.ftpPortLabel.Text = "Port";
            // 
            // ftpPasswordTextBox
            // 
            this.ftpPasswordTextBox.Location = new System.Drawing.Point(64, 74);
            this.ftpPasswordTextBox.Name = "ftpPasswordTextBox";
            this.ftpPasswordTextBox.Size = new System.Drawing.Size(185, 20);
            this.ftpPasswordTextBox.TabIndex = 5;
            this.ftpPasswordTextBox.UseSystemPasswordChar = true;
            // 
            // ftpPasswordLabel
            // 
            this.ftpPasswordLabel.AutoSize = true;
            this.ftpPasswordLabel.Location = new System.Drawing.Point(5, 77);
            this.ftpPasswordLabel.Name = "ftpPasswordLabel";
            this.ftpPasswordLabel.Size = new System.Drawing.Size(53, 13);
            this.ftpPasswordLabel.TabIndex = 4;
            this.ftpPasswordLabel.Text = "Password";
            // 
            // ftpUsernameTextBox
            // 
            this.ftpUsernameTextBox.Location = new System.Drawing.Point(64, 45);
            this.ftpUsernameTextBox.Name = "ftpUsernameTextBox";
            this.ftpUsernameTextBox.Size = new System.Drawing.Size(185, 20);
            this.ftpUsernameTextBox.TabIndex = 3;
            // 
            // ftpUsernameLabel
            // 
            this.ftpUsernameLabel.AutoSize = true;
            this.ftpUsernameLabel.Location = new System.Drawing.Point(3, 48);
            this.ftpUsernameLabel.Name = "ftpUsernameLabel";
            this.ftpUsernameLabel.Size = new System.Drawing.Size(55, 13);
            this.ftpUsernameLabel.TabIndex = 1;
            this.ftpUsernameLabel.Text = "Username";
            // 
            // OkButton
            // 
            this.OkButton.Location = new System.Drawing.Point(248, 129);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(75, 23);
            this.OkButton.TabIndex = 2;
            this.OkButton.Text = "OK";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(329, 129);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 3;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // FtpDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(412, 163);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.ftpPanel);
            this.Name = "FtpDlg";
            this.Text = "FtpDlg";
            this.ftpPanel.ResumeLayout(false);
            this.ftpPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox ftpHostnameTextBox;
        private System.Windows.Forms.Label ftpHostnameLabel;
        private System.Windows.Forms.Panel ftpPanel;
        private System.Windows.Forms.TextBox ftpPortTextBox;
        private System.Windows.Forms.Label ftpPortLabel;
        private System.Windows.Forms.TextBox ftpPasswordTextBox;
        private System.Windows.Forms.Label ftpPasswordLabel;
        private System.Windows.Forms.TextBox ftpUsernameTextBox;
        private System.Windows.Forms.Label ftpUsernameLabel;
        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.Button cancelButton;
    }
}