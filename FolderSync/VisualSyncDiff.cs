﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using SyncApi;
using VisualDiff;
using ZetaLongPaths;
using VisualTree;
using DocumentReader;

namespace FolderSync
{
    public partial class frmVisualDiffSync : Form
    {
        private MyTreeView leftTreeView = null;
        private MyTreeView rightTreeView = null;

        private MyListView leftListView = null;
        private MyListView rightListView = null;

        private TreeNode mLeftTreeRootItem = null;
        private TreeNode mRightTreeRootItem = null;

        private IFolderTreeComparerEx mFolderComparer = null;
        private FolderTreeNode mRootMergedNode = null;
        private string mFolder1 = null;
        private string mFolder2 = null;
        private bool mIsScrolling = false;
        private bool mIsExpanding = false;
        private bool mIsCollapsing = false;
        private bool mIsListViewSelecting = false;
        private SyncOptions mSyncOptions = null;
        private CompareOptions mCompareOptions = null;
        private VisualOptions mVisualOptions = null;

        private TreeNode mLeftSelectedTreeNode = null;
        private TreeNode mRightSelectedTreeNode = null;
        private ListViewItem mLeftSelectedListViewItem = null;
        private ListViewItem mRightSelectedListViewItem = null;

        private FolderTreeNode mSelectedFolderNode = null;

        FtpCredentials mFtpCredentials = null;

        private const int FILE_ICON_IMAGE_INDEX = 0;
        private const int FOLDER_ICON_IMAGE_INDEX = 1;

        private Font mFont = null;

        private CancellationTokenSource mCancellationTokenSource = null;

        private const int COMPARE_ICON_IMAGE_INDEX = 5;
        private const int CANCEL_ICON_IMAGE_INDEX = 6;

        public frmVisualDiffSync()
        {
            InitializeComponent();

            //If there is no SyncOptions.xml file yet just use some sensible default values. The
            //first time the user selects the Options button and clicks OK the options will be
            //serialized
            mSyncOptions = SyncOptions.Deserialize("SyncOptions.xml");
            if (mSyncOptions == null)
            {
                mSyncOptions = new SyncOptions()
                {
                    PropagateDeletes = true,
                    TempPath = System.IO.Path.GetTempPath(),
                    LogSyncPosFilenamePrefix = System.IO.Path.GetTempPath() + @"syncpos.txt",
                    LogFilenamePrefix = System.IO.Path.GetTempPath() + @"synclog.txt",
                };
            }

            mCompareOptions = CompareOptions.Deserialize("CompareOptions.xml");
            if (mCompareOptions == null)
            {
                mCompareOptions = new CompareOptions();
            }

            mVisualOptions = VisualOptions.Deserialize("VisualOptions.xml");
            if (mVisualOptions == null)
            {
                mVisualOptions = new VisualOptions();
            }

            mFont = new Font(mVisualOptions.FontName, mVisualOptions.FontSize);

            InitializeLeftView();
            InitializeRightView();

            leftTreeView.VScroll += LeftTreeView_VScroll;
            leftTreeView.HScroll += LeftTreeView_HScroll;

            rightTreeView.VScroll += RightTreeView_VScroll;
            rightTreeView.HScroll += RightTreeView_HScroll;

            leftListView.VScroll += LeftListView_VScroll;
            leftListView.HScroll += LeftListView_HScroll;

            rightListView.VScroll += RightListView_VScroll;
            rightListView.HScroll += RightListView_HScroll;

            //Left tree context menu
            this.contextMenuLeftSideTree.Items[0].Click += FrmVisualDiffSync_SyncFolderLeftToRightClick;
            this.contextMenuLeftSideTree.Items[1].Click += FrmVisualDiffSync_SyncFolderBothWaysClickLeft;
            //this.contextMenuLeftSide.Items[2] -- This is the separator

            //Right tree context menu
            this.contextMenuRightSideTree.Items[0].Click += FrmVisualDiffSync_SyncFolderRightToLeftClick;
            this.contextMenuRightSideTree.Items[1].Click += FrmVisualDiffSync_SyncFolderBothWaysClickRight;
            //this.contextMenuRightSide.Items[2] -- This is the separator

            //Left list context menu
            this.contextMenuLeftSideList.Items[0].Click += FrmVisualDiffSync_SyncFileLeftToRightClick;
            this.contextMenuLeftSideList.Items[1].Click += FrmVisualDiffSync_SyncFileBothWaysClickLeft;
            //this.contextMenuLeftSideList.Items[2] -- This is the separator
            this.contextMenuLeftSideList.Items[3].Click += FrmVisualDiffSync_DiffLeftToRightClick;

            //Right list context menu
            this.contextMenuRightSideList.Items[0].Click += FrmVisualDiffSync_SyncFileRightToLeftClick;
            this.contextMenuRightSideList.Items[1].Click += FrmVisualDiffSync_SyncFileBothWaysClickRight;
            //this.contextMenuRightSideList.Items[2] -- This is the separator
            this.contextMenuRightSideList.Items[3].Click += FrmVisualDiffSync_DiffRightToLeftClick;
        }

        private void InitializeLeftView()
        {
            this.leftTreeView = new FolderSync.MyTreeView();
            this.leftListView = new FolderSync.MyListView();

            // 
            // leftTreeView
            // 
            this.leftTreeView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.leftTreeView.ContextMenuStrip = this.contextMenuLeftSideTree;
            this.leftTreeView.Location = new System.Drawing.Point(3, 3);
            this.leftTreeView.Name = "leftTreeView";
            this.leftTreeView.Size = new System.Drawing.Size(150, 331);
            this.leftTreeView.TabIndex = 4;
            this.leftTreeView.ImageList = IconImages;
            this.leftTreeView.Font = mFont;
            this.leftTreeView.AfterCollapse += new System.Windows.Forms.TreeViewEventHandler(this.leftTreeView_AfterCollapse);
            this.leftTreeView.AfterExpand += new System.Windows.Forms.TreeViewEventHandler(this.leftTreeView_AfterExpand);
            this.leftTreeView.BeforeSelect += new System.Windows.Forms.TreeViewCancelEventHandler(this.leftTreeView_BeforeSelect);
            this.leftTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.leftTreeView_AfterSelect);
            // 
            // leftListView
            // 
            this.leftListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.leftListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.leftFilenameHeader,
            this.leftFileSizeHeader,
            this.leftLastModifiedHeader});
            this.leftListView.ContextMenuStrip = this.contextMenuLeftSideList;
            this.leftListView.FullRowSelect = true;
            this.leftListView.Location = new System.Drawing.Point(159, 3);
            this.leftListView.Name = "leftListView";
            this.leftListView.Size = new System.Drawing.Size(285, 331);
            this.leftListView.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.leftListView.TabIndex = 5;
            this.leftListView.UseCompatibleStateImageBehavior = false;
            this.leftListView.View = System.Windows.Forms.View.Details;
            this.leftListView.SmallImageList = IconImages;
            this.leftListView.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.leftListView_ItemSelectionChanged);
            this.leftListView.MouseDoubleClick += LeftListView_MouseDoubleClick;

            this.tableLayoutPanel1.Controls.Add(this.leftTreeView, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.leftListView, 1, 0);
        }

        private void InitializeRightView()
        {
            this.rightTreeView = new FolderSync.MyTreeView();
            this.rightListView = new FolderSync.MyListView();

            // 
            // rightTreeView
            // 
            this.rightTreeView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rightTreeView.ContextMenuStrip = this.contextMenuRightSideTree;
            this.rightTreeView.Location = new System.Drawing.Point(3, 3);
            this.rightTreeView.Name = "rightTreeView";
            this.rightTreeView.Size = new System.Drawing.Size(150, 331);
            this.rightTreeView.TabIndex = 5;
            this.rightTreeView.ImageList = IconImages;
            this.rightTreeView.Font = mFont;
            this.rightTreeView.AfterCollapse += new System.Windows.Forms.TreeViewEventHandler(this.rightTreeView_AfterCollapse);
            this.rightTreeView.AfterExpand += new System.Windows.Forms.TreeViewEventHandler(this.rightTreeView_AfterExpand);
            this.rightTreeView.BeforeSelect += new System.Windows.Forms.TreeViewCancelEventHandler(this.rightTreeView_BeforeSelect);
            this.rightTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.rightTreeView_AfterSelect);
            // 
            // rightListView
            // 
            this.rightListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rightListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.rightFilenameHeader,
            this.rightSizeHeader,
            this.rightLastModifiedHeader});
            this.rightListView.ContextMenuStrip = this.contextMenuRightSideList;
            this.rightListView.FullRowSelect = true;
            this.rightListView.Location = new System.Drawing.Point(159, 3);
            this.rightListView.Name = "rightListView";
            this.rightListView.Size = new System.Drawing.Size(286, 331);
            this.rightListView.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.rightListView.TabIndex = 6;
            this.rightListView.UseCompatibleStateImageBehavior = false;
            this.rightListView.View = System.Windows.Forms.View.Details;
            this.rightListView.SmallImageList = IconImages;
            this.rightListView.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.rightListView_ItemSelectionChanged);
            this.rightListView.MouseDoubleClick += RightListView_MouseDoubleClick;

            this.tableLayoutPanel2.Controls.Add(this.rightTreeView, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.rightListView, 1, 0);
        }


        private void LeftListView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ListViewHitTestInfo info = leftListView.HitTest(e.X, e.Y);
            ListViewItem item = info.Item;

            if (item != null)
            {
                var selectedNode = mSelectedFolderNode.Children.Single(x => x.Name == item.Text);
                var selectedFolderNode = selectedNode as FolderTreeNode;
                if (selectedFolderNode != null)
                {
                    PopulateListViews(selectedFolderNode);
                    mSelectedFolderNode = selectedFolderNode;

                    leftSideFolderComboBox.Text = mFolderComparer.BasePath1 + mSelectedFolderNode.RelativePath;
                    rightSideFolderComboBox.Text = mFolderComparer.BasePath2 + mSelectedFolderNode.RelativePath;
                }
            }
        }

        private void RightListView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ListViewHitTestInfo info = rightListView.HitTest(e.X, e.Y);
            ListViewItem item = info.Item;

            if (item != null)
            {
                var selectedNode = mSelectedFolderNode.Children.Single(x => x.Name == item.Text);
                var selectedFolderNode = selectedNode as FolderTreeNode;
                if (selectedFolderNode != null)
                {
                    PopulateListViews(selectedFolderNode);
                    mSelectedFolderNode = selectedFolderNode;

                    leftSideFolderComboBox.Text = mFolderComparer.BasePath1 + mSelectedFolderNode.RelativePath;
                    rightSideFolderComboBox.Text = mFolderComparer.BasePath2 + mSelectedFolderNode.RelativePath;
                }
            }
        }

        private void FrmVisualDiffSync_DiffLeftToRightClick(object sender, EventArgs e)
        {
            var selectedItems = leftListView.SelectedItems;
            if (selectedItems.Count == 0)
            {
                MessageBox.Show("No files selected");
            }
            else if (selectedItems.Count > 1)
            {
                MessageBox.Show("You can only perform a Diff on a single selected file");
            }
            else
            {
                string path = GetPathFromListViewItem(mSelectedFolderNode, selectedItems[0].Text);

                IFolderChildNode node = mFolderComparer.GetNode(path);
                FileTreeNode fileTreeNode = node as FileTreeNode;
                if (fileTreeNode != null)
                {
                    if (fileTreeNode.NodeLocation == SyncApi.Location.Both)
                    {
                        string leftFile = mFolderComparer.BasePath1 + fileTreeNode.RelativePath;
                        string rightFile = mFolderComparer.BasePath2 + fileTreeNode.RelativePath;
                        VisualDiffFrm diffFrm = new VisualDiffFrm(leftFile, rightFile);
                        diffFrm.Show();
                    }
                    else
                    {
                        if (fileTreeNode.NodeLocation != SyncApi.Location.Both)
                            MessageBox.Show("Diff can only be performed on files that exist in both locations");
                        else
                            MessageBox.Show("Unknown Diff error");
                    }
                }
                else
                {
                    MessageBox.Show("Diff can only be performed on two files");
                }

            }
        }

        private void FrmVisualDiffSync_DiffRightToLeftClick(object sender, EventArgs e)
        {
            var selectedItems = rightListView.SelectedItems;
            if (selectedItems.Count == 0)
            {
                MessageBox.Show("No files selected");
            }
            else if (selectedItems.Count > 1)
            {
                MessageBox.Show("You can only perform a Diff on a single selected file");
            }
            else
            {
                string path = GetPathFromListViewItem(mSelectedFolderNode, selectedItems[0].Text);

                IFolderChildNode node = mFolderComparer.GetNode(path);
                FileTreeNode fileTreeNode = node as FileTreeNode;
                if (fileTreeNode != null)
                {
                    if (fileTreeNode.NodeLocation == SyncApi.Location.Both)
                    {
                        string leftFile = mFolderComparer.BasePath2 + fileTreeNode.RelativePath;
                        string rightFile = mFolderComparer.BasePath1 + fileTreeNode.RelativePath;
                        VisualDiffFrm diffFrm = new VisualDiffFrm(leftFile, rightFile);
                        diffFrm.Show();
                    }
                    else
                    {
                        if (fileTreeNode.NodeLocation != SyncApi.Location.Both)
                            MessageBox.Show("Diff can only be performed on files that exist in both locations");
                        else
                            MessageBox.Show("Unknown Diff error");
                    }
                }
                else
                {
                    MessageBox.Show("Diff can only be performed on two files");
                }
            }
        }

        private async void FrmVisualDiffSync_SyncFolderLeftToRightClick(object sender, EventArgs e)
        {
            TreeNode selectedNode = leftTreeView.SelectedNode;
            if (selectedNode != null)
            {
                string path = GetPathFromTreeViewItem(selectedNode);

                IFolderChildNode node = mFolderComparer.GetNode(path);

                if (node is FolderTreeNode)
                {
                    FolderTreeNode folderNode = (FolderTreeNode)node;

                    ISyncManager syncManager = GetSyncManager();

                    await SyncFolderTreeAsync(
                        new FolderTreeOneWaySync(mFolderComparer.BasePath1, mFolderComparer.BasePath2, folderNode, SyncDirection.LeftToRight, syncManager, mSyncOptions));
                }
            }
        }

        private async void FrmVisualDiffSync_SyncFileLeftToRightClick(object sender, EventArgs e)
        {
            var selectedItems = leftListView.SelectedItems;
            if (selectedItems.Count == 0)
            {
                MessageBox.Show("No files selected");
            }
            else if (selectedItems.Count > 1)
            {
                MessageBox.Show("You can only perform a file sync on a single selected file");
            }
            else
            {
                string path = GetPathFromListViewItem(mSelectedFolderNode, selectedItems[0].Text);

                IFolderChildNode node = mFolderComparer.GetNode(path);

                if (node is FileTreeNode)
                {
                    FileTreeNode fileNode = (FileTreeNode)node;

                    IFileTreeSync fileSync = new FileTreeOneWaySync(mFolderComparer.BasePath1, mFolderComparer.BasePath2, SyncDirection.LeftToRight, new SyncDirect(), mSyncOptions);
                    fileSync.Sync(fileNode);

                    MessageBox.Show("Sync Completed", "Sync");
                }
                else if (node is FolderTreeNode)
                {
                    FolderTreeNode folderNode = (FolderTreeNode)node;

                    ISyncManager syncManager = GetSyncManager();

                    await SyncFolderTreeAsync(
                        new FolderTreeOneWaySync(mFolderComparer.BasePath1, mFolderComparer.BasePath2, folderNode, SyncDirection.LeftToRight, syncManager, mSyncOptions));
                }
                else
                {
                    MessageBox.Show("ERROR: Invalid item in list: " + path, "Sync");
                }
                //}
            }
        }
        
        private async void FrmVisualDiffSync_SyncFolderBothWaysClickLeft(object sender, EventArgs e)
        {
            TreeNode selectedNode = leftTreeView.SelectedNode;
            if (selectedNode != null)
            {
                string path = GetPathFromTreeViewItem(selectedNode);
                IFolderChildNode node = mFolderComparer.GetNode(path);

                if (node is FolderTreeNode)
                {
                    FolderTreeNode folderNode = (FolderTreeNode)node;

                    ISyncManager syncManager = GetSyncManager();

                    await SyncFolderTreeAsync(
                        new FolderTreeTwoWaySync(mFolderComparer.BasePath1, mFolderComparer.BasePath2, folderNode, syncManager, mSyncOptions));
                }
            }
        }

        private async void FrmVisualDiffSync_SyncFileBothWaysClickLeft(object sender, EventArgs e)
        {
            var selectedItems = leftListView.SelectedItems;
            if (selectedItems.Count == 0)
            {
                MessageBox.Show("No files selected");
            }
            else if (selectedItems.Count > 1)
            {
                MessageBox.Show("You can only perform a file sync on a single selected file");
            }
            else
            {
                string path = GetPathFromListViewItem(mSelectedFolderNode, selectedItems[0].Text);

                IFolderChildNode node = mFolderComparer.GetNode(path);

                if (node is FileTreeNode)
                {
                    FileTreeNode fileNode = (FileTreeNode)node;

                    IFileTreeSync fileSync = new FileTreeTwoWaySync(mFolderComparer.BasePath1, mFolderComparer.BasePath2, new SyncDirect(), mSyncOptions);
                    fileSync.Sync(fileNode);

                    MessageBox.Show("Sync Completed", "Sync");
                }
                else if (node is FolderTreeNode)
                {
                    FolderTreeNode folderNode = (FolderTreeNode)node;

                    ISyncManager syncManager = GetSyncManager();

                    await SyncFolderTreeAsync(
                        new FolderTreeTwoWaySync(mFolderComparer.BasePath1, mFolderComparer.BasePath2, folderNode, syncManager, mSyncOptions));
                }
                else
                {
                    MessageBox.Show("ERROR: Invalid item in list: " + path, "Sync");
                }
            }
        }

        private async void FrmVisualDiffSync_SyncFolderRightToLeftClick(object sender, EventArgs e)
        {
            TreeNode selectedNode = rightTreeView.SelectedNode;
            if (selectedNode != null)
            {
                string path = GetPathFromTreeViewItem(selectedNode);
                IFolderChildNode node = mFolderComparer.GetNode(path);

                if (node is FolderTreeNode)
                {
                    FolderTreeNode folderNode = (FolderTreeNode)node;

                    ISyncManager syncManager = GetSyncManager();

                    await SyncFolderTreeAsync(
                        new FolderTreeOneWaySync(mFolderComparer.BasePath2, mFolderComparer.BasePath1, folderNode, SyncDirection.RightToLeft, syncManager, mSyncOptions));
                }
            }
        }

        private async void FrmVisualDiffSync_SyncFileRightToLeftClick(object sender, EventArgs e)
        {
            var selectedItems = rightListView.SelectedItems;
            if (selectedItems.Count == 0)
            {
                MessageBox.Show("No files selected");
            }
            else if (selectedItems.Count > 1)
            {
                MessageBox.Show("You can only perform a file sync on a single selected file");
            }
            else
            {
                string path = GetPathFromListViewItem(mSelectedFolderNode, selectedItems[0].Text);

                IFolderChildNode node = mFolderComparer.GetNode(path);

                if (node is FileTreeNode)
                {
                    FileTreeNode fileNode = (FileTreeNode)node;

                    IFileTreeSync fileSync = new FileTreeOneWaySync(mFolderComparer.BasePath2, mFolderComparer.BasePath1, SyncDirection.RightToLeft, new SyncDirect(), mSyncOptions);
                    fileSync.Sync(fileNode);

                    MessageBox.Show("Sync Completed", "Sync");
                }
                else if (node is FolderTreeNode)
                {
                    FolderTreeNode folderNode = (FolderTreeNode)node;

                    ISyncManager syncManager = GetSyncManager();

                    await SyncFolderTreeAsync(
                        new FolderTreeOneWaySync(mFolderComparer.BasePath2, mFolderComparer.BasePath1, folderNode, SyncDirection.RightToLeft, syncManager, mSyncOptions));
                }
                else
                {
                    MessageBox.Show("ERROR: Invalid item in list: " + path, "Sync");
                }
            }
        }

        private async void FrmVisualDiffSync_SyncFolderBothWaysClickRight(object sender, EventArgs e)
        {
            TreeNode selectedNode = rightTreeView.SelectedNode;
            if (selectedNode != null)
            {
                string path = GetPathFromTreeViewItem(selectedNode);
                IFolderChildNode node = mFolderComparer.GetNode(path);

                if (node is FolderTreeNode)
                {
                    FolderTreeNode folderNode = (FolderTreeNode)node;

                    ISyncManager syncManager = GetSyncManager();

                    await SyncFolderTreeAsync(
                        new FolderTreeTwoWaySync(mFolderComparer.BasePath1, mFolderComparer.BasePath2, folderNode, syncManager, mSyncOptions));
                }
            }
        }

        private async void FrmVisualDiffSync_SyncFileBothWaysClickRight(object sender, EventArgs e)
        {
            var selectedItems = rightListView.SelectedItems;
            if (selectedItems.Count == 0)
            {
                MessageBox.Show("No files selected");
            }
            else if (selectedItems.Count > 1)
            {
                MessageBox.Show("You can only perform a file sync on a single selected file");
            }
            else
            {
                string path = GetPathFromListViewItem(mSelectedFolderNode, selectedItems[0].Text);

                IFolderChildNode node = mFolderComparer.GetNode(path);

                if (node is FileTreeNode)
                {
                    FileTreeNode fileNode = (FileTreeNode)node;

                    IFileTreeSync fileSync = new FileTreeTwoWaySync(mFolderComparer.BasePath1, mFolderComparer.BasePath2, new SyncDirect(), mSyncOptions);
                    fileSync.Sync(fileNode);

                    MessageBox.Show("Sync Completed", "Sync");
                }
                else if (node is FolderTreeNode)
                {
                    FolderTreeNode folderNode = (FolderTreeNode)node;

                    ISyncManager syncManager = GetSyncManager();

                    await SyncFolderTreeAsync(
                        new FolderTreeTwoWaySync(mFolderComparer.BasePath1, mFolderComparer.BasePath2, folderNode, syncManager, mSyncOptions));
                }
                else
                {
                    MessageBox.Show("ERROR: Invalid item in list: " + path, "Sync");
                }
            }
        }

        private async Task SyncFolderTreeAsync(IFolderTreeSync folderTreeSync)
        {
            try
            {
                //Before starting the sync we want to turn on the progress bar
                TurnOnProgressBar();
                syncToolTips.SetToolTip(this.compareButton, "Cancel Sync");
                compareButton.ImageIndex = CANCEL_ICON_IMAGE_INDEX;

                mCancellationTokenSource = new CancellationTokenSource();

                await folderTreeSync.Sync(mCancellationTokenSource.Token);

                //Sync completed so turn off the progress bar
                TurnOffProgressBar();
                syncToolTips.SetToolTip(this.compareButton, "Compare Folders");
                compareButton.ImageIndex = COMPARE_ICON_IMAGE_INDEX;

                MessageBox.Show("Sync Completed", "Sync");
            }
            catch(OperationCanceledException ex)
            {
                TurnOffProgressBar();
                syncToolTips.SetToolTip(this.compareButton, "Compare Folders");
                compareButton.ImageIndex = COMPARE_ICON_IMAGE_INDEX;
                MessageBox.Show(ex.Message, "Sync Cancelled");
            }
            finally
            {
                ResetCancellationTokenSource();
            }
        }

        private void TurnOnProgressBar()
        {
            DisableControls();

            progressBar.Visible = true;
            progressBar.Style = ProgressBarStyle.Marquee;
            progressBar.MarqueeAnimationSpeed = 3;
        }

        private void TurnOffProgressBar()
        {
            progressBar.Visible = false;

            EnableControls();
        }

        private void EnableControls()
        {
            SyncLeftRightButton.Enabled = true;
            SyncBothWaysLeftButton.Enabled = true;
            SyncRightLeftButton.Enabled = true;
            SyncBothWaysRightButton.Enabled = true;

            contextMenuLeftSideTree.Enabled = true;
            contextMenuRightSideTree.Enabled = true;
            contextMenuLeftSideList.Enabled = true;
            contextMenuRightSideList.Enabled = true;
        }

        private void DisableControls()
        {
            SyncLeftRightButton.Enabled = false;
            SyncBothWaysLeftButton.Enabled = false;
            SyncRightLeftButton.Enabled = false;
            SyncBothWaysRightButton.Enabled = false;

            contextMenuLeftSideTree.Enabled = false;
            contextMenuRightSideTree.Enabled = false;
            contextMenuLeftSideList.Enabled = false;
            contextMenuRightSideList.Enabled = false;
        }

        private void LeftTreeView_VScroll(int pos)
        {
            if (!mIsScrolling)
            {
                mIsScrolling = true;
                rightTreeView.ScrollToVPos(pos);
            }

            mIsScrolling = false;
        }

        private void LeftTreeView_HScroll(int pos)
        {
            if (!mIsScrolling)
            {
                mIsScrolling = true;
                rightTreeView.ScrollToHPos(pos);
            }

            mIsScrolling = false;
        }

        private void RightTreeView_VScroll(int pos)
        {
            if (!mIsScrolling)
            {
                mIsScrolling = true;
                leftTreeView.ScrollToVPos(pos);
            }

            mIsScrolling = false;
        }

        private void RightTreeView_HScroll(int pos)
        {
            if (!mIsScrolling)
            {
                mIsScrolling = true;
                leftTreeView.ScrollToHPos(pos);
            }

            mIsScrolling = false;
        }

        private void LeftListView_VScroll(int pos)
        {
            if (!mIsScrolling)
            {
                mIsScrolling = true;

                string leftTopName = leftListView.TopItem.Text;
                foreach (ListViewItem item in rightListView.Items)
                {
                    if (item.Text == leftTopName)
                    {
                        rightListView.TopItem = item;
                        break;
                    }
                }
            }

            mIsScrolling = false;
        }

        private void LeftListView_HScroll(int pos)
        {
            if (!mIsScrolling)
            {
                mIsScrolling = true;
                rightListView.ScrollToHPos(pos);
            }

            mIsScrolling = false;
        }

        private void RightListView_VScroll(int pos)
        {
            if (!mIsScrolling)
            {
                mIsScrolling = true;

                string rightTopName = rightListView.TopItem.Text;
                foreach (ListViewItem item in leftListView.Items)
                {
                    if (item.Text == rightTopName)
                    {
                        leftListView.TopItem = item;
                        break;
                    }
                }
            }

            mIsScrolling = false;
        }

        private void RightListView_HScroll(int pos)
        {
            if (!mIsScrolling)
            {
                mIsScrolling = true;
                leftListView.ScrollToHPos(pos);
            }

            mIsScrolling = false;
        }

        private async void frmVisualDiffSync_Load(object sender, EventArgs e)
        {
            //TODO
            //* Now that we can continue or rollback a sync, we need to prohibit any kind of compare
            //  or sync until the previous sync has been continued or rolled back
            if (SyncFromLogFile.CheckIfLogExists(mSyncOptions.LogSyncPosFilenamePrefix))
            {
                ContinueRollbackDlg dlg = new ContinueRollbackDlg();
                DialogResult dlgResult = dlg.ShowDialog(this);
                if (dlgResult == DialogResult.OK)
                {
                    if (dlg.UserSyncDecision == ContinueRollbackDlg.SyncDecision.CONTINUE)
                    {
                        await ContinueSync(mSyncOptions.LogSyncPosFilenamePrefix, mSyncOptions.LogFilenamePrefix, false);
                    }
                    else if (dlg.UserSyncDecision == ContinueRollbackDlg.SyncDecision.ROLLBACK)
                    {
                        await RollbackSync(mSyncOptions.LogSyncPosFilenamePrefix, mSyncOptions.LogFilenamePrefix);
                    }
                }
            }

            TurnOffProgressBar();
            syncToolTips.SetToolTip(this.SyncLeftRightButton, "Sync Left to Right");
            syncToolTips.SetToolTip(this.SyncBothWaysLeftButton, "Sync Both Ways");
            syncToolTips.SetToolTip(this.SyncRightLeftButton, "Sync Right to Left");
            syncToolTips.SetToolTip(this.SyncBothWaysRightButton, "Sync Both Ways");
            syncToolTips.SetToolTip(this.compareButton, "Compare Folders");
        }

        private async Task ContinueSync(string logSyncPosFilePath, string logFilePath, bool retainLogFile)
        {
            try
            {
                TurnOnProgressBar();
                syncToolTips.SetToolTip(this.compareButton, "Cancel Sync");
                compareButton.ImageIndex = CANCEL_ICON_IMAGE_INDEX;

                mCancellationTokenSource = new CancellationTokenSource();
                await SyncFromLogFile.ContinueSync(logSyncPosFilePath, logFilePath, retainLogFile, mCancellationTokenSource.Token);

                TurnOffProgressBar();
                syncToolTips.SetToolTip(this.compareButton, "Compare Folders");
                compareButton.ImageIndex = COMPARE_ICON_IMAGE_INDEX;

                MessageBox.Show("Sync Completed", "Sync");
            }
            catch (OperationCanceledException ex)
            {
                TurnOffProgressBar();
                syncToolTips.SetToolTip(this.compareButton, "Compare Folders");
                compareButton.ImageIndex = COMPARE_ICON_IMAGE_INDEX;
                MessageBox.Show(ex.Message, "Continue Sync was cancelled");
                this.Close();
            }
            finally
            {
                ResetCancellationTokenSource();
            }
        }

        private async Task RollbackSync(string logSyncPosFilePath, string logFilePath)
        {
            try
            {
                TurnOnProgressBar();
                syncToolTips.SetToolTip(this.compareButton, "Cancel Rollback");
                compareButton.ImageIndex = CANCEL_ICON_IMAGE_INDEX;

                mCancellationTokenSource = new CancellationTokenSource();

                await SyncFromLogFile.RollbackSync(logSyncPosFilePath, logFilePath, mCancellationTokenSource.Token);

                TurnOffProgressBar();
                syncToolTips.SetToolTip(this.compareButton, "Compare Folders");
                compareButton.ImageIndex = COMPARE_ICON_IMAGE_INDEX;

                MessageBox.Show("Rollback Completed", "Rollback");
            }
            catch (OperationCanceledException ex)
            {
                TurnOffProgressBar();
                syncToolTips.SetToolTip(this.compareButton, "Compare Folders");
                compareButton.ImageIndex = COMPARE_ICON_IMAGE_INDEX;
                MessageBox.Show(ex.Message, "Rollback was cancelled");
                this.Close();
            }
            finally
            {
                ResetCancellationTokenSource();
            }
        }


        private void leftSideBrowseButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog browseFolder = new FolderBrowserDialog();
            DialogResult browseResult = browseFolder.ShowDialog(this);
            browseFolder.RootFolder = Environment.SpecialFolder.Personal;
            browseFolder.Description = "Select left folder you would like to compare";
            browseFolder.ShowNewFolderButton = false;
            if (browseResult == DialogResult.OK)
            {
                leftSideFolderComboBox.Text = browseFolder.SelectedPath;

                if (!leftSideFolderComboBox.Items.Contains(browseFolder.SelectedPath))
                {
                    leftSideFolderComboBox.Items.Add(browseFolder.SelectedPath);
                }
            }
        }

        private void rightSideBrowseButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog browseFolder = new FolderBrowserDialog();
            DialogResult browseResult = browseFolder.ShowDialog(this);
            browseFolder.RootFolder = Environment.SpecialFolder.Personal;
            browseFolder.Description = "Select right folder you would like to compare";
            browseFolder.ShowNewFolderButton = false;
            if (browseResult == DialogResult.OK)
            {
                rightSideFolderComboBox.Text = browseFolder.SelectedPath;

                if (!rightSideFolderComboBox.Items.Contains(browseFolder.SelectedPath))
                {
                    rightSideFolderComboBox.Items.Add(browseFolder.SelectedPath);
                }
            }
        }

        private async void compareButton_Click(object sender, EventArgs e)
        {
            if (mCancellationTokenSource == null)
            {
                mFolder1 = leftSideFolderComboBox.Text;
                mFolder2 = rightSideFolderComboBox.Text;
                if (mFolder1.Length == 0)
                    MessageBox.Show("Need to select folder 1");
                else if (!ZlpIOHelper.DirectoryExists(mFolder1))
                    MessageBox.Show("Invalid folder 1");
                else if (mFolder2.Length == 0)
                    MessageBox.Show("Need to select folder 2");
                else if (!ZlpIOHelper.DirectoryExists(mFolder2))
                    MessageBox.Show("Invalid folder 2");
                else
                {
                    await Compare(mFolder1, mFolder2);
                }
            }
            else
            {
                mCancellationTokenSource.Cancel();
            }
        }

        private async Task Compare(string folder1, string folder2)
        {
            try
            {
                TurnOnProgressBar();
                syncToolTips.SetToolTip(this.compareButton, "Cancel Comparison");
                compareButton.ImageIndex = CANCEL_ICON_IMAGE_INDEX;

                mCancellationTokenSource = new CancellationTokenSource();
                mFolderComparer = new FolderTreeComparerEx(mFolder1, mFolder2, mCompareOptions, null);

                mRootMergedNode = await mFolderComparer.Compare(mCancellationTokenSource.Token);

                UpdateUI(mRootMergedNode);
            }
            catch (OperationCanceledException ex)
            {
                TurnOffProgressBar();
                syncToolTips.SetToolTip(this.compareButton, "Compare Folders");
                compareButton.ImageIndex = COMPARE_ICON_IMAGE_INDEX;
                MessageBox.Show(ex.Message, "Compare was cancelled");
            }
            finally
            {
                ResetCancellationTokenSource();
            }
        }

        private void UpdateUI(FolderTreeNode compareResult)
        {
            try
            {
                leftTreeView.Nodes.Clear();
                rightTreeView.Nodes.Clear();

                leftListView.Items.Clear();
                rightListView.Items.Clear();

                mSelectedFolderNode = compareResult;

                TreeNode leftRootNode = CreateTreeNodeBoth(compareResult.Name, mSelectedFolderNode.InBothButChildrenDiffer, FOLDER_ICON_IMAGE_INDEX);
                leftTreeView.Nodes.Add(leftRootNode);

                TreeNode rightRootNode = CreateTreeNodeBoth(compareResult.Name, mSelectedFolderNode.InBothButChildrenDiffer, FOLDER_ICON_IMAGE_INDEX);
                rightTreeView.Nodes.Add(rightRootNode);

                PopulateTree(leftRootNode, rightRootNode, mRootMergedNode);

                TurnOffProgressBar();
                syncToolTips.SetToolTip(this.compareButton, "Compare Folders");
                compareButton.ImageIndex = COMPARE_ICON_IMAGE_INDEX;

                mLeftTreeRootItem = leftTreeView.Nodes[0];
                mRightTreeRootItem = rightTreeView.Nodes[0];

                //Only need to expand and select the root of the left tree as the
                //event handlers will select and expand the root of the right tree
                mLeftTreeRootItem.Expand();
            }
            catch (CompareException ex)
            {
                MessageBox.Show(this, ex.Message);
            }
        }

        private TreeNode CreateTreeNodeBoth(string name, bool inBothButChildrenDiffer, int imageIndex)
        {
            TreeNode treeNode = new TreeNode(name);
            treeNode.Name = name;
            treeNode.NodeFont = mFont;

            if (inBothButChildrenDiffer)
                treeNode.ForeColor = mVisualOptions.ChildrenDifferColor;
            else
                treeNode.ForeColor = Color.Black;

            treeNode.ImageIndex = imageIndex;
            treeNode.SelectedImageIndex = imageIndex;

            return treeNode;
        }

        private TreeNode CreateTreeNodeLeft(string name, Location location, int imageIndex)
        {
            TreeNode treeNode = new TreeNode(name);
            treeNode.Name = name;
            treeNode.NodeFont = mFont;

            if (location == SyncApi.Location.UniqueLeft)
                treeNode.ForeColor = mVisualOptions.AddColor;
            else if (location == SyncApi.Location.UniqueRight)
                treeNode.ForeColor = mVisualOptions.DeleteColor;
            else
                treeNode.ForeColor = mVisualOptions.ChangeColor;

            treeNode.ImageIndex = imageIndex;
            treeNode.SelectedImageIndex = imageIndex;

            return treeNode;
        }

        private TreeNode CreateTreeNodeRight(string name, Location location, int imageIndex)
        {
            TreeNode treeNode = new TreeNode(name);
            treeNode.Name = name;
            treeNode.NodeFont = mFont;

            if (location == SyncApi.Location.UniqueRight)
                treeNode.ForeColor = mVisualOptions.AddColor;
            else if (location == SyncApi.Location.UniqueLeft)
                treeNode.ForeColor = mVisualOptions.DeleteColor;
            else
                treeNode.ForeColor = mVisualOptions.ChangeColor;

            treeNode.ImageIndex = imageIndex;
            treeNode.SelectedImageIndex = imageIndex;

            return treeNode;
        }

        private ListViewItem CreateLeftListViewItem(string name, string size, string lastModified, Location location, int imageIndex)
        {
            ListViewItem listViewItem = null;

            if (location == SyncApi.Location.UniqueLeft)
            {
                listViewItem = new ListViewItem(
                    new string[] { name, size, lastModified }, imageIndex, mVisualOptions.AddColor, Color.White, mFont);
            }
            else if (location == SyncApi.Location.UniqueRight)
            {
                listViewItem = new ListViewItem(
                    new string[] { name, size, lastModified }, imageIndex, mVisualOptions.DeleteColor, Color.White, mFont);
            }
            else
            {
                listViewItem = new ListViewItem(
                    new string[] { name, size, lastModified }, imageIndex, mVisualOptions.ChangeColor, Color.White, mFont);
            }

            return listViewItem;
        }

        private ListViewItem CreateRightListViewItem(string name, string size, string lastModified, Location location, int imageIndex)
        {
            ListViewItem listViewItem = null;

            Font font = new Font("Arial", 8);

            if (location == SyncApi.Location.UniqueRight)
            {
                listViewItem = new ListViewItem(
                    new string[] { name, size, lastModified }, imageIndex, mVisualOptions.AddColor, Color.White, mFont);
            }
            else if (location == SyncApi.Location.UniqueLeft)
            {
                listViewItem = new ListViewItem(
                    new string[] { name, size, lastModified }, imageIndex, mVisualOptions.DeleteColor, Color.White, mFont);
            }
            else
            {
                listViewItem = new ListViewItem(
                    new string[] { name, size, lastModified }, imageIndex, mVisualOptions.ChangeColor, Color.White, mFont);
            }

            return listViewItem;
        }

        private ListViewItem CreateListViewItemBoth(string name, string size, string lastModified, bool inBothButChildrenDiffer, int imageIndex)
        {
            ListViewItem listViewItem = null;

            Font font = new Font("Arial", 8);

            if (inBothButChildrenDiffer)
            {
                listViewItem = new ListViewItem(
                    new string[] { name, size, lastModified }, imageIndex, mVisualOptions.ChildrenDifferColor, Color.White, mFont);
            }
            else
            {
                listViewItem = new ListViewItem(
                    new string[] { name, size, lastModified }, imageIndex, Color.Black, Color.White, mFont);
            }

            return listViewItem;
        }

        private void PopulateTree(TreeNode parentLeftNode, TreeNode parentRightNode, FolderTreeNode parentFolderNode)
        {
            foreach(var item in parentFolderNode.Children)
            {
                FolderTreeNode folder = item as FolderTreeNode;
                if (folder != null)
                {
                    TreeNode leftChildItem = null;
                    TreeNode rightChildItem = null;

                    if (folder.NodeLocation == SyncApi.Location.Both)
                    {
                        leftChildItem = CreateTreeNodeBoth(folder.Name, folder.InBothButChildrenDiffer, FOLDER_ICON_IMAGE_INDEX);
                        rightChildItem = CreateTreeNodeBoth(folder.Name, folder.InBothButChildrenDiffer, FOLDER_ICON_IMAGE_INDEX);
                    }
                    else
                    {
                        leftChildItem = CreateTreeNodeLeft(folder.Name, folder.NodeLocation, FOLDER_ICON_IMAGE_INDEX);
                        rightChildItem = CreateTreeNodeRight(folder.Name, folder.NodeLocation, FOLDER_ICON_IMAGE_INDEX);

                    }

                    parentLeftNode.Nodes.Add(leftChildItem);
                    parentRightNode.Nodes.Add(rightChildItem);
                    
                    PopulateTree(leftChildItem, rightChildItem, folder);
                }
            }
        }

        private void PopulateListViews(FolderTreeNode folderNode)
        {
            leftListView.Items.Clear();
            rightListView.Items.Clear();

            ListViewItem leftListViewItem = null;
            ListViewItem rightListViewItem = null;

            var children = folderNode.Children.OrderBy(x => x.RelativePath);

            foreach (var item in children)
            {
                if (item is FileTreeNode)
                {
                    FileTreeNode file = item as FileTreeNode;

                    if (file.NodeLocation == SyncApi.Location.Both)
                    {
                        if (file.DiffInfo.InfoSame())
                        {
                            leftListViewItem = CreateListViewItemBoth(file.Name, file.FileSizeLeft.ToString(), file.LastWriteTimeLeft.ToString(), false, FILE_ICON_IMAGE_INDEX);
                            rightListViewItem = CreateListViewItemBoth(file.Name, file.FileSizeRight.ToString(), file.LastWriteTimeRight.ToString(), false, FILE_ICON_IMAGE_INDEX);
                        }
                        else
                        {
                            leftListViewItem = CreateLeftListViewItem(file.Name, file.FileSizeLeft.ToString(), file.LastWriteTimeLeft.ToString(), file.NodeLocation, FILE_ICON_IMAGE_INDEX);
                            rightListViewItem = CreateRightListViewItem(file.Name, file.FileSizeRight.ToString(), file.LastWriteTimeRight.ToString(), file.NodeLocation, FILE_ICON_IMAGE_INDEX);
                        }
                    }
                    else
                    {
                        leftListViewItem = CreateLeftListViewItem(file.Name, file.FileSizeLeft.ToString(), file.LastWriteTimeLeft.ToString(), file.NodeLocation, FILE_ICON_IMAGE_INDEX);
                        rightListViewItem = CreateRightListViewItem(file.Name, file.FileSizeRight.ToString(), file.LastWriteTimeRight.ToString(), file.NodeLocation, FILE_ICON_IMAGE_INDEX);
                    }
                    
                    leftListView.Items.Add(leftListViewItem);
                    rightListView.Items.Add(rightListViewItem);
                }
                else if (item is FolderTreeNode)
                {
                    FolderTreeNode folder = item as FolderTreeNode;

                    if (folder.NodeLocation == SyncApi.Location.Both)
                    {
                        leftListViewItem = CreateListViewItemBoth(folder.Name, "", folder.LastWriteTimeLeft.ToString(), folder.InBothButChildrenDiffer, FOLDER_ICON_IMAGE_INDEX);
                        rightListViewItem = CreateListViewItemBoth(folder.Name, "", folder.LastWriteTimeLeft.ToString(), folder.InBothButChildrenDiffer, FOLDER_ICON_IMAGE_INDEX);
                    }
                    else
                    {
                        leftListViewItem = CreateLeftListViewItem(folder.Name, "", folder.LastWriteTimeLeft.ToString(), folder.NodeLocation, FOLDER_ICON_IMAGE_INDEX);
                        rightListViewItem = CreateRightListViewItem(folder.Name, "", folder.LastWriteTimeRight.ToString(), folder.NodeLocation, FOLDER_ICON_IMAGE_INDEX);
                    }

                    leftListView.Items.Add(leftListViewItem);
                    rightListView.Items.Add(rightListViewItem);
                }
                else
                {
                    throw new Exception("Invalid node type");
                }
            }
        }

        private string GetPathFromTreeViewItem(TreeNode item)
        {
            return item.FullPath;
        }

        private string GetPathFromListViewItem(FolderTreeNode folderTreeNode, string name)
        {
            if (mFolderComparer.CommonRootName == "..")
            {
                return mFolderComparer.CommonRootName + @"\" + folderTreeNode.RelativePath + @"\" + name;
            }
            else
            {
                return folderTreeNode.RelativePath + @"\" + name;
            }
        }

        private void ResetCancellationTokenSource()
        {
            mCancellationTokenSource.Dispose();
            mCancellationTokenSource = null;
        }

        private void leftTreeView_AfterExpand(object sender, TreeViewEventArgs e)
        {
            if (!mIsExpanding)
            {
                if (e.Node != null)
                {
                    ExpandTree(mRightTreeRootItem, e.Node.FullPath);
                }
            }
        }

        private void leftTreeView_AfterCollapse(object sender, TreeViewEventArgs e)
        {
            if (!mIsCollapsing)
            {
                if (e.Node != null)
                {
                    CollapseTree(mRightTreeRootItem, e.Node.FullPath);
                }
            }
        }

        private void rightTreeView_AfterExpand(object sender, TreeViewEventArgs e)
        {
            if (!mIsExpanding)
            {
                if (e.Node != null)
                {
                    ExpandTree(mLeftTreeRootItem, e.Node.FullPath);
                }
            }
        }

        private void rightTreeView_AfterCollapse(object sender, TreeViewEventArgs e)
        {
            if (!mIsCollapsing)
            {
                if (e.Node != null)
                {
                    CollapseTree(mLeftTreeRootItem, e.Node.FullPath);
                }
            }
        }

        private void ExpandTree(TreeNode rootNode, string path)
        {
            //Flag that we are expanding the tree, as when expanding, the expanded event
            //gets fired again which will be redundant
            mIsExpanding = true;

            TreeNode parent = rootNode;
            TreeNode child = null;
            string[] hierarchy = path.Split('\\');

            for (int i = 1; i < hierarchy.Length; ++i)
            {
                //We expect to find the node, if not then the two trees have gone out
                //of sync somehow. Single will throw an exception if the item is not
                //found (or there is more than one item but that should never happen
                //either)
                child = parent.Nodes.Cast<TreeNode>().Single(x => x.Text == hierarchy[i]);

                if (!child.IsExpanded)
                    child.Expand();

                parent = child;
            }

            if (!parent.IsExpanded)
                parent.Expand();

            //We're no longer expanding
            mIsExpanding = false;
        }

        private void CollapseTree(TreeNode rootNode, string path)
        {
            //Flag that we are collapsing the tree, as when collapsing, the collapsed event
            //gets fired again which will be redundant
            mIsCollapsing = true;

            TreeNode parent = rootNode;
            TreeNode child = null;
            string[] hierarchy = path.Split('\\');

            for (int i = 1; i < hierarchy.Length; ++i)
            {
                //We expect to find the node, if not then the two trees have gone out
                //of sync somehow. Single will throw an exception if the item is not
                //found (or there is more than one item but that should never happen
                //either)
                child = parent.Nodes.Cast<TreeNode>().Single(x => x.Text == hierarchy[i]);

                parent = child;
            }

            if (parent.IsExpanded)
                parent.Collapse(true);

            //We're no longer expanding
            mIsCollapsing = false;
        }

        private async void SyncLeftRightButton_Click(object sender, EventArgs e)
        {
            mFolder1 = leftSideFolderComboBox.Text;
            mFolder2 = rightSideFolderComboBox.Text;
            if (mFolder1.Length == 0)
                MessageBox.Show("Need to select folder 1");
            else if (!ZlpIOHelper.DirectoryExists(mFolder1))
                MessageBox.Show("Invalid folder 1");
            else if (mFolder2.Length == 0)
                MessageBox.Show("Need to select folder 2");
            else if (!ZlpIOHelper.DirectoryExists(mFolder2))
                MessageBox.Show("Invalid folder 2");
            else
            {
                await SyncDirect(mFolder1, mFolder2, mSyncOptions, SyncApi.SyncApi.SyncType.ONEWAY);
            }
        }

        private async void SyncRightLeftButton_Click(object sender, EventArgs e)
        {
            mFolder1 = leftSideFolderComboBox.Text;
            mFolder2 = rightSideFolderComboBox.Text;
            if (mFolder1.Length == 0)
                MessageBox.Show("Need to select folder 1");
            else if (!ZlpIOHelper.DirectoryExists(mFolder1))
                MessageBox.Show("Invalid folder 1");
            else if (mFolder2.Length == 0)
                MessageBox.Show("Need to select folder 2");
            else if (!ZlpIOHelper.DirectoryExists(mFolder2))
                MessageBox.Show("Invalid folder 2");
            else
            {
                await SyncDirect(mFolder2, mFolder1, mSyncOptions, SyncApi.SyncApi.SyncType.ONEWAY);
            }
        }

        private async void SyncBothWaysLeftButton_Click(object sender, EventArgs e)
        {
            mFolder1 = leftSideFolderComboBox.Text;
            mFolder2 = rightSideFolderComboBox.Text;
            if (mFolder1.Length == 0)
                MessageBox.Show("Need to select folder 1");
            else if (!ZlpIOHelper.DirectoryExists(mFolder1))
                MessageBox.Show("Invalid folder 1");
            else if (mFolder2.Length == 0)
                MessageBox.Show("Need to select folder 2");
            else if (!ZlpIOHelper.DirectoryExists(mFolder2))
                MessageBox.Show("Invalid folder 2");
            else
            {
                await SyncDirect(mFolder1, mFolder2, mSyncOptions, SyncApi.SyncApi.SyncType.TWOWAY);
            }
        }

        private async void SyncBothWaysRightButton_Click(object sender, EventArgs e)
        {
            mFolder1 = leftSideFolderComboBox.Text;
            mFolder2 = rightSideFolderComboBox.Text;
            if (mFolder1.Length == 0)
                MessageBox.Show("Need to select folder 1");
            else if (!ZlpIOHelper.DirectoryExists(mFolder1))
                MessageBox.Show("Invalid folder 1");
            else if (mFolder2.Length == 0)
                MessageBox.Show("Need to select folder 2");
            else if (!ZlpIOHelper.DirectoryExists(mFolder2))
                MessageBox.Show("Invalid folder 2");
            else
            {
                await SyncDirect(mFolder1, mFolder2, mSyncOptions, SyncApi.SyncApi.SyncType.TWOWAY);
            }
        }

        private async Task SyncDirect(string source, string target, SyncOptions syncOptions, SyncApi.SyncApi.SyncType syncType)
        {
            try
            {
                TurnOnProgressBar();
                syncToolTips.SetToolTip(this.compareButton, "Cancel Sync");
                compareButton.ImageIndex = CANCEL_ICON_IMAGE_INDEX;

                ISyncManager syncManager = GetSyncManager();

                mCancellationTokenSource = new CancellationTokenSource();

                SyncApi.SyncApi syncApi = new SyncApi.SyncApi(source, target, syncManager, syncType, syncOptions, null);
                await syncApi.Sync(mCancellationTokenSource.Token);

                //Sync completed so turn off the progress bar
                TurnOffProgressBar();
                syncToolTips.SetToolTip(this.compareButton, "Compare Folders");
                compareButton.ImageIndex = COMPARE_ICON_IMAGE_INDEX;

                MessageBox.Show("Sync Completed", "Sync");
            }
            catch(OperationCanceledException ex)
            {
                TurnOffProgressBar();
                syncToolTips.SetToolTip(this.compareButton, "Compare Folders");
                compareButton.ImageIndex = COMPARE_ICON_IMAGE_INDEX;
                MessageBox.Show(ex.Message, "Sync Cancelled");
            }
            finally
            {
                ResetCancellationTokenSource();
            }
        }

        private ISyncManager GetSyncManager()
        {
            ISyncManager syncManager = null;

            if (mSyncOptions.SyncDirect)
                syncManager = new SyncDirect();
            else
                syncManager = new SyncLogged(mSyncOptions);

            return syncManager;
        }

        private void SyncOptionsButton_Click(object sender, EventArgs e)
        {
            HandleSyncOptionsDialog();
        }

        private void HandleSyncOptionsDialog()
        {
            OptionsDlg optionsDlg = new OptionsDlg(mSyncOptions, mCompareOptions, mVisualOptions);
            DialogResult res = optionsDlg.ShowDialog(this);
            if (res == DialogResult.OK)
            {
                mSyncOptions = optionsDlg.SyncOptions;
                mCompareOptions = optionsDlg.CompareOptions;
                mVisualOptions = optionsDlg.VisualOptions;

                SyncOptions.Serialize("SyncOptions.xml", mSyncOptions);
                CompareOptions.Serialize("CompareOptions.xml", mCompareOptions);
                VisualOptions.Serialize("VisualOptions.xml", mVisualOptions);

                if (mRootMergedNode != null && optionsDlg.VisualOptionsChanged)
                {
                    mFont = new Font(mVisualOptions.FontName, mVisualOptions.FontSize);
                    UpdateUI(mRootMergedNode);
                }
            }
        }

        private void leftTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            TreeNode rightTreeNode = FindTreeNode(mRightTreeRootItem, e.Node.FullPath);

            if (rightTreeNode != null)
            {
                //We've selected a folder from the left tree, now populate the left and right
                //list views with the files in the folder
                string path = GetPathFromTreeViewItem(e.Node);

                IFolderChildNode node = mFolderComparer.GetNode(path);
                FolderTreeNode folderNode = node as FolderTreeNode;
                if (folderNode != null)
                {
                    leftListView.Items.Clear();
                    rightListView.Items.Clear();

                    PopulateListViews(folderNode);

                    mSelectedFolderNode = folderNode;

                    leftSideFolderComboBox.Text = mFolderComparer.BasePath1 + mSelectedFolderNode.RelativePath;
                    rightSideFolderComboBox.Text = mFolderComparer.BasePath2 + mSelectedFolderNode.RelativePath;
                }

                //Ensure that the right tree node and right list view item remain
                //selected after we've selected a node in the left tree
                rightTreeView.SelectedNode = rightTreeNode;
                rightTreeNode.BackColor = Color.SkyBlue;
                mRightSelectedTreeNode = rightTreeNode;
            }
        }

        private void rightTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            TreeNode leftTreeNode = FindTreeNode(mLeftTreeRootItem, e.Node.FullPath);

            if (leftTreeNode != null)
            {
                //Ensure that the left tree node and left list view item remain
                //selected after we've selected a node in the right tree
                leftTreeView.SelectedNode = leftTreeNode;
                leftTreeNode.BackColor = Color.SkyBlue;
                mLeftSelectedTreeNode = leftTreeNode;
            }
        }


        private void leftTreeView_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            if (mLeftSelectedTreeNode != null)
                mLeftSelectedTreeNode.BackColor = Color.White;

            if (mRightSelectedTreeNode != null)
                mRightSelectedTreeNode.BackColor = Color.White;
        }

        private void rightTreeView_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            if (mLeftSelectedTreeNode != null)
                mLeftSelectedTreeNode.BackColor = Color.White;

            if (mRightSelectedTreeNode != null)
                mRightSelectedTreeNode.BackColor = Color.White;
        }

        private TreeNode FindTreeNode(TreeNode rootNode, string path)
        {
            TreeNode parent = rootNode;
            TreeNode child = null;
            string[] hierarchy = path.Split('\\');

            for (int i = 1; i < hierarchy.Length; ++i)
            {
                //We expect to find the node, if not then the two trees have gone out
                //of sync somehow. Single will throw an exception if the item is not
                //found (or there is more than one item but that should never happen
                //either)
                child = parent.Nodes.Cast<TreeNode>().Single(x => x.Text == hierarchy[i]);

                parent = child;
            }

            return parent;
        }

        private void leftListView_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (e.IsSelected && !mIsListViewSelecting)
            {
                mIsListViewSelecting = true;

                if (mLeftSelectedListViewItem != null && mLeftSelectedListViewItem != e.Item)
                {
                    mLeftSelectedListViewItem.Selected = false;
                    mLeftSelectedListViewItem.BackColor = Color.White;
                }

                mLeftSelectedListViewItem = e.Item;

                if (mRightSelectedListViewItem != null && mRightSelectedListViewItem != e.Item)
                {
                    mRightSelectedListViewItem.Selected = false;
                    mRightSelectedListViewItem.BackColor = Color.White;
                }

                mRightSelectedListViewItem = rightListView.Items[e.ItemIndex];
                mRightSelectedListViewItem.Selected = true;
                mRightSelectedListViewItem.BackColor = Color.SkyBlue;

                mIsListViewSelecting = false;
            }
        }

        private void rightListView_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (e.IsSelected && !mIsListViewSelecting)
            {
                mIsListViewSelecting = true;

                if (mRightSelectedListViewItem != null && mRightSelectedListViewItem != e.Item)
                {
                    mRightSelectedListViewItem.Selected = false;
                    mRightSelectedListViewItem.BackColor = Color.White;
                }

                mRightSelectedListViewItem = e.Item;

                if (mLeftSelectedListViewItem != null && mLeftSelectedListViewItem != e.Item)
                {
                    mLeftSelectedListViewItem.Selected = false;
                    mLeftSelectedListViewItem.BackColor = Color.White;
                }

                mLeftSelectedListViewItem = leftListView.Items[e.ItemIndex];
                mLeftSelectedListViewItem.Selected = true;
                mLeftSelectedListViewItem.BackColor = Color.SkyBlue;

                mIsListViewSelecting = false;
            }
        }

        private void leftSideBrowseTypeButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                int x = leftSideFolderComboBox.Left + leftSideBrowseTypeButton.Left;
                int y = leftSideFolderPanel.Bottom + leftSideBrowseTypeButton.Bottom;
                leftBrowseTypeContextMenu.Show(this, new Point(x, y));
            }
        }

        private void rightSideBrowseTypeButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                int x = rightSideFolderPanel.Left + rightSideFolderComboBox.Left + rightSideBrowseTypeButton.Left;
                int y = rightSideFolderPanel.Bottom + rightSideBrowseTypeButton.Bottom;
                rightBrowseTypeContextMenu.Show(this, new Point(x, y));
            }
        }

        private void leftArchiveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var rootTreeItemNode = LoadTreeFromArchive("File 1");
            if (rootTreeItemNode != null)
            {
                //Convert the root TreeItemNode tree to FolderTreeNode
            }
        }

        private async void leftFTPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var rootTreeItemNode = await LoadTreeFromFtp();
            if (rootTreeItemNode != null)
            {
                //Convert the root TreeItemNode tree to FolderTreeNode
            }
        }

        private void rightArchiveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var rootTreeItemNode = LoadTreeFromArchive("File 2");
            if (rootTreeItemNode != null)
            {
                //Convert the root TreeItemNode tree to FolderTreeNode
            }
        }

        private async void rightFTPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var rootTreeItemNode = await LoadTreeFromFtp();
            if (rootTreeItemNode != null)
            {
                //Convert the root TreeItemNode tree to FolderTreeNode
            }
        }

        private TreeItemNode LoadTreeFromArchive(string title)
        {
            OpenFileDialog fileDlg = new OpenFileDialog();
            fileDlg.Title = title;
            fileDlg.RestoreDirectory = true;
            if (fileDlg.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
            {
                ITreeItemNodeReader reader = new ZipArchiveReader(fileDlg.FileName);
                return reader.ReadTree();
            }

            return null;
        }

        private async Task<TreeItemNode> LoadTreeFromFtp()
        {
            try
            {
                LoadFtpCredentials();

                FtpDlg ftpDlg = new FtpDlg(mFtpCredentials);

                if (ftpDlg.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
                {
                    mFtpCredentials = ftpDlg.Credentials;

                    ITreeItemNodeReader reader = new FtpReader(mFtpCredentials.Hostname, mFtpCredentials.Username, mFtpCredentials.Password);
                    await reader.ConnectAsync();
                    return await reader.ReadTreeAsync();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "FTP Error");
            }

            return null;
        }

        private void LoadFtpCredentials()
        {
            if (mFtpCredentials == null)
            {
                mFtpCredentials = FtpCredentials.Deserialize("FtpCredentials.xml");
                if (mFtpCredentials == null)
                {
                    mFtpCredentials = new FtpCredentials();
                }
            }
        }

        private void SaveFtpCredentials()
        {
            if (mFtpCredentials != null)
            {
                FtpCredentials.Serialize("FtpCredentials.xml", mFtpCredentials);
            }
        }

        private FolderTreeNode ConvertTreeNodeToFolderTreeNode(TreeItemNode rootTreeItemNode)
        {
            throw new NotImplementedException();
        }

        private void ConvertTreeNodeToFolderTreeNode(FolderTreeNode parentFolderTreeNode, TreeItemNode rootTreeItemNode)
        {
            throw new NotImplementedException();
        }
    }
}
