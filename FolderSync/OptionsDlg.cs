﻿using System;
using System.Drawing;
using System.Windows.Forms;
using SyncApi;

namespace FolderSync
{
    public partial class OptionsDlg : Form
    {
        public VisualOptions VisualOptions
        {
            get;
            private set;
        }

        public bool VisualOptionsChanged
        {
            get;
            private set;
        }


        public SyncOptions SyncOptions
        {
            get;
            private set;
        }

        public CompareOptions CompareOptions
        {
            get;
            private set;
        }

        public OptionsDlg(SyncOptions syncOptions, CompareOptions compareOptions, VisualOptions visualOptions)
        {
            InitializeComponent();

            SyncOptions = syncOptions;
            CompareOptions = compareOptions;
            VisualOptions = visualOptions;

            VisualOptionsChanged = false;
        }

        private void OptionsDlg_Load(object sender, EventArgs e)
        {
            SetSyncOptionsOnDlg();
            SetCompareOptionsOnDlg();
            SetVisualOptionsOnDlg();
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            PopulateSyncOptionsFromDlg();
            PopulateCompareOptionsFromDlg();
            PopulateVisualOptionsFromDlg();

            DialogResult = DialogResult.OK;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void SetSyncOptionsOnDlg()
        {
            if (SyncOptions.SyncDirect)
            {
                SyncLoggedRadioButton.Checked = false;
                SyncDirectRadioButton.Checked = true;
            }
            else
            {
                SyncLoggedRadioButton.Checked = true;
                SyncDirectRadioButton.Checked = false;
            }

            RetainLogCheckBox.Checked = SyncOptions.RetainLastSuccessfulSync;

            MoveToRecycleBinCheckBox.Checked = SyncOptions.MoveDeletedFilesToRecycleBin;

            if (!string.IsNullOrEmpty(SyncOptions.TempPath))
            {
                TempPathTextBox.Text = SyncOptions.TempPath;
            }

            PropagateDeletesCheckBox.Checked = SyncOptions.PropagateDeletes;

            CopyFileAttributesCheckBox.Checked = SyncOptions.CopyFileAttributes;

            OnlyCopyIfSourceNewerCheckBox.Checked = SyncOptions.OnlyCopyFileIfSourceNewer;

            if (!string.IsNullOrEmpty(SyncOptions.OutputLogFile))
            {
                OutputLogFileTextBox.Text = SyncOptions.OutputLogFile;
            }
        }

        private void SetCompareOptionsOnDlg()
        {
            TolarenceTextBox.Text = CompareOptions.ToleranceSeconds.ToString();

            LastModifiedTimeCheckBox.Checked = CompareOptions.CompareLastModifiedTime;
            FileSizeCheckBox.Checked = CompareOptions.CompareFileSize;
            FileContentCheckBox.Checked = CompareOptions.CompareContent;
            IncludeSubFoldersCheckBox.Checked = CompareOptions.IncludeSubFolders;

            if (!string.IsNullOrEmpty(CompareOptions.ExtensionFilters))
            {
                ExtensionFiltersTextBox.Text = CompareOptions.ExtensionFilters;
            }
        }

        private void SetVisualOptionsOnDlg()
        {
            addColorButton.BackColor = VisualOptions.AddColor;
            deleteColorButton.BackColor = VisualOptions.DeleteColor;
            changedColorButton.BackColor = VisualOptions.ChangeColor;
            childrenDifferColorButton.BackColor = VisualOptions.ChildrenDifferColor;


            foreach (FontFamily fontFamily in FontFamily.Families)
            {
                fontListComboBox.Items.Add(fontFamily.Name);
            }

            fontListComboBox.Text = VisualOptions.FontName;

            for (int i = 8; i < 15; ++i)
            {
                fontSizeComboBox.Items.Add(i);
            }

            fontSizeComboBox.Text = VisualOptions.FontSize.ToString();
        }

        private void PopulateSyncOptionsFromDlg()
        {
            SyncOptions.SyncDirect = SyncDirectRadioButton.Checked;

            SyncOptions.RetainLastSuccessfulSync = RetainLogCheckBox.Checked;

            SyncOptions.MoveDeletedFilesToRecycleBin = MoveToRecycleBinCheckBox.Checked;

            SyncOptions.TempPath = TempPathTextBox.Text;

            SyncOptions.PropagateDeletes = PropagateDeletesCheckBox.Checked;

            SyncOptions.CopyFileAttributes = CopyFileAttributesCheckBox.Checked;

            SyncOptions.OnlyCopyFileIfSourceNewer = OnlyCopyIfSourceNewerCheckBox.Checked;

            SyncOptions.OutputLogFile = OutputLogFileTextBox.Text;

            //For now hardcode the names, but need to add these to the dialog so they can
            //be set by the user
            SyncOptions.LogSyncPosFilenamePrefix = SyncOptions.TempPath + @"syncpos.txt";
            SyncOptions.LogFilenamePrefix = SyncOptions.TempPath + @"synclog.txt";
        }

        private void PopulateCompareOptionsFromDlg()
        {
            CompareOptions.ToleranceSeconds = Convert.ToInt32(TolarenceTextBox.Text);
            CompareOptions.CompareLastModifiedTime = LastModifiedTimeCheckBox.Checked;
            CompareOptions.CompareFileSize = FileSizeCheckBox.Checked;
            CompareOptions.CompareContent = FileContentCheckBox.Checked;
            CompareOptions.IncludeSubFolders = IncludeSubFoldersCheckBox.Checked;
            CompareOptions.ExtensionFilters = ExtensionFiltersTextBox.Text;
        }

        private void PopulateVisualOptionsFromDlg()
        {
            VisualOptions.AddColor = addColorButton.BackColor;
            VisualOptions.DeleteColor = deleteColorButton.BackColor;
            VisualOptions.ChangeColor = changedColorButton.BackColor;
            VisualOptions.ChildrenDifferColor = childrenDifferColorButton.BackColor;

            VisualOptions.FontName = fontListComboBox.Text;
            VisualOptions.FontSize = (float)Convert.ToDouble(fontSizeComboBox.Text);
        }
        private void addColorButton_Click(object sender, EventArgs e)
        {
            ColorDialog dlg = new ColorDialog();
            dlg.FullOpen = true;
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (VisualOptions.AddColor != dlg.Color)
                {
                    VisualOptions.AddColor = dlg.Color;
                    addColorButton.BackColor = VisualOptions.AddColor;
                    VisualOptionsChanged = true;
                }
            }
        }

        private void deleteColorButton_Click(object sender, EventArgs e)
        {
            ColorDialog dlg = new ColorDialog();
            dlg.FullOpen = true;
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (VisualOptions.DeleteColor != dlg.Color)
                {
                    VisualOptions.DeleteColor = dlg.Color;
                    deleteColorButton.BackColor = VisualOptions.DeleteColor;
                    VisualOptionsChanged = true;
                }
            }
        }

        private void changedColorButton_Click(object sender, EventArgs e)
        {
            ColorDialog dlg = new ColorDialog();
            dlg.FullOpen = true;
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (VisualOptions.ChangeColor != dlg.Color)
                {
                    VisualOptions.ChangeColor = dlg.Color;
                    changedColorButton.BackColor = VisualOptions.ChangeColor;
                    VisualOptionsChanged = true;
                }
            }
        }

        private void childrenDifferColor_Click(object sender, EventArgs e)
        {
            ColorDialog dlg = new ColorDialog();
            dlg.FullOpen = true;
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (VisualOptions.ChildrenDifferColor != dlg.Color)
                {
                    VisualOptions.ChildrenDifferColor = dlg.Color;
                    childrenDifferColorButton.BackColor = VisualOptions.ChildrenDifferColor;
                    VisualOptionsChanged = true;
                }
            }
        }

        private void fontListComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string fontName = null;
            
            if (fontListComboBox.SelectedItem != null)
            {
                fontName = (string)fontListComboBox.SelectedItem;
            }
            
            if (fontName != null && fontName != VisualOptions.FontName)
            {
                VisualOptions.FontName = (string)fontListComboBox.SelectedItem;
                VisualOptionsChanged = true;
            }

            fontSampleTextLabel.Font = new System.Drawing.Font(VisualOptions.FontName, VisualOptions.FontSize);
            fontSampleTextLabel.Text = "Font applied";
        }

        private void fontSizeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            int fontSize = 0;

            if (fontSizeComboBox.SelectedItem != null)
            {
                fontSize = (int)fontSizeComboBox.SelectedItem;
            }

            if (fontSize != VisualOptions.FontSize)
            {
                VisualOptions.FontSize = (int)fontSizeComboBox.SelectedItem;
                VisualOptionsChanged = true;
            }

            fontSampleTextLabel.Font = new System.Drawing.Font(VisualOptions.FontName, VisualOptions.FontSize);
            fontSampleTextLabel.Text = "Font applied";
        }
    }
}
